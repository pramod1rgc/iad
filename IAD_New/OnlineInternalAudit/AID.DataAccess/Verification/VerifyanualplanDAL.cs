﻿using Common;
using BusinessModel.AuditStructure;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Text;
using System.Threading.Tasks;

namespace AID.DataAccess.Verification
{
    public class VerifyanualplanDAL
    {
        private Database iasdb = null;
        public VerifyanualplanDAL(Database database)
        {
            iasdb = database;
        }
        public async Task<IEnumerable<CreateAuditPlanModel>> Getauditlistforverify(int FYID,int RoleUserMapId)
        {
            List<CreateAuditPlanModel> response = new List<CreateAuditPlanModel>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = iasdb.GetStoredProcCommand("[OIAS].[GetAllAuditListForVerify_SP]"))
                {
                    iasdb.AddInParameter(dbCommand, "@FYID", DbType.Int32, FYID);
                    iasdb.AddInParameter(dbCommand, "@RoleUserMapId", DbType.Int32, RoleUserMapId);
                    using (IDataReader rdr = iasdb.ExecuteReader(dbCommand))
                    {
                        while (rdr.Read())
                        {
                            CreateAuditPlanModel mstAudit = new CreateAuditPlanModel();
                            
                            mstAudit.AuditId = Convert.ToInt32(rdr["AuditId"]);
                            mstAudit.AuditName = rdr["AuditName"].ToString();
                            mstAudit.FinancialYearId = Convert.ToInt32(rdr["FinancialYearId"]);
                            mstAudit.PAOName = rdr["PaoName"].ToString();
                            mstAudit.ControllerCode = rdr["ControllerCode"].ToString();
                            mstAudit.City = rdr["City"].ToString(); ;


                            mstAudit.Quarter = rdr["Quarter"].ToString();
                            mstAudit.PAOId = Convert.ToInt32(rdr["PAOId"].ToString());
                            mstAudit.PAOCode = rdr["PAOCode"].ToString();
                            mstAudit.AuditFromDate = rdr["AuditFromDate"].ToString();
                            mstAudit.AuditToDate = rdr["AuditToDate"].ToString();
                            mstAudit.AuditIntimationDate = rdr["AuditIntimationDate"].ToString();
                            mstAudit.AuditStatusId = rdr["AuditStatusId"].ToString();
                            mstAudit.AuditStatus = rdr["Status"].ToString();
                            mstAudit.FinancialYear = rdr["FinancialYear"].ToString();
                            mstAudit.AuditReason = rdr["AuditReason"].ToString();
                            mstAudit.RejectionRemark = rdr["RejectionRemark"].ToString();
                            mstAudit.IsActive = rdr["IsActive"].ToString();

                            response.Add(mstAudit);
                        }
                    }
                }
            });
            return response;
        }



        public async Task<IEnumerable<CreateAuditPlanModel>> getAllPendingQuarter(int FYID, int userId)
        {
            List<CreateAuditPlanModel> response = new List<CreateAuditPlanModel>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = iasdb.GetStoredProcCommand("[OIAS].[GetPendingQuarter_SP]"))
                {
                    iasdb.AddInParameter(dbCommand, "FYId", DbType.Int32, FYID);
                    iasdb.AddInParameter(dbCommand, "userId", DbType.Int32, userId);
                    using (IDataReader rdr = iasdb.ExecuteReader(dbCommand))
                    {
                        while (rdr.Read())
                        {
                            CreateAuditPlanModel mstAudit = new CreateAuditPlanModel();
                            mstAudit.FinancialYearId = Convert.ToInt32(rdr["FinancialYearId"]);
                            mstAudit.Quarter = rdr["Quarter"].ToString();
                       
                            response.Add(mstAudit);
                        }
                    }
                }
            });
            return response;
        }

        public async Task<string> AnualplanApprove(int FYID,int RoleUserMapId)
        {

            string message = string.Empty;
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = iasdb.GetStoredProcCommand("[OIAs].[AprovedAnualAudit_SP]"))
                {
                    iasdb.AddInParameter(dbCommand, "FYID", DbType.Int32, FYID);
                    iasdb.AddInParameter(dbCommand, "RoleUserMapId", DbType.Int32, RoleUserMapId);
                    iasdb.AddInParameter(dbCommand, "ipAddress", DbType.String, CommonFunction.GetIPAddress());
                    iasdb.AddOutParameter(dbCommand, "Msg_Out_Status", DbType.String, 500);
                    int i = iasdb.ExecuteNonQuery(dbCommand);
                    message = Convert.ToString(iasdb.GetParameterValue(dbCommand, "Msg_Out_Status"));

                    //if (i > 0)
                    //{
                    //    message = "Annual plan approve successfully";
                    //}

                }
            });
            return  message;
        }



        public async Task<string> RejectanualsAudit(int FYID,string rejectreson,int RoleUserMapId)
        {

            string message = string.Empty;
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = iasdb.GetStoredProcCommand("[OIAs].[RejectAnualAudit_SP]"))
                {
                    iasdb.AddInParameter(dbCommand, "FYID", DbType.Int32, FYID);
                    iasdb.AddInParameter(dbCommand, "RejectionRemark", DbType.String, rejectreson);
                    iasdb.AddInParameter(dbCommand, "RoleUserMapId", DbType.Int32, RoleUserMapId);
                    iasdb.AddInParameter(dbCommand, "ipAddress", DbType.String, CommonFunction.GetIPAddress());
                    iasdb.AddOutParameter(dbCommand, "Msg_Out_Status", DbType.String, 500);
                    int i = iasdb.ExecuteNonQuery(dbCommand);
                    message = Convert.ToString(iasdb.GetParameterValue(dbCommand, "Msg_Out_Status"));

                    //if (i > 0)
                    //{
                    //    message = "Annual plan reject successfully";
                    //}

                }
            });
            return message;
        }


        public async Task<IEnumerable<CreateAuditPlanModel>> GetauditlistforverifyQuarterly(int FYID, string Quarte,int userId)
        {
            List<CreateAuditPlanModel> response = new List<CreateAuditPlanModel>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = iasdb.GetStoredProcCommand("[OIAS].[GetAllAuditListQuarterForVerify_SP]"))
                {
                    iasdb.AddInParameter(dbCommand, "FYID", DbType.Int32, FYID);
                    iasdb.AddInParameter(dbCommand, "userId", DbType.Int32, userId);
                    iasdb.AddInParameter(dbCommand, "quarter", DbType.String, Quarte);
                    using (IDataReader rdr = iasdb.ExecuteReader(dbCommand))
                    {
                        while (rdr.Read())
                        {
                            CreateAuditPlanModel mstAudit = new CreateAuditPlanModel();

                            mstAudit.AuditId = Convert.ToInt32(rdr["AuditId"]);
                            mstAudit.AuditName = rdr["AuditName"].ToString();
                            mstAudit.FinancialYearId = Convert.ToInt32(rdr["FinancialYearId"]);
                            mstAudit.PAOName = rdr["PaoName"].ToString();
                            mstAudit.ControllerCode = rdr["ControllerCode"].ToString();
                            mstAudit.City = rdr["City"].ToString(); ;


                            mstAudit.Quarter = rdr["Quarter"].ToString();
                            mstAudit.PAOId = Convert.ToInt32(rdr["PAOId"].ToString());
                            mstAudit.PAOCode = rdr["PAOCode"].ToString();
                            mstAudit.AuditFromDate = rdr["AuditFromDate"].ToString();
                            mstAudit.AuditToDate = rdr["AuditToDate"].ToString();
                            mstAudit.AuditIntimationDate = rdr["AuditIntimationDate"].ToString();
                            mstAudit.AuditStatusId = rdr["AuditStatusId"].ToString();
                            mstAudit.AuditStatus = rdr["Status"].ToString();
                            mstAudit.FinancialYear = rdr["FinancialYear"].ToString();
                            mstAudit.AuditReason = rdr["AuditReason"].ToString();
                            mstAudit.RejectionRemark = rdr["RejectionRemark"].ToString();
                            mstAudit.IsActive = rdr["IsActive"].ToString();

                            response.Add(mstAudit);
                        }
                    }
                }
            });
            return response;
        }

        public async Task<string> QuarterlylplanApprove(int FYID,string Quarter,int RoleUserMapId)
        {

            string message = string.Empty;
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = iasdb.GetStoredProcCommand("[OIAs].[AprovedQuarterlyAudit_For_test_SP]"))//"[OIAs].[AprovedQuarterlyAudit_SP]"))
                {
                    iasdb.AddInParameter(dbCommand, "FYID", DbType.Int32, FYID);
                    iasdb.AddInParameter(dbCommand, "RoleUserMapId", DbType.Int32, RoleUserMapId);
                    iasdb.AddInParameter(dbCommand, "Quarter", DbType.String, Quarter);
                    iasdb.AddInParameter(dbCommand, "ipAddress", DbType.String, CommonFunction.GetIPAddress());
                    iasdb.AddOutParameter(dbCommand, "Msg_Out_Status", DbType.String, 500);
                    int i = iasdb.ExecuteNonQuery(dbCommand);
                    message = Convert.ToString(iasdb.GetParameterValue(dbCommand, "Msg_Out_Status"));

                    //if (i > 0)
                    //{
                    //    message = "Quarterly plan approve successfully";
                    //}

                }
            });
            return message;
        }

        public async Task<string> RejectQuarterlyAudit(int FYID, string rejectreson,string Quarter, int RoleUserMapId)
        {

            string message = string.Empty;
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = iasdb.GetStoredProcCommand("[OIAs].[RejectQuarterlyAudit_For_Test_SP]"))//"[OIAs].[RejectQuarterlyAudit_SP]"))
                {
                    iasdb.AddInParameter(dbCommand, "FYID", DbType.Int32, FYID);
                    iasdb.AddInParameter(dbCommand, "RejectionRemark", DbType.String, rejectreson);
                    iasdb.AddInParameter(dbCommand, "RoleUserMapId", DbType.Int32, RoleUserMapId);
                    iasdb.AddInParameter(dbCommand, "ipAddress", DbType.String, CommonFunction.GetIPAddress());
                    iasdb.AddInParameter(dbCommand, "Quarter", DbType.String, Quarter);
                    iasdb.AddOutParameter(dbCommand, "Msg_Out_Status", DbType.String, 500);
                    int i = iasdb.ExecuteNonQuery(dbCommand);
                     message = Convert.ToString(iasdb.GetParameterValue(dbCommand, "Msg_Out_Status"));

                    //if (i > 0)
                    //{
                    //    message = "Quarterly plan rejected successfully";
                       
                    //}

                }
            });
            return message;
        }


        public string SupervisorTosupervisor(string currFYear, int roleUserMapId, int userId)
        {
            string message = string.Empty;
            using (DbCommand dbCommand = iasdb.GetStoredProcCommand("[OIAS].[SupervisorAnnualPlanToAnotherSupervisor_SP]"))//IASConstant.AssignToSupervisorAnnualPlan_SP))
            {

                iasdb.AddInParameter(dbCommand, "currFYear", DbType.String, currFYear);
                iasdb.AddInParameter(dbCommand, "forwardedToUserId", DbType.Int32, roleUserMapId);
                iasdb.AddInParameter(dbCommand, "userId", DbType.Int32, userId);
                iasdb.AddInParameter(dbCommand, "ipAddress", DbType.String, CommonFunction.GetIPAddress());
                iasdb.AddOutParameter(dbCommand, "Msg_Out_Status", DbType.String, 500);
                int i = iasdb.ExecuteNonQuery(dbCommand);
                message = Convert.ToString(iasdb.GetParameterValue(dbCommand, "Msg_Out_Status"));

            }
            return message;

        }
        public string SupervisorTosupervisorQuarterlyPlan(string currFYear, int roleUserMapId, int userId,string Quarter)
        {
            string message = string.Empty;
            using (DbCommand dbCommand = iasdb.GetStoredProcCommand("[OIAS].[SupervisorQuarterlyPlanToAnotherSupervisor_SP]"))//IASConstant.AssignToSupervisorAnnualPlan_SP))
            {

                iasdb.AddInParameter(dbCommand, "currFYear", DbType.String, currFYear);
                iasdb.AddInParameter(dbCommand, "forwardedToUserId", DbType.Int32, roleUserMapId);
                iasdb.AddInParameter(dbCommand, "userId", DbType.Int32, userId);
                iasdb.AddInParameter(dbCommand, "Quarter", DbType.String, Quarter);
                iasdb.AddInParameter(dbCommand, "ipAddress", DbType.String, CommonFunction.GetIPAddress());
                iasdb.AddOutParameter(dbCommand, "Msg_Out_Status", DbType.String, 500);
                int i = iasdb.ExecuteNonQuery(dbCommand);
                message = Convert.ToString(iasdb.GetParameterValue(dbCommand, "Msg_Out_Status"));

            }
            return message;

        }
    }
}
