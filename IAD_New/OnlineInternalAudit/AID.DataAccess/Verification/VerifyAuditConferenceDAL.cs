﻿using BusinessModel.Verification;
using Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Text;
using System.Threading.Tasks;

namespace AID.DataAccess.Verification
{
    public class VerifyAuditConferenceDAL
    {
        private Database iasdb = null;
        public VerifyAuditConferenceDAL(Database database)
        {
            iasdb = database;
        }

        public async Task<List<GetAuditsOfConferenceForVerificationResponse>> GetAuditsOfConferenceForVerification(string type)
        {
            List<GetAuditsOfConferenceForVerificationResponse> response = new List<GetAuditsOfConferenceForVerificationResponse>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = iasdb.GetStoredProcCommand(IASConstant.SP_GetAuditsOfConferenceForVerification))
                {
                    iasdb.AddInParameter(dbCommand, "@type", DbType.String, type);
                    using (IDataReader objReader = iasdb.ExecuteReader(dbCommand))
                    {
                        while (objReader.Read())
                        {
                            GetAuditsOfConferenceForVerificationResponse obj = new GetAuditsOfConferenceForVerificationResponse();
                            obj.EntryConferenceId = objReader["EntryConferenceId"] != DBNull.Value ? Convert.ToInt32(objReader["EntryConferenceId"]) : 0;
                            obj.ExitConferenceId = objReader["ExitConferenceId"] != DBNull.Value ? Convert.ToInt32(objReader["ExitConferenceId"]) : 0;
                            obj.AuditId = objReader["AuditId"] != DBNull.Value ? Convert.ToInt32(objReader["AuditId"]) : 0;
                            obj.PAOId = objReader["PAOId"] != DBNull.Value ? Convert.ToInt32(objReader["PAOId"]) : 0;
                            obj.Name = objReader["Name"] != DBNull.Value ? Convert.ToString(objReader["Name"]) : null;
                            obj.AuditStatusId = objReader["AuditStatusId"] != DBNull.Value ? Convert.ToInt32(objReader["AuditStatusId"]) : 0;
                            obj.AuditFromDate = objReader["AuditFromDate"] != DBNull.Value ? Convert.ToDateTime(objReader["AuditFromDate"]) : (DateTime?)null;
                            obj.AuditToDate = objReader["AuditToDate"] != DBNull.Value ? Convert.ToDateTime(objReader["AuditToDate"]) : (DateTime?)null;
                            obj.AuditIntimationDate = objReader["AuditIntimationDate"] != DBNull.Value ? Convert.ToDateTime(objReader["AuditIntimationDate"]) : (DateTime?)null;

                            response.Add(obj);
                        }
                    }
                }
            });
            return response;
        }
    }
}
