﻿using BusinessModel.Verification;
using Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Text;
using System.Threading.Tasks;

namespace AID.DataAccess.Verification
{
    public class VerifyAuditTeamDAL
    {
        private Database iasdb = null;
        public VerifyAuditTeamDAL(Database database)
        {
            iasdb = database;
        }


        public async Task<List<GetAuditTeamForVerificationResponse>> GetTeamOfAuditForVerification(int auditId)
        {
            List<GetAuditTeamForVerificationResponse> response = new List<GetAuditTeamForVerificationResponse>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = iasdb.GetStoredProcCommand(IASConstant.SP_GetTeamOfAuditForVerification))
                {
                    iasdb.AddInParameter(dbCommand, "@auditId", DbType.Int32, auditId);
                    using (IDataReader objReader = iasdb.ExecuteReader(dbCommand))
                    {
                        while (objReader.Read())
                        {
                            GetAuditTeamForVerificationResponse obj = new GetAuditTeamForVerificationResponse();
                            obj.EmpId = objReader["EmpID"] != DBNull.Value ? Convert.ToInt32(objReader["EmpID"]) : 0;
                            obj.EmpName = objReader["EmpName"] != DBNull.Value ? Convert.ToString(objReader["EmpName"]) : null;
                            obj.DesignationId = objReader["DesignationId"] != DBNull.Value ? Convert.ToInt32(objReader["DesignationId"]) : 0;
                            obj.Email = objReader["Email"] != DBNull.Value ? Convert.ToString(objReader["Email"]) : null;
                            obj.PhoneNo = objReader["PhoneNo"] != DBNull.Value ? Convert.ToString(objReader["PhoneNo"]) : null;
                            obj.DesignationName = objReader["DesignationName"] != DBNull.Value ? Convert.ToString(objReader["DesignationName"]) : null;
                            obj.DesignationId = objReader["DesignationId"] != DBNull.Value ? Convert.ToInt32(objReader["DesignationId"]) : 0;
                            obj.AuditId = objReader["AuditId"] != DBNull.Value ? Convert.ToInt32(objReader["AuditId"]) : 0;
                            obj.AuditFromDate = objReader["AuditFromDate"] != DBNull.Value ? Convert.ToDateTime(objReader["AuditFromDate"]) : (DateTime?)null;
                            obj.AuditToDate = objReader["AuditToDate"] != DBNull.Value ? Convert.ToDateTime(objReader["AuditToDate"]) : (DateTime?)null;
                            obj.AuditStatusId = objReader["AuditStatusId"] != DBNull.Value ? Convert.ToInt32(objReader["AuditStatusId"]) : 0;
                            obj.Quarter = objReader["Quarter"] != DBNull.Value ? Convert.ToString(objReader["Quarter"]) : null;
                            obj.RoleId = objReader["RoleId"] != DBNull.Value ? Convert.ToInt32(objReader["RoleId"]) : 0;
                            obj.RoleName = objReader["RoleName"] != DBNull.Value ? Convert.ToString(objReader["RoleName"]) : null;
                            obj.Name = objReader["Name"] != DBNull.Value ? Convert.ToString(objReader["Name"]) : null;
                            obj.Status = objReader["Status"] != DBNull.Value ? Convert.ToString(objReader["Status"]) : null;

                            response.Add(obj);
                        }
                    }
                }
            });
            return response;
        }


        public async Task<int> UpdateTeamOfAuditForVerification(UpdateAuditTeamForVerificationRequest request)
        {
            int result = 0;
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = iasdb.GetStoredProcCommand(IASConstant.SP_UpdateTeamOfAuditForVerification))
                {
                    iasdb.AddInParameter(dbCommand, "@statusId", DbType.Int32, request.StatusId);
                    iasdb.AddInParameter(dbCommand, "@rejectionRemark", DbType.String, request.RejectionRemark);
                    iasdb.AddInParameter(dbCommand, "@auditId", DbType.Int32, request.AuditId);
                    iasdb.AddInParameter(dbCommand, "@userId", DbType.Int32, request.UserId);
                    iasdb.AddInParameter(dbCommand, "@ipAddress", DbType.String, request.IPAddress);
                    iasdb.AddInParameter(dbCommand, "@forwardedToUserId", DbType.Int32, request.ForwardedToUserId);
                    iasdb.AddInParameter(dbCommand, "@auditDecisionStatusId", DbType.Int32, request.AuditDecisionStatusId);

                    result = iasdb.ExecuteNonQuery(dbCommand);
                }
            });
            return result;
        }

        public async Task<List<GetAuditDetailsForVerificationDropdownResponse>> GetAuditDetailsForVerificationDropdown(int roleUserMapId)
        {
            List<GetAuditDetailsForVerificationDropdownResponse> response = new List<GetAuditDetailsForVerificationDropdownResponse>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = iasdb.GetStoredProcCommand(IASConstant.SP_GetAuditDetailsForVerificationDropdown))
                {
                    iasdb.AddInParameter(dbCommand, "@roleUserMapId", DbType.Int32, roleUserMapId);
                    using (IDataReader objReader = iasdb.ExecuteReader(dbCommand))
                    {
                        while (objReader.Read())
                        {
                            GetAuditDetailsForVerificationDropdownResponse obj = new GetAuditDetailsForVerificationDropdownResponse();
                            obj.AuditId = objReader["AuditId"] != DBNull.Value ? Convert.ToInt32(objReader["AuditId"]) : 0;
                            obj.Name = objReader["Name"] != DBNull.Value ? Convert.ToString(objReader["Name"]) : null;
                            obj.AuditFromDate = objReader["AuditFromDate"] != DBNull.Value ? Convert.ToDateTime(objReader["AuditFromDate"]) : (DateTime?)null;
                            obj.AuditToDate = objReader["AuditToDate"] != DBNull.Value ? Convert.ToDateTime(objReader["AuditToDate"]) : (DateTime?)null;
                            obj.Quarter = objReader["Quarter"] != DBNull.Value ? Convert.ToString(objReader["Quarter"]) : null;
                            response.Add(obj);
                        }
                    }
                }
            });
            return response;
        }
    }
}
