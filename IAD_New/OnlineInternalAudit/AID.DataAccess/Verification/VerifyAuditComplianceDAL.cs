﻿using BusinessModel.Verification;
using Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Text;
using System.Threading.Tasks;

namespace AID.DataAccess.Verification
{
    public class VerifyAuditComplianceDAL
    {
        private Database iasdb = null;
        public VerifyAuditComplianceDAL(Database database)
        {
            iasdb = database;
        }

        public async Task<List<GetAuditsForwardedToSupervisorResponse>> GetAuditsForwardedToSupervisor()
        {
            List<GetAuditsForwardedToSupervisorResponse> response = new List<GetAuditsForwardedToSupervisorResponse>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = iasdb.GetStoredProcCommand(IASConstant.SP_GetAuditsForwardedToAuditSupervisor))
                {
                    using (IDataReader objReader = iasdb.ExecuteReader(dbCommand))
                    {
                        while (objReader.Read())
                        {
                            GetAuditsForwardedToSupervisorResponse obj = new GetAuditsForwardedToSupervisorResponse();
                            obj.AuditId = objReader["AuditId"] != DBNull.Value ? Convert.ToInt32(objReader["AuditId"]) : 0;
                            obj.AuditFromDate = objReader["AuditFromDate"] != DBNull.Value ? Convert.ToDateTime(objReader["AuditFromDate"]) : (DateTime?)null;
                            obj.AuditToDate = objReader["AuditToDate"] != DBNull.Value ? Convert.ToDateTime(objReader["AuditToDate"]) : (DateTime?)null;
                            obj.AuditStatusId = objReader["AuditStatusId"] != DBNull.Value ? Convert.ToInt32(objReader["AuditStatusId"]) : 0;
                            obj.Name = objReader["Name"] != DBNull.Value ? Convert.ToString(objReader["Name"]) : null;
                            obj.Status = objReader["Status"] != DBNull.Value ? Convert.ToString(objReader["Status"]) : null;
                            obj.AuditIntimationDate = objReader["AuditIntimationDate"] != DBNull.Value ? Convert.ToDateTime(objReader["AuditIntimationDate"]) : (DateTime?)null;
                            obj.PaoId = objReader["PAOId"] != DBNull.Value ? Convert.ToInt32(objReader["PAOId"]) : 0;

                            response.Add(obj);
                        }
                    }
                }
            });
            return response;
        }


        public async Task<List<GetAuditsForwardedToSupervisorResponse>> GetAuditsWhoseParasOrComplainceForwardedToSupervisor(string type)
        {
            List<GetAuditsForwardedToSupervisorResponse> response = new List<GetAuditsForwardedToSupervisorResponse>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = iasdb.GetStoredProcCommand(IASConstant.SP_GetAuditsWhoseParasOrComplainceForwardedToSupervisor))
                {
                    iasdb.AddInParameter(dbCommand, "@type", DbType.String, type);
                    using (IDataReader objReader = iasdb.ExecuteReader(dbCommand))
                    {
                        while (objReader.Read())
                        {
                            GetAuditsForwardedToSupervisorResponse obj = new GetAuditsForwardedToSupervisorResponse();
                            obj.AuditId = objReader["AuditId"] != DBNull.Value ? Convert.ToInt32(objReader["AuditId"]) : 0;
                            obj.AuditFromDate = objReader["AuditFromDate"] != DBNull.Value ? Convert.ToDateTime(objReader["AuditFromDate"]) : (DateTime?)null;
                            obj.AuditToDate = objReader["AuditToDate"] != DBNull.Value ? Convert.ToDateTime(objReader["AuditToDate"]) : (DateTime?)null;
                            obj.AuditStatusId = objReader["AuditStatusId"] != DBNull.Value ? Convert.ToInt32(objReader["AuditStatusId"]) : 0;
                            obj.Name = objReader["Name"] != DBNull.Value ? Convert.ToString(objReader["Name"]) : null;
                            obj.Status = objReader["Status"] != DBNull.Value ? Convert.ToString(objReader["Status"]) : null;
                            obj.AuditIntimationDate = objReader["AuditIntimationDate"] != DBNull.Value ? Convert.ToDateTime(objReader["AuditIntimationDate"]) : (DateTime?)null;
                            obj.PaoId = objReader["PAOId"] != DBNull.Value ? Convert.ToInt32(objReader["PAOId"]) : 0;

                            response.Add(obj);
                        }
                    }
                }
            });
            return response;
        }


        public async Task<List<GetAuditsForwardedToSupervisorResponse>> GetAuditsWhoseSettlementLetterForwardedToSupervisor()
        {
            List<GetAuditsForwardedToSupervisorResponse> response = new List<GetAuditsForwardedToSupervisorResponse>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = iasdb.GetStoredProcCommand(IASConstant.SP_GetAuditsWhoseSettlementLetterForwardedToSupervisor))
                {
                    using (IDataReader objReader = iasdb.ExecuteReader(dbCommand))
                    {
                        while (objReader.Read())
                        {
                            GetAuditsForwardedToSupervisorResponse obj = new GetAuditsForwardedToSupervisorResponse();
                            obj.AuditId = objReader["AuditId"] != DBNull.Value ? Convert.ToInt32(objReader["AuditId"]) : 0;
                            obj.AuditFromDate = objReader["AuditFromDate"] != DBNull.Value ? Convert.ToDateTime(objReader["AuditFromDate"]) : (DateTime?)null;
                            obj.AuditToDate = objReader["AuditToDate"] != DBNull.Value ? Convert.ToDateTime(objReader["AuditToDate"]) : (DateTime?)null;
                            obj.AuditStatusId = objReader["AuditStatusId"] != DBNull.Value ? Convert.ToInt32(objReader["AuditStatusId"]) : 0;
                            obj.Name = objReader["Name"] != DBNull.Value ? Convert.ToString(objReader["Name"]) : null;
                            obj.Status = objReader["Status"] != DBNull.Value ? Convert.ToString(objReader["Status"]) : null;
                            obj.AuditIntimationDate = objReader["AuditIntimationDate"] != DBNull.Value ? Convert.ToDateTime(objReader["AuditIntimationDate"]) : (DateTime?)null;
                            obj.PaoId = objReader["PAOId"] != DBNull.Value ? Convert.ToInt32(objReader["PAOId"]) : 0;

                            response.Add(obj);
                        }
                    }
                }
            });
            return response;
        }
    }
}
