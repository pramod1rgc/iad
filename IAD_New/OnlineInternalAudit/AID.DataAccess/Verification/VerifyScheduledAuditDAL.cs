﻿
using BusinessModel.Verification;
using Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;

namespace AID.DataAccess.Verification
{
    public class VerifyScheduledAuditDAL
    {
        private Database iasdb = null;
        public VerifyScheduledAuditDAL(Database database)
        {
            iasdb = database;
        }


        public async Task<List<GetScheduledAuditsForVerificationResponse>> GetScheduledAuditsForVerification(int finYearId, string quarter, int roleUserMapId)
        {
            List<GetScheduledAuditsForVerificationResponse> response = new List<GetScheduledAuditsForVerificationResponse>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = iasdb.GetStoredProcCommand(IASConstant.SP_GetScheduledAuditForVerification))
                {
                    iasdb.AddInParameter(dbCommand, "@finYearId", DbType.Int32, finYearId);
                    iasdb.AddInParameter(dbCommand, "@quarter", DbType.String, quarter);
                    iasdb.AddInParameter(dbCommand, "@roleUserMapId", DbType.Int32, roleUserMapId);
                    using (IDataReader objReader = iasdb.ExecuteReader(dbCommand))
                    {
                        while (objReader.Read())
                        {
                            GetScheduledAuditsForVerificationResponse obj = new GetScheduledAuditsForVerificationResponse();
                            obj.AuditId = objReader["AuditId"] != DBNull.Value ? Convert.ToInt32(objReader["AuditId"]) : 0;
                            obj.PAOId = objReader["PAOId"] != DBNull.Value ? Convert.ToInt32(objReader["PAOId"]) : 0;
                            obj.FinancialYearId = objReader["FinancialYearId"] != DBNull.Value ? Convert.ToInt32(objReader["FinancialYearId"]) : 0;
                            obj.AuditName = objReader["AuditName"] != DBNull.Value ? Convert.ToString(objReader["AuditName"]) : null;
                            obj.AuditStatusId = objReader["AuditStatusId"] != DBNull.Value ? Convert.ToInt32(objReader["AuditStatusId"]) : 0;
                            obj.AuditFromDate = objReader["AuditFromDate"] != DBNull.Value ? Convert.ToDateTime(objReader["AuditFromDate"]) : (DateTime?)null;
                            obj.AuditToDate = objReader["AuditToDate"] != DBNull.Value ? Convert.ToDateTime(objReader["AuditToDate"]) : (DateTime?)null;
                            obj.Quarter = objReader["Quarter"] != DBNull.Value ? Convert.ToString(objReader["Quarter"]) : null;
                            obj.PAOName = objReader["Name"] != DBNull.Value ? Convert.ToString(objReader["Name"]) : null;
                            obj.PAOCode = objReader["PAOCode"] != DBNull.Value ? Convert.ToString(objReader["PAOCode"]) : null;
                            obj.ControllerCode = objReader["ControllerCode"] != DBNull.Value ? Convert.ToString(objReader["ControllerCode"]) : null;
                            obj.Status = objReader["Status"] != DBNull.Value ? Convert.ToString(objReader["Status"]) : null;

                            response.Add(obj);
                        }
                    }
                }
            });
            return response;
        }


        public async Task<int> UpdateScheduleAuditForVerification(UpdateScheduleAuditForVerificationRequest request)
        {
            int result = 0;
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = iasdb.GetStoredProcCommand(IASConstant.SP_UpdateScheduledAuditForVerification))
                {
                    var table = CommonFunction.CreateDataTable(request.AuditSchedulesType);
                    SqlParameter param = new SqlParameter("@auditSchedulesType", table);
                    param.SqlDbType = SqlDbType.Structured;
                    dbCommand.Parameters.Add(param);
                    iasdb.AddInParameter(dbCommand, "@userId", DbType.Int32, request.UserId);
                    iasdb.AddInParameter(dbCommand, "@statusId", DbType.Int32, request.StatusId);
                    iasdb.AddInParameter(dbCommand, "@ipAddress", DbType.String, request.IPAddress);
                    iasdb.AddInParameter(dbCommand, "@rejectionRemark", DbType.String, request.RejectionRemark);
                    iasdb.AddInParameter(dbCommand, "@forwardedToUserId", DbType.Int32, request.ForwardedToUserId);
                    iasdb.AddInParameter(dbCommand, "@auditDecisionStatusId", DbType.Int32, request.AuditDecisionStatusId);

                    result = iasdb.ExecuteNonQuery(dbCommand);
                }
            });
            return result;
        }

        public async Task<GetPendingTaskCountResponse> GetPendingTaskCount(int RoleUserMapId)
        {
            GetPendingTaskCountResponse response = new GetPendingTaskCountResponse();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = iasdb.GetStoredProcCommand(IASConstant.SP_GetPendingTaskCounts))
                {
                    iasdb.AddInParameter(dbCommand, "@roleUserMapId", DbType.Int32, RoleUserMapId);
                    using (IDataReader objReader = iasdb.ExecuteReader(dbCommand))
                    {
                        while (objReader.Read())
                        {
                            response.AnnualPlanCount = objReader["AnnualPlanCount"] != DBNull.Value ? Convert.ToInt32(objReader["AnnualPlanCount"]) : 0;
                            response.QuarterlyPlanCount = objReader["QuarterlyPlanCount"] != DBNull.Value ? Convert.ToInt32(objReader["QuarterlyPlanCount"]) : 0;
                            response.AuditScheduleCount = objReader["AuditScheduleCount"] != DBNull.Value ? Convert.ToInt32(objReader["AuditScheduleCount"]) : 0;
                            response.AuditTeamCount = objReader["AuditTeamCount"] != DBNull.Value ? Convert.ToInt32(objReader["AuditTeamCount"]) : 0;
                            response.ComplianceReportCount = objReader["ComplianceReportCount"] != DBNull.Value ? Convert.ToInt32(objReader["ComplianceReportCount"]) : 0;
                            response.ParaReportCount = objReader["ParaReportCount"] != DBNull.Value ? Convert.ToInt32(objReader["ParaReportCount"]) : 0;
                            response.ParaSettlementCount = objReader["ParaSettlementCount"] != DBNull.Value ? Convert.ToInt32(objReader["ParaSettlementCount"]) : 0;
                            response.EntryConferenceCount = objReader["EntryConferenceCount"] != DBNull.Value ? Convert.ToInt32(objReader["EntryConferenceCount"]) : 0;
                            response.ExitConferenceCount = objReader["ExitConferenceCount"] != DBNull.Value ? Convert.ToInt32(objReader["ExitConferenceCount"]) : 0;
                        }
                    }
                }
            });
            return response;
        }

        public async Task<List<GetDistinctQuartersBasedOnStatusAndFYResponse>> GetDistinctQuartersBasedOnStatusAndFY(int fyId, string statusId, int statusId2)
        {
            List<GetDistinctQuartersBasedOnStatusAndFYResponse> response = new List<GetDistinctQuartersBasedOnStatusAndFYResponse>();

            await Task.Run(() =>
            {
                using (DbCommand dbCommand = iasdb.GetStoredProcCommand(IASConstant.SP_GetDistinctQuartersBasedOnStatusAndFY))
                {
                    iasdb.AddInParameter(dbCommand, "@fyId", DbType.Int32, fyId);
                    iasdb.AddInParameter(dbCommand, "@statusId", DbType.String, statusId);
                    iasdb.AddInParameter(dbCommand, "@statusId2", DbType.Int32, statusId2);
                    using (IDataReader objReader = iasdb.ExecuteReader(dbCommand))
                    {
                        while (objReader.Read())
                        {
                            GetDistinctQuartersBasedOnStatusAndFYResponse obj = new GetDistinctQuartersBasedOnStatusAndFYResponse();

                            obj.Quarter = objReader["Quarter"] != DBNull.Value ? Convert.ToString(objReader["Quarter"]) : null;

                            response.Add(obj);
                        }
                    }
                }
            });
            return response;
        }
    }
}
