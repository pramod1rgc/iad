﻿using BusinessModel.AuditReport;
using Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Text;
using System.Threading.Tasks;

namespace AID.DataAccess.AuditReport
{
    public class AuditReportDAL
    {
        private Database iasdb = null;
        public AuditReportDAL(Database database)
        {
            iasdb = database;
        }


        public async Task<List<GetAuditsForAuditReportBySupervisorResponse>> GetAuditsForAuditReportBySupervisor(int pageSize, int pageNumber, string searchTerm)
        {
            List<GetAuditsForAuditReportBySupervisorResponse> response = new List<GetAuditsForAuditReportBySupervisorResponse>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = iasdb.GetStoredProcCommand(IASConstant.SP_GetAuditsForAuditReportBySupervisor))
                {
                    iasdb.AddInParameter(dbCommand, "@pageSize", DbType.Int32, pageSize);
                    iasdb.AddInParameter(dbCommand, "@pageNumber", DbType.Int32, pageNumber);
                    iasdb.AddInParameter(dbCommand, "@searchTerm", DbType.String, string.IsNullOrEmpty(searchTerm) ? string.Empty : searchTerm.Trim());

                    using (IDataReader objReader = iasdb.ExecuteReader(dbCommand))
                    {
                        while (objReader.Read())
                        {
                            GetAuditsForAuditReportBySupervisorResponse obj = new GetAuditsForAuditReportBySupervisorResponse();
                            obj.AuditId = objReader["AuditId"] != DBNull.Value ? Convert.ToInt32(objReader["AuditId"]) : 0;
                            obj.PaoId = objReader["PAOId"] != DBNull.Value ? Convert.ToInt32(objReader["PAOId"]) : 0;
                            obj.Name = objReader["Name"] != DBNull.Value ? Convert.ToString(objReader["Name"]) : null;
                            obj.AuditFromDate = objReader["AuditFromDate"] != DBNull.Value ? Convert.ToDateTime(objReader["AuditFromDate"]) : (DateTime?)null;
                            obj.AuditToDate = objReader["AuditToDate"] != DBNull.Value ? Convert.ToDateTime(objReader["AuditToDate"]) : (DateTime?)null;
                            obj.AuditIntimationDate = objReader["AuditIntimationDate"] != DBNull.Value ? Convert.ToDateTime(objReader["AuditIntimationDate"]) : (DateTime?)null;
                            obj.AuditStatusId = objReader["AuditStatusId"] != DBNull.Value ? Convert.ToInt32(objReader["AuditStatusId"]) : 0;
                            obj.Status = objReader["Status"] != DBNull.Value ? Convert.ToString(objReader["Status"]) : null;
                            obj.TotalCount = objReader["TotalCount"] != DBNull.Value ? Convert.ToInt32(objReader["TotalCount"]) : 0;

                            response.Add(obj);
                        }
                    }
                }
            });
            return response;
        }


        public async Task<GetAuditReportResponse> GetAuditReport(int auditId)
        {
           GetAuditReportResponse response = new GetAuditReportResponse();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = iasdb.GetStoredProcCommand(IASConstant.SP_GetAuditReport))
                {
                    iasdb.AddInParameter(dbCommand, "@auditId", DbType.Int32, auditId);
                    using (IDataReader objReader = iasdb.ExecuteReader(dbCommand))
                    {
                        while (objReader.Read())
                        {
                            response.AuditId = objReader["AuditId"] != DBNull.Value ? Convert.ToInt32(objReader["AuditId"]) : 0;
                            response.PaoId = objReader["PAOId"] != DBNull.Value ? Convert.ToInt32(objReader["PAOId"]) : 0;
                            response.Name = objReader["Name"] != DBNull.Value ? Convert.ToString(objReader["Name"]) : null;
                            response.AuditFromDate = objReader["AuditFromDate"] != DBNull.Value ? Convert.ToDateTime(objReader["AuditFromDate"]) : (DateTime?)null;
                            response.AuditToDate = objReader["AuditToDate"] != DBNull.Value ? Convert.ToDateTime(objReader["AuditToDate"]) : (DateTime?)null;
                            response.AuditIntimationDate = objReader["AuditIntimationDate"] != DBNull.Value ? Convert.ToDateTime(objReader["AuditIntimationDate"]) : (DateTime?)null;
                            response.AuditStatusId = objReader["AuditStatusId"] != DBNull.Value ? Convert.ToInt32(objReader["AuditStatusId"]) : 0;
                            response.Status = objReader["Status"] != DBNull.Value ? Convert.ToString(objReader["Status"]) : null;
                            response.EntryConferenceId = objReader["EntryConferenceId"] != DBNull.Value ? Convert.ToInt32(objReader["EntryConferenceId"]) : 0;
                            response.EntryConferenceDate = objReader["EntryConferenceDate"] != DBNull.Value ? Convert.ToDateTime(objReader["EntryConferenceDate"]) : (DateTime?)null;
                            response.ExitConferenceId = objReader["ExitConferenceId"] != DBNull.Value ? Convert.ToInt32(objReader["ExitConferenceId"]) : 0;
                            response.ExitConferenceDate = objReader["ExitConferenceDate"] != DBNull.Value ? Convert.ToDateTime(objReader["ExitConferenceDate"]) : (DateTime?)null;
                            response.ParaCreatedDate = objReader["ParaCreatedDate"] != DBNull.Value ? Convert.ToDateTime(objReader["ParaCreatedDate"]) : (DateTime?)null;
                            response.AuditParaSettlementLetterId = objReader["AuditParaSettlementLetterId"] != DBNull.Value ? Convert.ToInt32(objReader["AuditParaSettlementLetterId"]) : 0;
                            response.ParaSettlementLetterDate = objReader["ParaSettlementLetterDate"] != DBNull.Value ? Convert.ToDateTime(objReader["ParaSettlementLetterDate"]) : (DateTime?)null;
                            response.TotalParaCount = objReader["TotalParaCount"] != DBNull.Value ? Convert.ToInt32(objReader["TotalParaCount"]) : 0;
                            response.TotalApprovedParaCount = objReader["TotalApprovedParaCount"] != DBNull.Value ? Convert.ToInt32(objReader["TotalApprovedParaCount"]) : 0;
                            response.TotalOpenParaCount = objReader["TotalOpenParaCount"] != DBNull.Value ? Convert.ToInt32(objReader["TotalOpenParaCount"]) : 0;
                            response.TotalPartialParaCount = objReader["TotalPartialParaCount"] != DBNull.Value ? Convert.ToInt32(objReader["TotalPartialParaCount"]) : 0;
                            response.TotalLowSeverityParas = objReader["TotalLowSeverityParas"] != DBNull.Value ? Convert.ToInt32(objReader["TotalLowSeverityParas"]) : 0;
                            response.TotalMediumSeverityParas = objReader["TotalMediumSeverityParas"] != DBNull.Value ? Convert.ToInt32(objReader["TotalMediumSeverityParas"]) : 0;
                            response.TotalHighSeverityParas = objReader["TotalHighSeverityParas"] != DBNull.Value ? Convert.ToInt32(objReader["TotalHighSeverityParas"]) : 0;
                            response.LastParaReplyDate = objReader["LastParaReplyDate"] != DBNull.Value ? Convert.ToDateTime(objReader["LastParaReplyDate"]) : (DateTime?)null;
                        }
                    }
                }
            });
            return response;
        }
    }
}
