﻿using BusinessModel.Role;
using Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Text;
using System.Threading.Tasks;

namespace AID.DataAccess
{
    public class RoleDAL
    {
        private Database iasdb = null;
        public RoleDAL(Database database)
        {
            iasdb = database;
        }

        public async Task<List<GetRoleResponse>> GetRoles()
        {
            List<GetRoleResponse> response = new List<GetRoleResponse>();
            string result = string.Empty;
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = iasdb.GetStoredProcCommand(IASConstant.SP_GetRoles))
                {
                    using (IDataReader objReader = iasdb.ExecuteReader(dbCommand))
                    {
                        while (objReader.Read())
                        {
                            GetRoleResponse obj = new GetRoleResponse();
                            obj.RoleId = objReader["RoleID"] != DBNull.Value ? Convert.ToInt32(objReader["RoleID"]) : 0;
                            obj.RoleName = objReader["RoleName"] != DBNull.Value ? Convert.ToString(objReader["RoleName"]) : null;
                            obj.RoleDescription = objReader["RoleDescription"] != DBNull.Value ? Convert.ToString(objReader["RoleDescription"]) : null;
                            obj.RoleHierarchy = objReader["RoleHierarchy"] != DBNull.Value ? Convert.ToInt32(objReader["RoleHierarchy"]) : 0;
                            obj.Reason = objReader["Reason"] != DBNull.Value ? Convert.ToString(objReader["Reason"]) : null;
                            obj.CreatedDate = objReader["CreatedDate"] != DBNull.Value ? Convert.ToDateTime(objReader["CreatedDate"]) : (DateTime?) null;
                            obj.ModifiedDate = objReader["ModifiedDate"] != DBNull.Value ? Convert.ToDateTime(objReader["ModifiedDate"]) : (DateTime?)null;
                            obj.IsActive = objReader["IsActive"] != DBNull.Value ? Convert.ToBoolean(objReader["IsActive"]) : false;
                            obj.DeactivationDate = objReader["DeactivationDate"] != DBNull.Value ? Convert.ToDateTime(objReader["DeactivationDate"]) : (DateTime?)null;

                            response.Add(obj);
                        }
                    }
                }
            });
            return response;
        }



        public async Task<int> UpsertRoles(UpsertRoleRequest request)
        {
            int result = 0;
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = iasdb.GetStoredProcCommand(IASConstant.SP_UpsertRoles))
                {
                    iasdb.AddInParameter(dbCommand, "@RoleId", DbType.Int32, request.RoleId);
                    iasdb.AddInParameter(dbCommand, "@RoleName", DbType.String, request.RoleName);
                    iasdb.AddInParameter(dbCommand, "@RoleDescription", DbType.String, request.RoleDescription);
                    iasdb.AddInParameter(dbCommand, "@RoleHierarchy", DbType.Int32, request.RoleHierarchy);
                    iasdb.AddInParameter(dbCommand, "@UserId", DbType.Int32, request.UserId);
                    iasdb.AddInParameter(dbCommand, "@IPAddress", DbType.String, request.IPAddress);

                    result = iasdb.ExecuteNonQuery(dbCommand);
                }
            });
            return result;
        }


        public async Task<int> ToggleRoleActivation(ToggleRoleRequest request)
        {
            int result = 0;
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = iasdb.GetStoredProcCommand(IASConstant.SP_ToggleActivationOfRole))
                {
                    iasdb.AddInParameter(dbCommand, "@RoleId", DbType.Int32, request.RoleId);
                    iasdb.AddInParameter(dbCommand, "@Reason", DbType.String, request.Reason);
                    iasdb.AddInParameter(dbCommand, "@IsActive", DbType.Boolean, request.IsActive);
                    iasdb.AddInParameter(dbCommand, "@UserId", DbType.Int32, request.UserId);
                    iasdb.AddInParameter(dbCommand, "@IPAddress", DbType.String, request.IPAddress);

                    result = iasdb.ExecuteNonQuery(dbCommand);
                }
            });
            return result;
        }
    }
}
