﻿using BusinessModel.Employee;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Text;
using System.Threading.Tasks;

namespace AID.DataAccess
{
    public class EmployeeDAL
    {
        List<EmployeeModel> lstEmployee;
        private Database iasdb = null;
        public EmployeeDAL(Database database)
        {
            iasdb = database;
        }


        /// <summary>
        /// Get All Employee
        /// </summary>
        /// <returns></returns>
        public async Task<List<EmployeeModel>> GetAllEmployee()
        {
            lstEmployee = new List<EmployeeModel>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = iasdb.GetStoredProcCommand("GetAllEmployee_SP"))
                {
                    using (IDataReader rdr = iasdb.ExecuteReader(dbCommand))
                    {
                        while (rdr.Read())
                        {
                            EmployeeModel Mstemployee = new EmployeeModel();
                            Mstemployee.EmpID = Convert.ToInt32(rdr["EmpID"]);
                            Mstemployee.EmpName = rdr["EmpName"].ToString();
                            Mstemployee.Designation = rdr["Designation"].ToString();
                            Mstemployee.Email = rdr["Email"].ToString();
                            Mstemployee.DesigID = Convert.ToInt32(rdr["DesigID"]);
                            Mstemployee.MobileNo =rdr["MobileNo"].ToString();
                            Mstemployee.PhoneNo= rdr["PhoneNo"].ToString();
                            Mstemployee.IsActive = rdr["IsActive"].ToString();
                            lstEmployee.Add(Mstemployee);
                        }
                    }

                }
            });
            return lstEmployee;
        }


        /// <summary>
        /// Get All Employee
        /// </summary>
        /// <returns></returns>
        public async Task<List<EmployeeModel>> GetAllDesignation()
        {
            lstEmployee = new List<EmployeeModel>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = iasdb.GetStoredProcCommand("Designation_SP"))
                {
                    using (IDataReader rdr = iasdb.ExecuteReader(dbCommand))
                    {
                        while (rdr.Read())
                        {
                            EmployeeModel Mstemployee = new EmployeeModel();
                            Mstemployee.DesigID = Convert.ToInt32(rdr["ID"]);
                            Mstemployee.DesigName = rdr["DesignationName"].ToString();
                            lstEmployee.Add(Mstemployee);
                        }
                    }

                }
            });
            return lstEmployee;
        }


        /// <summary>
        /// Save Employee
        /// </summary>
        /// <param name="hdnEmpId"></param>
        /// <param name="objEmp"></param>
        /// <returns></returns>
        public async Task<string> SaveEmployee(string hdnEmpId, EmployeeModel objEmp)
        {
            try
            {
                string message = string.Empty;
                await Task.Run(() =>
                {
                    using (DbCommand dbCommand = iasdb.GetStoredProcCommand("[OIAS].InsEmpDetails_SP"))
                    {
                        iasdb.AddInParameter(dbCommand, "EmpId", DbType.String, hdnEmpId);
                        iasdb.AddInParameter(dbCommand, "EmpName", DbType.String, objEmp.EmpName.Trim());
                        iasdb.AddInParameter(dbCommand, "EmpCode", DbType.String, "");
                        iasdb.AddInParameter(dbCommand, "DesignID", DbType.String, objEmp.DesigName.Trim());
                        iasdb.AddInParameter(dbCommand, "PhoneNo", DbType.String, objEmp.PhoneNo);
                        iasdb.AddInParameter(dbCommand, "MobileNo", DbType.String, objEmp.MobileNo);
                        iasdb.AddInParameter(dbCommand, "Email", DbType.String, objEmp.Email.Trim());
                        iasdb.AddInParameter(dbCommand, "IsActive", DbType.Boolean, 1); /*objEmp.IsActive.Trim()*/
                        iasdb.AddOutParameter(dbCommand, "Msg_Out_Status", DbType.String, 500);
                        iasdb.ExecuteNonQuery(dbCommand);
                        message = Convert.ToString(iasdb.GetParameterValue(dbCommand, "Msg_Out_Status"));


                    }
                });
                return message;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }


        /// <summary>
        /// Active Deactive User
        /// </summary>
        /// <param name="objemployee"></param>
        /// <returns></returns>
        public string ActiveDeactiveUser(EmployeeModel objemployee)
        {
            string message = string.Empty;
            try
            {
               

                string IsActive = "";
                if (objemployee.IsActive == "true")
                {
                    IsActive = "1";
                }
                else
                {
                    IsActive = "0";
                }
                using (DbCommand dbCommand = iasdb.GetStoredProcCommand("[OIAS].[Mst_EmployeeActiveAndDeActive_SP]"))
                {
                    iasdb.AddInParameter(dbCommand, "EmpId", DbType.String, objemployee.EmpID);
                    iasdb.AddInParameter(dbCommand, "IsActive", DbType.String, IsActive.ToString()); /*objEmp.IsActive.Trim()*/
                    iasdb.AddOutParameter(dbCommand, "Msg_Out_Status", DbType.String, 500);
                    iasdb.ExecuteNonQuery(dbCommand);
                    message = Convert.ToString(iasdb.GetParameterValue(dbCommand, "Msg_Out_Status"));


                }
                return message;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

    }
}
