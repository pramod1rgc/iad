﻿
using BusinessModel.AssignUser;
using Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;

namespace AID.DataAccess.AssignUser
{
    public class AssignAuditTeamDAL
    {
        private Database iasdb = null;
        public AssignAuditTeamDAL(Database database)
        {
            iasdb = database;
        }


        public async Task<List<GetAssignAuditTeamResponse>> GetAssignAuditTeam(int auditId)
        {
            List<GetAssignAuditTeamResponse> response = new List<GetAssignAuditTeamResponse>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = iasdb.GetStoredProcCommand(IASConstant.SP_GetAuditTeam))
                {
                    iasdb.AddInParameter(dbCommand, "@AuditId", DbType.Int32, auditId);
                    using (IDataReader objReader = iasdb.ExecuteReader(dbCommand))
                    {
                        while (objReader.Read())
                        {
                            GetAssignAuditTeamResponse obj = new GetAssignAuditTeamResponse();
                            obj.EmpId = objReader["EmpID"] != DBNull.Value ? Convert.ToInt32(objReader["EmpID"]) : 0;
                            obj.EmpName = objReader["EmpName"] != DBNull.Value ? Convert.ToString(objReader["EmpName"]) : null;
                            obj.EmpCode = objReader["EmpCode"] != DBNull.Value ? Convert.ToString(objReader["EmpCode"]) : null;
                            obj.DesignationId = objReader["DesignationId"] != DBNull.Value ? Convert.ToInt32(objReader["DesignationId"]) : 0;
                            obj.Email = objReader["Email"] != DBNull.Value ? Convert.ToString(objReader["Email"]) : null;
                            obj.PhoneNo = objReader["PhoneNo"] != DBNull.Value ? Convert.ToString(objReader["PhoneNo"]) : null;
                            obj.MobileNo = objReader["MobileNo"] != DBNull.Value ? Convert.ToString(objReader["MobileNo"]) : null;
                            obj.IsActive = objReader["IsActive"] != DBNull.Value ? Convert.ToBoolean(objReader["IsActive"]) : false;
                            obj.Designation = objReader["Designation"] != DBNull.Value ? Convert.ToString(objReader["Designation"]) : null;
                            obj.UserId = objReader["UserID"] != DBNull.Value ? Convert.ToInt32(objReader["UserID"]) : 0;
                            obj.UserName = objReader["UserName"] != DBNull.Value ? Convert.ToString(objReader["UserName"]) : null;
                            obj.RoleId = objReader["RoleID"] != DBNull.Value ? Convert.ToInt32(objReader["RoleID"]) : 0;
                            obj.RoleUserMapId = objReader["RoleUserMapID"] != DBNull.Value ? Convert.ToInt32(objReader["RoleUserMapID"]) : 0;
                            obj.AuditId = objReader["AuditId"] != DBNull.Value ? Convert.ToInt32(objReader["AuditId"]) : 0;
                            obj.AuditFromDate = objReader["AuditFromDate"] != DBNull.Value ? Convert.ToDateTime(objReader["AuditFromDate"]) : (DateTime?)null;
                            obj.AuditToDate = objReader["AuditToDate"] != DBNull.Value ? Convert.ToDateTime(objReader["AuditToDate"]) : (DateTime?)null;

                            response.Add(obj);
                        }
                    }
                }
            });
            return response;
        }



        public async Task<CheckIfUserAlreadyAssignedToAnotherAuditResponse> CheckIfUserAlreadyAssignedToAnotherAudit(CheckIfUserAlreadyAssignedToAnotherAuditRequest request)
        {
            CheckIfUserAlreadyAssignedToAnotherAuditResponse response = new CheckIfUserAlreadyAssignedToAnotherAuditResponse();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = iasdb.GetStoredProcCommand(IASConstant.SP_CheckIfUserAlreadyAssignedToAnotherAudit))
                {
                    iasdb.AddInParameter(dbCommand, "@roleUserMapId", DbType.Int32, request.RoleUserMapId);
                    iasdb.AddInParameter(dbCommand, "@auditId", DbType.Int32, request.AuditId);
                    iasdb.AddInParameter(dbCommand, "@auditFromDate", DbType.DateTime, request.AuditFromDate);
                    iasdb.AddInParameter(dbCommand, "@auditToDate", DbType.DateTime, request.AuditToDate);
                    using (IDataReader objReader = iasdb.ExecuteReader(dbCommand))
                    {
                        while (objReader.Read())
                        {
                            response.Count = objReader["Count"] != DBNull.Value ? Convert.ToInt32(objReader["Count"]) : 0;
                            response.PaoName = objReader["PaoName"] != DBNull.Value ? Convert.ToString(objReader["PaoName"]) : null;
                            response.AssignedAuditId = objReader["AssignedAuditId"] != DBNull.Value ? Convert.ToInt32(objReader["AssignedAuditId"]) : 0;
                            response.AssignedFromDate = objReader["AssignedFromDate"] != DBNull.Value ? Convert.ToDateTime(objReader["AssignedFromDate"]) : (DateTime?)null;
                            response.AssignedToDate = objReader["AssignedToDate"] != DBNull.Value ? Convert.ToDateTime(objReader["AssignedToDate"]) : (DateTime?)null;
                        }
                    }
                }
            });
            return response;
        }


        public async Task<int> AssignAuditTeam(AssignAuditTeamRequest request)
        {
            int result = 0;
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = iasdb.GetStoredProcCommand(IASConstant.SP_AssignAuditTeam))
                {
                    iasdb.AddInParameter(dbCommand, "@IPAddess", DbType.String, request.IPAddress);
                    iasdb.AddInParameter(dbCommand, "@IPUserId", DbType.Int32, request.IPUserId);
                    iasdb.AddInParameter(dbCommand, "@Password", DbType.String, request.Password);
                    iasdb.AddInParameter(dbCommand, "@IPRoleId", DbType.Int32, request.IPRoleId);
                    iasdb.AddInParameter(dbCommand, "@AuditId", DbType.Int32, request.AuditId);

                    var table = CommonFunction.CreateDataTable(request.AssignUserRole);
                    SqlParameter param = new SqlParameter("@AssignUserRole", table);
                    param.SqlDbType = SqlDbType.Structured;
                    dbCommand.Parameters.Add(param);

                    result = iasdb.ExecuteNonQuery(dbCommand);
                }
            });
            return result;
        }


        public async Task<List<GetAuditsForDropdownResponse>> GetAuditsForDropdown()
        {
            List<GetAuditsForDropdownResponse> response = new List<GetAuditsForDropdownResponse>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = iasdb.GetStoredProcCommand(IASConstant.SP_GetAuditDetailsForDropdown))
                {
                    using (IDataReader objReader = iasdb.ExecuteReader(dbCommand))
                    {
                        while (objReader.Read())
                        {
                            GetAuditsForDropdownResponse obj = new GetAuditsForDropdownResponse();
                            obj.AuditId = objReader["AuditId"] != DBNull.Value ? Convert.ToInt32(objReader["AuditId"]) : 0;
                            obj.Name = objReader["Name"] != DBNull.Value ? Convert.ToString(objReader["Name"]) : null;
                            obj.AuditFromDate = objReader["AuditFromDate"] != DBNull.Value ? Convert.ToDateTime(objReader["AuditFromDate"]) : (DateTime?)null;
                            obj.AuditToDate = objReader["AuditToDate"] != DBNull.Value ? Convert.ToDateTime(objReader["AuditToDate"]) : (DateTime?)null;
                            obj.Quarter = objReader["Quarter"] != DBNull.Value ? Convert.ToString(objReader["Quarter"]) : null;
                            obj.AuditStatusId = objReader["AuditStatusId"] != DBNull.Value ? Convert.ToInt32(objReader["AuditStatusId"]) : 0;
                            obj.AuditStatus = objReader["Status"] != DBNull.Value ? Convert.ToString(objReader["Status"]) : null;
                            obj.RejectionRemark = objReader["RejectionRemark"] != DBNull.Value ? Convert.ToString(objReader["RejectionRemark"]) : null;
                            response.Add(obj);
                        }
                    }
                }
            });
            return response;
        }
    }
}
