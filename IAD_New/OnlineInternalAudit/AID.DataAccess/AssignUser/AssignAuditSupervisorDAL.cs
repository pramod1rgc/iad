﻿
using BusinessModel.AssignUser;
using Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;

namespace AID.DataAccess.AssignUser
{
    public class AssignAuditSupervisorDAL
    {
        private Database iasdb = null;
        public AssignAuditSupervisorDAL(Database database)
        {
            iasdb = database;
        }


        public async Task<List<GetUserRoleEmployeesResponse>> GetUserRoleEmployees(int roleId)
        {
            List<GetUserRoleEmployeesResponse> response = new List<GetUserRoleEmployeesResponse>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = iasdb.GetStoredProcCommand(IASConstant.SP_GetUserRoleEmployees))
                {
                    iasdb.AddInParameter(dbCommand, "@roleId", DbType.Int32, roleId);
                    using (IDataReader objReader = iasdb.ExecuteReader(dbCommand))
                    {
                        while (objReader.Read())
                        {
                            GetUserRoleEmployeesResponse obj = new GetUserRoleEmployeesResponse();
                            obj.EmpId = objReader["EmpID"] != DBNull.Value ? Convert.ToInt32(objReader["EmpID"]) : 0;
                            obj.EmpName = objReader["EmpName"] != DBNull.Value ? Convert.ToString(objReader["EmpName"]) : null;
                            obj.EmpCode = objReader["EmpCode"] != DBNull.Value ? Convert.ToString(objReader["EmpCode"]) : null;
                            obj.DesignationId = objReader["DesignationId"] != DBNull.Value ? Convert.ToInt32(objReader["DesignationId"]) : 0;
                            obj.Email = objReader["Email"] != DBNull.Value ? Convert.ToString(objReader["Email"]) : null;
                            obj.PhoneNo = objReader["PhoneNo"] != DBNull.Value ? Convert.ToString(objReader["PhoneNo"]) : null;
                            obj.MobileNo = objReader["MobileNo"] != DBNull.Value ? Convert.ToString(objReader["MobileNo"]) : null;
                            obj.IsActive = objReader["IsActive"] != DBNull.Value ? Convert.ToBoolean(objReader["IsActive"]) : false;
                            obj.Designation = objReader["Designation"] != DBNull.Value ? Convert.ToString(objReader["Designation"]) : null;
                            obj.UserId = objReader["UserID"] != DBNull.Value ? Convert.ToInt32(objReader["UserID"]) : 0;
                            obj.UserName = objReader["UserName"] != DBNull.Value ? Convert.ToString(objReader["UserName"]) : null;
                            obj.RoleId = objReader["RoleID"] != DBNull.Value ? Convert.ToInt32(objReader["RoleID"]) : 0;
                            obj.RoleUserMapId = objReader["RoleUserMapID"] != DBNull.Value ? Convert.ToInt32(objReader["RoleUserMapID"]) : 0;

                            response.Add(obj);
                        }
                    }
                }
            });
            return response;
        }


        public async Task<int> AssignUserRole(AssignUserRoleRequest request)
        {
            int result = 0;
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = iasdb.GetStoredProcCommand(IASConstant.SP_AssignUserRole))
                {
                    iasdb.AddInParameter(dbCommand, "@IPAddess", DbType.String, request.IPAddress);
                    iasdb.AddInParameter(dbCommand, "@IPUserId", DbType.Int32, request.IPUserId);
                    iasdb.AddInParameter(dbCommand, "@Password", DbType.String, request.Password);
                    iasdb.AddInParameter(dbCommand, "@IPRoleId", DbType.Int32, request.IPRoleId);

                    var table = CommonFunction.CreateDataTable(request.AssignUserRole);
                    SqlParameter param = new SqlParameter("@AssignUserRole", table);
                    param.SqlDbType = SqlDbType.Structured;
                    dbCommand.Parameters.Add(param);

                    result = iasdb.ExecuteNonQuery(dbCommand);
                }
            });
            return result;
        }
    }
}
