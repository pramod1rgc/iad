﻿using BusinessModel.Audits;
using Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Text;
using System.Threading.Tasks;

namespace AID.DataAccess.Audits
{
    public class AuditScheduleDAL
    {
        private Database iasdb = null;
        public AuditScheduleDAL(Database database)
        {
            iasdb = database;
        }


        public async Task<List<GetApprovedScheduledAuditsResponse>> GetApprovedScheduledAudits(int finYearId, string quarter, int roleUserMapId)
        {
            List<GetApprovedScheduledAuditsResponse> response = new List<GetApprovedScheduledAuditsResponse>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = iasdb.GetStoredProcCommand(IASConstant.SP_GetApprovedScheduledAudits))
                {
                    iasdb.AddInParameter(dbCommand, "@finYearId", DbType.Int32, finYearId);
                    iasdb.AddInParameter(dbCommand, "@quarter", DbType.String, quarter);
                    iasdb.AddInParameter(dbCommand, "@roleUserMapId", DbType.Int32, roleUserMapId);
                    using (IDataReader objReader = iasdb.ExecuteReader(dbCommand))
                    {
                        while (objReader.Read())
                        {
                            GetApprovedScheduledAuditsResponse obj = new GetApprovedScheduledAuditsResponse();
                            obj.AuditId = objReader["AuditId"] != DBNull.Value ? Convert.ToInt32(objReader["AuditId"]) : 0;
                            obj.PAOId = objReader["PAOId"] != DBNull.Value ? Convert.ToInt32(objReader["PAOId"]) : 0;
                            obj.FinancialYearId = objReader["FinancialYearId"] != DBNull.Value ? Convert.ToInt32(objReader["FinancialYearId"]) : 0;
                            obj.AuditName = objReader["AuditName"] != DBNull.Value ? Convert.ToString(objReader["AuditName"]) : null;
                            obj.AuditStatusId = objReader["AuditStatusId"] != DBNull.Value ? Convert.ToInt32(objReader["AuditStatusId"]) : 0;
                            obj.AuditFromDate = objReader["AuditFromDate"] != DBNull.Value ? Convert.ToDateTime(objReader["AuditFromDate"]) : (DateTime?)null;
                            obj.AuditToDate = objReader["AuditToDate"] != DBNull.Value ? Convert.ToDateTime(objReader["AuditToDate"]) : (DateTime?)null;
                            obj.Quarter = objReader["Quarter"] != DBNull.Value ? Convert.ToString(objReader["Quarter"]) : null;
                            obj.PAOName = objReader["Name"] != DBNull.Value ? Convert.ToString(objReader["Name"]) : null;
                            obj.PAOCode = objReader["PAOCode"] != DBNull.Value ? Convert.ToString(objReader["PAOCode"]) : null;
                            obj.ControllerCode = objReader["ControllerCode"] != DBNull.Value ? Convert.ToString(objReader["ControllerCode"]) : null;
                            obj.Status = objReader["Status"] != DBNull.Value ? Convert.ToString(objReader["Status"]) : null;

                            response.Add(obj);
                        }
                    }
                }
            });
            return response;
        }
    }
}
