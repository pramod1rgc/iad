﻿using BusinessModel.Audits;
using Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Text;
using System.Threading.Tasks;

namespace AID.DataAccess.Audits
{
    public class SendIntimationMailsDAL
    {
        private Database iasdb = null;
        public SendIntimationMailsDAL(Database database)
        {
            iasdb = database;
        }

        /// <summary>
        ///GetSendIntimationMailsPao 
        /// </summary>
        /// <returns></returns>
        public async Task<List<SendIntimationMailsBM>> GetSendIntimationMailsPao(int fYearid)
        {
            List<SendIntimationMailsBM> response = new List<SendIntimationMailsBM>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = iasdb.GetStoredProcCommand(IASConstant.SP_GetSendIntimationMailsAudit))
                {
                    iasdb.AddInParameter(dbCommand, "fYearid", DbType.Int32, fYearid);
                    using (IDataReader objReader = iasdb.ExecuteReader(dbCommand))
                    {                        
                        while (objReader.Read())
                        {
                            SendIntimationMailsBM obj = new SendIntimationMailsBM();
                            obj.PAOId = objReader["PAOId"] != DBNull.Value ? Convert.ToInt32(objReader["PAOId"]) : 0;
                            obj.PaoName = objReader["PaoName"] != DBNull.Value ? Convert.ToString(objReader["PaoName"]) : null;
                            obj.AuditFromDate = objReader["AuditFromDate"] != DBNull.Value ? Convert.ToDateTime(objReader["AuditFromDate"]):(DateTime?)null;
                            obj.AuditToDate = objReader["AuditToDate"] != DBNull.Value ? Convert.ToDateTime(objReader["AuditToDate"]):(DateTime?)null;
                            obj.Email = objReader["Email"] != DBNull.Value ? Convert.ToString(objReader["Email"]) : null;
                            obj.SerialNo= objReader["PAOId"] != DBNull.Value ? Convert.ToInt32(objReader["PAOId"]) : 0;
                            obj.IntimationStatus = objReader["IntimationStatus"]!= DBNull.Value ? objReader["IntimationStatus"].ToString() :null;
                            obj.AuditTeamStatus = objReader["AuditTeamStatus"] != DBNull.Value ? Convert.ToInt32(objReader["AuditTeamStatus"]) : 0;
                            response.Add(obj);
                        }
                    }
                }
            });
            return response;
        }
        /// <summary>
        /// EmailIntimationStatus
        /// </summary>
        /// <param name="PAOId"></param>
        /// <returns></returns>
        public async Task<int> EmailIntimationStatus(int PAOId)
        {
            int i = 0;
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = iasdb.GetStoredProcCommand(IASConstant.usp_EmailIntimationStatus))
                {              
                    iasdb.AddInParameter(dbCommand, "PAOId", DbType.String, PAOId);              
                    i=iasdb.ExecuteNonQuery(dbCommand);
                }
            });
            return i;
        }
        /// <summary>
        /// SendMail
        /// </summary>
        /// <param name="requestmodel"></param>
        /// <returns></returns>
        public async Task<string> SendMail(SendIntimationMailsBM requestmodel)
        {      
            string message = string.Empty;
            string Password = string.Empty;
            string response = null;
            await Task.Run(() =>
                    {
                        using (DbCommand dbCommand = iasdb.GetStoredProcCommand(IASConstant.Sp_CreateEmp_AssignUser))
                        {
                            iasdb.AddInParameter(dbCommand, "Email", DbType.String, requestmodel.Email);
                            iasdb.AddInParameter(dbCommand, "PAOId", DbType.Int32, requestmodel.PAOId);             
                            iasdb.AddOutParameter(dbCommand, "Msg_Out_Status", DbType.String, 500);
                            iasdb.AddOutParameter(dbCommand, "OutPassword", DbType.String,500);
                            iasdb.ExecuteNonQuery(dbCommand);
                            message = Convert.ToString(iasdb.GetParameterValue(dbCommand, "Msg_Out_Status"));
                            Password = Convert.ToString(iasdb.GetParameterValue(dbCommand, "OutPassword"));
                        }
                    });                                       
            if(message== "Success")
            {
                response = CommonFunction.SendMail(requestmodel.Email, requestmodel.Subject, requestmodel.Remarks, Password);
                if(response== "OnSuccess")
                {
                    EmailIntimationStatus(requestmodel.PAOId);
                }
            }
            else
            {
                response = message;
            }
            return response;
        }

        
    }
}
