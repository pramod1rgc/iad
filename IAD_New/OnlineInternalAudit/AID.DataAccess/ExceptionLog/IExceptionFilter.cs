﻿using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.Text;

namespace AID.DataAccess.ExceptionLog
{
    interface IExceptionFilter
    {
        void OnException(ExceptionContext filterContext);
    }
}
