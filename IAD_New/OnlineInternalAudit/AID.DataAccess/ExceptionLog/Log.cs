﻿using BusinessModel.ExceptionLog;
using Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.IO;
using System.Text;

namespace AID.DataAccess.ExceptionLog
{

    public sealed class Log
    {
        Database epsdatabase = null;
        public Log()
        {
            DatabaseProviderFactory factory = new DatabaseProviderFactory();
            epsdatabase = factory.Create(IASConstant.IASDataBaseConnection);
        }
        private static readonly Lazy<Log> instance = new Lazy<Log>(() => new Log());
        public static Log GetInstance
        {
            get
            {
                return instance.Value;
            }
        }
        public void ExceptionLogFile(ExceptionLogModel fileLog)
        {
            string fileName = string.Format(@"\LogFile\logs\{0}-{1}.log", "Exception", DateTime.Now.ToString("yyyy-MM-dd"));
            string logFilePath = string.Format(@"{0}\{1}", AppDomain.CurrentDomain.BaseDirectory, fileName);
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("----------------------------------------");
            sb.AppendLine(DateTime.Now.ToString());
            sb.AppendLine(fileLog.URL + "|"
                  + fileLog.Logged + "|"
                  + fileLog.Level + "|"
                  + fileLog.Message + "|"
                  + fileLog.Logger + "|"
                  + fileLog.Callsite + "|"
                  + fileLog.Exception + "|"
                  + fileLog.StackTrace);
            using (StreamWriter writer = new StreamWriter(logFilePath, true))
            {
                writer.Write(sb.ToString());
                writer.Flush();
            }
        }


        public int ExceptionLogDataBase(ExceptionLogModel logger)
        {
            int Response = 0;
            using (DbCommand dbCommand = epsdatabase.GetStoredProcCommand("[OIAS].SP_Exception_NLog"))
            {
                epsdatabase.AddInParameter(dbCommand, "@MachineName", DbType.String, logger.MachineName);
                epsdatabase.AddInParameter(dbCommand, "@URL", DbType.String, logger.URL);
                epsdatabase.AddInParameter(dbCommand, "@Port", DbType.String, logger.Port);
                epsdatabase.AddInParameter(dbCommand, "@timeStamp", DbType.DateTime, logger.Logged);
                epsdatabase.AddInParameter(dbCommand, "@Level", DbType.String, logger.Level);
                epsdatabase.AddInParameter(dbCommand, "@Message", DbType.String, logger.Message);
                epsdatabase.AddInParameter(dbCommand, "@Logger", DbType.String, logger.Logger);
                epsdatabase.AddInParameter(dbCommand, "@StackTrace", DbType.String, logger.StackTrace);
                epsdatabase.AddInParameter(dbCommand, "@CallSite", DbType.String, logger.Callsite);
                epsdatabase.AddInParameter(dbCommand, "@Exception", DbType.String, logger.Exception);
                Response = epsdatabase.ExecuteNonQuery(dbCommand);
            }
            return Response;
        }

    }
}
