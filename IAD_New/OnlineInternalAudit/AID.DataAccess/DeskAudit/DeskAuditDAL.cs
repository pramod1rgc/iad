﻿using BusinessModel.DeskAudit;
using Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AID.DataAccess.DeskAudit
{
    public class DeskAuditDAL
    {
        private Database iasdb = null;
        public DeskAuditDAL(Database database)
        {
            iasdb = database;
        }
        /// <summary>
        /// SaveDeskAudit
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>

        public async Task<string> SaveDeskAudit(DeskAuditBM request)
        {
            string result = string.Empty;
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = iasdb.GetStoredProcCommand("[OIAS].InsertDeskAudit"))
                {
                    iasdb.AddInParameter(dbCommand, "@userId", DbType.Int32, request.CreatedbyId);
                    iasdb.AddInParameter(dbCommand, "@auditId", DbType.Int32, request.AuditId);
                    iasdb.AddInParameter(dbCommand, "@IPAddress", DbType.String, CommonFunction.GetIPAddress());
                    if (request.UploadFiles != null && request.UploadFiles.Count > 0)
                    {
                        var table = CommonFunction.CreateDataTable(request.UploadFiles);
                        SqlParameter param = new SqlParameter("@files", table);
                        param.SqlDbType = SqlDbType.Structured;
                        dbCommand.Parameters.Add(param);
                    }
                    try
                    {
                        int i = iasdb.ExecuteNonQuery(dbCommand);
                        if (i > 0)
                        {
                            result = "Record saved successfully!";
                        }
                        else
                        {
                            result = "Record not saved. Contact to administrator!";
                        }
                    }
                    catch (Exception e)
                    {
                    }

                }
            });
            return result;
        }

     /// <summary>
     /// 
     /// </summary>
     /// <param name="objemployee"></param>
     /// <returns></returns>
        public async Task<string> active_deactiveDeskAudit(DeactiveDeskAuditBM _objDeskAudit)
        {
            string message = string.Empty;
            try
            {
                await Task.Run(() =>
                {
                    using (DbCommand dbCommand = iasdb.GetStoredProcCommand("[OIAS].[DeskAuditeActiveAndDeActive_SP]"))
                    {
                        iasdb.AddInParameter(dbCommand, "DocumentName", DbType.String, _objDeskAudit.DocumentName.Trim());
                        iasdb.AddInParameter(dbCommand, "IsActive", DbType.String, _objDeskAudit.isActive.ToString());
                        iasdb.AddInParameter(dbCommand, "@auditId", DbType.Int32, _objDeskAudit.AuditId);
                        iasdb.AddOutParameter(dbCommand, "Msg_Out_Status", DbType.String, 500);
                        iasdb.ExecuteNonQuery(dbCommand);
                        message = Convert.ToString(iasdb.GetParameterValue(dbCommand, "Msg_Out_Status"));
                    }
                });
                return message;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        /// <summary>
        /// getDeskAuditFiles
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task<List<GetDeskAuditUploadFiles>> getDeskAuditFiles(int auditId)
        {

            object[] listTable = new object[2];
            List<GetDeskAuditUploadFiles> response = new List<GetDeskAuditUploadFiles>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = iasdb.GetStoredProcCommand("[OIAS].[GetDeskAuditRecord]"))
                {
                    iasdb.AddInParameter(dbCommand, "@AuditId", DbType.String, auditId);
                    DataSet ds = iasdb.ExecuteDataSet(dbCommand);
                    DataTable dt = ds.Tables[0];
                    DataTable dt2 = ds.Tables[1];
                    listTable[0] = ds.Tables[0];
                    listTable[1] = ds.Tables[1];

                    foreach (DataRow rs in dt.Rows)
                    {
                        var list = new GetDeskAuditUploadFiles();
                        string DocumentName = rs["DocumentName"].ToString();
                        list.DocumentName = DocumentName;
                        list.isActive =Convert.ToBoolean(rs["IsActive"]);
                        list.Children = new List<DeskAuditUploadFilesRequest>();
                        foreach (DataRow rs1 in dt2.Rows)
                        {
                            if (rs["DocumentName"].ToString() == rs1["DocumentName"].ToString())
                            {
                                var obj = new DeskAuditUploadFilesRequest();
                                obj.FileName = rs1["FileName"].ToString();
                                obj.DocumentName = rs1["DocumentName"].ToString();
                                obj.RelativePath = rs1["RelativePath"].ToString();
                                obj.FileType = rs1["FileType"].ToString();
                                list.Children.Add(obj);
                            }
                        }
                        response.Add(list);
                    }

                }
            });

            if (response == null)
                return null;
            else
                return response;
        }

    }
}
