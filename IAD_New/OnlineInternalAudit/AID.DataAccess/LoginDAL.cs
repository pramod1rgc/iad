﻿using BusinessModel;
using Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess
{
    public class LoginDAL
    {
        private Database iasdb = null;
        public LoginDAL(Database database)
        {
            iasdb = database;
        }

        public async Task<List<LoginResponseBM>> Login(string UserName, string Password)
        {
            List<LoginResponseBM> response = new List<LoginResponseBM>();
            string result = string.Empty;
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = iasdb.GetStoredProcCommand(IASConstant.SP_UserLogin))
                {
                    iasdb.AddInParameter(dbCommand, "@UserName", DbType.String, UserName);
                    iasdb.AddInParameter(dbCommand, "@Password", DbType.String, Password);
                    using (IDataReader objReader = iasdb.ExecuteReader(dbCommand))
                    {
                        while (objReader.Read())
                        {
                            LoginResponseBM obj = new LoginResponseBM();
                            obj.UserId = objReader["userId"] != DBNull.Value ? Convert.ToInt32(objReader["userId"]) : 0;
                            obj.RoleId = objReader["roleId"] != DBNull.Value ? Convert.ToInt32(objReader["roleId"]) : 0;
                            obj.RoleName = objReader["roleName"] != DBNull.Value ? Convert.ToString(objReader["roleName"]) : null;
                            obj.RoleDescription = objReader["roleDescription"] != DBNull.Value ? Convert.ToString(objReader["roleDescription"]) : null;
                            obj.RoleUserMapId = objReader["RoleUserMapId"] != DBNull.Value ? Convert.ToInt32(objReader["RoleUserMapId"]) : 0;
                            obj.Username = UserName;
                            obj.Password = Password;

                            response.Add(obj);
                        }
                    }
                }
            });
            return response;
        }
    }
}
