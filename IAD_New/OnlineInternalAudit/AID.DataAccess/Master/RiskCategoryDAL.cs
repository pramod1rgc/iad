﻿using BusinessModel.Master;
using Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Text;
using System.Threading.Tasks;

namespace AID.DataAccess.Master
{
    public class RiskCategoryDAL
    {
        private Database iasdb = null;
        List<CategoryDetailsModel> lstCategoryDetailsModel = null;
        public RiskCategoryDAL(Database database)
        {
            iasdb = database;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="hdnCategoryId"></param>
        /// <param name="MstCategoryDetails"></param>
        /// <returns></returns>
        public async Task<string> SaveRiskCategory(string hdnCategoryId, CategoryDetailsModel MstCategoryDetails)
        {
            try
            {
                string message = string.Empty;
                await Task.Run(() =>
                {
                    using (DbCommand dbCommand = iasdb.GetStoredProcCommand(IASConstant.InsUpdateRiskCategory_SP))
                    {
                        iasdb.AddInParameter(dbCommand, "RiskCategoryID", DbType.Int32, hdnCategoryId);
                        iasdb.AddInParameter(dbCommand, "ParentRiskCategoryID", DbType.String, MstCategoryDetails.Category);
                        iasdb.AddInParameter(dbCommand, "RiskCategoryName", DbType.String, MstCategoryDetails.RiskCategoryName.Trim());
                        iasdb.AddInParameter(dbCommand, "RiskCategoryDesc", DbType.String, MstCategoryDetails.RiskCategoryDesc.Trim());
                        //iasdb.AddInParameter(dbCommand, "IsActive", DbType.String, "1"); /*objEmp.IsActive.Trim()*/
                        iasdb.AddOutParameter(dbCommand, "Msg_Out_Status", DbType.String, 500);
                        iasdb.ExecuteNonQuery(dbCommand);
                        message = Convert.ToString(iasdb.GetParameterValue(dbCommand, "Msg_Out_Status"));


                    }
                });
                return message;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public async Task<List<CategoryDetailsModel>> GetAllCategoryDetails()
        {
            lstCategoryDetailsModel = new List<CategoryDetailsModel>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = iasdb.GetStoredProcCommand(IASConstant.SP_GetCategoryInGride))//"GetRiskCategory_SP"))
                {
                    using (IDataReader rdr = iasdb.ExecuteReader(dbCommand))
                    {
                        while (rdr.Read())
                        {
                            CategoryDetailsModel MstCategoryDetailsModel = new CategoryDetailsModel();
                            MstCategoryDetailsModel.RiskCategoryID = Convert.ToInt32(rdr["RiskCategoryID"]);
                            MstCategoryDetailsModel.RiskCategoryName = rdr["RiskCategoryName"].ToString();
                            MstCategoryDetailsModel.ParentCategoryName = rdr["ParentCategoryName"].ToString();
                            MstCategoryDetailsModel.RiskCategoryDesc = rdr["RiskCategoryDesc"].ToString();
                            MstCategoryDetailsModel.ParentRiskCategoryID= Convert.ToInt32(rdr["ParentRiskCategoryID"].ToString());
                            MstCategoryDetailsModel.IsActive = Convert.ToBoolean(rdr["IsActive"]);
                            lstCategoryDetailsModel.Add(MstCategoryDetailsModel);
                        }
                    }

                }
            });
            return lstCategoryDetailsModel;
        }
        /// <summary>
        /// ActiveDeactive
        /// </summary>
        /// <param name="objemployee"></param>
        /// <returns></returns>
        public async Task<string> ActiveDeactive(CategoryDetailsModel objemployee)
        {
            string message = string.Empty;
            try
            {


                //string IsActive = "";
                //if (objemployee.IsActive == "true")
                //{
                //    IsActive = "1";
                //}
                //else
                //{
                //    IsActive = "0";
                //}
                await Task.Run(() =>
                {
                    using (DbCommand dbCommand = iasdb.GetStoredProcCommand("[OIAS].[Mst_CategoryActiveAndDeActive_SP]"))
                    {
                        iasdb.AddInParameter(dbCommand, "RiskCategoryID", DbType.String, objemployee.RiskCategoryID);
                        iasdb.AddInParameter(dbCommand, "IsActive", DbType.String, objemployee.IsActive.ToString()); /*objEmp.IsActive.Trim()*/
                        iasdb.AddOutParameter(dbCommand, "Msg_Out_Status", DbType.String, 500);
                        iasdb.ExecuteNonQuery(dbCommand);
                        message = Convert.ToString(iasdb.GetParameterValue(dbCommand, "Msg_Out_Status"));


                    }
                });
                return message;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        /// <summary>
        /// BindDropDawnCategory
        /// </summary>
        /// <returns></returns>
        public async Task<List<CategoryDetailsModel>> BindDropDawnCategory()
        {
            lstCategoryDetailsModel = new List<CategoryDetailsModel>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = iasdb.GetStoredProcCommand(IASConstant.SP_GetCategoryDDL))
                {
                    using (IDataReader rdr = iasdb.ExecuteReader(dbCommand))
                    {
                        while (rdr.Read())
                        {
                            CategoryDetailsModel MstCategoryDetailsModel = new CategoryDetailsModel();
                            MstCategoryDetailsModel.RiskCategoryID = Convert.ToInt32(rdr["RiskCategoryID"]);
                            MstCategoryDetailsModel.RiskCategoryName = rdr["RiskCategoryName"].ToString();
                            //MstCategoryDetailsModel.IsActive = Convert.ToBoolean(rdr["IsActive"]);
                            lstCategoryDetailsModel.Add(MstCategoryDetailsModel);
                        }
                    }

                }
            });
            return lstCategoryDetailsModel;
        }
    }
}
