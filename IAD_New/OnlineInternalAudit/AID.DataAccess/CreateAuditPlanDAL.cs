﻿
using BusinessModel.AuditStructure;
using Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;

namespace AID.DataAccess
{
    public class CreateAuditPlanDAL
    {
        List<CreateAuditPlanModel> lstAuditPlan;
        private Database iasdb = null;
        public CreateAuditPlanDAL(Database database)
        {
            iasdb = database;
        }
        /// <summary>
        /// BindFYearDropDown
        /// </summary>
        /// <returns></returns>
        public async Task<List<CreateAuditPlanModel>> BindFYearDropDown()
        {
            lstAuditPlan = new List<CreateAuditPlanModel>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = iasdb.GetStoredProcCommand(IASConstant.FinancialYear_SP))
                {
                    using (IDataReader rdr = iasdb.ExecuteReader(dbCommand))
                    {
                        while (rdr.Read())
                        {

                            CreateAuditPlanModel mstAuditPlan = new CreateAuditPlanModel();
                            mstAuditPlan.FinancialYearId = Convert.ToInt32(rdr["FinancialYearId"]);
                            mstAuditPlan.FinancialYear = rdr["FinancialYear"].ToString();
                            mstAuditPlan.IsActive = rdr["IsActive"].ToString();
                            lstAuditPlan.Add(mstAuditPlan);
                        }
                    }

                }
            });
            return lstAuditPlan;
        }
        public async Task<List<CreateAuditPlanModel>> pendingFinancialForVerifyQuarter(int roleUserMapId)
        {
            lstAuditPlan = new List<CreateAuditPlanModel>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = iasdb.GetStoredProcCommand(IASConstant.GetFinancialForVerifyQuarter_SP))
                {
                    iasdb.AddInParameter(dbCommand, "userId", DbType.Int32, roleUserMapId);
                    using (IDataReader rdr = iasdb.ExecuteReader(dbCommand))
                    {
                        while (rdr.Read())
                        {

                            CreateAuditPlanModel mstAuditPlan = new CreateAuditPlanModel();
                            mstAuditPlan.FinancialYearId = Convert.ToInt32(rdr["FinancialYearId"]);
                            mstAuditPlan.FinancialYear = rdr["FinancialYear"].ToString();
                          
                            lstAuditPlan.Add(mstAuditPlan);
                        }
                    }

                }
            });
            return lstAuditPlan;
        }

        /// <summary>
        /// Get All Controller
        /// </summary>
        /// <returns></returns>
        public async Task<List<CreateAuditPlanModel>> GetController()
        {
            lstAuditPlan = new List<CreateAuditPlanModel>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = iasdb.GetStoredProcCommand(IASConstant.GetAllControler_sp))
                {
                    using (IDataReader rdr = iasdb.ExecuteReader(dbCommand))
                    {
                        while (rdr.Read())
                        {

                            CreateAuditPlanModel mstAuditPlan = new CreateAuditPlanModel();
                            mstAuditPlan.ContollerId = Convert.ToInt32(rdr["ControllerID"]);
                            mstAuditPlan.ControllerCode = rdr["ControllerCode"].ToString();
                            mstAuditPlan.ControllerName = rdr["ControllerName"].ToString();
                            lstAuditPlan.Add(mstAuditPlan);
                        }
                    }

                }
            });
            return lstAuditPlan;
        }

        /// <summary>
        /// GetAllPAO
        /// </summary>
        /// <param name="ControllerID"></param>
        /// <returns></returns>
        public async Task<List<CreateAuditPlanModel>> GetAllPAO(int ControllerID)
        {
            lstAuditPlan = new List<CreateAuditPlanModel>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = iasdb.GetStoredProcCommand(IASConstant.GetAllControlerByPAO_sp))
                {
                    iasdb.AddInParameter(dbCommand, "ControllerCode", DbType.Int32, ControllerID);
                    using (IDataReader rdr = iasdb.ExecuteReader(dbCommand))
                    {
                        while (rdr.Read())
                        {

                            CreateAuditPlanModel mstAuditPlan = new CreateAuditPlanModel();
                            mstAuditPlan.PAOId = Convert.ToInt32(rdr["PAOId"]);
                            mstAuditPlan.PAOName = rdr["PAOName"].ToString();
                            lstAuditPlan.Add(mstAuditPlan);
                        }
                    }

                }
            });
            return lstAuditPlan;
        }
        /// <summary>
        /// AssignAuditByController
        /// </summary>
        /// <param name="PAOID"></param>
        /// <param name="Status"></param>
        /// <param name="currFYear"></param>
        /// <param name="paoreason"></param>
        /// <returns></returns>
        public string AssignAuditByController(int PAOID,string Status,string currFYear,string paoreason)
        {
            string message = string.Empty;
            using (DbCommand dbCommand = iasdb.GetStoredProcCommand(IASConstant.UpdateAuditByPAO_sp))
            {
                iasdb.AddInParameter(dbCommand, "PAOID", DbType.Int32, PAOID);
                iasdb.AddInParameter(dbCommand, "status", DbType.String, Status);
                iasdb.AddInParameter(dbCommand, "currFYear", DbType.String, currFYear);
                iasdb.AddInParameter(dbCommand, "reson", DbType.String, paoreason);
                iasdb.AddOutParameter(dbCommand, "Msg_Out_Status", DbType.String, 500);          
                int i= iasdb.ExecuteNonQuery(dbCommand);
                message = Convert.ToString(iasdb.GetParameterValue(dbCommand, "Msg_Out_Status"));
               
            }
            return message;

        }
        /// <summary>
        /// AssignToSupervisor
        /// </summary>
        /// <param name="currFYear"></param>
        /// <param name="roleUserMapId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public string AssignToSupervisor(string currFYear,int roleUserMapId,int userId)
        {
            string message = string.Empty;
            using (DbCommand dbCommand = iasdb.GetStoredProcCommand(IASConstant.AssignToSupervisorAnnualPlanOther_SP))//"[OIAS].[AssignToSupervisorAnnualPlanOther_SP]"))//IASConstant.AssignToSupervisorAnnualPlan_SP))
            {

                iasdb.AddInParameter(dbCommand, "currFYear", DbType.String, currFYear);
                iasdb.AddInParameter(dbCommand, "forwardedToUserId", DbType.Int32, roleUserMapId);
                iasdb.AddInParameter(dbCommand, "userId", DbType.Int32, userId);
                iasdb.AddInParameter(dbCommand, "ipAddress", DbType.String, CommonFunction.GetIPAddress());
                iasdb.AddOutParameter(dbCommand, "Msg_Out_Status", DbType.String, 500);
                int i = iasdb.ExecuteNonQuery(dbCommand);
                message = Convert.ToString(iasdb.GetParameterValue(dbCommand, "Msg_Out_Status"));

            }
            return message;

        }

        /// <summary>
        /// Getauditlist
        /// </summary>
        /// <param name="FYID"></param>
        /// <returns></returns>
        public async Task<IEnumerable<CreateAuditPlanModel>> Getauditlist(int FYID)
        {
            lstAuditPlan = new List<CreateAuditPlanModel>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = iasdb.GetStoredProcCommand(IASConstant.GetAllAuditList_SP))
                {
                    iasdb.AddInParameter(dbCommand, "FYID", DbType.Int32, FYID);
                    using (IDataReader rdr = iasdb.ExecuteReader(dbCommand))
                    {
                        while (rdr.Read())
                        {

                            CreateAuditPlanModel mstAudit = new CreateAuditPlanModel();
                            mstAudit.AuditId = Convert.ToInt32(rdr["AuditId"]);
                            mstAudit.AuditName = rdr["AuditName"].ToString();
                            mstAudit.FinancialYearId = Convert.ToInt32(rdr["FinancialYearId"]);
                            mstAudit.PAOName = rdr["PaoName"].ToString();
                            mstAudit.ControllerCode = rdr["ControllerCode"].ToString();
                            mstAudit.City = rdr["City"].ToString(); ;


                            mstAudit.Quarter = rdr["Quarter"].ToString();
                            mstAudit.PAOId = Convert.ToInt32(rdr["PAOId"].ToString());
                            mstAudit.PAOCode = rdr["PAOCode"].ToString();
                            mstAudit.AuditFromDate = rdr["AuditFromDate"].ToString();
                            mstAudit.AuditToDate = rdr["AuditToDate"].ToString();
                            mstAudit.AuditIntimationDate = rdr["AuditIntimationDate"].ToString();
                            mstAudit.AuditStatusId = rdr["AuditStatusId"].ToString();
                            mstAudit.AuditStatus = rdr["Status"].ToString();
                            mstAudit.FinancialYear = rdr["FinancialYear"].ToString();
                            mstAudit.AuditReason = rdr["AuditReason"].ToString();
                            mstAudit.RejectionRemark = rdr["RejectionRemark"].ToString();
                            mstAudit.IsActive = rdr["IsActive"].ToString();
                            lstAuditPlan.Add(mstAudit);
                        }
                    }

                }
            });
            return lstAuditPlan;
        }

        //Quartaly Audit
        /// <summary>
        /// BindAuditInGrid
        /// </summary>
        /// <param name="FYID"></param>
        /// <returns></returns>
        public async Task<IEnumerable<CreateAuditPlanModel>> BindAuditInGrid(int FYID)
        {
            lstAuditPlan = new List<CreateAuditPlanModel>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = iasdb.GetStoredProcCommand(IASConstant.GetQuarterAudit_SP))
                {
                    iasdb.AddInParameter(dbCommand, "FYID", DbType.Int32, FYID);
                    using (IDataReader rdr = iasdb.ExecuteReader(dbCommand))
                    {
                        while (rdr.Read())
                        {

                            CreateAuditPlanModel mstAudit = new CreateAuditPlanModel();
                            mstAudit.PAOId = Convert.ToInt32(rdr["PAOId"]);
                            mstAudit.PAOName = rdr["Name"].ToString();
                            mstAudit.PAOCode = rdr["PAOCode"].ToString();
                            mstAudit.QuarterStatus = rdr["QuarterStatus"].ToString();
                            mstAudit.AuditStatus = rdr["AuditStatus"].ToString();
                            mstAudit.RejectionRemark = rdr["RejectionRemark"].ToString();
                            mstAudit.AuditStatusId = rdr["AuditStatusId"].ToString();

                            lstAuditPlan.Add(mstAudit);
                        }
                    }

                }
            });
            return lstAuditPlan;
        }
        //End Of 


        //Update Proc 
        /// <summary>
        /// GetassignPaoAudit
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public async Task<string> GetassignPaoAudit(List<CreateAuditPlanModel> obj)
        {
            //lstAuditPlan = new List<CreateAuditPlanModel>();

            //DataTable dt = new DataTable();
            //DataTable dtunselected = new DataTable();
            //string PaoId = string.Empty;
            //bool IsActive = false;
            //dt.Columns.AddRange(new DataColumn[2] { new DataColumn("PAOId", typeof(int)), new DataColumn("IsActive", typeof(bool)) });
            //for (int i = 0; i < obj.Length; i++)
            //{
            //    PaoId = obj[i];
            //    UserId = mstRole.UserID.ToString();
            //    dt.Rows.Add(UserId, RolId, myIP);
            //}

            //await Task.Run(() =>
            //{
            //    using (DbCommand dbCommand = iasdb.GetStoredProcCommand(""))
            //    {
            //        iasdb.AddInParameter(dbCommand, "FYID", DbType.Int32, 00);
            //        using (IDataReader rdr = iasdb.ExecuteReader(dbCommand))
            //        {
            //            while (rdr.Read())
            //            {

            //                CreateAuditPlanModel mstAudit = new CreateAuditPlanModel();
            //                mstAudit.AuditId = Convert.ToInt32(rdr["AuditId"]);
            //                mstAudit.AuditName = rdr["AuditName"].ToString();
            //                mstAudit.FinancialYearId = Convert.ToInt32(rdr["FinancialYearId"]);
            //                mstAudit.PAOName = rdr["PaoName"].ToString();
            //                mstAudit.ControllerCode = rdr["ControllerCode"].ToString();
            //                mstAudit.City = rdr["City"].ToString(); ;


            //                mstAudit.Quarter = rdr["Quarter"].ToString();
            //                mstAudit.PAOId = Convert.ToInt32(rdr["PAOId"].ToString());
            //                mstAudit.PAOCode = rdr["PAOCode"].ToString();
            //                mstAudit.AuditFromDate = rdr["AuditFromDate"].ToString();
            //                mstAudit.AuditToDate = rdr["AuditToDate"].ToString();
            //                mstAudit.AuditIntimationDate = rdr["AuditIntimationDate"].ToString();
            //                mstAudit.AuditStatusId = rdr["AuditStatusId"].ToString();
            //                mstAudit.AuditStatus = rdr["Status"].ToString();
            //                mstAudit.FinancialYear = rdr["FinancialYear"].ToString();
            //                mstAudit.IsActive = rdr["IsActive"].ToString();
            //                lstAuditPlan.Add(mstAudit);
            //            }
            //        }

            //    }
            //});
            return "";
        }


        //Quartaly Audit
        /// <summary>
        /// SaveQuartalyAudit
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public async Task<string> SaveQuartalyAudit(List<CreateAuditPlanModel> obj)
        {

            string message = string.Empty;
          
       
            DataTable dt = new DataTable();
            dt.Columns.AddRange(new DataColumn[1] { new DataColumn("PAOid", typeof(int)) });
            for (int i = 0; i < obj.Count; i++)
            {                
                dt.Rows.Add(obj[i].PAOId);
            }
            if (dt.Rows.Count > 0)
            {
                using (DbCommand dbCommand = iasdb.GetStoredProcCommand(IASConstant.AssignAuditQuartaly_SP))
                {
                    iasdb.AddInParameter(dbCommand, "Quarter", DbType.String, obj[0].Quarter);
                    iasdb.AddInParameter(dbCommand, "FinancialYearId", DbType.String, obj[0].FinancialYearId);
                    SqlParameter tblpara = new SqlParameter("@tblAuditQuartaly", dt);
                    tblpara.SqlDbType = SqlDbType.Structured;
                    dbCommand.Parameters.Add(tblpara);
                    int i = iasdb.ExecuteNonQuery(dbCommand);
                    if (i > 0)
                    {
                        message = "Quarterly Plan assigned successfully";
                    }

                }
            }

            return message;

        }

        //Quartaly Audit
        /// <summary>
        /// SaveQuartalyPlanAudit
        /// </summary>
        /// <param name="objquarterpaoplan"></param>
        /// <param name="financialYearId"></param>
        /// <returns></returns>
        public async Task<string> SaveQuartalyPlanAudit(List<object> objquarterpaoplan,string financialYearId)
        {

            string message = string.Empty;


            DataTable dt = new DataTable();
            dt.Columns.AddRange(new DataColumn[3] { new DataColumn("PAOid", typeof(int)), new DataColumn("Quarter", typeof(string)), new DataColumn("FinancialYearId", typeof(string)) });
            for (int i = 0; i < objquarterpaoplan.Count; i++)
            {
                string arrpaoQuarte = objquarterpaoplan[i].ToString().Replace("rd_", "");

                string[] Arrpaoid = arrpaoQuarte.Split('_');
                string paoID = Arrpaoid[0];
                string Quarter = Arrpaoid[1];

                dt.Rows.Add(paoID, Quarter, financialYearId);
            }
            if (dt.Rows.Count > 0)
            {
                using (DbCommand dbCommand = iasdb.GetStoredProcCommand(IASConstant.AssignAuditQuartaly_SP))
                {
                    //iasdb.AddInParameter(dbCommand, "Quarter", DbType.String, obj[0].Quarter);
                    //iasdb.AddInParameter(dbCommand, "FinancialYearId", DbType.String, financialYearId);
                    SqlParameter tblpara = new SqlParameter("@tblAuditQuartaly", dt);
                    tblpara.SqlDbType = SqlDbType.Structured;
                    dbCommand.Parameters.Add(tblpara);
                    int i = iasdb.ExecuteNonQuery(dbCommand);
                    if (i > 0)
                    {
                        message = "Quarterly Plan assigned successfully";
                    }

                }
            }

            return message;

        }
        /// <summary>
        /// AssignQuarterlyPlanToSupervisor
        /// </summary>
        /// <param name="currFYear"></param>
        /// <param name="roleUserMapId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public string AssignQuarterlyPlanToSupervisor(string currFYear, int roleUserMapId, int userId)
        {
            string message = string.Empty;
            using (DbCommand dbCommand = iasdb.GetStoredProcCommand(IASConstant.QuarterlyForwardedtoSupervisor_SP))//"[OIAS].[QuarterlyForwardedtoSupervisor_For_Test_SP]"))//
            {

                iasdb.AddInParameter(dbCommand, "currFYear", DbType.String, currFYear);
                iasdb.AddInParameter(dbCommand, "forwardedToUserId", DbType.Int32, roleUserMapId);
                iasdb.AddInParameter(dbCommand, "userId", DbType.Int32, userId);
                iasdb.AddInParameter(dbCommand, "ipAddress", DbType.String, CommonFunction.GetIPAddress());
                iasdb.AddOutParameter(dbCommand, "Msg_Out_Status", DbType.String, 500);
                int i = iasdb.ExecuteNonQuery(dbCommand);
                message = Convert.ToString(iasdb.GetParameterValue(dbCommand, "Msg_Out_Status"));

            }
            return message;

        }
        /// <summary>
        /// BindSupervisorDropDown
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task<List<CreateAuditPlanModel>> BindSupervisorDropDown(int userId)
        {
            lstAuditPlan = new List<CreateAuditPlanModel>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = iasdb.GetStoredProcCommand(IASConstant.GetAuditSupervisors))//"OIAS.GetAuditSupervisors"))
                {
                    iasdb.AddInParameter(dbCommand, "userID", DbType.String, userId);
                    using (IDataReader rdr = iasdb.ExecuteReader(dbCommand))
                    {
                        while (rdr.Read())
                        {

                            CreateAuditPlanModel mstAuditPlan = new CreateAuditPlanModel();
                            mstAuditPlan.RoleUserMapID = Convert.ToInt32(rdr["RoleUserMapID"]);
                            mstAuditPlan.UserID = Convert.ToInt32(rdr["UserID"]);
                            mstAuditPlan.RoleID = Convert.ToInt32(rdr["RoleID"]);
                            mstAuditPlan.UserName = rdr["UserName"].ToString();
                            mstAuditPlan.EmpName = rdr["EmpName"].ToString();
                            lstAuditPlan.Add(mstAuditPlan);
                        }
                    }

                }
            });
            return lstAuditPlan;
        }

    }
}
