﻿using BusinessModel.AuditProgramme;
using Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Text;
using System.Threading.Tasks;

namespace AID.DataAccess.AuditProgramme
{
    public class AuditProgrammeDAL
    {
        private Database iasdb = null;
        public AuditProgrammeDAL(Database database)
        {
            iasdb = database;
        }


        public async Task<List<GetAuditProgrammesResponse>> GetAuditProgrammes(int finYear)
        {
            List<GetAuditProgrammesResponse> response = new List<GetAuditProgrammesResponse>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = iasdb.GetStoredProcCommand(IASConstant.SP_GetAuditsForAuditProgramme))
                {
                    iasdb.AddInParameter(dbCommand, "@finYear", DbType.Int32, finYear);
                    using (IDataReader objReader = iasdb.ExecuteReader(dbCommand))
                    {
                        while (objReader.Read())
                        {
                            GetAuditProgrammesResponse obj = new GetAuditProgrammesResponse();
                            obj.AuditId = objReader["AuditId"] != DBNull.Value ? Convert.ToInt32(objReader["AuditId"]) : 0;
                            obj.PaoId = objReader["PAOId"] != DBNull.Value ? Convert.ToInt32(objReader["PAOId"]) : 0;
                            obj.Name = objReader["Name"] != DBNull.Value ? Convert.ToString(objReader["Name"]) : null;
                            obj.AuditFromDate = objReader["AuditFromDate"] != DBNull.Value ? Convert.ToDateTime(objReader["AuditFromDate"]) : (DateTime?)null;
                            obj.AuditToDate = objReader["AuditToDate"] != DBNull.Value ? Convert.ToDateTime(objReader["AuditToDate"]) : (DateTime?)null;
                            obj.EmpName = objReader["EmpName"] != DBNull.Value ? Convert.ToString(objReader["EmpName"]) : null;
                            obj.DesignationName = objReader["DesignationName"] != DBNull.Value ? Convert.ToString(objReader["DesignationName"]) : null;

                            response.Add(obj);
                        }
                    }
                }
            });
            return response;
        }
    }
}
