﻿using BusinessModel.AuditObservation;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;
using Common;


namespace AID.DataAccess.AuditObservation
{
    public class AuditExitConferenceDAL
    {
        private Database iasdb = null;
        public AuditExitConferenceDAL(Database database)
        {
            iasdb = database;
        }


        public async Task<List<GetAuditExitConferenceResponse>> GetAuditExitConference(int auditId)
        {
            List<GetAuditExitConferenceResponse> response = new List<GetAuditExitConferenceResponse>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = iasdb.GetStoredProcCommand(IASConstant.SP_GetAuditExitConference))
                {
                    iasdb.AddInParameter(dbCommand, "@auditId", DbType.Int32, auditId);
                    using (IDataReader objReader = iasdb.ExecuteReader(dbCommand))
                    {
                        while (objReader.Read())
                        {
                            GetAuditExitConferenceResponse obj = new GetAuditExitConferenceResponse();
                            obj.AuditId = objReader["AuditId"] != DBNull.Value ? Convert.ToInt32(objReader["AuditId"]) : 0;
                            obj.ExitConferenceId = objReader["ExitConferenceId"] != DBNull.Value ? Convert.ToInt32(objReader["ExitConferenceId"]) : 0;
                            obj.ConferenceDate = objReader["ConferenceDate"] != DBNull.Value ? Convert.ToDateTime(objReader["ConferenceDate"]) : (DateTime?)null;
                            obj.Agenda = objReader["Agenda"] != DBNull.Value ? Convert.ToString(objReader["Agenda"]) : null;
                            obj.Decision = objReader["Decision"] != DBNull.Value ? Convert.ToString(objReader["Decision"]) : null;
                            obj.ExitConferenceParticipantId = objReader["ExitConferenceParticipantId"] != DBNull.Value ? Convert.ToInt32(objReader["ExitConferenceParticipantId"]) : 0;
                            obj.AuditUserMapId = objReader["AuditUserMapId"] != DBNull.Value ? Convert.ToInt32(objReader["AuditUserMapId"]) : 0;
                            obj.EmpID = objReader["EmpID"] != DBNull.Value ? Convert.ToInt32(objReader["EmpID"]) : 0;
                            obj.EmpName = objReader["EmpName"] != DBNull.Value ? Convert.ToString(objReader["EmpName"]) : null;
                            obj.DesignationId = objReader["DesignationId"] != DBNull.Value ? Convert.ToInt32(objReader["DesignationId"]) : 0;
                            obj.DesignationName = objReader["DesignationName"] != DBNull.Value ? Convert.ToString(objReader["DesignationName"]) : null;
                            obj.RoleId = objReader["RoleID"] != DBNull.Value ? Convert.ToInt32(objReader["RoleID"]) : 0;
                            obj.StatusId = objReader["StatusId"] != DBNull.Value ? Convert.ToInt32(objReader["StatusId"]) : 0;
                            obj.Status = objReader["Status"] != DBNull.Value ? Convert.ToString(objReader["Status"]) : null;
                            obj.RejectionRemark = objReader["RejectionRemark"] != DBNull.Value ? Convert.ToString(objReader["RejectionRemark"]) : null;

                            response.Add(obj);
                        }
                    }
                }
            });
            return response;
        }


        public async Task<int> UpdateAuditExitConference(UpdateAuditExitConferenceRequest request)
        {
            int result = 0;
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = iasdb.GetStoredProcCommand(IASConstant.SP_UpdateAuditExitConference))
                {
                    iasdb.AddInParameter(dbCommand, "@exitConferenceId", DbType.Int32, request.ExitConferenceId);
                    iasdb.AddInParameter(dbCommand, "@auditId", DbType.Int32, request.AuditId);
                    iasdb.AddInParameter(dbCommand, "@conferenceDate", DbType.DateTime, request.ConferenceDate);
                    iasdb.AddInParameter(dbCommand, "@agenda", DbType.String, request.Agenda);
                    iasdb.AddInParameter(dbCommand, "@decision", DbType.String, request.Decision);
                    iasdb.AddInParameter(dbCommand, "@userId", DbType.Int32, request.UserId);
                    iasdb.AddInParameter(dbCommand, "@userIPAddress", DbType.String, request.UserIPAddress);

                    var table = CommonFunction.CreateDataTable(request.Participants);
                    SqlParameter param = new SqlParameter("@participants", table);
                    param.SqlDbType = SqlDbType.Structured;
                    dbCommand.Parameters.Add(param);

                    result = iasdb.ExecuteNonQuery(dbCommand);
                }
            });
            return result;
        }

        public async Task<int> UpdateExitConferenceStatus(UpdateExitConferenceStatusRequest request)
        {
            int result = 0;
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = iasdb.GetStoredProcCommand(IASConstant.SP_UpdateExitConferenceStatus))
                {
                    iasdb.AddInParameter(dbCommand, "@statusId", DbType.Int32, request.StatusId);
                    iasdb.AddInParameter(dbCommand, "@exitConferenceId", DbType.Int32, request.ExitConferenceId);
                    iasdb.AddInParameter(dbCommand, "@rejectionRemark", DbType.String, request.RejectionRemark);
                    iasdb.AddInParameter(dbCommand, "@userId", DbType.Int32, request.UserId);
                    iasdb.AddInParameter(dbCommand, "@ipAddress", DbType.String, request.IpAddress);

                    result = iasdb.ExecuteNonQuery(dbCommand);
                }
            });
            return result;
        }
    }
}
