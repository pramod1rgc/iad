﻿
using BusinessModel.AuditObservation;
using Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Dynamic;
using System.Text;
using System.Threading.Tasks;

namespace AID.DataAccess.AuditObservation
{
    public class AuditParaDAL
    {
        private Database iasdb = null;
        public AuditParaDAL(Database database)
        {
            iasdb = database;
        }

        public async Task<List<GetAuditParaResponse>> GetAuditPara(int auditId, int pageSize, int pageNumber, string searchTerm)
        {
            List<GetAuditParaResponse> response = new List<GetAuditParaResponse>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = iasdb.GetStoredProcCommand(IASConstant.SP_GetAuditParas))
                {
                    iasdb.AddInParameter(dbCommand, "@auditId", DbType.Int32, auditId);
                    iasdb.AddInParameter(dbCommand, "@pageSize", DbType.Int32, pageSize);
                    iasdb.AddInParameter(dbCommand, "@pageNumber", DbType.Int32, pageNumber);
                    iasdb.AddInParameter(dbCommand, "@searchTerm", DbType.String, String.IsNullOrEmpty(searchTerm) ? string.Empty : searchTerm.Trim());
                    using (IDataReader objReader = iasdb.ExecuteReader(dbCommand))
                    {
                        while (objReader.Read())
                        {
                            GetAuditParaResponse obj = new GetAuditParaResponse();
                            obj.ParaId = objReader["ParaId"] != DBNull.Value ? Convert.ToInt32(objReader["ParaId"]) : 0;
                            obj.AuditId = objReader["AuditId"] != DBNull.Value ? Convert.ToInt32(objReader["AuditId"]) : 0;
                            obj.Title = objReader["Title"] != DBNull.Value ? Convert.ToString(objReader["Title"]) : null;
                            obj.Criteria = objReader["Criteria"] != DBNull.Value ? Convert.ToString(objReader["Criteria"]) : null;
                            obj.Conditions = objReader["Conditions"] != DBNull.Value ? Convert.ToString(objReader["Conditions"]) : null;
                            obj.Consequences = objReader["Consequences"] != DBNull.Value ? Convert.ToString(objReader["Consequences"]) : null;
                            obj.Causes = objReader["Causes"] != DBNull.Value ? Convert.ToString(objReader["Causes"]) : null;
                            obj.CorrectiveActions = objReader["CorrectiveActions"] != DBNull.Value ? Convert.ToString(objReader["CorrectiveActions"]) : null;
                            obj.Severity = objReader["Severity"] != DBNull.Value ? Convert.ToString(objReader["Severity"]) : null;
                            obj.RiskCategoryId = objReader["RiskCategoryId"] != DBNull.Value ? Convert.ToInt32(objReader["RiskCategoryId"]) : 0;
                            obj.RiskCategoryName = objReader["RiskCategoryName"] != DBNull.Value ? Convert.ToString(objReader["RiskCategoryName"]) : null;
                            obj.RiskCategoryDesc = objReader["RiskCategoryDesc"] != DBNull.Value ? Convert.ToString(objReader["RiskCategoryDesc"]) : null;
                            obj.StatusId = objReader["StatusId"] != DBNull.Value ? Convert.ToInt32(objReader["StatusId"]) : 0;
                            obj.Status = objReader["Status"] != DBNull.Value ? Convert.ToString(objReader["Status"]) : null;
                            obj.AuditeeStatusId = objReader["AuditeeStatusId"] != DBNull.Value ? Convert.ToInt32(objReader["AuditeeStatusId"]) : 0;
                            obj.AuditeeStatus = objReader["AuditeeStatus"] != DBNull.Value ? Convert.ToString(objReader["AuditeeStatus"]) : null;
                            obj.CreatedOn = objReader["CreatedOn"] != DBNull.Value ? Convert.ToDateTime(objReader["CreatedOn"]) : (DateTime?)null;
                            obj.CommentId = objReader["CommentId"] != DBNull.Value ? Convert.ToInt32(objReader["CommentId"]) : 0;
                            obj.TotalCount = objReader["TotalCount"] != DBNull.Value ? Convert.ToInt32(objReader["TotalCount"]) : 0;

                            response.Add(obj);
                        }
                    }
                }
            });
            return response;
        }


        public async Task<int> UpdateAuditPara(UpdateAuditParaRequest request)
        {
            int result = 0;
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = iasdb.GetStoredProcCommand(IASConstant.SP_UpdateAuditPara))
                {
                    iasdb.AddInParameter(dbCommand, "@auditId", DbType.Int32, request.AuditId);
                    iasdb.AddInParameter(dbCommand, "@paraId", DbType.Int32, request.ParaId);
                    iasdb.AddInParameter(dbCommand, "@title", DbType.String, request.Title);
                    iasdb.AddInParameter(dbCommand, "@criteria", DbType.String, request.Criteria);
                    iasdb.AddInParameter(dbCommand, "@conditions", DbType.String, request.Conditions);
                    iasdb.AddInParameter(dbCommand, "@consequences", DbType.String, request.Consequences);
                    iasdb.AddInParameter(dbCommand, "@causes", DbType.String, request.Causes);
                    iasdb.AddInParameter(dbCommand, "@correctiveActions", DbType.String, request.CorrectiveActions);
                    iasdb.AddInParameter(dbCommand, "@severity", DbType.String, request.Severity);
                    iasdb.AddInParameter(dbCommand, "@riskCategoryId", DbType.Int32, request.RiskCategoryId);
                    iasdb.AddInParameter(dbCommand, "@userId", DbType.Int32, request.UserId);
                    iasdb.AddInParameter(dbCommand, "@IPAddress", DbType.String, request.IPAddress);

                    if (request.UploadFiles != null && request.UploadFiles.Count > 0)
                    {
                        var table = CommonFunction.CreateDataTable(request.UploadFiles);
                        SqlParameter param = new SqlParameter("@evidences", table);
                        param.SqlDbType = SqlDbType.Structured;
                        dbCommand.Parameters.Add(param);
                    }

                    if (request.ParaFinancialImplications != null && request.ParaFinancialImplications.Count > 0)
                    {
                        var table = CommonFunction.CreateDataTable(request.ParaFinancialImplications);
                        SqlParameter param = new SqlParameter("@paraFinancialImplications", table);
                        param.SqlDbType = SqlDbType.Structured;
                        dbCommand.Parameters.Add(param);
                    }

                    result = iasdb.ExecuteNonQuery(dbCommand);
                }
            });
            return result;
        }


        public async Task<List<GetAuditParaDetails>> GetAuditParaDetails(int paraId)
        {
            List<GetAuditParaDetails> response = new List<GetAuditParaDetails>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = iasdb.GetStoredProcCommand(IASConstant.SP_GetAuditParaDetails))
                {
                    iasdb.AddInParameter(dbCommand, "@paraId", DbType.Int32, paraId);
                    using (IDataReader objReader = iasdb.ExecuteReader(dbCommand))
                    {
                        while (objReader.Read())
                        {
                            GetAuditParaDetails obj = new GetAuditParaDetails();
                            obj.ParaId = objReader["ParaId"] != DBNull.Value ? Convert.ToInt32(objReader["ParaId"]) : 0;
                            obj.AuditId = objReader["AuditId"] != DBNull.Value ? Convert.ToInt32(objReader["AuditId"]) : 0;
                            obj.Title = objReader["Title"] != DBNull.Value ? Convert.ToString(objReader["Title"]) : null;
                            obj.Criteria = objReader["Criteria"] != DBNull.Value ? Convert.ToString(objReader["Criteria"]) : null;
                            obj.Conditions = objReader["Conditions"] != DBNull.Value ? Convert.ToString(objReader["Conditions"]) : null;
                            obj.Consequences = objReader["Consequences"] != DBNull.Value ? Convert.ToString(objReader["Consequences"]) : null;
                            obj.Causes = objReader["Causes"] != DBNull.Value ? Convert.ToString(objReader["Causes"]) : null;
                            obj.CorrectiveActions = objReader["CorrectiveActions"] != DBNull.Value ? Convert.ToString(objReader["CorrectiveActions"]) : null;
                            obj.Severity = objReader["Severity"] != DBNull.Value ? Convert.ToString(objReader["Severity"]) : null;
                            obj.RiskCategoryId = objReader["RiskCategoryId"] != DBNull.Value ? Convert.ToInt32(objReader["RiskCategoryId"]) : 0;
                            obj.RiskCategoryDesc = objReader["RiskCategoryDesc"] != DBNull.Value ? Convert.ToString(objReader["RiskCategoryDesc"]) : null;
                            obj.CreatedOn = objReader["CreatedOn"] != DBNull.Value ? Convert.ToDateTime(objReader["CreatedOn"]) : (DateTime?)null;
                            obj.ParaFileId = objReader["ParaFileId"] != DBNull.Value ? Convert.ToInt16(objReader["ParaFileId"]) : 0;
                            obj.FileId = objReader["FileId"] != DBNull.Value ? Convert.ToInt16(objReader["FileId"]) : 0;
                            obj.FileName = objReader["FileName"] != DBNull.Value ? Convert.ToString(objReader["FileName"]) : null;
                            obj.FileSize = objReader["FileSize"] != DBNull.Value ? Convert.ToString(objReader["FileSize"]) : null;
                            obj.FileType = objReader["FileType"] != DBNull.Value ? Convert.ToString(objReader["FileType"]) : null;
                            obj.RelativePath = objReader["RelativePath"] != DBNull.Value ? Convert.ToString(objReader["RelativePath"]) : null;
                            obj.FileUrl = objReader["FileUrl"] != DBNull.Value ? Convert.ToString(objReader["FileUrl"]) : null;
                            obj.RejectionRemark = objReader["RejectionRemark"] != DBNull.Value ? Convert.ToString(objReader["RejectionRemark"]) : null;
                            obj.StatusId = objReader["StatusId"] != DBNull.Value ? Convert.ToInt32(objReader["StatusId"]) : 0;
                            obj.AuditeeStatusId = objReader["AuditeeStatusId"] != DBNull.Value ? Convert.ToInt32(objReader["AuditeeStatusId"]) : 0;

                            response.Add(obj);
                        }
                    }
                }
            });
            return response;
        }

        public async Task<int> DeleteAuditPara(DeleteAuditParaRequest request)
        {
            int result = 0;
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = iasdb.GetStoredProcCommand(IASConstant.SP_DeleteAuditPara))
                {
                    iasdb.AddInParameter(dbCommand, "@paraId", DbType.Int32, request.ParaId);
                    iasdb.AddInParameter(dbCommand, "@userId", DbType.Int32, request.UserId);
                    iasdb.AddInParameter(dbCommand, "@IPAddress", DbType.String, request.IPAddress);

                    result = iasdb.ExecuteNonQuery(dbCommand);
                }
            });
            return result;
        }

        public async Task<int> PostParaCommentByAuditor(PostParaCommentByAuditorRequest request)
        {
            int result = 0;
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = iasdb.GetStoredProcCommand(IASConstant.SP_PostParaCommentByAuditor))
                {
                    iasdb.AddInParameter(dbCommand, "@auditId", DbType.Int32, request.AuditId);
                    iasdb.AddInParameter(dbCommand, "@paraId", DbType.Int32, request.ParaId);
                    iasdb.AddInParameter(dbCommand, "@comment", DbType.String, request.Comment);
                    iasdb.AddInParameter(dbCommand, "@userId", DbType.Int32, request.UserId);
                    iasdb.AddInParameter(dbCommand, "@IPAddress", DbType.String, request.IPAddress);

                    if (request.Files != null && request.Files.Count > 0)
                    {
                        var table = CommonFunction.CreateDataTable(request.Files);
                        SqlParameter param = new SqlParameter("@files", table);
                        param.SqlDbType = SqlDbType.Structured;
                        dbCommand.Parameters.Add(param);
                    }

                    result = iasdb.ExecuteNonQuery(dbCommand);
                }
            });
            return result;
        }


        public async Task<List<GetParaComment>> GetParaComments(int paraId)
        {
            List<GetParaComment> response = new List<GetParaComment>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = iasdb.GetStoredProcCommand(IASConstant.SP_GetParaComments))
                {
                    iasdb.AddInParameter(dbCommand, "@paraId", DbType.Int32, paraId);
                    using (IDataReader objReader = iasdb.ExecuteReader(dbCommand))
                    {
                        while (objReader.Read())
                        {
                            GetParaComment obj = new GetParaComment();
                            obj.CommentId = objReader["CommentId"] != DBNull.Value ? Convert.ToInt32(objReader["CommentId"]) : 0;
                            obj.ParaId = objReader["ParaId"] != DBNull.Value ? Convert.ToInt32(objReader["ParaId"]) : 0;
                            obj.AuditUserMapId = objReader["AuditUserMapId"] != DBNull.Value ? Convert.ToInt32(objReader["AuditUserMapId"]) : 0;
                            obj.Comment = objReader["Comment"] != DBNull.Value ? Convert.ToString(objReader["Comment"]) : null;
                            obj.CreatedOn = objReader["CreatedOn"] != DBNull.Value ? Convert.ToDateTime(objReader["CreatedOn"]) : (DateTime?)null;
                            obj.CommentFileId = objReader["CommentFileId"] != DBNull.Value ? Convert.ToInt32(objReader["CommentFileId"]) : 0;
                            obj.FileId = objReader["FileId"] != DBNull.Value ? Convert.ToInt32(objReader["FileId"]) : 0;
                            obj.FileName = objReader["FileName"] != DBNull.Value ? Convert.ToString(objReader["FileName"]) : null;
                            obj.FileSize = objReader["FileSize"] != DBNull.Value ? Convert.ToString(objReader["FileSize"]) : null;
                            obj.FileType = objReader["FileType"] != DBNull.Value ? Convert.ToString(objReader["FileType"]) : null;
                            obj.RelativePath = objReader["RelativePath"] != DBNull.Value ? Convert.ToString(objReader["RelativePath"]) : null;
                            obj.FileUrl = objReader["FileUrl"] != DBNull.Value ? Convert.ToString(objReader["FileUrl"]) : null;
                            obj.RoleId = objReader["RoleID"] != DBNull.Value ? Convert.ToInt32(objReader["RoleID"]) : 0;
                            obj.RoleName = objReader["RoleName"] != DBNull.Value ? Convert.ToString(objReader["RoleName"]) : null;
                            obj.UserId = objReader["UserID"] != DBNull.Value ? Convert.ToInt32(objReader["UserID"]) : 0;
                            obj.EmpId = objReader["EmpID"] != DBNull.Value ? Convert.ToInt32(objReader["EmpID"]) : 0;
                            obj.EmpName = objReader["EmpName"] != DBNull.Value ? Convert.ToString(objReader["EmpName"]) : null;

                            response.Add(obj);
                        }
                    }
                }
            });
            return response;
        }


        public async Task<int> UpdateAuditParaStatus(AuditParaStatusRequest request)
        {
            int result = 0;
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = iasdb.GetStoredProcCommand(IASConstant.SP_UpdateAuditParaStatus))
                {
                    iasdb.AddInParameter(dbCommand, "@auditId", DbType.Int32, request.AuditId);
                    iasdb.AddInParameter(dbCommand, "@paraId", DbType.Int32, request.ParaId);
                    iasdb.AddInParameter(dbCommand, "@statusId", DbType.Int32, request.StatusId);
                    iasdb.AddInParameter(dbCommand, "@rejectionRemark", DbType.String, request.RejectionRemark);

                    result = iasdb.ExecuteNonQuery(dbCommand);
                }
            });
            return result;
        }

        public async Task<int> UpdateAuditComplianceStatus(AuditParaStatusRequest request)
        {
            int result = 0;
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = iasdb.GetStoredProcCommand(IASConstant.SP_UpdateAuditComplianceStatus))
                {
                    iasdb.AddInParameter(dbCommand, "@auditId", DbType.Int32, request.AuditId);
                    iasdb.AddInParameter(dbCommand, "@paraId", DbType.Int32, request.ParaId);
                    iasdb.AddInParameter(dbCommand, "@statusId", DbType.Int32, request.StatusId);
                    iasdb.AddInParameter(dbCommand, "@rejectionRemark", DbType.String, request.RejectionRemark);

                    result = iasdb.ExecuteNonQuery(dbCommand);
                }
            });
            return result;
        }

        public async Task<List<GetAllFinancialImplicationCasesResponse>> GetAllFinancialImplicationCases()
        {
            List<GetAllFinancialImplicationCasesResponse> response = new List<GetAllFinancialImplicationCasesResponse>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = iasdb.GetStoredProcCommand(IASConstant.SP_GetAllFinancialImplicationCases))
                {
                    using (IDataReader objReader = iasdb.ExecuteReader(dbCommand))
                    {
                        while (objReader.Read())
                        {
                            GetAllFinancialImplicationCasesResponse obj = new GetAllFinancialImplicationCasesResponse();
                            obj.FinancialImplicationCaseId = objReader["FinancialImplicationCaseId"] != DBNull.Value ? Convert.ToInt32(objReader["FinancialImplicationCaseId"]) : 0;
                            obj.FinancialImplicationCase = objReader["FinancialImplicationCase"] != DBNull.Value ? Convert.ToString(objReader["FinancialImplicationCase"]) : null;

                            response.Add(obj);
                        }
                    }
                }
            });
            return response;
        }

        public async Task<List<GetAuditParaFinancialImplicationCasesResponse>> GetAuditParaFinancialImplicationCases(int paraId)
        {
            List<GetAuditParaFinancialImplicationCasesResponse> response = new List<GetAuditParaFinancialImplicationCasesResponse>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = iasdb.GetStoredProcCommand(IASConstant.SP_GetAuditParaFinancialImplicationCases))
                {
                    iasdb.AddInParameter(dbCommand, "@paraId", DbType.Int32, paraId);
                    using (IDataReader objReader = iasdb.ExecuteReader(dbCommand))
                    {
                        while (objReader.Read())
                        {
                            GetAuditParaFinancialImplicationCasesResponse obj = new GetAuditParaFinancialImplicationCasesResponse();
                            obj.AuditParaFinancialImplicationMappingId = objReader["AuditParaFinancialImplicationMappingId"] != DBNull.Value ? Convert.ToInt32(objReader["AuditParaFinancialImplicationMappingId"]) : 0;
                            obj.ParaId = objReader["ParaId"] != DBNull.Value ? Convert.ToInt32(objReader["ParaId"]) : 0;
                            obj.FinancialImplicationCaseId = objReader["FinancialImplicationCaseId"] != DBNull.Value ? Convert.ToInt32(objReader["FinancialImplicationCaseId"]) : 0;
                            obj.FinancialImplicationCase = objReader["FinancialImplicationCase"] != DBNull.Value ? Convert.ToString(objReader["FinancialImplicationCase"]) : null;
                            obj.Amount = objReader["Amount"] != DBNull.Value ? Convert.ToDouble(objReader["Amount"]) : 0;
                            obj.Remarks = objReader["Remarks"] != DBNull.Value ? Convert.ToString(objReader["Remarks"]) : null;
                            obj.IsActive = objReader["IsActive"] != DBNull.Value ? Convert.ToBoolean(objReader["IsActive"]) : true;
                            obj.IsDeleted = objReader["IsDeleted"] != DBNull.Value ? Convert.ToBoolean(objReader["IsDeleted"]) : false;
                            obj.CreatedOn = objReader["CreatedOn"] != DBNull.Value ? Convert.ToDateTime(objReader["CreatedOn"]) : (DateTime?)null;

                            response.Add(obj);
                        }
                    }
                }
            });
            return response;
        }
    }
}
