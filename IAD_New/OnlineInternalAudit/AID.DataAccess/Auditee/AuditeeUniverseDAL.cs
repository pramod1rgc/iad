﻿using BusinessModel.Auditee;
using Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Text;
using System.Threading.Tasks;

namespace AID.DataAccess.Auditee
{
    public class AuditeeUniverseDAL
    {

        List<AuditeeUniverseBM> list;
        private Database iasdb = null;
        public AuditeeUniverseDAL(Database database)
        {
            iasdb = database;
        }

        /// <summary>
        /// Get All Employee
        /// </summary>
        /// <returns></returns>
        public async Task<List<AuditeeUniverseBM>> GetAllAuditee(int userid)
        {
            list = new List<AuditeeUniverseBM>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = iasdb.GetStoredProcCommand(IASConstant.GetAllAuditeeUniverse_SP))
                {
                    iasdb.AddInParameter(dbCommand, "CreatedByID", DbType.String, userid.ToString());
                    using (IDataReader rdr = iasdb.ExecuteReader(dbCommand))
                    {
                        while (rdr.Read())
                        {
                            AuditeeUniverseBM Mstemployee = new AuditeeUniverseBM();
                            Mstemployee.EmpID = Convert.ToInt32(rdr["EmpID"]);
                            Mstemployee.EmpName = rdr["EmpName"].ToString();
                            Mstemployee.Designation = rdr["DesigName"].ToString();
                            Mstemployee.Email = rdr["Email"].ToString();
                            Mstemployee.RoleName = rdr["RoleName"].ToString();
                            Mstemployee.RoleID = Convert.ToInt32(rdr["RoleID"]);
                            Mstemployee.DesigID = Convert.ToInt32(rdr["DesigID"]);
                            Mstemployee.MobileNo = rdr["MobileNo"].ToString();
                            Mstemployee.PhoneNo = rdr["PhoneNo"].ToString();
                            Mstemployee.IsActive = rdr["IsActive"].ToString();
                            list.Add(Mstemployee);
                        }
                    }

                }
            });
            return list;
        }


        /// <summary>
        /// Get All Employee
        /// </summary>
        /// <returns></returns>
        public async Task<List<AuditeeUniverseBM>> GetAllDesignation(int rollId)
        {
            list = new List<AuditeeUniverseBM>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = iasdb.GetStoredProcCommand(IASConstant.GetAuditeeDesignationByRoleId_SP))
                {
                    iasdb.AddInParameter(dbCommand, "RoleID", DbType.Int32, rollId);
                    using (IDataReader rdr = iasdb.ExecuteReader(dbCommand))
                    {
                        while (rdr.Read())
                        {
                            AuditeeUniverseBM Mstemployee = new AuditeeUniverseBM();
                            Mstemployee.DesigID = Convert.ToInt32(rdr["ID"]);
                            Mstemployee.DesigName = rdr["DesignationName"].ToString();
                            list.Add(Mstemployee);
                        }
                    }

                }
            });
            return list;
        }

        /// <summary>
        /// Get All Employee
        /// </summary>
        /// <returns></returns>
        public async Task<List<AuditeeUniverseBM>> GetAuditeeRole()
        {
            list = new List<AuditeeUniverseBM>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = iasdb.GetStoredProcCommand(IASConstant.AuditeeRole_SP))
                {
                    using (IDataReader rdr = iasdb.ExecuteReader(dbCommand))
                    {
                        while (rdr.Read())
                        {
                            AuditeeUniverseBM Mstemployee = new AuditeeUniverseBM();
                            Mstemployee.RoleID = Convert.ToInt32(rdr["RoleID"]);
                            Mstemployee.RoleName = rdr["RoleName"].ToString();
                            list.Add(Mstemployee);
                        }
                    }

                }
            });
            return list;
        }


        /// <summary>
        /// Save Employee
        /// </summary>
        /// <param name="hdnEmpId"></param>
        /// <param name="objEmp"></param>
        /// <returns></returns>

        public async Task<string> SaveAuditee(string hdnEmpId, AuditeeUniverseBM objEmp)
        {
            try
            {
                string response = null;
                string message = string.Empty;
                int Msg_Out_StatusForEmail =0;
                string Password = string.Empty;
                string Subject = string.Empty;
                string Remarks = string.Empty ;
 
                await Task.Run(() =>

                    {
                        // string.IsNullOrEmpty(hdnEmpId);
                        if (string.IsNullOrEmpty(hdnEmpId) == true || hdnEmpId == "null" || hdnEmpId == "")
                        {
                            objEmp.EmpID = 0;
                        }
                        else
                        {
                            objEmp.EmpID = Convert.ToInt32(hdnEmpId);
                        }

                        using (DbCommand dbCommand = iasdb.GetStoredProcCommand(IASConstant.InsAuditeeUniverseDetails_SP))
                        {
                            iasdb.AddInParameter(dbCommand, "roleID", DbType.Int32, objEmp.RoleID);
                            iasdb.AddInParameter(dbCommand, "EmpId", DbType.Int32, objEmp.EmpID);
                            iasdb.AddInParameter(dbCommand, "EmpName", DbType.String, objEmp.EmpName.Trim());
                            iasdb.AddInParameter(dbCommand, "DesignID", DbType.Int32, Convert.ToInt32(objEmp.DesigID));
                            iasdb.AddInParameter(dbCommand, "PhoneNo", DbType.String, objEmp.PhoneNo);
                            iasdb.AddInParameter(dbCommand, "MobileNo", DbType.String, objEmp.MobileNo);
                            iasdb.AddInParameter(dbCommand, "Email", DbType.String, objEmp.Email.Trim());
                            iasdb.AddInParameter(dbCommand, "IsActive", DbType.Boolean, 0); /*objEmp.IsActive.Trim()*/
                            iasdb.AddInParameter(dbCommand, "CreatedByID", DbType.String, objEmp.userId.ToString()); /*objEmp.IsActive.Trim()*/
                            iasdb.AddOutParameter(dbCommand, "Msg_Out_Status", DbType.String, 500);
                            iasdb.AddOutParameter(dbCommand, "Msg_Out_StatusForEmail", DbType.Int32, 2);
                            iasdb.AddOutParameter(dbCommand, "OutPassword", DbType.String, 500);
                            iasdb.ExecuteNonQuery(dbCommand);
                            message = Convert.ToString(iasdb.GetParameterValue(dbCommand, "Msg_Out_Status"));
                            Msg_Out_StatusForEmail = Convert.ToInt32(iasdb.GetParameterValue(dbCommand, "Msg_Out_StatusForEmail"));
                            Password = Convert.ToString(iasdb.GetParameterValue(dbCommand, "OutPassword"));
                            if(Msg_Out_StatusForEmail==1)
                            {
                                string RoleName = string.Empty;
                                if(objEmp.RoleID==9)
                                {
                                    RoleName = "Auditee Supervisor";
                                }
                                else
                                {
                                    RoleName = "Auditee DH";
                                }
                                response = CommonFunction.SendMailAuditeeUniverse(objEmp.Email.Trim(), Subject, RoleName,Remarks, Password);
                                if (response == "OnSuccess")
                                {
                                    EmailIntimationStatus( objEmp.Email);
                                }
                            }

                        }
                    });
                return message;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        /// <summary>
        /// EmailIntimationStatus
        /// </summary>
        /// <param name="PAOId"></param>
        /// <param name="Email"></param>
        /// <returns></returns>
        public async Task<int> EmailIntimationStatus( string Email)
        {
            int i = 0;
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = iasdb.GetStoredProcCommand(IASConstant.usp_AuditeeUniverseEmailStatus))
                {
                    iasdb.AddInParameter(dbCommand, "Email", DbType.String, Email);
                    i = iasdb.ExecuteNonQuery(dbCommand);
                }
            });
            return i;
        }
        /// <summary>
        /// Active Deactive User
        /// </summary>
        /// <param name="objemployee"></param>
        /// <returns></returns>
        public string ActiveDeactiveUser(AuditeeUniverseBM objemployee)
        {
            string message = string.Empty;
            try
            {


                string IsActive = "";
                if (objemployee.IsActive == "true")
                {
                    IsActive = "1";
                }
                else
                {
                    IsActive = "0";
                }
                using (DbCommand dbCommand = iasdb.GetStoredProcCommand(IASConstant.Mst_AuditeeUniverseActiveAndDeActive_SP))
                {
                    iasdb.AddInParameter(dbCommand, "EmpId", DbType.String, objemployee.EmpID);
                    iasdb.AddInParameter(dbCommand, "IsActive", DbType.String, IsActive.ToString()); /*objEmp.IsActive.Trim()*/
                    iasdb.AddInParameter(dbCommand, "userId", DbType.String, objemployee.userId); /*objEmp.IsActive.Trim()*/
                    iasdb.AddOutParameter(dbCommand, "Msg_Out_Status", DbType.String, 500);
                    iasdb.ExecuteNonQuery(dbCommand);
                    message = Convert.ToString(iasdb.GetParameterValue(dbCommand, "Msg_Out_Status"));


                }
                return message;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

    }
}

