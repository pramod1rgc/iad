﻿
using BusinessModel.AuditeeObservation;
using Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;


namespace AID.DataAccess.AuditeeObservation
{
    public class AuditeeEntryConferenceDAL
    {
        private Database iasdb = null;
        public AuditeeEntryConferenceDAL(Database database)
        {
            iasdb = database;
        }


        public async Task<List<GetAuditeeDHAuditsListResponse>> GetAuditDHAuditsList(int auditUserId)
        {
            List<GetAuditeeDHAuditsListResponse> response = new List<GetAuditeeDHAuditsListResponse>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = iasdb.GetStoredProcCommand(IASConstant.SP_GetAuditDHAuditsList))
                {
                    iasdb.AddInParameter(dbCommand, "@auditUserId", DbType.Int32, auditUserId);
                    using (IDataReader objReader = iasdb.ExecuteReader(dbCommand))
                    {
                        while (objReader.Read())
                        {
                            GetAuditeeDHAuditsListResponse obj = new GetAuditeeDHAuditsListResponse();
                            obj.AuditId = objReader["AuditId"] != DBNull.Value ? Convert.ToInt32(objReader["AuditId"]) : 0;
                            obj.PaoId = objReader["PAOId"] != DBNull.Value ? Convert.ToInt32(objReader["PAOId"]) : 0;
                            obj.Name = objReader["Name"] != DBNull.Value ? Convert.ToString(objReader["Name"]) : null;
                            obj.PaoCode = objReader["PAOCode"] != DBNull.Value ? Convert.ToString(objReader["PAOCode"]) : null;
                            obj.Email = objReader["Email"] != DBNull.Value ? Convert.ToString(objReader["Email"]) : null;
                            obj.Phone = objReader["Phone"] != DBNull.Value ? Convert.ToString(objReader["Phone"]) : null;
                            obj.Address = objReader["Address"] != DBNull.Value ? Convert.ToString(objReader["Address"]) : null;
                            obj.District = objReader["District"] != DBNull.Value ? Convert.ToString(objReader["District"]) : null;
                            obj.City = objReader["City"] != DBNull.Value ? Convert.ToString(objReader["City"]) : null;
                            obj.AuditStatusId = objReader["AuditStatusId"] != DBNull.Value ? Convert.ToInt32(objReader["AuditStatusId"]) : 0;
                            obj.Status = objReader["Status"] != DBNull.Value ? Convert.ToString(objReader["Status"]) : null;
                            obj.ControllerCode = objReader["ControllerCode"] != DBNull.Value ? Convert.ToString(objReader["ControllerCode"]) : null;
                            obj.EntryConferenceId = objReader["EntryConferenceId"] != DBNull.Value ? Convert.ToInt32(objReader["EntryConferenceId"]) : 0;
                            obj.ExitConferenceId = objReader["ExitConferenceId"] != DBNull.Value ? Convert.ToInt32(objReader["ExitConferenceId"]) : 0;
                            obj.AuditFromDate = objReader["AuditFromDate"] != DBNull.Value ? Convert.ToDateTime(objReader["AuditFromDate"]) : (DateTime?)null;
                            obj.AuditToDate = objReader["AuditToDate"] != DBNull.Value ? Convert.ToDateTime(objReader["AuditToDate"]) : (DateTime?)null;
                            obj.AuditIntimationDate = objReader["AuditIntimationDate"] != DBNull.Value ? Convert.ToDateTime(objReader["AuditIntimationDate"]) : (DateTime?)null;
                            obj.EntryConferenceDate = objReader["EntryConferenceDate"] != DBNull.Value ? Convert.ToDateTime(objReader["EntryConferenceDate"]) : (DateTime?)null;
                            obj.ExitConferenceDate = objReader["ExitConferenceDate"] != DBNull.Value ? Convert.ToDateTime(objReader["ExitConferenceDate"]) : (DateTime?)null;
                            obj.ParaCreatedOnDate = objReader["ParaCreatedOnDate"] != DBNull.Value ? Convert.ToDateTime(objReader["ParaCreatedOnDate"]) : (DateTime?)null;
                            obj.AuditParaSettlementLetterId = objReader["AuditParaSettlementLetterId"] != DBNull.Value ? Convert.ToInt32(objReader["AuditParaSettlementLetterId"]) : 0;
                            obj.AuditParaSettlementLetterDate = objReader["AuditParaSettlementLetterDate"] != DBNull.Value ? Convert.ToDateTime(objReader["AuditParaSettlementLetterDate"]) : (DateTime?)null;
                            response.Add(obj);
                        }
                    }
                }
            });
            return response;
        }


        public async Task<List<GetAuditeeEntryConferenceResponse>> GetAuditEntryConference(int auditId)
        {
            List<GetAuditeeEntryConferenceResponse> response = new List<GetAuditeeEntryConferenceResponse>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = iasdb.GetStoredProcCommand(IASConstant.SP_GetAuditEntryConference))
                {
                    iasdb.AddInParameter(dbCommand, "@auditId", DbType.Int32, auditId);
                    using (IDataReader objReader = iasdb.ExecuteReader(dbCommand))
                    {
                        while (objReader.Read())
                        {
                            GetAuditeeEntryConferenceResponse obj = new GetAuditeeEntryConferenceResponse();
                            obj.AuditId = objReader["AuditId"] != DBNull.Value ? Convert.ToInt32(objReader["AuditId"]) : 0;
                            obj.EntryConferenceId = objReader["EntryConferenceId"] != DBNull.Value ? Convert.ToInt32(objReader["EntryConferenceId"]) : 0;
                            obj.ConferenceDate = objReader["ConferenceDate"] != DBNull.Value ? Convert.ToDateTime(objReader["ConferenceDate"]) : (DateTime?)null;
                            obj.Agenda = objReader["Agenda"] != DBNull.Value ? Convert.ToString(objReader["Agenda"]) : null;
                            obj.EntryConferenceParticipantId = objReader["EntryConferenceParticipantId"] != DBNull.Value ? Convert.ToInt32(objReader["EntryConferenceParticipantId"]) : 0;
                            obj.AuditUserMapId = objReader["AuditUserMapId"] != DBNull.Value ? Convert.ToInt32(objReader["AuditUserMapId"]) : 0;
                            obj.EmpID = objReader["EmpID"] != DBNull.Value ? Convert.ToInt32(objReader["EmpID"]) : 0;
                            obj.EmpName = objReader["EmpName"] != DBNull.Value ? Convert.ToString(objReader["EmpName"]) : null;
                            obj.DesignationId = objReader["DesignationId"] != DBNull.Value ? Convert.ToInt32(objReader["DesignationId"]) : 0;
                            obj.DesignationName = objReader["DesignationName"] != DBNull.Value ? Convert.ToString(objReader["DesignationName"]) : null;
                            obj.RoleId = objReader["RoleID"] != DBNull.Value ? Convert.ToInt32(objReader["RoleID"]) : 0;
                            response.Add(obj);
                        }
                    }
                }
            });
            return response;
        }


        public async Task<int> UpdateAuditEntryConference(UpdateAuditeeEntryConferenceRequest request)
        {
            int result = 0;
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = iasdb.GetStoredProcCommand(IASConstant.SP_UpdateAuditEntryConference))
                {
                    iasdb.AddInParameter(dbCommand, "@entryConferenceId", DbType.Int32, request.EntryConferenceId);
                    iasdb.AddInParameter(dbCommand, "@auditId", DbType.Int32, request.AuditId);
                    iasdb.AddInParameter(dbCommand, "@conferenceDate", DbType.DateTime, request.ConferenceDate);
                    iasdb.AddInParameter(dbCommand, "@agenda", DbType.String, request.Agenda);
                    iasdb.AddInParameter(dbCommand, "@userId", DbType.Int32, request.UserId);
                    iasdb.AddInParameter(dbCommand, "@userIPAddress", DbType.String, request.UserIPAddress);
                    var table = CommonFunction.CreateDataTable(request.Participants);
                    SqlParameter param = new SqlParameter("@participants", table);
                    param.SqlDbType = SqlDbType.Structured;
                    dbCommand.Parameters.Add(param);

                    result = iasdb.ExecuteNonQuery(dbCommand);
                }
            });
            return result;
        }

        public async Task<int> UpdateAuditstatus(AuditeeStatusUpdateRequest request)
        {
            int result = 0;
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = iasdb.GetStoredProcCommand(IASConstant.UpdateStatusAuditee_SP))
                {
                    iasdb.AddInParameter(dbCommand, "@statusId", DbType.Int32, request.StatusId);
                    iasdb.AddInParameter(dbCommand, "@auditId", DbType.Int32, request.AuditId);
                    iasdb.AddInParameter(dbCommand, "@userId", DbType.Int32, request.UserId);
                    iasdb.AddInParameter(dbCommand, "@paraId", DbType.Int32, request.ParaId);
                    iasdb.AddInParameter(dbCommand, "@ipAddress", DbType.String, request.IPAddress);

                    result = iasdb.ExecuteNonQuery(dbCommand);
                }
            });
            return result;
        }
    }
}
