﻿using BusinessModel.AuditeeObservation;
using Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Text;
using System.Threading.Tasks;

namespace AID.DataAccess.AuditeeObservation
{
   public class AuditeeParaSettlementLetterDAL
    {
        private Database iasdb = null;
        public AuditeeParaSettlementLetterDAL(Database database)
        {
            iasdb = database;
        }

        public async Task<GetAuditeeParaSettlementLetterResponse> GetAuditParaSettlementLetter(int auditId)
        {
            GetAuditeeParaSettlementLetterResponse response = new GetAuditeeParaSettlementLetterResponse();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = iasdb.GetStoredProcCommand(IASConstant.SP_GetAuditParaSettlementLetter))
                {
                    iasdb.AddInParameter(dbCommand, "@auditId", DbType.Int32, auditId);
                    using (IDataReader objReader = iasdb.ExecuteReader(dbCommand))
                    {
                        while (objReader.Read())
                        {
                            response.AuditParaSettlementLetterId = objReader["AuditParaSettlementLetterId"] != DBNull.Value ? Convert.ToInt32(objReader["AuditParaSettlementLetterId"]) : 0;
                            response.LetterContent = objReader["LetterContent"] != DBNull.Value ? Convert.ToString(objReader["LetterContent"]) : null;
                            response.LetterNumber = objReader["LetterNumber"] != DBNull.Value ? Convert.ToString(objReader["LetterNumber"]) : null;
                            response.LetterDate = objReader["LetterDate"] != DBNull.Value ? Convert.ToDateTime(objReader["LetterDate"]) : (DateTime?)null;
                            response.LetterTo = objReader["LetterTo"] != DBNull.Value ? Convert.ToString(objReader["LetterTo"]) : null;
                            response.LetterSubject = objReader["LetterSubject"] != DBNull.Value ? Convert.ToString(objReader["LetterSubject"]) : null;
                            response.LetterFrom = objReader["LetterFrom"] != DBNull.Value ? Convert.ToString(objReader["LetterFrom"]) : null;
                            response.LetterCopyTo = objReader["LetterCopyTo"] != DBNull.Value ? Convert.ToString(objReader["LetterCopyTo"]) : null;
                            response.AuditId = objReader["AuditId"] != DBNull.Value ? Convert.ToInt32(objReader["AuditId"]) : 0;
                            response.CreatedOn = objReader["CreatedOn"] != DBNull.Value ? Convert.ToDateTime(objReader["CreatedOn"]) : (DateTime?)null;
                            response.StatusId = objReader["StatusId"] != DBNull.Value ? Convert.ToInt32(objReader["StatusId"]) : 0;
                            response.RejectionRemark = objReader["RejectionRemark"] != DBNull.Value ? Convert.ToString(objReader["RejectionRemark"]) : null;
                        }
                    }
                }
            });
            return response;
        }


        public async Task<int> UpdateAuditParaSettlementLetter(UpdateAuditeeParaSettlementLetterRequest request)
        {
            int result = 0;
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = iasdb.GetStoredProcCommand(IASConstant.SP_UpdateAuditParaSettlementLetter))
                {
                    iasdb.AddInParameter(dbCommand, "@auditId", DbType.Int32, request.AuditId);
                    iasdb.AddInParameter(dbCommand, "@auditParaSettlementLetterId", DbType.Int32, request.AuditParaSettlementLetterId);
                    iasdb.AddInParameter(dbCommand, "@letterContent", DbType.String, request.LetterContent);
                    iasdb.AddInParameter(dbCommand, "@letterNumber", DbType.String, request.LetterNumber);
                    iasdb.AddInParameter(dbCommand, "@letterDate", DbType.DateTime, request.LetterDate);
                    iasdb.AddInParameter(dbCommand, "@letterTo", DbType.String, request.LetterTo);
                    iasdb.AddInParameter(dbCommand, "@letterSubject", DbType.String, request.LetterSubject);
                    iasdb.AddInParameter(dbCommand, "@letterFrom", DbType.String, request.LetterFrom);
                    iasdb.AddInParameter(dbCommand, "@letterCopyTo", DbType.String, request.LetterCopyTo);
                    iasdb.AddInParameter(dbCommand, "@userId", DbType.Int32, request.UserId);
                    iasdb.AddInParameter(dbCommand, "@ipAddress", DbType.String, request.IPAddress);

                    result = iasdb.ExecuteNonQuery(dbCommand);
                }
            });
            return result;
        }

        public async Task<int> UpdateParaSettlementLetterStatus(int auditId, int statusId, string rejectionRemark)
        {
            int result = 0;
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = iasdb.GetStoredProcCommand(IASConstant.SP_UpdateParaSettlementLetterStatus))
                {
                    iasdb.AddInParameter(dbCommand, "@auditId", DbType.Int32, auditId);
                    iasdb.AddInParameter(dbCommand, "@statusId", DbType.Int32, statusId);
                    iasdb.AddInParameter(dbCommand, "@rejectionRemark", DbType.String, rejectionRemark);

                    result = iasdb.ExecuteNonQuery(dbCommand);
                }
            });
            return result;
        }
    }
}
