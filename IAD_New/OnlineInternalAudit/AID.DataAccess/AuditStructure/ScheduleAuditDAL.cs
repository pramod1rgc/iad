﻿
using BusinessModel.AuditStructure;
using Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;

namespace AID.DataAccess.AuditStructure
{
    public class ScheduleAuditDAL
    {
        private Database iasdb = null;
        public ScheduleAuditDAL(Database database)
        {
            iasdb = database;
        }


        public async Task<List<GetScheduledAuditsResponse>> GetScheduledAudits(int finYearId, string quarter)
        {
            List<GetScheduledAuditsResponse> response = new List<GetScheduledAuditsResponse>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = iasdb.GetStoredProcCommand(IASConstant.SP_GetScheduledAudits))
                {
                    iasdb.AddInParameter(dbCommand, "@finYearId", DbType.Int32, finYearId);
                    iasdb.AddInParameter(dbCommand, "@quarter", DbType.String, quarter);
                    using (IDataReader objReader = iasdb.ExecuteReader(dbCommand))
                    {
                        while (objReader.Read())
                        {
                            GetScheduledAuditsResponse obj = new GetScheduledAuditsResponse();
                            obj.AuditId = objReader["AuditId"] != DBNull.Value ? Convert.ToInt32(objReader["AuditId"]) : 0;
                            obj.PAOId = objReader["PAOId"] != DBNull.Value ? Convert.ToInt32(objReader["PAOId"]) : 0;
                            obj.FinancialYearId = objReader["FinancialYearId"] != DBNull.Value ? Convert.ToInt32(objReader["FinancialYearId"]) : 0;
                            obj.AuditName = objReader["AuditName"] != DBNull.Value ? Convert.ToString(objReader["AuditName"]) : null;
                            obj.AuditStatusId = objReader["AuditStatusId"] != DBNull.Value ? Convert.ToInt32(objReader["AuditStatusId"]) : 0;
                            obj.AuditFromDate = objReader["AuditFromDate"] != DBNull.Value ? Convert.ToDateTime(objReader["AuditFromDate"]) : (DateTime?)null;
                            obj.AuditToDate = objReader["AuditToDate"] != DBNull.Value ? Convert.ToDateTime(objReader["AuditToDate"]) : (DateTime?)null;
                            obj.Quarter = objReader["Quarter"] != DBNull.Value ? Convert.ToString(objReader["Quarter"]) : null;
                            obj.PAOName = objReader["Name"] != DBNull.Value ? Convert.ToString(objReader["Name"]) : null;
                            obj.PAOCode = objReader["PAOCode"] != DBNull.Value ? Convert.ToString(objReader["PAOCode"]) : null;
                            obj.ControllerCode = objReader["ControllerCode"] != DBNull.Value ? Convert.ToString(objReader["ControllerCode"]) : null;
                            obj.Status = objReader["Status"] != DBNull.Value ? Convert.ToString(objReader["Status"]) : null;
                            obj.RejectionRemark = objReader["RejectionRemark"] != DBNull.Value ? Convert.ToString(objReader["RejectionRemark"]) : null;

                            response.Add(obj);
                        }
                    }
                }
            });
            return response;
        }


        public async Task<int> UpdateScheduledAudit(UpdateScheduleAuditRequest request)
        {
            int result = 0;
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = iasdb.GetStoredProcCommand(IASConstant.SP_UpdateScheduledAudit))
                {
                    var table = CommonFunction.CreateDataTable(request.AuditSchedulesType);
                    SqlParameter param = new SqlParameter("@auditSchedulesType", table);
                    param.SqlDbType = SqlDbType.Structured;
                    dbCommand.Parameters.Add(param);
                    iasdb.AddInParameter(dbCommand, "@userId", DbType.Int32, request.UserId);
                    iasdb.AddInParameter(dbCommand, "@ipAddress", DbType.String, request.IPAddress);

                    result = iasdb.ExecuteNonQuery(dbCommand);
                }
            });
            return result;
        }

        public async Task<int> DeleteScheduledAudit(DeleteScheduleAuditRequest request)
        {
            int result = 0;
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = iasdb.GetStoredProcCommand(IASConstant.SP_UpdateScheduledAudit))
                {
                    iasdb.AddInParameter(dbCommand, "@auditId", DbType.Int32, request.AuditId);
                    iasdb.AddInParameter(dbCommand, "@userId", DbType.Int32, request.UserId);
                    iasdb.AddInParameter(dbCommand, "@ipAddress", DbType.String, request.IPAddress);

                    result = dbCommand.ExecuteNonQuery();
                }
            });
            return result;
        }
    }
}
