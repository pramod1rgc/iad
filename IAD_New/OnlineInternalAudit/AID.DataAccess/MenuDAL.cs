﻿using BusinessModel.UserManagement;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Text;
using System.Threading.Tasks;

namespace AID.DataAccess
{
    public class MenuDAL
    {
        List<MenuModel> lstMenu;
        private Database iasdb = null;
        public MenuDAL(Database database)
        {
            iasdb = database;
        }


        /// <summary>
        /// BindMenuInGride
        /// </summary>
        /// <returns></returns>
        public async Task<List<MenuModel>> BindMenuInGride()
        {
            lstMenu = new List<MenuModel>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = iasdb.GetStoredProcCommand("GetAllMenuInGride_SP"))
                {
                    using (IDataReader rdr = iasdb.ExecuteReader(dbCommand))
                    {
                        while (rdr.Read())
                        {
                            MenuModel MstMenu = new MenuModel();
                            MstMenu.MenuID = Convert.ToInt32(rdr["MenuID"]);
                            MstMenu.MenuDescriptionName = rdr["MenuDescriptionName"].ToString();
                            MstMenu.MenuURL = rdr["MenuURL"].ToString();
                            MstMenu.ParentMenu = Convert.ToInt32(rdr["ParentMenuID"]);
                            MstMenu.MenuHierarchy = rdr["MenuHierarchy"].ToString();
                            MstMenu.Status = rdr["Status"].ToString();
                            MstMenu.IPAddress = rdr["IPAddress"].ToString();
                            MstMenu.CreatedBy = rdr["CreatedBy"].ToString();
                            MstMenu.CreatedBy = rdr["CreatedBy"].ToString();
                            //MstMenu.CreatedStatus = rdr["CreatedStatus"].ToString();
                            MstMenu.ModifiedBy = rdr["ModifiedBy"].ToString();
                            MstMenu.ModifiedDate = rdr["ModifiedDate"].ToString();
                            MstMenu.IsActive = rdr["IsActive"].ToString();
                            lstMenu.Add(MstMenu);
                        }
                    }

                }
            });
            return lstMenu;
        }

        /// <summary>
        /// BindMenuInGride
        /// </summary>
        /// <returns></returns>
        public async Task<List<MenuModel>> BindMenuInDropDown()
        {
            lstMenu = new List<MenuModel>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = iasdb.GetStoredProcCommand("[OIAS].[GetMenuInDDL_SP]"))
                {
                    using (IDataReader rdr = iasdb.ExecuteReader(dbCommand))
                    {
                        while (rdr.Read())
                        {
                            MenuModel MstMenu = new MenuModel();
                            MstMenu.MenuID = Convert.ToInt32(rdr["MenuID"]);
                            MstMenu.MenuDescriptionName = rdr["MenuDescriptionName"].ToString();
                            MstMenu.IsActive = rdr["IsActive"].ToString();
                            lstMenu.Add(MstMenu);
                        }
                    }

                }
            });
            return lstMenu;
        }


        /// <summary>
        /// BindMenuInGride
        /// </summary>
        /// <returns></returns>
        public async Task<List<MenuModel>> GetAllPredefineRole()
        {
            lstMenu = new List<MenuModel>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = iasdb.GetStoredProcCommand("[OIAS].[GetAllPredefineRole]"))
                {
                    using (IDataReader rdr = iasdb.ExecuteReader(dbCommand))
                    {
                        while (rdr.Read())
                        {
                            MenuModel MstMenu = new MenuModel();
                            MstMenu.RoleID = Convert.ToInt32(rdr["RoleID"]);
                            MstMenu.RoleName = rdr["RoleName"].ToString();
                            MstMenu.IsActive = rdr["IsActive"].ToString();
                            lstMenu.Add(MstMenu);
                        }
                    }

                }
            });
            return lstMenu;
        }

    }
}
