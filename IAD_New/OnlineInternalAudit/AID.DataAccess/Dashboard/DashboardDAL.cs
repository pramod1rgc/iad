﻿using AID.BusinessModels.Dashboard;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AID.DataAccess.Dashboard
{
    public class DashboardDAL
    {
        private Database iasdb = null;
        public DashboardDAL(Database database)
        {
            iasdb = database;
        }



        /// <summary>
        /// Get All Menus By User
        /// </summary>
        /// <param name=""></param>
        /// <param name=""></param>
        /// <returns></returns>
        public async Task<List<DashboardModel>> getAllMenus(int roleId)
        {
            List<DashboardModel> headerTree = null;
            List<DashboardModel> categories = new List<DashboardModel>();
            using (DbCommand dbCommand = iasdb.GetStoredProcCommand("[OIAS].[GetAllmenues_SP]"))
            {
                iasdb.AddInParameter(dbCommand, "@roleId", DbType.Int32, roleId);
                using (IDataReader rdr = iasdb.ExecuteReader(dbCommand))
                {
                    while (rdr.Read())
                    {
                        categories.Add(
                            new DashboardModel
                            {
                                menuID = Convert.ToInt32(rdr["MenuID"]),
                                displayName = rdr["MenuDescriptionName"].ToString(),
                                parentMenu = (Convert.ToInt32(rdr["ParentMenuID"]) != 0) ? Convert.ToInt32(rdr["ParentMenuID"]) : (int?)null,
                                route = rdr["MenuURL"].ToString(),
                                iconName = rdr["IconName"].ToString() != "" ? rdr["IconName"].ToString() : "",//"star_rate"
                                menuHeirarchy = Convert.ToInt32(rdr["MenuHierarchy"])
                            });
                    }

                    headerTree = FillRecursive(categories, null);
                    headerTree = headerTree.OrderBy(a => a.menuHeirarchy).ToList();
                    return headerTree;
                }

            }
        }

        /// <summary>
        /// Fill Recursive
        /// </summary>
        /// <param name="flatObjects"></param>
        /// <param name="parentId"></param>
        /// <returns></returns>
        public List<DashboardModel> FillRecursive(List<DashboardModel> flatObjects, int? parentId = null)
        {
            return flatObjects.Where(x => x.parentMenu.Equals(parentId)).Select(item => new DashboardModel
            {
                displayName = item.displayName,
                menuID = item.menuID,
                route = item.route,
                iconName = item.iconName,
                menuHeirarchy = item.menuHeirarchy,
                Children = FillRecursive(flatObjects, item.menuID)
            }).ToList();
        }

    }
}
