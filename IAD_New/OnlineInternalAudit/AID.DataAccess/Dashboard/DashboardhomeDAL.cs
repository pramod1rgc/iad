﻿using BusinessModel.AuditStructure;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Text;
using System.Threading.Tasks;

namespace AID.DataAccess.Dashboard
{
    public class DashboardhomeDAL
    {
        private Database iasdb = null;
        public DashboardhomeDAL(Database database)
        {
            iasdb = database;
        }


        
     public async Task<object[]> GetAll_IADHead_DashboardRecord(string username)
        {
            object[] listTable = new object[8];

            await Task.Run(() =>
            {
                using (DbCommand dbCommand = iasdb.GetStoredProcCommand("[OIAS].[GetAll_IADHead_DashboardRecord_SP]"))
                {
                    //iasdb.AddInParameter(dbCommand, "username", DbType.String, username);
                    DataSet ds = iasdb.ExecuteDataSet(dbCommand);
                    DataTable dt = ds.Tables[0];
                    listTable[0] = ds.Tables[0];
                    listTable[1] = ds.Tables[1];
                    listTable[2] = ds.Tables[2];
                    listTable[3] = ds.Tables[3];
                    listTable[4] = ds.Tables[4];
                    listTable[5] = ds.Tables[5];
                    listTable[6] = ds.Tables[6];

                }
            });
            if (listTable == null)
                return null;
            else
                return listTable;
        }

        public async Task<object[]> GetAllDashboardRecord(string username)
        {
            object[] listTable = new object[8];

            await Task.Run(() =>
            {
                using (DbCommand dbCommand = iasdb.GetStoredProcCommand("[OIAS].[GetallDashboardData_SP]"))
                {
                    //iasdb.AddInParameter(dbCommand, "username", DbType.String, username);
                    DataSet ds = iasdb.ExecuteDataSet(dbCommand);
                    DataTable dt = ds.Tables[0];
                    listTable[0] = ds.Tables[0];
                    listTable[1] = ds.Tables[1];
                    listTable[2] = ds.Tables[2];
                    listTable[3] = ds.Tables[3];
                    listTable[4] = ds.Tables[4];
                    listTable[5] = ds.Tables[5];
                    listTable[6] = ds.Tables[6];

                }
            });
            if (listTable == null)
                return null;
            else
                return listTable;
        }


        public async Task<object[]> GetAuditDhDashboardAllRecord(string username)
        {
            object[] listTable = new object[4];

            await Task.Run(() =>
            {
                using (DbCommand dbCommand = iasdb.GetStoredProcCommand("[OIAS].[GetAll_IADDHDashboardRecord_SP]"))
                {
                    iasdb.AddInParameter(dbCommand, "userId", DbType.String, username);
                    DataSet ds = iasdb.ExecuteDataSet(dbCommand);
                    DataTable dt = ds.Tables[0];
                    listTable[0] = ds.Tables[0];
                    listTable[1] = ds.Tables[1];
                    listTable[2] = ds.Tables[2];
                    listTable[3] = ds.Tables[3];
                    //listTable[4] = ds.Tables[4];
                    //listTable[5] = ds.Tables[5];
                    //listTable[6] = ds.Tables[6];

                }
            });
            if (listTable == null)
                return null;
            else
                return listTable;
        }
        


        public async Task<List<GetScheduledAuditsResponse>> getallschedule()
        {
            List<GetScheduledAuditsResponse> response = new List<GetScheduledAuditsResponse>();
            await Task.Run(() =>
            {
                using (DbCommand dbCommand = iasdb.GetStoredProcCommand("[OIAS].[GetAllScheduleForSupervisor_SP]"))
                {
                    using (IDataReader objReader = iasdb.ExecuteReader(dbCommand))
                    {
                        while (objReader.Read())
                        {
                            GetScheduledAuditsResponse obj = new GetScheduledAuditsResponse();
                            obj.AuditId = objReader["AuditId"] != DBNull.Value ? Convert.ToInt32(objReader["AuditId"]) : 0;
                            obj.PAOId = objReader["PAOId"] != DBNull.Value ? Convert.ToInt32(objReader["PAOId"]) : 0;
                            obj.FinancialYearId = objReader["FinancialYearId"] != DBNull.Value ? Convert.ToInt32(objReader["FinancialYearId"]) : 0;
                            obj.AuditName = objReader["AuditName"] != DBNull.Value ? Convert.ToString(objReader["AuditName"]) : null;
                            obj.AuditStatusId = objReader["AuditStatusId"] != DBNull.Value ? Convert.ToInt32(objReader["AuditStatusId"]) : 0;
                            obj.AuditFromDate = objReader["AuditFromDate"] != DBNull.Value ? Convert.ToDateTime(objReader["AuditFromDate"]) : (DateTime?)null;
                            obj.AuditToDate = objReader["AuditToDate"] != DBNull.Value ? Convert.ToDateTime(objReader["AuditToDate"]) : (DateTime?)null;
                            obj.Quarter = objReader["Quarter"] != DBNull.Value ? Convert.ToString(objReader["Quarter"]) : null;
                            obj.PAOName = objReader["Name"] != DBNull.Value ? Convert.ToString(objReader["Name"]) : null;
                            obj.PAOCode = objReader["PAOCode"] != DBNull.Value ? Convert.ToString(objReader["PAOCode"]) : null;
                            obj.ControllerCode = objReader["ControllerCode"] != DBNull.Value ? Convert.ToString(objReader["ControllerCode"]) : null;
                            obj.Status = objReader["Status"] != DBNull.Value ? Convert.ToString(objReader["Status"]) : null;
                            obj.RejectionRemark = objReader["RejectionRemark"] != DBNull.Value ? Convert.ToString(objReader["RejectionRemark"]) : null;

                            response.Add(obj);
                        }
                    }
                }
            });
            return response;
        }
        //public async Task<object> getallschedule()
        //{
        //    object[] listTable = new object[3];

        //    await Task.Run(() =>
        //    {
        //        using (DbCommand dbCommand = iasdb.GetStoredProcCommand("[OIAS].[GetAllScheduleForSupervisor_SP]"))
        //        {

        //            DataSet ds = iasdb.ExecuteDataSet(dbCommand);
        //            DataTable dt = ds.Tables[0];
        //            listTable[0] = ds.Tables[0];
        //            listTable[1] = ds.Tables[1];


        //        }
        //    });
        //    if (listTable == null)
        //        return null;
        //    else
        //        return listTable;
        //}
        public async Task<object[]> Getall_IADHeadInchart(string userid)
        {
            object[] listTable = new object[3];

            await Task.Run(() =>
            {
                using (DbCommand dbCommand = iasdb.GetStoredProcCommand("[OIAS].[GetallIADHeadDashboardDataInChart_SP]"))
                {
                    iasdb.AddInParameter(dbCommand, "userid", DbType.String, userid);
                    DataSet ds = iasdb.ExecuteDataSet(dbCommand);
                    DataTable dt = ds.Tables[0];
                    listTable[0] = ds.Tables[0];
                    //listTable[1] = ds.Tables[1];
                    //listTable[2] = ds.Tables[2];


                }
            });
            if (listTable == null)
                return null;
            else
                return listTable;
        }
        public async Task<object[]> GetallAuditDHInchart(string userid)
        {
            object[] listTable = new object[3];

            await Task.Run(() =>
            {
                using (DbCommand dbCommand = iasdb.GetStoredProcCommand("[OIAS].[GetallAuditDHDashboardDataInChart_SP]"))
                {
                    iasdb.AddInParameter(dbCommand, "userid", DbType.String, userid);
                    DataSet ds = iasdb.ExecuteDataSet(dbCommand);
                    DataTable dt = ds.Tables[0];
                    listTable[0] = ds.Tables[0];
                    //listTable[1] = ds.Tables[1];
                    //listTable[2] = ds.Tables[2];


                }
            });
            if (listTable == null)
                return null;
            else
                return listTable;
        }
        public async Task<object[]> GetallAuditInchart()
        {
            object[] listTable = new object[3];

            await Task.Run(() =>
            {
                using (DbCommand dbCommand = iasdb.GetStoredProcCommand("[OIAS].[GetallDashboardDataInChart_SP]"))
                {
                    //iasdb.AddInParameter(dbCommand, "username", DbType.String, username);
                    DataSet ds = iasdb.ExecuteDataSet(dbCommand);
                    DataTable dt = ds.Tables[0];
                    listTable[0] = ds.Tables[0];
                    //listTable[1] = ds.Tables[1];
                    //listTable[2] = ds.Tables[2];


                }
            });
            if (listTable == null)
                return null;
            else
                return listTable;
        }

    }
}
