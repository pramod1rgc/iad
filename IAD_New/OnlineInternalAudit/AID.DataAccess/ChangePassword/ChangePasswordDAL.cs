﻿using BusinessModel.ChangePassword;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Text;
using System.Threading.Tasks;

namespace AID.DataAccess.ChangePassword
{
    public class ChangePasswordDAL
    {
        private Database iasdb = null;
        public ChangePasswordDAL(Database database)
        {
            iasdb = database;
        }
        public async Task<string> ChangePassword(ChangePasswordBM objChangePWD)
        {
            try
            {
                string message = string.Empty;
                await Task.Run(() =>
                {
                    using (DbCommand dbCommand = iasdb.GetStoredProcCommand("[OIAS].usp_ChangePassword"))
                    {
                        iasdb.AddInParameter(dbCommand, "username", DbType.String, objChangePWD.username.Trim());
                        iasdb.AddInParameter(dbCommand, "currentpassword", DbType.String, objChangePWD.currentpassword.Trim());
                        iasdb.AddInParameter(dbCommand, "newpassword", DbType.String, objChangePWD.newpassword.Trim());         
                        iasdb.AddOutParameter(dbCommand, "Msg_Out_Status", DbType.String, 500);
                        iasdb.ExecuteNonQuery(dbCommand);
                        message = Convert.ToString(iasdb.GetParameterValue(dbCommand, "Msg_Out_Status"));
                    }
                });
                return message;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
}
