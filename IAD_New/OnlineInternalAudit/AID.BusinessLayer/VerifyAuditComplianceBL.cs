﻿using AID.DataAccess.Verification;
using BusinessModel.Verification;
using Common;
using IRepository;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AID.BusinessLayer.VerifyAuditComplianceBL
{
   public class VerifyAuditComplianceBL : IVerifyAuditComplianceRepository
    {
        readonly Database IASDatabase;
        public VerifyAuditComplianceBL()
        {
            DatabaseProviderFactory factory = new DatabaseProviderFactory();
            IASDatabase = factory.Create(IASConstant.IASDataBaseConnection);
        }

        async Task<List<GetAuditsForwardedToSupervisorResponse>> IVerifyAuditComplianceRepository.GetAuditsForwardedToSupervisor()
        {
            VerifyAuditComplianceDAL obj = new VerifyAuditComplianceDAL(IASDatabase);
            return await obj.GetAuditsForwardedToSupervisor();
        }
        async Task<List<GetAuditsForwardedToSupervisorResponse>> IVerifyAuditComplianceRepository.GetAuditsWhoseParasOrComplainceForwardedToSupervisor(string type)
        {
            VerifyAuditComplianceDAL obj = new VerifyAuditComplianceDAL(IASDatabase);
            return await obj.GetAuditsWhoseParasOrComplainceForwardedToSupervisor(type);
        }
        async Task<List<GetAuditsForwardedToSupervisorResponse>> IVerifyAuditComplianceRepository.GetAuditsWhoseSettlementLetterForwardedToSupervisor()
        {
            VerifyAuditComplianceDAL obj = new VerifyAuditComplianceDAL(IASDatabase);
            return await obj.GetAuditsWhoseSettlementLetterForwardedToSupervisor();
        }
    }
}
