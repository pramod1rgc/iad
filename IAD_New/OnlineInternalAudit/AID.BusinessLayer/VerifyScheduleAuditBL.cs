﻿using AID.DataAccess.Verification;

using BusinessModel.Verification;
using Common;
using IRepository;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AID.BusinessLayer.VerifyScheduleAuditBL
{
    public class VerifyScheduleAuditBL : IVerifyScheduleAuditRepository
    {
        readonly Database IASDatabase;
        public VerifyScheduleAuditBL()
        {
            DatabaseProviderFactory factory = new DatabaseProviderFactory();
            IASDatabase = factory.Create(IASConstant.IASDataBaseConnection);
        }


        async Task<List<GetScheduledAuditsForVerificationResponse>> IVerifyScheduleAuditRepository.GetScheduledAuditsForVerification(int finYearId, string quarter, int roleUserMapId)
        {
            VerifyScheduledAuditDAL obj = new VerifyScheduledAuditDAL(IASDatabase);
            return await obj.GetScheduledAuditsForVerification(finYearId, quarter, roleUserMapId);
        }

        async Task<int> IVerifyScheduleAuditRepository.UpdateScheduleAuditForVerification(UpdateScheduleAuditForVerificationRequest request)
        {
            VerifyScheduledAuditDAL obj = new VerifyScheduledAuditDAL(IASDatabase);
            request.IPAddress = CommonFunction.GetIPAddress();
            return await obj.UpdateScheduleAuditForVerification(request);
        }

        async Task<GetPendingTaskCountResponse> IVerifyScheduleAuditRepository.GetPendingTaskCount(int RoleUserMapId)
        {
            VerifyScheduledAuditDAL obj = new VerifyScheduledAuditDAL(IASDatabase);
            return await obj.GetPendingTaskCount(RoleUserMapId);
        }

        async Task<List<GetDistinctQuartersBasedOnStatusAndFYResponse>> IVerifyScheduleAuditRepository.GetDistinctQuartersBasedOnStatusAndFY(int fyId, string statusId, int statusId2)
        {
            VerifyScheduledAuditDAL obj = new VerifyScheduledAuditDAL(IASDatabase);
            return await obj.GetDistinctQuartersBasedOnStatusAndFY(fyId, statusId, statusId2);
        }
    }
}
