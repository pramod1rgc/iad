﻿using AID.DataAccess.DeskAudit;
using BusinessModel.DeskAudit;
using Common;
using IRepository;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AID.BusinessLayer
{
    public class DeskAuditBL : IDeskAuditRepository
    {
        readonly Database IASDatabase;
        /// <summary>
        /// DeskAuditBL
        /// </summary>
        public DeskAuditBL()
        {
            DatabaseProviderFactory factory = new DatabaseProviderFactory();
            IASDatabase = factory.Create(IASConstant.IASDataBaseConnection);
        }
        /// <summary>
        /// Activate And Deactivate
        /// </summary>
        /// <param name="objauditee"></param>
        /// <returns></returns>
        public async Task<string> active_deactiveDeskAudit(DeactiveDeskAuditBM objauditee)
        {
            DeskAuditDAL obj = new DeskAuditDAL(IASDatabase);
            return await obj.active_deactiveDeskAudit(objauditee);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task<List<GetDeskAuditUploadFiles>> getDeskAuditFiles(int auditId)    
        {
            DeskAuditDAL obj = new DeskAuditDAL(IASDatabase);
            return await obj.getDeskAuditFiles(auditId);
        
        }

        /// <summary>
        /// SaveDeskAudit
        /// </summary>
        /// <param name="objdeskaudit"></param>
        /// <returns></returns>
        async Task<string> IDeskAuditRepository.SaveDeskAudit(DeskAuditBM objdeskaudit)
        {
            DeskAuditDAL obj = new DeskAuditDAL(IASDatabase);
            return await obj.SaveDeskAudit(objdeskaudit);
        }



        
    }
}
