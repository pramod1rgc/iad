﻿using AID.BusinessModels.Dashboard;
using AID.DataAccess.Dashboard;
using Common;
using IRepository;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AID.BusinessLayer.DashboardBL
{
    public class DashboardBL: IDashboardRepository
    {
        readonly Database IASDatabase;
        public DashboardBL()
        {
            DatabaseProviderFactory factory = new DatabaseProviderFactory();
            IASDatabase = factory.Create(IASConstant.IASDataBaseConnection);
        }
        async Task<List<DashboardModel>> IDashboardRepository.getAllMenus(int roleId)
        {
            DashboardDAL obj = new DashboardDAL(IASDatabase);
            return await obj.getAllMenus(roleId);
        }
    }
}
