﻿
using AID.DataAccess;
using BusinessModel.Role;
using Common;
using IRepository;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AID.BusinessLayer.RoleBL
{
    public class RoleBL : IRoleRepository
    {
        readonly Database IASDatabase;
        public RoleBL()
        {
            DatabaseProviderFactory factory = new DatabaseProviderFactory();
            IASDatabase = factory.Create(IASConstant.IASDataBaseConnection);
        }


        async Task<List<GetRoleResponse>> IRoleRepository.GetRoles()
        {
            RoleDAL obj = new RoleDAL(IASDatabase);
            return await obj.GetRoles();
        }

        async Task<int> IRoleRepository.UpsertRoles(UpsertRoleRequest request)
        {
            request.IPAddress = CommonFunction.GetIPAddress();
            RoleDAL obj = new RoleDAL(IASDatabase);
            return await obj.UpsertRoles(request);
        }

        async Task<int> IRoleRepository.ToggleRoleActivation(ToggleRoleRequest request)
        {
            request.IPAddress = CommonFunction.GetIPAddress();
            RoleDAL obj = new RoleDAL(IASDatabase);
            return await obj.ToggleRoleActivation(request);
        }
    }
}
