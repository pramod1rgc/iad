﻿using AID.DataAccess;
using BusinessModel.Employee;
using Common;
using IRepository;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AID.BusinessLayer.EmployeeBL
{
    public class EmployeeBL : IEmployeeRepository
    {
        readonly Database IASDatabase;
        public EmployeeBL()
        {
            DatabaseProviderFactory factory = new DatabaseProviderFactory();
            IASDatabase = factory.Create(IASConstant.IASDataBaseConnection);
        }
        async Task<List<EmployeeModel>> IEmployeeRepository.GetAllEmployee()
        {
            EmployeeDAL obj = new EmployeeDAL(IASDatabase);
            return await obj.GetAllEmployee();
        }
        async Task<List<EmployeeModel>> IEmployeeRepository.GetAllDesignation()
        {
            EmployeeDAL obj = new EmployeeDAL(IASDatabase);
            return await obj.GetAllDesignation();
        }
        async Task<string> IEmployeeRepository.SaveEmployee(string hdnEmpId, EmployeeModel objemployee)
        {
            EmployeeDAL obj = new EmployeeDAL(IASDatabase);
            return await obj.SaveEmployee(hdnEmpId, objemployee);
        }
        string IEmployeeRepository.ActiveDeactiveUser(EmployeeModel objemployee)
        {
            EmployeeDAL obj = new EmployeeDAL(IASDatabase);
            return obj.ActiveDeactiveUser(objemployee);
        }
    }
}
