﻿using AID.DataAccess.Master;
using BusinessModel.Master;
using Common;
using IRepository;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AID.BusinessLayer.RiskCategoryBL
{
    public class RiskCategoryBL : IRiskCategoryRepository
    {
        readonly Database IASDatabase;
        public RiskCategoryBL()
        {
            DatabaseProviderFactory factory = new DatabaseProviderFactory();
            IASDatabase = factory.Create(IASConstant.IASDataBaseConnection);
        }
        async Task<string> IRiskCategoryRepository.SaveRiskCategory(string hdnCategoryId, CategoryDetailsModel MstCategoryDetails)
        {
            RiskCategoryDAL obj = new RiskCategoryDAL(IASDatabase);
            return await obj.SaveRiskCategory(hdnCategoryId, MstCategoryDetails);
        }

        async Task<List<CategoryDetailsModel>> IRiskCategoryRepository.GetAllCategoryDetails()
        {
            RiskCategoryDAL obj = new RiskCategoryDAL(IASDatabase);
            return await obj.GetAllCategoryDetails();
        }
        async Task<string> IRiskCategoryRepository.ActiveDeactive(CategoryDetailsModel MstCategoryDetails)
        {
            RiskCategoryDAL obj = new RiskCategoryDAL(IASDatabase);
            return await obj.ActiveDeactive( MstCategoryDetails);
        }
        async Task<List<CategoryDetailsModel>> IRiskCategoryRepository.BindDropDawnCategory()
        {
            RiskCategoryDAL obj = new RiskCategoryDAL(IASDatabase);
            return await obj.BindDropDawnCategory();
        }
    }
}
