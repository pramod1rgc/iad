﻿using AID.DataAccess.Verification;
using BusinessModel.Verification;
using Common;
using IRepository;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AID.BusinessLayer.VerifyAuditConferenceBL
{
    public class VerifyAuditConferenceBL : IVerifyAuditConferenceRepository
    {
        readonly Database IASDatabase;
        public VerifyAuditConferenceBL()
        {
            DatabaseProviderFactory factory = new DatabaseProviderFactory();
            IASDatabase = factory.Create(IASConstant.IASDataBaseConnection);
        }

        async Task<List<GetAuditsOfConferenceForVerificationResponse>> IVerifyAuditConferenceRepository.GetAuditsOfConferenceForVerification(string type)
        {
            VerifyAuditConferenceDAL obj = new VerifyAuditConferenceDAL(IASDatabase);
            return await obj.GetAuditsOfConferenceForVerification(type);
        }
    }
}
