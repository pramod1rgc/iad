﻿using AID.DataAccess.Verification;
using BusinessModel.AuditStructure;
using Common;
using IRepository;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AID.BusinessLayer.VerifyanualplanBL
{
    public class VerifyanualplanBL: IVerifyannualplan
    {
        readonly Database IASDatabase;
        public VerifyanualplanBL()
        {
            DatabaseProviderFactory factory = new DatabaseProviderFactory();
            IASDatabase = factory.Create(IASConstant.IASDataBaseConnection);
        }



        async Task<IEnumerable<CreateAuditPlanModel>> IVerifyannualplan.Getauditlistforverify(int FYID, int RoleUserMapId)
        {
            VerifyanualplanDAL obj = new VerifyanualplanDAL(IASDatabase);
            return await obj.Getauditlistforverify(FYID, RoleUserMapId);
        }

        async Task<IEnumerable<CreateAuditPlanModel>> IVerifyannualplan.getAllPendingQuarter(int FYID,int userId)
        {
            VerifyanualplanDAL obj = new VerifyanualplanDAL(IASDatabase);
            return await obj.getAllPendingQuarter(FYID, userId);
        }
        Task<string> IVerifyannualplan.AnualplanApprove(int FYID,int RoleUserMapId)
        {
            VerifyanualplanDAL obj = new VerifyanualplanDAL(IASDatabase);
            return obj.AnualplanApprove(FYID, RoleUserMapId);
        }
        Task<string> IVerifyannualplan.RejectanualsAudit(int FYID,string rejectreson, int RoleUserMapId)
        {
            VerifyanualplanDAL obj = new VerifyanualplanDAL(IASDatabase);
            return obj.RejectanualsAudit(FYID, rejectreson, RoleUserMapId);
        }
        async Task<IEnumerable<CreateAuditPlanModel>> IVerifyannualplan.GetauditlistforverifyQuarterly(int FYID,string Quarter,int userId)
        {
            VerifyanualplanDAL obj = new VerifyanualplanDAL(IASDatabase);
            return await obj.GetauditlistforverifyQuarterly(FYID, Quarter, userId);
        }
        Task<string> IVerifyannualplan.QuarterlylplanApprove(int FYID,string Quarter,int RoleUserMapId)
        {
            VerifyanualplanDAL obj = new VerifyanualplanDAL(IASDatabase);
            return obj.QuarterlylplanApprove(FYID, Quarter, RoleUserMapId);
        }
        Task<string> IVerifyannualplan.RejectQuarterlyAudit(int FYID, string rejectreson,string Quarter, int RoleUserMapId)
        {
            VerifyanualplanDAL obj = new VerifyanualplanDAL(IASDatabase);
            return obj.RejectQuarterlyAudit(FYID, rejectreson, Quarter, RoleUserMapId);
        }

        public string SupervisorTosupervisor(string currFYear, int roleUserMapId, int userId)
        {
            VerifyanualplanDAL obj = new VerifyanualplanDAL(IASDatabase);
            return obj.SupervisorTosupervisor(currFYear, roleUserMapId, userId);
        }

        public string SupervisorTosupervisorQuarterlyPlan(string currFYear, int roleUserMapId, int userId, string Quarter)
        {
            VerifyanualplanDAL obj = new VerifyanualplanDAL(IASDatabase);
            return obj.SupervisorTosupervisorQuarterlyPlan(currFYear, roleUserMapId, userId, Quarter);
        }
    }
}
