﻿using AID.DataAccess.Auditee;
using BusinessModel.Auditee;
using Common;
using IRepository;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AID.BusinessLayer.AuditeeUniverseBL
{
 public  class AuditeeUniverseBL : IAuditeeUniverseRepository
    {
        readonly Database IASDatabase;
        public AuditeeUniverseBL()
        {
            DatabaseProviderFactory factory = new DatabaseProviderFactory();
            IASDatabase = factory.Create(IASConstant.IASDataBaseConnection);
        }
        string IAuditeeUniverseRepository.ActiveDeactiveUser(AuditeeUniverseBM objauditee)
        {
            AuditeeUniverseDAL obj = new AuditeeUniverseDAL(IASDatabase);
            return obj.ActiveDeactiveUser(objauditee);
        }  
        async Task<List<AuditeeUniverseBM>> IAuditeeUniverseRepository.GetAllAuditee(int userid)
        {
            AuditeeUniverseDAL obj = new AuditeeUniverseDAL(IASDatabase);
            return await obj.GetAllAuditee(userid);
        }
        async Task<List<AuditeeUniverseBM>> IAuditeeUniverseRepository.GetAuditeeRole()
        {
            AuditeeUniverseDAL obj = new AuditeeUniverseDAL(IASDatabase);
            return await obj.GetAuditeeRole();
        }
        async Task<List<AuditeeUniverseBM>> IAuditeeUniverseRepository.GetAllDesignation(int rollId)
        {
            AuditeeUniverseDAL obj = new AuditeeUniverseDAL(IASDatabase);
            return await obj.GetAllDesignation(rollId);
        }
        async Task<string> IAuditeeUniverseRepository.SaveAuditee(string hdnEmpId, AuditeeUniverseBM objauditee)
        {
            AuditeeUniverseDAL obj = new AuditeeUniverseDAL(IASDatabase);
            return await obj.SaveAuditee(hdnEmpId, objauditee);
        }
      
    }
}
