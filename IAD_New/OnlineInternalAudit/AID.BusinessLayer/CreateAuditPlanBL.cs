﻿using AID.DataAccess;
using BusinessModel.AuditStructure;
using Common;
using IRepository;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AID.BusinessLayer.CreateAuditPlanBL
{
    public class CreateAuditPlanBL:ICreateAuditPlan
    {
        readonly Database IASDatabase;
        public CreateAuditPlanBL()
        {
            DatabaseProviderFactory factory = new DatabaseProviderFactory();
            IASDatabase = factory.Create(IASConstant.IASDataBaseConnection);
        }
        async Task<List<CreateAuditPlanModel>> ICreateAuditPlan.BindFYearDropDown()
        {
            CreateAuditPlanDAL obj = new CreateAuditPlanDAL(IASDatabase);
            return await obj.BindFYearDropDown();
        }
        async Task<List<CreateAuditPlanModel>> ICreateAuditPlan.pendingFinancialForVerifyQuarter(int roleUserMapId)
        {
            CreateAuditPlanDAL obj = new CreateAuditPlanDAL(IASDatabase);
            return await obj.pendingFinancialForVerifyQuarter(roleUserMapId);
        }
        /// <summary>
        /// Get All Controller
        /// </summary>
        /// <returns></returns>
        async Task<List<CreateAuditPlanModel>> ICreateAuditPlan.GetController()
        {
            CreateAuditPlanDAL obj = new CreateAuditPlanDAL(IASDatabase);
            return await obj.GetController();
        }

        async Task<List<CreateAuditPlanModel>> ICreateAuditPlan.GetAllPAO(int ControllerID)
        {
            CreateAuditPlanDAL obj = new CreateAuditPlanDAL(IASDatabase);
            return await obj.GetAllPAO(ControllerID);
        }
       string ICreateAuditPlan.AssignAuditByController(int PAOID,string Status, string currFYear,string paoreason)
        {
            CreateAuditPlanDAL obj = new CreateAuditPlanDAL(IASDatabase);
            return  obj.AssignAuditByController(PAOID, Status,currFYear, paoreason);
        }

        string ICreateAuditPlan.AssignToSupervisor( string currFYear,int roleUserMapId,int userId)
        {
            CreateAuditPlanDAL obj = new CreateAuditPlanDAL(IASDatabase);
            return obj.AssignToSupervisor( currFYear, roleUserMapId, userId);
        }

        async Task<IEnumerable<CreateAuditPlanModel>> ICreateAuditPlan.Getauditlist( int FYID)
        {
            CreateAuditPlanDAL obj = new CreateAuditPlanDAL(IASDatabase);
            return await obj.Getauditlist(FYID);
        }
        async Task<string> ICreateAuditPlan.GetassignPaoAudit(List<CreateAuditPlanModel> ObjcreateAuditPlan)
        {
            CreateAuditPlanDAL obj = new CreateAuditPlanDAL(IASDatabase);
            return await obj.GetassignPaoAudit(ObjcreateAuditPlan);
        }

        async Task<IEnumerable<CreateAuditPlanModel>> ICreateAuditPlan.BindAuditInGrid(int FYID)
        {
            CreateAuditPlanDAL obj = new CreateAuditPlanDAL(IASDatabase);
            return await obj.BindAuditInGrid(FYID);
        }

        async Task<string> ICreateAuditPlan.SaveQuartalyAudit(List<CreateAuditPlanModel> objCreateAuditPlanModel)
        {
            CreateAuditPlanDAL obj = new CreateAuditPlanDAL(IASDatabase);
            return await obj.SaveQuartalyAudit(objCreateAuditPlanModel);
        }

        async Task<string> ICreateAuditPlan.SaveQuartalyPlanAudit(List<object> objquarterpaoplan,string financialYearId)
        {
            CreateAuditPlanDAL obj = new CreateAuditPlanDAL(IASDatabase);
            return await obj.SaveQuartalyPlanAudit(objquarterpaoplan, financialYearId);
        }
        string ICreateAuditPlan.AssignQuarterlyPlanToSupervisor(string currFYear, int roleUserMapId, int userId)
        {
            CreateAuditPlanDAL obj = new CreateAuditPlanDAL(IASDatabase);
            return obj.AssignQuarterlyPlanToSupervisor(currFYear, roleUserMapId,userId);
        }

        public async Task<List<CreateAuditPlanModel>> BindSupervisorDropDown(int userId)
        {
            CreateAuditPlanDAL obj = new CreateAuditPlanDAL(IASDatabase);
            return await obj.BindSupervisorDropDown(userId);
        }
    }
}
