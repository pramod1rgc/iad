﻿using AID.DataAccess.ChangePassword;
using BusinessModel.ChangePassword;
using Common;
using IRepository;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AID.BusinessLayer.ChangePasswordBL
{
  public class ChangePasswordBL : IChangePassword
    {
        readonly Database IASDatabase;
        public ChangePasswordBL()
        {
            DatabaseProviderFactory factory = new DatabaseProviderFactory();
            IASDatabase = factory.Create(IASConstant.IASDataBaseConnection);
        }
        /// <summary>
        /// ChangePassword
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        async Task<string> IChangePassword.ChangePassword(ChangePasswordBM request)
        {
            ChangePasswordDAL obj = new ChangePasswordDAL(IASDatabase);
               return await obj.ChangePassword(request);
        }



    }
}
