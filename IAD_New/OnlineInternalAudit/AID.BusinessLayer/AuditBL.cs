﻿using AID.DataAccess.Audits;
using BusinessModel.Audits;
using Common;
using IRepository;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AID.BusinessLayer.AuditBL
{
    public class AuditBL : IAuditsRepository
    {
        readonly Database IASDatabase;
        public AuditBL()
        {
            DatabaseProviderFactory factory = new DatabaseProviderFactory();
            IASDatabase = factory.Create(IASConstant.IASDataBaseConnection);
        }


        async Task<List<GetApprovedScheduledAuditsResponse>> IAuditsRepository.GetApprovedScheduledAudits(int finYearId, string quarter, int roleUserMapId)
        {
            AuditScheduleDAL obj = new AuditScheduleDAL(IASDatabase);
            return await obj.GetApprovedScheduledAudits(finYearId, quarter, roleUserMapId);
        }

        async Task<List<GetApprovedAuditTeamResponse>> IAuditsRepository.GetApprovedAuditTeam(int auditId)
        {
            AuditTeamDAL obj = new AuditTeamDAL(IASDatabase);
            return await obj.GetApprovedAuditTeam(auditId);
        }

        async Task<int> IAuditsRepository.RevertAuditTeam(RevertAuditTeamRequest request)
        {
            AuditTeamDAL obj = new AuditTeamDAL(IASDatabase);
            return await obj.RevertAuditTeam(request);
        }

        async Task<List<GetApprovedAuditTeamAuditsForDropdownResponse>> IAuditsRepository.GetApprovedAuditTeamAuditsForDropdown(int roleUserMapId)
        {
            AuditTeamDAL obj = new AuditTeamDAL(IASDatabase);
            return await obj.GetApprovedAuditTeamAuditsForDropdown(roleUserMapId);
        }
    }
}
