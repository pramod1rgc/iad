﻿using AID.DataAccess.AuditStructure;

using BusinessModel.AuditStructure;
using Common;
using IRepository;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AID.BusinessLayer.ScheduleAuditBL
{
    public class ScheduleAuditBL : IScheduleAuditRepository
    {
        readonly Database IASDatabase;
        public ScheduleAuditBL()
        {
            DatabaseProviderFactory factory = new DatabaseProviderFactory();
            IASDatabase = factory.Create(IASConstant.IASDataBaseConnection);
        }


        async Task<List<GetScheduledAuditsResponse>> IScheduleAuditRepository.GetScheduledAudits(int finYearId, string quarter)
        {
            ScheduleAuditDAL obj = new ScheduleAuditDAL(IASDatabase);
            return await obj.GetScheduledAudits(finYearId, quarter);
        }

        async Task<int> IScheduleAuditRepository.UpdateScheduledAudit(UpdateScheduleAuditRequest request)
        {
            ScheduleAuditDAL obj = new ScheduleAuditDAL(IASDatabase);
            request.IPAddress = CommonFunction.GetIPAddress();
            return await obj.UpdateScheduledAudit(request);
        }

        async Task<int> IScheduleAuditRepository.DeleteScheduledAudit(DeleteScheduleAuditRequest request)
        {
            ScheduleAuditDAL obj = new ScheduleAuditDAL(IASDatabase);
            request.IPAddress = CommonFunction.GetIPAddress();
            return await obj.DeleteScheduledAudit(request);
        }
    }
}
