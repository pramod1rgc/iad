﻿using AID.DataAccess.AuditeeObservation;
using BusinessModel.AuditeeObservation;
using Common;
using IRepository;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AID.BusinessLayer.AuditeeObservationBL
{
    public class AuditeeObservationBL : IAuditeeObservationRepository
    {
        readonly Database IASDatabase;
        public AuditeeObservationBL()
        {
            DatabaseProviderFactory factory = new DatabaseProviderFactory();
            IASDatabase = factory.Create(IASConstant.IASDataBaseConnection);
        }

        async Task<List<GetAuditeeDHAuditsListResponse>> IAuditeeObservationRepository.GetAuditDHAuditsList(int auditUserId)
        {
            AuditeeEntryConferenceDAL obj = new AuditeeEntryConferenceDAL(IASDatabase);
            return await obj.GetAuditDHAuditsList(auditUserId);
        }

        async Task<List<AuditeeEntryConferenceResponse>> IAuditeeObservationRepository.GetAuditEntryConference(int auditId)
        {
            AuditeeEntryConferenceDAL obj = new AuditeeEntryConferenceDAL(IASDatabase);
            var response = await obj.GetAuditEntryConference(auditId);

            var groupedByData = (from a in response
                                 group a by a.EntryConferenceId);
            List<AuditeeEntryConferenceResponse> res = new List<AuditeeEntryConferenceResponse>();
            foreach (var data in groupedByData)
            {
                AuditeeEntryConferenceResponse ob = new AuditeeEntryConferenceResponse();
                ob.AuditId = data.FirstOrDefault().AuditId;
                ob.EntryConferenceId = data.FirstOrDefault().EntryConferenceId;
                ob.ConferenceDate = data.FirstOrDefault().ConferenceDate;
                ob.Agenda = data.FirstOrDefault().Agenda;
                ob.Participants = new List<AuditeeEntryConferenceParticipants>();
                foreach (var emp in data)
                {
                    AuditeeEntryConferenceParticipants em = new AuditeeEntryConferenceParticipants();
                    em.EntryConferenceParticipantId = emp.EntryConferenceParticipantId;
                    em.AuditUserMapId = emp.AuditUserMapId;
                    em.EmpID = emp.EmpID;
                    em.EmpName = emp.EmpName;
                    em.DesignationId = emp.DesignationId;
                    em.DesignationName = emp.DesignationName;
                    em.RoleId = emp.RoleId;
                    ob.Participants.Add(em);
                }
                res.Add(ob);
            }
            return res;
        }

        async Task<int> IAuditeeObservationRepository.UpdateAuditEntryConference(UpdateAuditeeEntryConferenceRequest request)
        {
            AuditeeEntryConferenceDAL obj = new AuditeeEntryConferenceDAL(IASDatabase);
            request.UserIPAddress = CommonFunction.GetIPAddress();
            return await obj.UpdateAuditEntryConference(request);
        }

        async Task<List<AuditeeExitConferenceResponse>> IAuditeeObservationRepository.GetAuditExitConference(int auditId)
        {
            AuditeeExitConferenceDAL obj = new AuditeeExitConferenceDAL(IASDatabase);
            var response = await obj.GetAuditExitConference(auditId);

            var groupedByData = (from a in response
                                 group a by a.ExitConferenceId);
            List<AuditeeExitConferenceResponse> res = new List<AuditeeExitConferenceResponse>();
            foreach (var data in groupedByData)
            {
                AuditeeExitConferenceResponse ob = new AuditeeExitConferenceResponse();
                ob.AuditId = data.FirstOrDefault().AuditId;
                ob.ExitConferenceId = data.FirstOrDefault().ExitConferenceId;
                ob.ConferenceDate = data.FirstOrDefault().ConferenceDate;
                ob.Agenda = data.FirstOrDefault().Agenda;
                ob.Decision = data.FirstOrDefault().Decision;
                ob.Participants = new List<AuditeeExitConferenceParticipants>();
                foreach (var emp in data)
                {
                    AuditeeExitConferenceParticipants em = new AuditeeExitConferenceParticipants();
                    em.ExitConferenceParticipantId = emp.ExitConferenceParticipantId;
                    em.AuditUserMapId = emp.AuditUserMapId;
                    em.EmpID = emp.EmpID;
                    em.EmpName = emp.EmpName;
                    em.DesignationId = emp.DesignationId;
                    em.DesignationName = emp.DesignationName;
                    em.RoleId = emp.RoleId;
                    ob.Participants.Add(em);
                }
                res.Add(ob);
            }
            return res;
        }

        async Task<int> IAuditeeObservationRepository.UpdateAuditExitConference(UpdateAuditeeExitConferenceRequest request)
        {
            AuditeeExitConferenceDAL obj = new AuditeeExitConferenceDAL(IASDatabase);
            request.UserIPAddress = CommonFunction.GetIPAddress();
            return await obj.UpdateAuditExitConference(request);
        }

        async Task<List<GetAuditeeParaResponse>> IAuditeeObservationRepository.GetAuditPara(int roleId,int auditId, int pageSize, int pageNumber, string searchTerm)
        {
            AuditeeParaDAL obj = new AuditeeParaDAL(IASDatabase);
            return await obj.GetAuditPara(roleId,auditId, pageSize, pageNumber, searchTerm);
        }

        async Task<int> IAuditeeObservationRepository.UpdateAuditPara(UpdateAuditeeParaRequest request)
        {
            AuditeeParaDAL obj = new AuditeeParaDAL(IASDatabase);
            request.IPAddress = CommonFunction.GetIPAddress();
            return await obj.UpdateAuditPara(request);
        }

        async Task<GetAuditeeParaDetailsResponse> IAuditeeObservationRepository.GetAuditParaDetails(int paraId)
        {
            AuditeeParaDAL obj = new AuditeeParaDAL(IASDatabase);
            var response = await obj.GetAuditParaDetails(paraId);
            var groupedByData = (from a in response
                                 group a by a.ParaId);
            GetAuditeeParaDetailsResponse res = new GetAuditeeParaDetailsResponse();
            foreach (var data in groupedByData)
            {
                res.ParaId = data.FirstOrDefault().ParaId;
                res.AuditId = data.FirstOrDefault().AuditId;
                res.Title = data.FirstOrDefault().Title;
                res.Criteria = data.FirstOrDefault().Criteria;
                res.Conditions = data.FirstOrDefault().Conditions;
                res.Consequences = data.FirstOrDefault().Consequences;
                res.Causes = data.FirstOrDefault().Causes;
                res.CorrectiveActions = data.FirstOrDefault().CorrectiveActions;
                res.Severity = data.FirstOrDefault().Severity;
                res.RiskCategoryId = data.FirstOrDefault().RiskCategoryId;
                res.RiskCategoryDesc = data.FirstOrDefault().RiskCategoryDesc;
                res.CreatedOn = data.FirstOrDefault().CreatedOn;
                res.RejectionRemark = data.FirstOrDefault().RejectionRemark;
                res.StatusId = data.FirstOrDefault().StatusId;
                res.AuditeeStatusId = data.FirstOrDefault().AuditeeStatusId;
                res.Files = new List<GetParaFileDetails>();
                foreach (var file in data)
                {
                    GetParaFileDetails ob = new GetParaFileDetails();
                    ob.ParaFileId = file.ParaFileId;
                    ob.FileId = file.FileId;
                    ob.FileName = file.FileName;
                    ob.FileSize = file.FileSize;
                    ob.FileType = file.FileType;
                    ob.RelativePath = file.RelativePath;
                    ob.FileUrl = file.FileUrl;
                    res.Files.Add(ob);
                }
            }
            return res;
        }

        async Task<int> IAuditeeObservationRepository.DeleteAuditPara(DeleteAuditeeParaRequest request)
        {
            AuditeeParaDAL obj = new AuditeeParaDAL(IASDatabase);
            request.IPAddress = CommonFunction.GetIPAddress();
            return await obj.DeleteAuditPara(request);
        }

        async Task<int> IAuditeeObservationRepository.PostParaCommentByAuditor(PostParaCommentByAuditorRequest request)
        {
            AuditeeParaDAL obj = new AuditeeParaDAL(IASDatabase);
            request.IPAddress = CommonFunction.GetIPAddress();
            return await obj.PostParaCommentByAuditor(request);
        }

        async Task<List<GetParaCommentResponse>> IAuditeeObservationRepository.GetParaComments(int paraId)
        {
            AuditeeParaDAL obj = new AuditeeParaDAL(IASDatabase);
            var response = await obj.GetParaComments(paraId);

            var groupedByData = (from a in response
                                 group a by a.CommentId);
            List<GetParaCommentResponse> res = new List<GetParaCommentResponse>();
            foreach (var data in groupedByData)
            {
                GetParaCommentResponse ob = new GetParaCommentResponse();
                ob.CommentId = data.FirstOrDefault().CommentId;
                ob.ParaId = data.FirstOrDefault().ParaId;
                ob.AuditUserMapId = data.FirstOrDefault().AuditUserMapId;
                ob.Comment = data.FirstOrDefault().Comment;
                ob.CreatedOn = data.FirstOrDefault().CreatedOn;
                ob.RoleId = data.FirstOrDefault().RoleId;
                ob.RoleName = data.FirstOrDefault().RoleName;
                ob.UserId = data.FirstOrDefault().UserId;
                ob.EmpId = data.FirstOrDefault().EmpId;
                ob.EmpName = data.FirstOrDefault().EmpName;
                ob.Files = new List<GetParaCommentFileResponse>();
                foreach (var file in data)
                {
                    GetParaCommentFileResponse f = new GetParaCommentFileResponse();
                    f.CommentFileId = file.CommentFileId;
                    f.FileId = file.FileId;
                    f.FileName = file.FileName;
                    f.FileSize = file.FileSize;
                    f.FileType = file.FileType;
                    f.RelativePath = file.RelativePath;
                    f.FileUrl = file.FileUrl;
                    ob.Files.Add(f);
                }
                res.Add(ob);
            }
            return res.OrderBy(a => a.CreatedOn).ToList();
        }

        async Task<GetAuditeeParaSettlementLetterResponse> IAuditeeObservationRepository.GetAuditParaSettlementLetter(int auditId)
        {
            AuditeeParaSettlementLetterDAL obj = new AuditeeParaSettlementLetterDAL(IASDatabase);
            return await obj.GetAuditParaSettlementLetter(auditId);
        }

        async Task<int> IAuditeeObservationRepository.UpdateAuditParaSettlementLetter(UpdateAuditeeParaSettlementLetterRequest request)
        {
            AuditeeParaSettlementLetterDAL obj = new AuditeeParaSettlementLetterDAL(IASDatabase);
            request.IPAddress = CommonFunction.GetIPAddress();
            return await obj.UpdateAuditParaSettlementLetter(request);
        }

        async Task<int> IAuditeeObservationRepository.UpdateParaSettlementLetterStatus(int auditId, int statusId, string rejectionRemark)
        {
            AuditeeParaSettlementLetterDAL obj = new AuditeeParaSettlementLetterDAL(IASDatabase);
            return await obj.UpdateParaSettlementLetterStatus(auditId, statusId, rejectionRemark);
        }

        async Task<int> IAuditeeObservationRepository.UpdateAuditstatus(AuditeeStatusUpdateRequest request)
        {
            AuditeeEntryConferenceDAL obj = new AuditeeEntryConferenceDAL(IASDatabase);
            return await obj.UpdateAuditstatus(request);
        }

        async Task<int> IAuditeeObservationRepository.UpdateAuditParaStatus(AuditeeParaStatusRequest request)
        {
            AuditeeParaDAL obj = new AuditeeParaDAL(IASDatabase);
            return await obj.UpdateAuditParaStatus(request);
        }
    }
}
