﻿using AID.DataAccess.AuditProgramme;
using BusinessModel.AuditProgramme;
using Common;
using IRepository;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AID.BusinessLayer.AuditProgrammeBL
{
    public class AuditProgrammeBL: IAuditProgrammeRepository
    {
        readonly Database IASDatabase;
        public AuditProgrammeBL()
        {
            DatabaseProviderFactory factory = new DatabaseProviderFactory();
            IASDatabase = factory.Create(IASConstant.IASDataBaseConnection);
        }

        async Task<List<AuditProgrammeResponse>> IAuditProgrammeRepository.GetAuditProgrammes(int finYear)
        {
            AuditProgrammeDAL obj = new AuditProgrammeDAL(IASDatabase);
            var response = await obj.GetAuditProgrammes(finYear);

            var groupedByData = (from a in response
                                group a by a.AuditId);
            List<AuditProgrammeResponse> res = new List<AuditProgrammeResponse>();
            foreach (var data in groupedByData)
            {
                AuditProgrammeResponse ob = new AuditProgrammeResponse();
                ob.AuditId = data.FirstOrDefault().AuditId;
                ob.Name = data.FirstOrDefault().Name;
                ob.PaoId = data.FirstOrDefault().PaoId;
                ob.AuditFromDate = data.FirstOrDefault().AuditFromDate;
                ob.AuditToDate = data.FirstOrDefault().AuditToDate;
                ob.EmpDetails = new List<EmpDetails>();
                foreach(var emp in data)
                {
                    EmpDetails em = new EmpDetails();
                    em.EmpName = emp.EmpName;
                    em.DesignationName = emp.DesignationName;
                    ob.EmpDetails.Add(em);
                }
                res.Add(ob);
            }
            return res;
        }
    }
}
