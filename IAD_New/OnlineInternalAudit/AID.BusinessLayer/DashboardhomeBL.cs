﻿using AID.DataAccess.Dashboard;
using BusinessModel.AuditStructure;
using Common;
using IRepository;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AID.BusinessLayer.DashboardhomeBL
{
   public class DashboardhomeBL: IDashboardhome
    {
        readonly Database IASDatabase;
        public DashboardhomeBL()
        {
            DatabaseProviderFactory factory = new DatabaseProviderFactory();
            IASDatabase = factory.Create(IASConstant.IASDataBaseConnection);
        }

        async Task<object[]> IDashboardhome.GetAllDashboardRecord(string UserName)
        {
            DashboardhomeDAL obj = new DashboardhomeDAL(IASDatabase);
            return await obj.GetAllDashboardRecord(UserName);
        }
        

        async Task<object[]> IDashboardhome.GetAuditDhDashboardAllRecord(string UserName)
        {
            DashboardhomeDAL obj = new DashboardhomeDAL(IASDatabase);
            return await obj.GetAuditDhDashboardAllRecord(UserName);
        }

        async Task<object[]> IDashboardhome.GetAll_IADHead_DashboardRecord(string UserName)
        {
            DashboardhomeDAL obj = new DashboardhomeDAL(IASDatabase);
            return await obj.GetAll_IADHead_DashboardRecord(UserName);
        }

        async Task<List<GetScheduledAuditsResponse>> IDashboardhome.getallschedule()
        {
            DashboardhomeDAL obj = new DashboardhomeDAL(IASDatabase);
            return await obj.getallschedule();
        }
        async Task<object[]> IDashboardhome.GetallAuditInchart()
        {
            DashboardhomeDAL obj = new DashboardhomeDAL(IASDatabase);
            return await obj.GetallAuditInchart();
        }
        async Task<object[]> IDashboardhome.GetallAuditDHInchart(string userid)
        {
            DashboardhomeDAL obj = new DashboardhomeDAL(IASDatabase);
            return await obj.GetallAuditDHInchart(userid);
        }
        async Task<object[]> IDashboardhome.Getall_IADHeadInchart  (string userid)
        {
            DashboardhomeDAL obj = new DashboardhomeDAL(IASDatabase);
            return await obj.Getall_IADHeadInchart(userid);
        }

    }
}
