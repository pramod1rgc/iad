﻿using AID.DataAccess.AuditObservation;

using BusinessModel.AuditObservation;
using Common;
using IRepository;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AID.BusinessLayer.AuditObservationBL
{
    public class AuditObservationBL : IAuditObservationRepository
    {
        readonly Database IASDatabase;
        public AuditObservationBL()
        {
            DatabaseProviderFactory factory = new DatabaseProviderFactory();
            IASDatabase = factory.Create(IASConstant.IASDataBaseConnection);
        }

        async Task<List<GetAuditDHAuditsListResponse>> IAuditObservationRepository.GetAuditDHAuditsList(int auditUserId)
        {
            AuditeeEntryConferenceDAL obj = new AuditeeEntryConferenceDAL(IASDatabase);
            return await obj.GetAuditDHAuditsList(auditUserId);
        }

        async Task<List<AuditEntryConferenceResponse>> IAuditObservationRepository.GetAuditEntryConference(int auditId)
        {
            AuditeeEntryConferenceDAL obj = new AuditeeEntryConferenceDAL(IASDatabase);
            var response = await obj.GetAuditEntryConference(auditId);

            var groupedByData = (from a in response
                                 group a by a.EntryConferenceId);
            List<AuditEntryConferenceResponse> res = new List<AuditEntryConferenceResponse>();
            foreach (var data in groupedByData)
            {
                AuditEntryConferenceResponse ob = new AuditEntryConferenceResponse();
                ob.AuditId = data.FirstOrDefault().AuditId;
                ob.EntryConferenceId = data.FirstOrDefault().EntryConferenceId;
                ob.ConferenceDate = data.FirstOrDefault().ConferenceDate;
                ob.Agenda = data.FirstOrDefault().Agenda;
                ob.StatusId = data.FirstOrDefault().StatusId;
                ob.Status = data.FirstOrDefault().Status;
                ob.RejectionRemark = data.FirstOrDefault().RejectionRemark;
                ob.Participants = new List<AuditEntryConferenceParticipants>();
                foreach (var emp in data)
                {
                    AuditEntryConferenceParticipants em = new AuditEntryConferenceParticipants();
                    em.EntryConferenceParticipantId = emp.EntryConferenceParticipantId;
                    em.AuditUserMapId = emp.AuditUserMapId;
                    em.EmpID = emp.EmpID;
                    em.EmpName = emp.EmpName;
                    em.DesignationId = emp.DesignationId;
                    em.DesignationName = emp.DesignationName;
                    em.RoleId = emp.RoleId;
                    ob.Participants.Add(em);
                }
                res.Add(ob);
            }
            return res;
        }

        async Task<int> IAuditObservationRepository.UpdateAuditEntryConference(UpdateAuditEntryConferenceRequest request)
        {
            AuditeeEntryConferenceDAL obj = new AuditeeEntryConferenceDAL(IASDatabase);
            request.UserIPAddress = CommonFunction.GetIPAddress();
            return await obj.UpdateAuditEntryConference(request);
        }

        async Task<List<AuditExitConferenceResponse>> IAuditObservationRepository.GetAuditExitConference(int auditId)
        {
            AuditExitConferenceDAL obj = new AuditExitConferenceDAL(IASDatabase);
            var response = await obj.GetAuditExitConference(auditId);

            var groupedByData = (from a in response
                                 group a by a.ExitConferenceId);
            List<AuditExitConferenceResponse> res = new List<AuditExitConferenceResponse>();
            foreach (var data in groupedByData)
            {
                AuditExitConferenceResponse ob = new AuditExitConferenceResponse();
                ob.AuditId = data.FirstOrDefault().AuditId;
                ob.ExitConferenceId = data.FirstOrDefault().ExitConferenceId;
                ob.ConferenceDate = data.FirstOrDefault().ConferenceDate;
                ob.Agenda = data.FirstOrDefault().Agenda;
                ob.Decision = data.FirstOrDefault().Decision;
                ob.StatusId = data.FirstOrDefault().StatusId;
                ob.Status = data.FirstOrDefault().Status;
                ob.RejectionRemark = data.FirstOrDefault().RejectionRemark;
                ob.Participants = new List<AuditExitConferenceParticipants>();
                foreach (var emp in data)
                {
                    AuditExitConferenceParticipants em = new AuditExitConferenceParticipants();
                    em.ExitConferenceParticipantId = emp.ExitConferenceParticipantId;
                    em.AuditUserMapId = emp.AuditUserMapId;
                    em.EmpID = emp.EmpID;
                    em.EmpName = emp.EmpName;
                    em.DesignationId = emp.DesignationId;
                    em.DesignationName = emp.DesignationName;
                    em.RoleId = emp.RoleId;
                    ob.Participants.Add(em);
                }
                res.Add(ob);
            }
            return res;
        }

        async Task<int> IAuditObservationRepository.UpdateAuditExitConference(UpdateAuditExitConferenceRequest request)
        {
            AuditExitConferenceDAL obj = new AuditExitConferenceDAL(IASDatabase);
            request.UserIPAddress = CommonFunction.GetIPAddress();
            return await obj.UpdateAuditExitConference(request);
        }

        async Task<List<GetAuditParaResponse>> IAuditObservationRepository.GetAuditPara(int auditId, int pageSize, int pageNumber, string searchTerm)
        {
            AuditParaDAL obj = new AuditParaDAL(IASDatabase);
            return await obj.GetAuditPara(auditId, pageSize, pageNumber, searchTerm);
        }

        async Task<int> IAuditObservationRepository.UpdateAuditPara(UpdateAuditParaRequest request)
        {
            AuditParaDAL obj = new AuditParaDAL(IASDatabase);
            request.IPAddress = CommonFunction.GetIPAddress();
            return await obj.UpdateAuditPara(request);
        }

        async Task<GetAuditParaDetailsResponse> IAuditObservationRepository.GetAuditParaDetails(int paraId)
        {
            AuditParaDAL obj = new AuditParaDAL(IASDatabase);
            var response = await obj.GetAuditParaDetails(paraId);
            var groupedByData = (from a in response
                                 group a by a.ParaId);
            GetAuditParaDetailsResponse res = new GetAuditParaDetailsResponse();
            foreach (var data in groupedByData)
            {
                res.ParaId = data.FirstOrDefault().ParaId;
                res.AuditId = data.FirstOrDefault().AuditId;
                res.Title = data.FirstOrDefault().Title;
                res.Criteria = data.FirstOrDefault().Criteria;
                res.Conditions = data.FirstOrDefault().Conditions;
                res.Consequences = data.FirstOrDefault().Consequences;
                res.Causes = data.FirstOrDefault().Causes;
                res.CorrectiveActions = data.FirstOrDefault().CorrectiveActions;
                res.Severity = data.FirstOrDefault().Severity;
                res.RiskCategoryId = data.FirstOrDefault().RiskCategoryId;
                res.RiskCategoryDesc = data.FirstOrDefault().RiskCategoryDesc;
                res.CreatedOn = data.FirstOrDefault().CreatedOn;
                res.RejectionRemark = data.FirstOrDefault().RejectionRemark;
                res.StatusId = data.FirstOrDefault().StatusId;
                res.AuditeeStatusId = data.FirstOrDefault().AuditeeStatusId;
                res.Files = new List<GetParaFileDetails>();
                foreach (var file in data)
                {
                    GetParaFileDetails ob = new GetParaFileDetails();
                    ob.ParaFileId = file.ParaFileId;
                    ob.FileId = file.FileId;
                    ob.FileName = file.FileName;
                    ob.FileSize = file.FileSize;
                    ob.FileType = file.FileType;
                    ob.RelativePath = file.RelativePath;
                    ob.FileUrl = file.FileUrl;
                    res.Files.Add(ob);
                }
            }
            return res;
        }

        async Task<int> IAuditObservationRepository.DeleteAuditPara(DeleteAuditParaRequest request)
        {
            AuditParaDAL obj = new AuditParaDAL(IASDatabase);
            request.IPAddress = CommonFunction.GetIPAddress();
            return await obj.DeleteAuditPara(request);
        }

        async Task<int> IAuditObservationRepository.PostParaCommentByAuditor(PostParaCommentByAuditorRequest request)
        {
            AuditParaDAL obj = new AuditParaDAL(IASDatabase);
            request.IPAddress = CommonFunction.GetIPAddress();
            return await obj.PostParaCommentByAuditor(request);
        }

        async Task<List<GetParaCommentResponse>> IAuditObservationRepository.GetParaComments(int paraId)
        {
            AuditParaDAL obj = new AuditParaDAL(IASDatabase);
            var response = await obj.GetParaComments(paraId);

            var groupedByData = (from a in response
                                 group a by a.CommentId);
            List<GetParaCommentResponse> res = new List<GetParaCommentResponse>();
            foreach (var data in groupedByData)
            {
                GetParaCommentResponse ob = new GetParaCommentResponse();
                ob.CommentId = data.FirstOrDefault().CommentId;
                ob.ParaId = data.FirstOrDefault().ParaId;
                ob.AuditUserMapId = data.FirstOrDefault().AuditUserMapId;
                ob.Comment = data.FirstOrDefault().Comment;
                ob.CreatedOn = data.FirstOrDefault().CreatedOn;
                ob.RoleId = data.FirstOrDefault().RoleId;
                ob.RoleName = data.FirstOrDefault().RoleName;
                ob.UserId = data.FirstOrDefault().UserId;
                ob.EmpId = data.FirstOrDefault().EmpId;
                ob.EmpName = data.FirstOrDefault().EmpName;
                ob.Files = new List<GetParaCommentFileResponse>();
                foreach (var file in data)
                {
                    GetParaCommentFileResponse f = new GetParaCommentFileResponse();
                    f.CommentFileId = file.CommentFileId;
                    f.FileId = file.FileId;
                    f.FileName = file.FileName;
                    f.FileSize = file.FileSize;
                    f.FileType = file.FileType;
                    f.RelativePath = file.RelativePath;
                    f.FileUrl = file.FileUrl;
                    ob.Files.Add(f);
                }
                res.Add(ob);
            }
            return res.OrderBy(a => a.CreatedOn).ToList();
        }

        async Task<GetAuditParaSettlementLetterResponse> IAuditObservationRepository.GetAuditParaSettlementLetter(int auditId)
        {
            AuditParaSettlementLetterDAL obj = new AuditParaSettlementLetterDAL(IASDatabase);
            return await obj.GetAuditParaSettlementLetter(auditId);
        }

        async Task<int> IAuditObservationRepository.UpdateAuditParaSettlementLetter(UpdateAuditParaSettlementLetterRequest request)
        {
            AuditParaSettlementLetterDAL obj = new AuditParaSettlementLetterDAL(IASDatabase);
            request.IPAddress = CommonFunction.GetIPAddress();
            return await obj.UpdateAuditParaSettlementLetter(request);
        }

        async Task<int> IAuditObservationRepository.UpdateParaSettlementLetterStatus(int auditId, int statusId, string rejectionRemark)
        {
            AuditParaSettlementLetterDAL obj = new AuditParaSettlementLetterDAL(IASDatabase);
            return await obj.UpdateParaSettlementLetterStatus(auditId, statusId, rejectionRemark);
        }

        async Task<int> IAuditObservationRepository.UpdateAuditstatus(AuditStatusUpdateRequest request)
        {
            AuditeeEntryConferenceDAL obj = new AuditeeEntryConferenceDAL(IASDatabase);
            request.IPAddress = CommonFunction.GetIPAddress();
            return await obj.UpdateAuditstatus(request);
        }

        async Task<int> IAuditObservationRepository.UpdateEntryConferenceStatus(UpdateEntryConferenceStatusRequest request)
        {
            AuditeeEntryConferenceDAL obj = new AuditeeEntryConferenceDAL(IASDatabase);
            request.IpAddress = CommonFunction.GetIPAddress();
            return await obj.UpdateEntryConferenceStatus(request);
        }

        async Task<int> IAuditObservationRepository.UpdateExitConferenceStatus(UpdateExitConferenceStatusRequest request)
        {
            AuditExitConferenceDAL obj = new AuditExitConferenceDAL(IASDatabase);
            request.IpAddress = CommonFunction.GetIPAddress();
            return await obj.UpdateExitConferenceStatus(request);
        }

        async Task<int> IAuditObservationRepository.UpdateAuditParaStatus(AuditParaStatusRequest request)
        {
            AuditParaDAL obj = new AuditParaDAL(IASDatabase);
            return await obj.UpdateAuditParaStatus(request);
        }

        async Task<int> IAuditObservationRepository.UpdateAuditComplianceStatus(AuditParaStatusRequest request)
        {
            AuditParaDAL obj = new AuditParaDAL(IASDatabase);
            return await obj.UpdateAuditComplianceStatus(request);
        }

        async Task<List<GetAllFinancialImplicationCasesResponse>> IAuditObservationRepository.GetAllFinancialImplicationCases()
        {
            AuditParaDAL obj = new AuditParaDAL(IASDatabase);
            return await obj.GetAllFinancialImplicationCases();
        }

        async Task<List<GetAuditParaFinancialImplicationCasesResponse>> IAuditObservationRepository.GetAuditParaFinancialImplicationCases(int paraId)
        {
            AuditParaDAL obj = new AuditParaDAL(IASDatabase);
            return await obj.GetAuditParaFinancialImplicationCases(paraId);
        }
    }
}
