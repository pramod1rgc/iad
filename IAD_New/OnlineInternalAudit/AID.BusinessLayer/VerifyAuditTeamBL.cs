﻿using AID.DataAccess.Verification;

using BusinessModel.Verification;
using Common;
using IRepository;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AID.BusinessLayer.VerifyAuditTeamBL
{
    public class VerifyAuditTeamBL : IVerifyAuditTeamRepository
    {
        readonly Database IASDatabase;
        public VerifyAuditTeamBL()
        {
            DatabaseProviderFactory factory = new DatabaseProviderFactory();
            IASDatabase = factory.Create(IASConstant.IASDataBaseConnection);
        }

        async Task<List<GetAuditTeamForVerificationResponse>> IVerifyAuditTeamRepository.GetTeamOfAuditForVerification(int auditId)
        {
            VerifyAuditTeamDAL obj = new VerifyAuditTeamDAL(IASDatabase);
            return await obj.GetTeamOfAuditForVerification(auditId);
        }

        async Task<int> IVerifyAuditTeamRepository.UpdateTeamOfAuditForVerification(UpdateAuditTeamForVerificationRequest request)
        {
            VerifyAuditTeamDAL obj = new VerifyAuditTeamDAL(IASDatabase);
            request.IPAddress = CommonFunction.GetIPAddress();
            var result = await obj.UpdateTeamOfAuditForVerification(request);

            //foreach (var user in request.AssignUserRole)
            //{
            //    string EmployeeEmailID = string.Empty;
            //    string Subject = string.Empty;
            //    string Body = string.Empty;
            //    switch (user.RoleId)
            //    {
            //        case (int)IASRolesEnums.AuditHead:
            //            EmployeeEmailID = user.Email;
            //            Subject = "New User created";
            //            Body = "<br/>You have been assigned as a <b> " + "Audit Head" + ".</b><br/><br/>Your login credentials are given below. <br/>" + EmployeeEmailID + "<br/>" + "Password:     " + request.Password + "<br/><br/>" + "Use the below given link for first time login and change the password." +
            //                         " <br/><br/><a href='" + request.APIUrl + "'>" + request.APIUrl + "</a> " + "<br/><br/>" + "From <b> " + "IAD Admin </b>";
            //            CommonFunction.SendMail(EmployeeEmailID, Subject, Body);
            //            break;

            //        case (int)IASRolesEnums.AuditDH:
            //            EmployeeEmailID = user.Email;
            //            Subject = "New User created";
            //            Body = "<br/>You have been assigned as a <b> " + "Audit Entry User" + ".</b><br/><br/>Your login credentials are given below. <br/>" + EmployeeEmailID + "<br/>" + "Password:     " + request.Password + "<br/><br/>" + "Use the below given link for first time login and change the password." +
            //                         " <br/><br/><a href='" + request.APIUrl + "'>" + request.APIUrl + "</a> " + "<br/><br/>" + "From <b> " + "IAD Admin </b>";
            //            CommonFunction.SendMail(EmployeeEmailID, Subject, Body);
            //            break;
            //    }

            //}
            return result;
        }

        async Task<List<GetAuditDetailsForVerificationDropdownResponse>> IVerifyAuditTeamRepository.GetAuditDetailsForVerificationDropdown(int roleUserMapId)
        {
            VerifyAuditTeamDAL dal = new VerifyAuditTeamDAL(IASDatabase);
            return await dal.GetAuditDetailsForVerificationDropdown(roleUserMapId);
        }
    }
}
