﻿using AID.DataAccess;
using BusinessModel.UserManagement;
using Common;
using IRepository;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AID.BusinessLayer.MenuBL
{
    public class MenuBL:IMenuRepository
    {
        readonly Database IASDatabase;
        public MenuBL()
        {
            DatabaseProviderFactory factory = new DatabaseProviderFactory();
            IASDatabase = factory.Create(IASConstant.IASDataBaseConnection);
        }
        async Task<List<MenuModel>> IMenuRepository.BindMenuInGride()
        {
            MenuDAL obj = new MenuDAL(IASDatabase);
            return await obj.BindMenuInGride();
        }
        async Task<List<MenuModel>> IMenuRepository.BindMenuInDropDown() 
        {
            MenuDAL obj = new MenuDAL(IASDatabase);
            return await obj.BindMenuInDropDown();
        }
        
             async Task<List<MenuModel>> IMenuRepository.GetAllPredefineRole()
        {
            MenuDAL obj = new MenuDAL(IASDatabase);
            return await obj.GetAllPredefineRole();
        }
    }
}
