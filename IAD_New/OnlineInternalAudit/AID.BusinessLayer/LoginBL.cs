﻿using BusinessModel;
using Common;
using DataAccess;
using IRepository;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AID.BusinessLayer.LoginBL
{
    public class LoginBL : ILoginRepository
    {
        readonly Database IASDatabase;
        public LoginBL()
        {
            DatabaseProviderFactory factory = new DatabaseProviderFactory();
            IASDatabase = factory.Create(IASConstant.IASDataBaseConnection);
        }    
              

        async Task<List<LoginResponseBM>> ILoginRepository.Login(string username, string password)
        {
            LoginDAL obj = new LoginDAL(IASDatabase);
            return await obj.Login(username, password);
        }

    }
}
