﻿using AID.DataAccess.AssignUser;
using BusinessModel.AssignUser;
using Common;
using IRepository;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AID.BusinessLayer.AssignUserBL
{
    public class AssignUserBL : IAssignUserRepository
    {
        readonly Database IASDatabase;
        public AssignUserBL()
        {
            DatabaseProviderFactory factory = new DatabaseProviderFactory();
            IASDatabase = factory.Create(IASConstant.IASDataBaseConnection);
        }


        async Task<List<GetUserRoleEmployeesResponse>> IAssignUserRepository.GetUserRoleEmployees(int roleId)
        {
            AssignAuditSupervisorDAL obj = new AssignAuditSupervisorDAL(IASDatabase);
            return await obj.GetUserRoleEmployees(roleId);
        }

        async Task<int> IAssignUserRepository.AssignUserRole(AssignUserRoleRequest request)
        {
            AssignAuditSupervisorDAL obj = new AssignAuditSupervisorDAL(IASDatabase);
            request.IPAddress = CommonFunction.GetIPAddress();
            var result = await obj.AssignUserRole(request);

            foreach (var user in request.AssignUserRole)
            {
                string EmployeeEmailID = string.Empty;
                string Subject = string.Empty;
                string Body = string.Empty;
                switch (user.RoleId)
                {
                    case (int)IASRolesEnums.AuditSupervisor:
                        EmployeeEmailID = user.Email;
                        Subject = "New User created";
                        Body = "<br/>You have been assigned as a <b> " + "Audit Supervisor" + ".</b><br/><br/>Your login credentials are given below. <br/>" + EmployeeEmailID + "<br/>" + "Password:     " + request.Password + "<br/><br/>" + "Use the below given link for first time login and change the password." +
                                     " <br/><br/><a href='" + request.APIUrl + "'>" + request.APIUrl + "</a> " + "<br/><br/>" + "From <b> " + "IAD Admin </b>";
                        CommonFunction.SendGenericMail(EmployeeEmailID, Subject, Body);
                        break;

                    case (int)IASRolesEnums.IADHead:
                        EmployeeEmailID = user.Email;
                        Subject = "New User created";
                        Body = "<br/>You have been assigned as a <b> " + "IAD Head" + ".</b><br/><br/>Your login credentials are given below. <br/>" + EmployeeEmailID + "<br/>" + "Password:     " + request.Password + "<br/><br/>" + "Use the below given link for first time login and change the password." +
                                     " <br/><br/><a href='" + request.APIUrl + "'>" + request.APIUrl + "</a> " + "<br/><br/>" + "From <b> " + "IAD Admin </b>";
                        CommonFunction.SendGenericMail(EmployeeEmailID, Subject, Body);
                        break;
                }

            }
            return result;
        }


        async Task<List<GetAssignAuditTeamResponse>> IAssignUserRepository.GetAssignAuditTeam(int auditId)
        {
            AssignAuditTeamDAL obj = new AssignAuditTeamDAL(IASDatabase);
            return await obj.GetAssignAuditTeam(auditId);
        }

        async Task<CheckIfUserAlreadyAssignedToAnotherAuditResponse> IAssignUserRepository.CheckIfUserAlreadyAssignedToAnotherAudit(CheckIfUserAlreadyAssignedToAnotherAuditRequest request)
        {
            AssignAuditTeamDAL obj = new AssignAuditTeamDAL(IASDatabase);
            return await obj.CheckIfUserAlreadyAssignedToAnotherAudit(request);
        }

        async Task<int> IAssignUserRepository.AssignAuditTeam(AssignAuditTeamRequest request)
        {
            AssignAuditTeamDAL obj = new AssignAuditTeamDAL(IASDatabase);
            request.IPAddress = CommonFunction.GetIPAddress();
            var result = await obj.AssignAuditTeam(request);

            //foreach (var user in request.AssignUserRole)
            //{
            //    string EmployeeEmailID = string.Empty;
            //    string Subject = string.Empty;
            //    string Body = string.Empty;
            //    switch (user.RoleId)
            //    {
            //        case (int)IASRolesEnums.AuditHead:
            //            EmployeeEmailID = user.Email;
            //            Subject = "New User created";
            //            Body = "<br/>You have been assigned as a <b> " + "Audit Head" + ".</b><br/><br/>Your login credentials are given below. <br/>" + EmployeeEmailID + "<br/>" + "Password:     " + request.Password + "<br/><br/>" + "Use the below given link for first time login and change the password." +
            //                         " <br/><br/><a href='" + request.APIUrl + "'>" + request.APIUrl + "</a> " + "<br/><br/>" + "From <b> " + "IAD Admin </b>";
            //            CommonFunctions.SendMail(EmployeeEmailID, Subject, Body);
            //            break;

            //        case (int)IASRolesEnums.AuditDH:
            //            EmployeeEmailID = user.Email;
            //            Subject = "New User created";
            //            Body = "<br/>You have been assigned as a <b> " + "Audit Entry User" + ".</b><br/><br/>Your login credentials are given below. <br/>" + EmployeeEmailID + "<br/>" + "Password:     " + request.Password + "<br/><br/>" + "Use the below given link for first time login and change the password." +
            //                         " <br/><br/><a href='" + request.APIUrl + "'>" + request.APIUrl + "</a> " + "<br/><br/>" + "From <b> " + "IAD Admin </b>";
            //            CommonFunctions.SendMail(EmployeeEmailID, Subject, Body);
            //            break;
            //    }

            //}
            return result;
        }

        async Task<List<GetAuditsForDropdownResponse>> IAssignUserRepository.GetAuditsForDropdown()
        {
            AssignAuditTeamDAL dal = new AssignAuditTeamDAL(IASDatabase);
            return await dal.GetAuditsForDropdown();
        }
    }
}
