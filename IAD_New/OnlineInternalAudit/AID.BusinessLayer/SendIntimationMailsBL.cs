﻿using AID.DataAccess.Audits;
using BusinessModel.Audits;
using BusinessModel.AuditStructure;
using Common;
using IRepository;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AID.BusinessLayer.SendIntimationMailsBL
{
    public class SendIntimationMailsBL : ISendIntimationMailsRepository
    {
        readonly Database IASDatabase;
        public SendIntimationMailsBL()
        {
            DatabaseProviderFactory factory = new DatabaseProviderFactory();
            IASDatabase = factory.Create(IASConstant.IASDataBaseConnection);
        }
        /// <summary>
        /// SendMail
        /// </summary>
        /// <param name="requestmodel"></param>
        /// <returns></returns>
        async Task<string> ISendIntimationMailsRepository.SendMail(SendIntimationMailsBM requestmodel)
        {
            SendIntimationMailsDAL obj = new SendIntimationMailsDAL(IASDatabase);
            return await obj.SendMail(requestmodel);
        }

        /// <summary>
        /// ISendIntimationMailsRepository
        /// </summary>
        /// <returns></returns>
        async Task<List<SendIntimationMailsBM>> ISendIntimationMailsRepository.GetSendIntimationMailsPao(int fYearid)
        {
            SendIntimationMailsDAL obj = new SendIntimationMailsDAL(IASDatabase);
            return await obj.GetSendIntimationMailsPao(fYearid);
        }

   
    }
}
