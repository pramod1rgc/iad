﻿using AID.DataAccess.AuditReport;
using BusinessModel.AuditReport;
using Common;
using IRepository;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AID.BusinessLayer.AuditReportBL
{
    public class AuditReportBL : IAuditReportRepository
    {
        readonly Database IASDatabase;
        public AuditReportBL()
        {
            DatabaseProviderFactory factory = new DatabaseProviderFactory();
            IASDatabase = factory.Create(IASConstant.IASDataBaseConnection);
        }

        async Task<List<GetAuditsForAuditReportBySupervisorResponse>> IAuditReportRepository.GetAuditsForAuditReportBySupervisor(int pageSize, int pageNumber, string searchTerm)
        {
            AuditReportDAL obj = new AuditReportDAL(IASDatabase);
            var response = await obj.GetAuditsForAuditReportBySupervisor(pageSize, pageNumber, searchTerm);
            return response;
        }

        async Task<GetAuditReportResponse> IAuditReportRepository.GetAuditReport(int auditId)
        {
            AuditReportDAL obj = new AuditReportDAL(IASDatabase);
            var response = await obj.GetAuditReport(auditId);
            return response;
        }
    }
}
