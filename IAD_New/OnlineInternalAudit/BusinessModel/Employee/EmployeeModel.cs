﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessModel.Employee
{
    public class EmployeeModel
    {
        public int EmpID { get; set; }
        public string EmpName { get; set; }
        public string EmpCode { get; set; }
        public string Designation { get; set; }
        public string Email { get; set; }
        public string PhoneNo { get; set; }
        public string MobileNo { get; set; }
        public string IsActive { get; set;}
        public int DesigID { get; set; }
        public string DesigName { get; set; }
    }

   
}
