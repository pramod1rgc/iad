﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessModel.Role
{
    public class GetRoleResponse
    {
        public int RoleId { get; set; }
        public string RoleName { get; set; }
        public string RoleDescription { get; set; }
        public int RoleHierarchy { get; set; }
        public string Reason { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public bool IsActive { get; set; }
        public DateTime? DeactivationDate { get; set; }
    }

    public class UpsertRoleRequest
    {
        public int RoleId { get; set; }
        public string RoleName { get; set; }
        public string RoleDescription { get; set; }
        public int RoleHierarchy { get; set; }
        public int UserId { get; set; }
        public string IPAddress { get; set; }
    }

    public class ToggleRoleRequest
    {
        public int RoleId { get; set; }
        public string Reason { get; set; }
        public bool IsActive { get; set; }
        public int UserId { get; set; }
        public string IPAddress { get; set; }
    }
}