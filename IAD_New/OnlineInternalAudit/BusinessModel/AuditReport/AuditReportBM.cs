﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessModel.AuditReport
{
    public class GetAuditsForAuditReportBySupervisorResponse
    {
        public int AuditId { get; set; }
        public int PaoId { get; set; }
        public string Name { get; set; }
        public DateTime? AuditFromDate { get; set; }
        public DateTime? AuditToDate { get; set; }
        public int AuditStatusId { get; set; }
        public string Status { get; set; }
        public DateTime? AuditIntimationDate { get; set; }
        public int TotalCount { get; set; }
    }

    public class GetAuditReportResponse
    {
        public int AuditId { get; set; }
        public int PaoId { get; set; }
        public string Name { get; set; }
        public DateTime? AuditFromDate { get; set; }
        public DateTime? AuditToDate { get; set; }
        public int AuditStatusId { get; set; }
        public string Status { get; set; }
        public DateTime? AuditIntimationDate { get; set; }
        public int EntryConferenceId { get; set; }
        public DateTime? EntryConferenceDate { get; set; }
        public int ExitConferenceId { get; set; }
        public DateTime? ExitConferenceDate { get; set; }
        public DateTime? ParaCreatedDate { get; set; }
        public int AuditParaSettlementLetterId { get; set; }
        public DateTime? ParaSettlementLetterDate { get; set; }
        public int TotalParaCount { get; set; }
        public int TotalApprovedParaCount { get; set; }
        public int TotalOpenParaCount { get; set; }
        public int TotalPartialParaCount { get; set; }
        public int TotalLowSeverityParas { get; set; }
        public int TotalMediumSeverityParas { get; set; }
        public int TotalHighSeverityParas { get; set; }
        public DateTime? LastParaReplyDate { get; set; }
    }
}
