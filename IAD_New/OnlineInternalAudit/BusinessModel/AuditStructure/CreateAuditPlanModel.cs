﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessModel.AuditStructure
{
    public class CreateAuditPlanModel
    {
        public int FinancialYearId { get; set; }
       
        public string FinancialYear { get; set; }
        public string IsActive { get; set; }

        public int PAOId { get; set; }
        public string PAOName { get; set; }
       public string RejectionRemark { get; set; }


        public string PAOCode { get; set; }
        public string MinistryId { get; set; }
        public string DesignationId { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public string District { get; set; }
        public string City { get; set; }
        public string Phone { get; set; }


        //Audit 

       
       public int AuditId { get; set; }
       public string AuditName { get; set; }
        public string Quarter { get; set; }
        public string AuditFromDate { get; set; }
        public string AuditToDate { get; set; }
        public string AuditIntimationDate { get; set; }
        public string AuditStatusId { get; set; }
        public string AuditStatus { get; set; }
        public string AuditReason { get; set; }
        public string QuarterStatus { get; set; }
        //end Of Audit


        //Controller 
        public int ContollerId { get; set; }
        public string ControllerName { get; set; }
        public string ControllerCode { get; set; }
        //End Of Contoller


        //For Supervisor dropDawn
        
             public int RoleUserMapID { get; set; }

        public int UserID { get; set; }
        public int RoleID { get; set; }
        public string UserName { get; set; }

        public string EmpName { get; set; }
        //End Of 



    }
}
