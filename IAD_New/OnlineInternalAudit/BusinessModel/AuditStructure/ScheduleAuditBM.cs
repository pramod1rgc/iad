﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessModel.AuditStructure
{
    public class GetScheduledAuditsResponse
    {
        public int AuditId { get; set; }
        public int PAOId { get; set; }
        public int FinancialYearId { get; set; }
        public string AuditName { get; set; }
        public int AuditStatusId { get; set; }
        public DateTime? AuditFromDate { get; set; }
        public DateTime? AuditToDate { get; set; }
        public string Quarter { get; set; }
        public string PAOName { get; set; }
        public string PAOCode { get; set; }
        public string ControllerCode { get; set; }
        public string Status { get; set; }
        public string RejectionRemark { get; set; }
    }

    public class UpdateScheduleAuditRequest
    {
        public UpdateScheduleAuditRequest()
        {
            this.AuditSchedulesType = new List<ScheduleAudits>();
        }
        public List<ScheduleAudits> AuditSchedulesType { get; set; }
        public int UserId { get; set; }
        public string IPAddress { get; set; }
    }

    public class ScheduleAudits
    {
        public int Id { get; set; }
        public int AuditId { get; set; }
        public DateTime? AuditFromDate { get; set; }
        public DateTime? AuditToDate { get; set; }
    }


    public class DeleteScheduleAuditRequest
    {
        public int AuditId { get; set; }
        public int UserId { get; set; }
        public string IPAddress { get; set; }
    }
}
