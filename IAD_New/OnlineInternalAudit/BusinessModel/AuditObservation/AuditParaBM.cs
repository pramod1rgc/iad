﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessModel.AuditObservation
{
    public class GetAuditParaResponse
    {
        public int ParaId { get; set; }
        public int AuditId { get; set; }
        public string Title { get; set; }
        public string Criteria { get; set; }
        public string Conditions { get; set; }
        public string Consequences { get; set; }
        public string Causes { get; set; }
        public string CorrectiveActions { get; set; }
        public string Severity { get; set; }
        public int RiskCategoryId { get; set; }
        public string RiskCategoryName { get; set; }
        public string RiskCategoryDesc { get; set; }
        public int StatusId { get; set; }
        public string Status { get; set; }
        public int AuditeeStatusId { get; set; }
        public string AuditeeStatus { get; set; }
        public DateTime? CreatedOn { get; set; }
        public int CommentId { get; set; }
        public int TotalCount { get; set; }
    }

    public class UpdateAuditParaRequest
    {
        public int AuditId { get; set; }
        public int ParaId { get; set; }
        public string Title { get; set; }
        public string Criteria { get; set; }
        public string Conditions { get; set; }
        public string Consequences { get; set; }
        public string Causes { get; set; }
        public string CorrectiveActions { get; set; }
        public string Severity { get; set; }
        public int RiskCategoryId { get; set; }
        public int UserId { get; set; }
        public string IPAddress { get; set; }
        public List<UploadFilesRequest> UploadFiles { get; set; }
        public List<FinancialImplicationCasesRequest> ParaFinancialImplications { get; set; }
    }

    public class UploadFilesRequest
    {
        public int Id { get; set; }
        public int ParaFileId { get; set; }
        public string FileName { get; set; }
        public string FileType { get; set; }
        public string FileSize { get; set; }
        public string RelativePath { get; set; }
        public string FileUrl { get; set; }
    }

    public class FinancialImplicationCasesRequest
    {
        public int Id { get; set; }
        public int AuditParaFinancialImplicationMappingId { get; set; }
        public int FinancialImplicationCaseId { get; set; }
        public double Amount { get; set; }
        public string Remarks { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }
    }

    public class GetAuditParaDetails
    {
        public int ParaId { get; set; }
        public int AuditId { get; set; }
        public string Title { get; set; }
        public string Criteria { get; set; }
        public string Conditions { get; set; }
        public string Consequences { get; set; }
        public string Causes { get; set; }
        public string CorrectiveActions { get; set; }
        public string Severity { get; set; }
        public int RiskCategoryId { get; set; }
        public string RiskCategoryDesc { get; set; }
        public DateTime? CreatedOn { get; set; }
        public int ParaFileId { get; set; }
        public int FileId { get; set; }
        public string FileName { get; set; }
        public string FileType { get; set; }
        public string FileSize { get; set; }
        public string RelativePath { get; set; }
        public string FileUrl { get; set; }
        public string RejectionRemark { get; set; }
        public int StatusId { get; set; }
        public int AuditeeStatusId { get; set; }
    }

    public class GetAuditParaDetailsResponse
    {
        public GetAuditParaDetailsResponse()
        {
            this.Files = new List<GetParaFileDetails>();
        }
        public int ParaId { get; set; }
        public int AuditId { get; set; }
        public string Title { get; set; }
        public string Criteria { get; set; }
        public string Conditions { get; set; }
        public string Consequences { get; set; }
        public string Causes { get; set; }
        public string CorrectiveActions { get; set; }
        public string Severity { get; set; }
        public int RiskCategoryId { get; set; }
        public string RiskCategoryDesc { get; set; }
        public DateTime? CreatedOn { get; set; }
        public string RejectionRemark { get; set; }
        public int StatusId { get; set; }
        public int AuditeeStatusId { get; set; }
        public List<GetParaFileDetails> Files { get; set; }
    }

    public class GetParaFileDetails
    {
        public int ParaFileId { get; set; }
        public int FileId { get; set; }
        public string FileName { get; set; }
        public string FileType { get; set; }
        public string FileSize { get; set; }
        public string RelativePath { get; set; }
        public string FileUrl { get; set; }
    }

    public class DeleteAuditParaRequest
    {
        public int ParaId { get; set; }
        public int UserId { get; set; }
        public string IPAddress { get; set; }
    }

    public class PostParaCommentByAuditorRequest
    {
        public PostParaCommentByAuditorRequest()
        {
            this.Files = new List<PostParaCommentFiles>();
        }
        public int ParaId { get; set; }
        public int AuditId { get; set; }
        public string Comment { get; set; }
        public string IPAddress { get; set; }
        public int UserId { get; set; }
        public List<PostParaCommentFiles> Files { get; set; }
    }

    public class PostParaCommentFiles
    {
        public int Id { get; set; }
        public string FileName { get; set; }
        public string FileType { get; set; }
        public string FileSize { get; set; }
        public string RelativePath { get; set; }
        public string FileUrl { get; set; }
    }


    public class GetParaComment
    {
        public int CommentId { get; set; }
        public int ParaId { get; set; }
        public int AuditUserMapId { get; set; }
        public string Comment { get; set; }
        public DateTime? CreatedOn { get; set; }
        public int RoleId { get; set; }
        public string RoleName { get; set; }
        public int UserId { get; set; }
        public int EmpId { get; set; }
        public string EmpName { get; set; }
        public int CommentFileId { get; set; }
        public int FileId { get; set; }
        public string FileName { get; set; }
        public string FileSize { get; set; }
        public string FileType { get; set; }
        public string RelativePath { get; set; }
        public string FileUrl { get; set; }
    }

    public class GetParaCommentResponse
    {
        public GetParaCommentResponse()
        {
            this.Files = new List<GetParaCommentFileResponse>();
        }
        public int CommentId { get; set; }
        public int ParaId { get; set; }
        public int AuditUserMapId { get; set; }
        public string Comment { get; set; }
        public DateTime? CreatedOn { get; set; }
        public int RoleId { get; set; }
        public string RoleName { get; set; }
        public int UserId { get; set; }
        public int EmpId { get; set; }
        public string EmpName { get; set; }
        public List<GetParaCommentFileResponse> Files { get; set; }
    }


    public class GetParaCommentFileResponse
    {
        public int CommentFileId { get; set; }
        public int FileId { get; set; }
        public string FileName { get; set; }
        public string FileSize { get; set; }
        public string FileType { get; set; }
        public string RelativePath { get; set; }
        public string FileUrl { get; set; }
    }

    public class AuditParaStatusRequest
    {   
        public int ParaId { get; set; }
        public int AuditId { get; set; }
        public int StatusId { get; set; }
        public string RejectionRemark { get; set; }
    }

    public class GetAllFinancialImplicationCasesResponse
    {
        public int FinancialImplicationCaseId { get; set; }
        public string FinancialImplicationCase { get; set; }
    }

    public class GetAuditParaFinancialImplicationCasesResponse
    {
        public int AuditParaFinancialImplicationMappingId { get; set; }
        public int ParaId { get; set; }
        public int FinancialImplicationCaseId { get; set; }
        public string FinancialImplicationCase { get; set; }
        public double Amount { get; set; }
        public string Remarks { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime? CreatedOn { get; set; }
    }
}
