﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessModel.AuditObservation
{
    public class GetAuditExitConferenceResponse
    {
        public int ExitConferenceId { get; set; }
        public int AuditId { get; set; }
        public DateTime? ConferenceDate { get; set; }
        public string Agenda { get; set; }
        public string Decision { get; set; }
        public int ExitConferenceParticipantId { get; set; }
        public int AuditUserMapId { get; set; }
        public int EmpID { get; set; }
        public string EmpName { get; set; }
        public int DesignationId { get; set; }
        public string DesignationName { get; set; }
        public int RoleId { get; set; }
        public int StatusId { get; set; }
        public string Status { get; set; }
        public string RejectionRemark { get; set; }
    }

    public class UpdateAuditExitConferenceRequest
    {
        public UpdateAuditExitConferenceRequest()
        {
            this.Participants = new List<AuditExitConferenceParticipant>();
        }
        public int ExitConferenceId { get; set; }
        public int AuditId { get; set; }
        public DateTime? ConferenceDate { get; set; }
        public string Agenda { get; set; }
        public string Decision { get; set; }
        public int UserId { get; set; }
        public string UserIPAddress { get; set; }
        public List<AuditExitConferenceParticipant> Participants { get; set; }
    }

    public class AuditExitConferenceParticipant
    {
        public int Id { get; set; }
        public int AuditUserMapId { get; set; }
    }


    public class AuditExitConferenceResponse
    {
        public AuditExitConferenceResponse()
        {
            this.Participants = new List<AuditExitConferenceParticipants>();
        }
        public int ExitConferenceId { get; set; }
        public int AuditId { get; set; }
        public DateTime? ConferenceDate { get; set; }
        public string Agenda { get; set; }
        public string Decision { get; set; }
        public int StatusId { get; set; }
        public string Status { get; set; }
        public string RejectionRemark { get; set; }
        public List<AuditExitConferenceParticipants> Participants { get; set; }
    }

    public class AuditExitConferenceParticipants
    {
        public int ExitConferenceParticipantId { get; set; }
        public int AuditUserMapId { get; set; }
        public int EmpID { get; set; }
        public string EmpName { get; set; }
        public int DesignationId { get; set; }
        public string DesignationName { get; set; }
        public int RoleId { get; set; }
    }

    public class UpdateExitConferenceStatusRequest
    {
        public int StatusId { get; set; }
        public int ExitConferenceId { get; set; }
        public string RejectionRemark { get; set; }
        public int UserId { get; set; }
        public string IpAddress { get; set; }
    }
}
