﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessModel.AuditObservation
{
    public class GetAuditDHAuditsListResponse
    {
        public int AuditId { get; set; }
        public int PaoId { get; set; }
        public string Name { get; set; }
        public string PaoCode { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Address { get; set; }
        public string District { get; set; }
        public string City { get; set; }
        public int AuditStatusId { get; set; }
        public string Status { get; set; }
        public DateTime? AuditFromDate { get; set; }
        public DateTime? AuditToDate { get; set; }
        public string ControllerCode { get; set; }
        public int EntryConferenceId { get; set; }
        public DateTime? AuditIntimationDate { get; set; }
        public DateTime? EntryConferenceDate { get; set; }
        public int ExitConferenceId { get; set; }
        public DateTime? ExitConferenceDate { get; set; }
        public DateTime? ParaCreatedOnDate { get; set; }
        public int AuditParaSettlementLetterId { get; set; }
        public DateTime? AuditParaSettlementLetterDate { get; set; }
    }

    public class GetAuditEntryConferenceResponse
    {
        public int EntryConferenceId { get; set; }
        public int AuditId { get; set; }
        public DateTime? ConferenceDate { get; set; }
        public string Agenda { get; set; }
        public int EntryConferenceParticipantId { get; set; }
        public int AuditUserMapId { get; set; }
        public int EmpID { get; set; }
        public string EmpName { get; set; }
        public int DesignationId { get; set; }
        public string DesignationName { get; set; }
        public int RoleId { get; set; }
        public int StatusId { get; set; }
        public string Status { get; set; }
        public string RejectionRemark { get; set; }
    }

    public class UpdateAuditEntryConferenceRequest
    {
        public UpdateAuditEntryConferenceRequest()
        {
            this.Participants = new List<AuditEntryConferenceParticipant>();
        }
        public int EntryConferenceId { get; set; }
        public int AuditId { get; set; }
        public DateTime? ConferenceDate { get; set; }
        public string Agenda { get; set; }
        public int UserId { get; set; }
        public string UserIPAddress { get; set; }
        public List<AuditEntryConferenceParticipant> Participants { get; set; }
    }

    public class AuditEntryConferenceParticipant
    {
        public int Id { get; set; }
        public int AuditUserMapId { get; set; }
    }


    public class AuditEntryConferenceResponse
    {
        public AuditEntryConferenceResponse()
        {
            this.Participants = new List<AuditEntryConferenceParticipants>();
        }
        public int EntryConferenceId { get; set; }
        public int AuditId { get; set; }
        public DateTime? ConferenceDate { get; set; }
        public string Agenda { get; set; }
        public int StatusId { get; set; }
        public string Status { get; set; }
        public string RejectionRemark { get; set; }
        public List<AuditEntryConferenceParticipants> Participants { get; set; }
    }

    public class AuditEntryConferenceParticipants
    {
        public int EntryConferenceParticipantId { get; set; }
        public int AuditUserMapId { get; set; }
        public int EmpID { get; set; }
        public string EmpName { get; set; }
        public int DesignationId { get; set; }
        public string DesignationName { get; set; }
        public int RoleId { get; set; }
    }

    public class AuditStatusUpdateRequest
    {
        public int AuditId { get; set; }
        public int StatusId { get; set; }
        public int UserId { get; set; }
        public string IPAddress { get; set; }
    }

    public class UpdateEntryConferenceStatusRequest
    {
        public int StatusId { get; set; }
        public int EntryConferenceId { get; set; }
        public string RejectionRemark { get; set; }
        public int UserId { get; set; }
        public string IpAddress { get; set; }
    }
}
