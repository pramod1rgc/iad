﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AID.BusinessModels.Dashboard
{
    public class DashboardModel
    {
        public List<DashboardModel> Children { get; set; }
        public int menuID { get; set; }
        public int? parentMenu { get; set; }
        public string mainMenuName { get; set; }
        public string menuURL { get; set; }
        public string displayName { get; set; }
        public string iconName { get; set; }
        public string route { get; set; }
        public string status { get; set; }
        public string mpstatus { get; set; }
        public int menupermissionID { get; set; }
        public int menuHeirarchy { get; set; }
    }
}
