﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessModel.UserManagement
{
    public class MenuModel
    {
        #region Menu Class  Property
        public int MenuID { get; set; }
        public string MenuDescriptionName { get; set; }
        public string MenuHierarchy { get; set; }
        public string MenuURL { get; set; }
        public int ParentMenu { get; set; }
        public string Status { get; set; }
        public string IPAddress { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public string ModifiedDate { get; set; }
        public string CreatedStatus { get; set; }
        public string IsActive { get; set; }


        public int RoleID { get; set; }
        public string RoleName { get; set; }
  
        #endregion End Property
    }
   
}
