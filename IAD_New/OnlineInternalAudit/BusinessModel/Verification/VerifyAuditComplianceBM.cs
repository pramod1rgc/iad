﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessModel.Verification
{
    public class GetAuditsForwardedToSupervisorResponse
    {
        public int AuditId { get; set; }
        public int PaoId { get; set; }
        public string Name { get; set; }
        public DateTime? AuditFromDate { get; set; }
        public DateTime? AuditToDate { get; set; }
        public int AuditStatusId { get; set; }
        public string Status { get; set; }
        public DateTime? AuditIntimationDate { get; set; }
    }
}
