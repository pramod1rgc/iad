﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessModel.Verification
{
    public class GetAuditDetailsForVerificationDropdownResponse
    {
        public int AuditId { get; set; }
        public string Name { get; set; }
        public DateTime? AuditFromDate { get; set; }
        public DateTime? AuditToDate { get; set; }
        public string Quarter { get; set; }
    }

    public class GetAuditTeamForVerificationResponse
    {
        public int AuditId { get; set; }
        public string Name { get; set; }
        public int EmpId { get; set; }
        public string EmpName { get; set; }
        public string Email { get; set; }
        public string PhoneNo { get; set; }
        public int DesignationId { get; set; }
        public string DesignationName { get; set; }
        public int AuditStatusId { get; set; }
        public string Status { get; set; }
        public DateTime? AuditFromDate { get; set; }
        public DateTime? AuditToDate { get; set; }
        public string Quarter { get; set; }
        public int RoleId { get; set; }
        public string RoleName { get; set; }
    }

    public class UpdateAuditTeamForVerificationRequest
    {
        public int StatusId { get; set; }
        public string RejectionRemark { get; set; }
        public int AuditId { get; set; }
        public int UserId { get; set; }
        public string IPAddress { get; set; }
        public int ForwardedToUserId { get; set; }
        public int AuditDecisionStatusId { get; set; }
    }
}
