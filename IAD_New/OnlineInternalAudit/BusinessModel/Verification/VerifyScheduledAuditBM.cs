﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessModel.Verification
{
    public class GetScheduledAuditsForVerificationResponse
    {
        public int AuditId { get; set; }
        public int PAOId { get; set; }
        public int FinancialYearId { get; set; }
        public string AuditName { get; set; }
        public int AuditStatusId { get; set; }
        public DateTime? AuditFromDate { get; set; }
        public DateTime? AuditToDate { get; set; }
        public string Quarter { get; set; }
        public string PAOName { get; set; }
        public string PAOCode { get; set; }
        public string ControllerCode { get; set; }
        public string Status { get; set; }
    }

    public class UpdateScheduleAuditForVerificationRequest
    {
        public UpdateScheduleAuditForVerificationRequest()
        {
            this.AuditSchedulesType = new List<VerifyScheduleAudits>();
        }
        public List<VerifyScheduleAudits> AuditSchedulesType { get; set; }
        public int StatusId { get; set; }
        public int UserId { get; set; }
        public string IPAddress { get; set; }
        public string RejectionRemark { get; set; }
        public int ForwardedToUserId { get; set; }
        public int AuditDecisionStatusId { get; set; }
    }

    public class VerifyScheduleAudits
    {
        public int Id { get; set; }
        public int AuditId { get; set; }
        public DateTime? AuditFromDate { get; set; }
        public DateTime? AuditToDate { get; set; }
    }

    public class GetPendingTaskCountResponse
    {
        public int AnnualPlanCount { get; set; }
        public int QuarterlyPlanCount { get; set; }
        public int AuditScheduleCount { get; set; }
        public int AuditTeamCount { get; set; }
        public int ComplianceReportCount { get; set; }
        public int ParaReportCount { get; set; }
        public int ParaSettlementCount { get; set; }
        public int EntryConferenceCount { get; set; }
        public int ExitConferenceCount { get; set; }
    }

    public class GetDistinctQuartersBasedOnStatusAndFYResponse
    {
        public string Quarter { get; set; }
    }
}
