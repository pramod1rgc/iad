﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessModel.DeskAudit
{
    public class DeskAuditBM
    {
        public int CreatedbyId { get; set; }
        public int AuditId { get; set; }
        public string NameofDocument { get; set;}
        public string uploadedPath { get; set; }
        public string UploadedDate{ get; set; } 
       public List<DeskAuditUploadFilesRequest> UploadFiles { get; set; }
    }
    public class DeactiveDeskAuditBM
    {
        public int CreatedbyId { get; set; }
        public int AuditId { get; set; }
        public string DocumentName { get; set;}
        public Boolean isActive { get; set; }

    }

    public class DeskAuditUploadFilesRequest
    {
        public int Id { get; set; }
        //public int ParaFileId { get; set; }
        public string DocumentName { get; set; }
        public string FileName { get; set; }
        public string FileType { get; set; }
        public string FileSize { get; set; }
        public string RelativePath { get; set; }
        public string FileUrl { get; set; }
    }

    public class GetDeskAuditUploadFiles
    {
        public string DocumentName { get; set; }
        public List<DeskAuditUploadFilesRequest> Children { get; set; }
        public Boolean isActive { get; set; }

    }
 

}
