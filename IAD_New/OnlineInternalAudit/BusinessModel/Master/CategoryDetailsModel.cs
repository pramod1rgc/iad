﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessModel.Master
{
    public class CategoryDetailsModel
    {
        public int RiskCategoryID { get; set; }
        public int Category { get; set; }
        public string RiskCategoryName { get; set; }
        public int ParentRiskCategoryID { get; set; }
        public string ParentCategoryName { get; set; }
        public string RiskCategoryDesc { get; set; }
        public bool IsActive { get; set; }
        

    }
}
