﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessModel.AuditeeObservation
{
    public class GetAuditeeExitConferenceResponse
    {
        public int ExitConferenceId { get; set; }
        public int AuditId { get; set; }
        public DateTime? ConferenceDate { get; set; }
        public string Agenda { get; set; }
        public string Decision { get; set; }
        public int ExitConferenceParticipantId { get; set; }
        public int AuditUserMapId { get; set; }
        public int EmpID { get; set; }
        public string EmpName { get; set; }
        public int DesignationId { get; set; }
        public string DesignationName { get; set; }
        public int RoleId { get; set; }
    }

    public class UpdateAuditeeExitConferenceRequest
    {
        public UpdateAuditeeExitConferenceRequest()
        {
            this.Participants = new List<ExitConferenceParticipant>();
        }
        public int ExitConferenceId { get; set; }
        public int AuditId { get; set; }
        public DateTime? ConferenceDate { get; set; }
        public string Agenda { get; set; }
        public string Decision { get; set; }
        public int UserId { get; set; }
        public string UserIPAddress { get; set; }
        public List<ExitConferenceParticipant> Participants { get; set; }
    }

    public class ExitConferenceParticipant
    {
        public int Id { get; set; }
        public int AuditUserMapId { get; set; }
    }


    public class AuditeeExitConferenceResponse
    {
        public AuditeeExitConferenceResponse()
        {
            this.Participants = new List<AuditeeExitConferenceParticipants>();
        }
        public int ExitConferenceId { get; set; }
        public int AuditId { get; set; }
        public DateTime? ConferenceDate { get; set; }
        public string Agenda { get; set; }
        public string Decision { get; set; }
        public List<AuditeeExitConferenceParticipants> Participants { get; set; }
    }

    public class AuditeeExitConferenceParticipants
    {
        public int ExitConferenceParticipantId { get; set; }
        public int AuditUserMapId { get; set; }
        public int EmpID { get; set; }
        public string EmpName { get; set; }
        public int DesignationId { get; set; }
        public string DesignationName { get; set; }
        public int RoleId { get; set; }
    }
}
