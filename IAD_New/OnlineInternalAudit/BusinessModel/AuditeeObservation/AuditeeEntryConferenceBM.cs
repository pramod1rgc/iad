﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessModel.AuditeeObservation
{
    public class GetAuditeeDHAuditsListResponse
    {
        public int AuditId { get; set; }
        public int PaoId { get; set; }
        public string Name { get; set; }
        public string PaoCode { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Address { get; set; }
        public string District { get; set; }
        public string City { get; set; }
        public int AuditStatusId { get; set; }
        public string Status { get; set; }
        public DateTime? AuditFromDate { get; set; }
        public DateTime? AuditToDate { get; set; }
        public string ControllerCode { get; set; }
        public int EntryConferenceId { get; set; }
        public DateTime? AuditIntimationDate { get; set; }
        public DateTime? EntryConferenceDate { get; set; }
        public int ExitConferenceId { get; set; }
        public DateTime? ExitConferenceDate { get; set; }
        public DateTime? ParaCreatedOnDate { get; set; }
        public int AuditParaSettlementLetterId { get; set; }
        public DateTime? AuditParaSettlementLetterDate { get; set; }
    }

    public class GetAuditeeEntryConferenceResponse
    {
        public int EntryConferenceId { get; set; }
        public int AuditId { get; set; }
        public DateTime? ConferenceDate { get; set; }
        public string Agenda { get; set; }
        public int EntryConferenceParticipantId { get; set; }
        public int AuditUserMapId { get; set; }
        public int EmpID { get; set; }
        public string EmpName { get; set; }
        public int DesignationId { get; set; }
        public string DesignationName { get; set; }
        public int RoleId { get; set; }
    }

    public class UpdateAuditeeEntryConferenceRequest
    {
        public UpdateAuditeeEntryConferenceRequest()
        {
            this.Participants = new List<EntryConferenceParticipant>();
        }
        public int EntryConferenceId { get; set; }
        public int AuditId { get; set; }
        public DateTime? ConferenceDate { get; set; }
        public string Agenda { get; set; }
        public int UserId { get; set; }
        public string UserIPAddress { get; set; }
        public List<EntryConferenceParticipant> Participants { get; set; }
    }

    public class EntryConferenceParticipant
    {
        public int Id { get; set; }
        public int AuditUserMapId { get; set; }
    }


    public class AuditeeEntryConferenceResponse
    {
        public AuditeeEntryConferenceResponse()
        {
            this.Participants = new List<AuditeeEntryConferenceParticipants>();
        }
        public int EntryConferenceId { get; set; }
        public int AuditId { get; set; }
        public DateTime? ConferenceDate { get; set; }
        public string Agenda { get; set; }
        public List<AuditeeEntryConferenceParticipants> Participants { get; set; }
    }

    public class AuditeeEntryConferenceParticipants
    {
        public int EntryConferenceParticipantId { get; set; }
        public int AuditUserMapId { get; set; }
        public int EmpID { get; set; }
        public string EmpName { get; set; }
        public int DesignationId { get; set; }
        public string DesignationName { get; set; }
        public int RoleId { get; set; }
    }

    public class AuditeeStatusUpdateRequest
    {
        public int AuditId { get; set; }
        public int StatusId { get; set; }
        public int UserId { get; set; }
        public string IPAddress { get; set; }
        public int ParaId { get; set; }
    }
}
