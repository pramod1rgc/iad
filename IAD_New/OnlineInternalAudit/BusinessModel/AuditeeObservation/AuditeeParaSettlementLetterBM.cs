﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessModel.AuditeeObservation
{
    public class GetAuditeeParaSettlementLetterResponse
    {
        public int AuditParaSettlementLetterId { get; set; }
        public string LetterNumber { get; set; }
        public DateTime? LetterDate { get; set; }
        public string LetterTo { get; set; }
        public string LetterSubject { get; set; }
        public string LetterFrom { get; set; }
        public string LetterCopyTo { get; set; }
        public string LetterContent { get; set; }
        public int AuditId { get; set; }
        public DateTime? CreatedOn { get; set; }
        public int StatusId { get; set; }
        public string RejectionRemark { get; set; }
    }

    public class UpdateAuditeeParaSettlementLetterRequest
    {
        public int AuditParaSettlementLetterId { get; set; }
        public string LetterNumber { get; set; }
        public DateTime? LetterDate { get; set; }
        public string LetterTo { get; set; }
        public string LetterSubject { get; set; }
        public string LetterFrom { get; set; }
        public string LetterCopyTo { get; set; }
        public string LetterContent { get; set; }
        public int AuditId { get; set; }
        public int UserId { get; set; }
        public string IPAddress { get; set; }
    }
}
