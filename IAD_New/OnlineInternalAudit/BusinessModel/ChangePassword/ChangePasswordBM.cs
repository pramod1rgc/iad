﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessModel.ChangePassword
{
  public  class ChangePasswordBM
    {
        public string username { get; set; }
        public string currentpassword { get; set; }
        public string newpassword { get; set; }
    }
}
