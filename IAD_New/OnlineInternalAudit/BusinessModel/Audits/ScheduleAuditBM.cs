﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessModel.Audits
{
    public class GetApprovedScheduledAuditsResponse
    {
        public int AuditId { get; set; }
        public int PAOId { get; set; }
        public int FinancialYearId { get; set; }
        public string AuditName { get; set; }
        public int AuditStatusId { get; set; }
        public DateTime? AuditFromDate { get; set; }
        public DateTime? AuditToDate { get; set; }
        public string Quarter { get; set; }
        public string PAOName { get; set; }
        public string PAOCode { get; set; }
        public string ControllerCode { get; set; }
        public string Status { get; set; }
    }
}
