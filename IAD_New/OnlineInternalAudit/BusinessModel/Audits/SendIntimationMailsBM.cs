﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessModel.Audits
{
  public class SendIntimationMailsBM
    {
        public int PAOId { get; set; }
        public int SerialNo { get; set; }
        public string PaoName { get; set; }
        public string Email { get; set; }
        public DateTime? AuditFromDate { get; set; }
        public DateTime? AuditToDate { get; set; }
        public Boolean ActionStatus { get; set; }
        public string MailTo { get; set; }
        public string Subject { get; set; }
        public string StrBody { get; set; }
        public string Remarks { get; set; }
        public string IntimationStatus { get; set; }
        public int AuditTeamStatus { get; set; }

    }
}
