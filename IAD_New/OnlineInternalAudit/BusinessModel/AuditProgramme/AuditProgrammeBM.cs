﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessModel.AuditProgramme
{
    public class GetAuditProgrammesResponse
    {
        public int AuditId { get; set; }
        public int PaoId { get; set; }
        public string Name { get; set; }
        public DateTime? AuditFromDate { get; set; }
        public DateTime? AuditToDate { get; set; }
        public string EmpName { get; set; }
        public string DesignationName { get; set; }
    }

    public class AuditProgrammeResponse
    {
        public AuditProgrammeResponse()
        {
            this.EmpDetails = new List<EmpDetails>();
        }
        public int AuditId { get; set; }
        public int PaoId { get; set; }
        public string Name { get; set; }
        public DateTime? AuditFromDate { get; set; }
        public DateTime? AuditToDate { get; set; }
        public List<EmpDetails> EmpDetails { get; set; }
    }

    public class EmpDetails
    {
        public string EmpName { get; set; }
        public string DesignationName { get; set; }
    }
}
