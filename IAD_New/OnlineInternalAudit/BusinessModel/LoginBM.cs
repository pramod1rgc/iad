﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessModel
{

    public class LoginRequestBM
    {
        public string Password { get; set; }
        public string Username { get; set; }
    }
    public class LoginResponseBM
    {
        public int UserId { get; set; }
        public int RoleId { get; set; }
        public string Password { get; set; }
        public string Username { get; set; }
        public string RoleName { get; set; }
        public string RoleDescription { get; set; }
        public int RoleUserMapId { get; set; }
    }
}
