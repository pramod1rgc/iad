﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessModel.AssignUser
{
    public class GetUserRoleEmployeesResponse
    {
        public int EmpId { get; set; }
        public string EmpName { get; set; }
        public string EmpCode { get; set; }
        public int DesignationId { get; set; }
        public string Email { get; set; }
        public string PhoneNo { get; set; }
        public string MobileNo { get; set; }
        public bool IsActive { get; set; }
        public string Designation { get; set; }
        public int UserId { get; set; }
        public string UserName { get; set; }
        public int RoleId { get; set; }
        public int RoleUserMapId { get; set; }
    }

    public class AssignUserRoleRequest
    {
        public AssignUserRoleRequest()
        {
            this.AssignUserRole = new List<AssignEmployeeRoleDetails>();
            this.APIUrl = "http://localhost:4200/login";
        }
        public string IPAddress { get; set; }
        public int IPUserId { get; set; }
        public string Password { get; set; }
        public int IPRoleId { get; set; }
        public string APIUrl { get; set; }
        public List<AssignEmployeeRoleDetails> AssignUserRole { get; set; }
    }

    public class AssignEmployeeRoleDetails
    {
        public int Id { get; set; }
        public int EmpId { get; set; }
        public int RoleId { get; set; }
        public string Email { get; set; }

    }
}
