﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessModel.AssignUser
{
    public class GetAssignAuditTeamResponse
    {
        public int EmpId { get; set; }
        public string EmpName { get; set; }
        public string EmpCode { get; set; }
        public int DesignationId { get; set; }
        public string Email { get; set; }
        public string PhoneNo { get; set; }
        public string MobileNo { get; set; }
        public bool IsActive { get; set; }
        public string Designation { get; set; }
        public int UserId { get; set; }
        public string UserName { get; set; }
        public int RoleId { get; set; }
        public int RoleUserMapId { get; set; }
        public int AuditId { get; set; }
        public DateTime? AuditFromDate { get; set; }
        public DateTime? AuditToDate { get; set; }

    }


    public class AssignAuditTeamRequest
    {
        public AssignAuditTeamRequest()
        {
            this.AssignUserRole = new List<AssignAuditTeamRoleDetails>();
            this.APIUrl = "http://localhost:4200/login";
        }
        public string IPAddress { get; set; }
        public int IPUserId { get; set; }
        public string Password { get; set; }
        public int IPRoleId { get; set; }
        public string APIUrl { get; set; }
        public int AuditId { get; set; }
        public List<AssignAuditTeamRoleDetails> AssignUserRole { get; set; }
    }

    public class AssignAuditTeamRoleDetails
    {
        public int Id { get; set; }
        public int EmpId { get; set; }
        public int RoleId { get; set; }
        public string Email { get; set; }
    }

    public class GetAuditsForDropdownResponse
    {
        public int AuditId { get; set; }
        public string Name { get; set; }
        public DateTime? AuditFromDate { get; set; }
        public DateTime? AuditToDate { get; set; }
        public string Quarter { get; set; }
        public int AuditStatusId { get; set; }
        public string AuditStatus { get; set; }
        public string RejectionRemark { get; set; }
    }

    public class CheckIfUserAlreadyAssignedToAnotherAuditRequest
    {
        public int RoleUserMapId { get; set; }
        public int AuditId { get; set; }
        public DateTime AuditFromDate { get; set; }
        public DateTime AuditToDate { get; set; }
    }

    public class CheckIfUserAlreadyAssignedToAnotherAuditResponse
    {
        public int Count { get; set; }
        public int AssignedAuditId { get; set; }
        public string PaoName { get; set; }
        public DateTime? AssignedFromDate { get; set; }
        public DateTime? AssignedToDate { get; set; }
    }

}
