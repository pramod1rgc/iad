﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AID.BusinessLayer.SendIntimationMailsBL;
using BusinessModel.Audits;
using IRepository;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebApi.CustomFilter;

namespace WebApi.Controllers.Audits
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    [IADExceptionFilter]
    [IADActionFilter]
    public class SendIntimationMailsController : ControllerBase
    {
        private ISendIntimationMailsRepository repository = null;
        public SendIntimationMailsController()
        {
            this.repository = new SendIntimationMailsBL();
        }
        /// <summary>
        /// GetAllPAaoAuditRecord
        /// </summary>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<ActionResult> GetAllPAaoAuditRecord(int fYearid)
        {
            var mytask = Task.Run(() => repository.GetSendIntimationMailsPao(fYearid));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }

        }
        /// <summary>
        /// SendMail
        /// </summary>
        /// <param name="requestmodel"></param>
        /// <returns></returns>
        [HttpPost("[action]")]
        public async Task<ActionResult> SendMail([FromBody]SendIntimationMailsBM requestmodel)
        {
            var mytask = Task.Run(() => repository.SendMail(requestmodel));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }
        }
    }
}