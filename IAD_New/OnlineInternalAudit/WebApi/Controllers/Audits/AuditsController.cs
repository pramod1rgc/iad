﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AID.BusinessLayer.AuditBL;
using BusinessModel.Audits;
using IRepository;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebApi.CustomFilter;

namespace WebApi.Controllers.Audits
{
    [Route("api/[controller]")]
    [ApiController]
    [IADExceptionFilter]
    [IADActionFilter]
    [Authorize]
    public class AuditsController : ControllerBase
    {
        private IAuditsRepository repository = null;
        public AuditsController()
        {
            this.repository = new AuditBL();
        }



        [HttpGet("[action]")]
        public async Task<ActionResult> GetApprovedScheduledAudits(int finYearId, string quarter, int roleUserMapId)
        {
            var mytask = Task.Run(() => repository.GetApprovedScheduledAudits(finYearId, quarter, roleUserMapId));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }

        }


        [HttpGet("[action]")]
        public async Task<ActionResult> GetApprovedAuditTeam(int auditId)
        {
            var mytask = Task.Run(() => repository.GetApprovedAuditTeam(auditId));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }

        }

        [HttpPost("[action]")]
        public async Task<ActionResult> RevertAuditTeam([FromBody] RevertAuditTeamRequest request)
        {
            var mytask = Task.Run(() => repository.RevertAuditTeam(request));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }

        }

        [HttpGet("[action]")]
        public async Task<ActionResult> GetApprovedAuditTeamAuditsForDropdown(int roleUserMapId)
        {
            var mytask = Task.Run(() => repository.GetApprovedAuditTeamAuditsForDropdown(roleUserMapId));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }

        }
    }
}