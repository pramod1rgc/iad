﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace WebApi.Controllers.Audits
{
    [Route("api/[controller]")]
    [ApiController]
    public class ActivateUserController : ControllerBase
    {
        [HttpGet("[action]")]
        public async Task<ActionResult> Activate(string uid)
        {          
            return Redirect("http://localhost:5500/changepassword?u="+ uid);
        }
    }
}