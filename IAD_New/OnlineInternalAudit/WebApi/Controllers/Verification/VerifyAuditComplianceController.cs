﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AID.BusinessLayer.VerifyAuditComplianceBL;
using IRepository;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebApi.CustomFilter;

namespace WebApi.Controllers.Verification
{
    [Route("api/[controller]")]
    [ApiController]
    [IADActionFilter]
    [IADExceptionFilter]
    [Authorize]
    public class VerifyAuditComplianceController : ControllerBase
    {
        private IVerifyAuditComplianceRepository repository = null;
        public VerifyAuditComplianceController()
        {
            this.repository = new VerifyAuditComplianceBL();
        }


        [HttpGet("[action]")]
        public async Task<ActionResult> GetAuditsForwardedToSupervisor()
        {
            var mytask = Task.Run(() => repository.GetAuditsForwardedToSupervisor());
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }
        }


        [HttpGet("[action]")]
        public async Task<ActionResult> GetAuditsWhoseParasOrComplainceForwardedToSupervisor(string type)
        {
            var mytask = Task.Run(() => repository.GetAuditsWhoseParasOrComplainceForwardedToSupervisor(type));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }
        }

        [HttpGet("[action]")]
        public async Task<ActionResult> GetAuditsWhoseSettlementLetterForwardedToSupervisor()
        {
            var mytask = Task.Run(() => repository.GetAuditsWhoseSettlementLetterForwardedToSupervisor());
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }
        }
    }
}