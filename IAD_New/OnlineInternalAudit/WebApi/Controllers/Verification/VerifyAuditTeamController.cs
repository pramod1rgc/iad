﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AID.BusinessLayer.VerifyAuditTeamBL;
using BusinessModel.Verification;
using IRepository;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebApi.CustomFilter;

namespace WebApi.Controllers.Verification
{
    [Route("api/[controller]")]
    [ApiController]
    [IADExceptionFilter]
    [IADActionFilter]
    [Authorize]
    public class VerifyAuditTeamController : ControllerBase
    {
        private IVerifyAuditTeamRepository repository = null;
        public VerifyAuditTeamController()
        {
            this.repository = new VerifyAuditTeamBL();
        }


        [HttpGet("[action]")]
        public async Task<ActionResult> GetTeamOfAuditForVerification(int auditId)
        {
            var mytask = Task.Run(() => repository.GetTeamOfAuditForVerification(auditId));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }
        }


        [HttpPost("[action]")]
        public async Task<ActionResult> UpdateTeamOfAuditForVerification([FromBody] UpdateAuditTeamForVerificationRequest request)
        {
            //var url = _CommonKeys;
            var mytask = Task.Run(() => repository.UpdateTeamOfAuditForVerification(request));
            var result = await mytask;
            if (result == 0)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }
        }

        [HttpGet("[action]")]
        public async Task<ActionResult> GetAuditDetailsForVerificationDropdown(int roleUserMapId)
        {
            var mytask = Task.Run(() => repository.GetAuditDetailsForVerificationDropdown(roleUserMapId));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }
        }
    }
}