﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AID.BusinessLayer.VerifyanualplanBL;
using BusinessModel.AuditStructure;
using IRepository;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebApi.CustomFilter;

namespace WebApi.Controllers.Verification
{
    [Route("api/[controller]")]
    [ApiController]
    [IADActionFilter]
    [IADExceptionFilter]
    //[Authorize]
    public class VerifyannualplanController : ControllerBase
    {
        private IVerifyannualplan repository = null;
        public VerifyannualplanController()
        {
            this.repository = new VerifyanualplanBL();
        }
        /// <summary>
        /// Getauditlistforverify
        /// </summary>
        /// <param name="FYID"></param>
        /// <param name="RoleUserMapId"></param>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async  Task<ActionResult> Getauditlistforverify(int FYID,int  RoleUserMapId)
        {
            var mytask = Task.Run(() => repository.Getauditlistforverify(FYID, RoleUserMapId));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }
        }
        /// <summary>
        /// AnualplanApprove
        /// </summary>
        /// <param name="FYID"></param>
        /// <param name="RoleUserMapId"></param>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> AnualplanApprove(int FYID,int RoleUserMapId)
        {
            //string result = string.Empty;
            //return result = repository.AnualplanApprove(FYID);
            var mytask = Task.Run(() => repository.AnualplanApprove(FYID, RoleUserMapId));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }
        }
        /// <summary>
        /// RejectanualsAudit
        /// </summary>
        /// <param name="FYID"></param>
        /// <param name="rejectreson"></param>
        /// <param name="RoleUserMapId"></param>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> RejectanualsAudit(int FYID,string rejectreson,int RoleUserMapId)
        {
            //string result = string.Empty;
            //return result = repository.AnualplanApprove(FYID);
            var mytask = Task.Run(() => repository.RejectanualsAudit(FYID, rejectreson, RoleUserMapId));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }
        }
        /// <summary>
        /// GetauditlistforverifyQuarterly
        /// </summary>
        /// <param name="FYID"></param>
        /// <param name="Quarter"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        //For Quarter Approved
        [HttpGet("[action]")]
        public async Task<ActionResult> GetauditlistforverifyQuarterly(int FYID,string Quarter ,int userId)
        {
            var mytask = Task.Run(() => repository.GetauditlistforverifyQuarterly(FYID, Quarter, userId));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }
        }

        //For Quarter Approved
        /// <summary>
        /// getAllPendingQuarter
        /// </summary>
        /// <param name="FYID"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<ActionResult> getAllPendingQuarter(int FYID, int userId)
        {
            var mytask = Task.Run(() => repository.getAllPendingQuarter(FYID, userId));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }
        }

        /// <summary>
        /// QuarterlylplanApprove
        /// </summary>
        /// <param name="FYID"></param>
        /// <param name="Quarter"></param>
        /// <param name="RoleUserMapId"></param>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> QuarterlylplanApprove(int FYID,string Quarter,int RoleUserMapId)
        {
            //string result = string.Empty;
            //return result = repository.AnualplanApprove(FYID);
            var mytask = Task.Run(() => repository.QuarterlylplanApprove(FYID, Quarter, RoleUserMapId));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }
        }
        /// <summary>
        /// RejectQuarterlyAudit
        /// </summary>
        /// <param name="FYID"></param>
        /// <param name="rejectreson"></param>
        /// <param name="Quarter"></param>
        /// <param name="RoleUserMapId"></param>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> RejectQuarterlyAudit(int FYID, string rejectreson, string Quarter,int RoleUserMapId)
        {
            //string result = string.Empty;
            //return result = repository.AnualplanApprove(FYID);
            var mytask = Task.Run(() => repository.RejectQuarterlyAudit(FYID, rejectreson, Quarter, RoleUserMapId));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }
        }
        /// <summary>
        /// SupervisorTosupervisor
        /// </summary>
        /// <param name="currFYear"></param>
        /// <param name="roleUserMapId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        [HttpGet("[action]")]
        public string SupervisorTosupervisor(string currFYear, int roleUserMapId, int userId)
        {
            return repository.SupervisorTosupervisor(currFYear, roleUserMapId, userId);


        }
        /// <summary>
        /// SupervisorTosupervisorQuarterlyPlan
        /// </summary>
        /// <param name="currFYear"></param>
        /// <param name="roleUserMapId"></param>
        /// <param name="userId"></param>
        /// <param name="Quarter"></param>
        /// <returns></returns>
        [HttpGet("[action]")]
        public string SupervisorTosupervisorQuarterlyPlan(string currFYear, int roleUserMapId, int userId, string Quarter)
        {
            return repository.SupervisorTosupervisorQuarterlyPlan(currFYear, roleUserMapId, userId, Quarter);


        }
    }
}