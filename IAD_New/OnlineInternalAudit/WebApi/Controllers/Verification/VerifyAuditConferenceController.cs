﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AID.BusinessLayer.VerifyAuditConferenceBL;
using IRepository;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebApi.CustomFilter;

namespace WebApi.Controllers.Verification
{
    [Route("api/[controller]")]
    [ApiController]
    [IADExceptionFilter]
    [IADActionFilter]
    [Authorize]
    public class VerifyAuditConferenceController : ControllerBase
    {
        private IVerifyAuditConferenceRepository repository = null;
        public VerifyAuditConferenceController()
        {
            this.repository = new VerifyAuditConferenceBL();
        }


        [HttpGet("[action]")]
        public async Task<ActionResult> GetAuditsOfConferenceForVerification(string type)
        {
            var mytask = Task.Run(() => repository.GetAuditsOfConferenceForVerification(type));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }

        }
    }
}