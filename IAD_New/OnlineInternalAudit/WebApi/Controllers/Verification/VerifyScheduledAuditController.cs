﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AID.BusinessLayer.VerifyScheduleAuditBL;
using BusinessModel.Verification;
using IRepository;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebApi.CustomFilter;

namespace WebApi.Controllers.Verification
{
    [Route("api/[controller]")]
    [ApiController]
    [IADExceptionFilter]
    [IADActionFilter]
    //[Authorize]
    public class VerifyScheduledAuditController : ControllerBase
    {
        private IVerifyScheduleAuditRepository repository = null;
        public VerifyScheduledAuditController()
        {
            this.repository = new VerifyScheduleAuditBL();
        }



        [HttpGet("[action]")]
        public async Task<ActionResult> GetScheduledAuditsForVerification(int finYearId, string quarter, int roleUserMapId)
        {
            var mytask = Task.Run(() => repository.GetScheduledAuditsForVerification(finYearId, quarter, roleUserMapId));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }

        }

        [HttpPost("[action]")]
        public async Task<ActionResult> UpdateScheduleAuditForVerification([FromBody]UpdateScheduleAuditForVerificationRequest request)
        {
            var mytask = Task.Run(() => repository.UpdateScheduleAuditForVerification(request));
            var result = await mytask;
            if (result == 0)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }

        }


        [HttpGet("[action]")]
        public async Task<ActionResult> GetPendingTaskCount(int RoleUserMapId)
        {
            var mytask = Task.Run(() => repository.GetPendingTaskCount(RoleUserMapId));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }
        }


        [HttpGet("[action]")]
        public async Task<ActionResult> GetDistinctQuartersBasedOnStatusAndFY(int fyId, string statusId, int statusId2)
        {
            var result = await Task.Run(() => repository.GetDistinctQuartersBasedOnStatusAndFY(fyId, statusId, statusId2));

            if(result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }
        }
    }
}