﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AID.BusinessLayer.RiskCategoryBL;
using BusinessModel.Master;
using IRepository;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebApi.CustomFilter;

namespace WebApi.Controllers.Master
{
    [Route("api/[controller]")]
    [ApiController]
    [IADExceptionFilter]
    [IADActionFilter]
    [Authorize]
    public class RiskCategoryController : ControllerBase
    {
        private IRiskCategoryRepository repository = null;
        public RiskCategoryController()
        {
            this.repository = new RiskCategoryBL();
        }
        /// <summary>
        /// SaveRiskCategory
        /// </summary>
        /// <param name="hdnCategoryId"></param>
        /// <param name="MstCategoryDetails"></param>
        /// <returns></returns>
        [HttpPost("[action]")]
        public async Task<string> SaveRiskCategory(string hdnCategoryId, CategoryDetailsModel MstCategoryDetails)
        {
            var result = await Task.Run(() => repository.SaveRiskCategory(hdnCategoryId, MstCategoryDetails));
            return result;

        }
        /// <summary>
        /// GetAllCategoryDetails
        /// </summary>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<List<CategoryDetailsModel>> GetAllCategoryDetails()
        {
            return await Task.Run(() => repository.GetAllCategoryDetails());

        }
        /// <summary>
        /// ActiveDeactive
        /// </summary>
        /// <param name="MstCategoryDetails"></param>
        /// <returns></returns>
        [HttpPost("[action]")]
        public async Task<string> ActiveDeactive(CategoryDetailsModel MstCategoryDetails)
        {
            var result = await Task.Run(() => repository.ActiveDeactive(MstCategoryDetails));
            return result;

        }
        /// <summary>
        /// BindDropDawnCategory
        /// </summary>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<List<CategoryDetailsModel>> BindDropDawnCategory()
        {
            return await Task.Run(() => repository.BindDropDawnCategory());

        }
    }
}
