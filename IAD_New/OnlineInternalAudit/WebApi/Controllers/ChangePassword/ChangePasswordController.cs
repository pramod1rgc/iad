﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AID.BusinessLayer.ChangePasswordBL;
using BusinessModel.ChangePassword;
using IRepository;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebApi.CustomFilter;

namespace WebApi.Controllers.changepassword
{
    [Route("api/[controller]")]
    [ApiController]
    [IADExceptionFilter]
    [IADActionFilter]
    public class ChangePasswordController : ControllerBase
    {
        private IChangePassword repository = null;
        public ChangePasswordController()
        {
            this.repository = new ChangePasswordBL();
        }
        /// <summary>
        /// ChangePassword
        /// </summary>
        /// <param name="requestmodel"></param>
        /// <returns></returns>
        [HttpPost("[action]")]
        public async Task<ActionResult> ChangePassword([FromBody]ChangePasswordBM requestmodel)
        {
            var mytask = Task.Run(() => repository.ChangePassword(requestmodel));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }

        }
    }
}