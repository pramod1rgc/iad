﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AID.BusinessLayer.AuditReportBL;
using IRepository;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebApi.CustomFilter;

namespace WebApi.Controllers.AuditReport
{
    [Route("api/[controller]")]
    [ApiController]
    [IADExceptionFilter]
    [IADActionFilter]
    //[Authorize]
    public class AuditReportController : ControllerBase
    {
        private IAuditReportRepository repository = null;

        public AuditReportController()
        {
            this.repository = new AuditReportBL();
        }


        [HttpGet("[action]")]
        public async Task<ActionResult> GetAuditsForAuditReportBySupervisor(int pageSize, int pageNumber, string searchTerm)
        {
            var mytask = Task.Run(() => repository.GetAuditsForAuditReportBySupervisor(pageSize, pageNumber, searchTerm));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }
        }

        [HttpGet("[action]")]
        public async Task<ActionResult> GetAuditReport(int auditId)
        {
            var mytask = Task.Run(() => repository.GetAuditReport(auditId));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }
        }
    }
}