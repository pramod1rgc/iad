﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using WebApi.CustomFilter;

namespace WebApi.Controllers
{
  
    [Route("api/[controller]")]
    [ApiController]
    [IADExceptionFilter]
   
    public class TestController : Controller
    {
        [HttpGet]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public string Index()
        {
            //DateTime datetime = new DateTime(55, 33, 2323);
            //return datetime.ToString();
            //int i = 0 / 100;  
           return "Test index";

        }
    }
}