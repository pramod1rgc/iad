﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AID.BusinessLayer.RoleBL;
using BusinessModel.Role;
using IRepository;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebApi.CustomFilter;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [IADExceptionFilter]
    [IADActionFilter]
    [Authorize]
    public class RoleController : ControllerBase
    {
        private IRoleRepository repository = null;
        public RoleController()
        {
            this.repository = new RoleBL();
        }

        [HttpGet("[action]")]
        public async Task<ActionResult> GetRoles()
        {
            var mytask = Task.Run(() => repository.GetRoles());
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }
        }


        [HttpPost("[action]")]
        public async Task<ActionResult> UpsertRoles([FromBody]UpsertRoleRequest request)
        {
            var mytask = Task.Run(() => repository.UpsertRoles(request));
            var result = await mytask;
            if (result == 0)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }
        }

        [HttpPost("[action]")]
        public async Task<ActionResult> ToggleRoleActivation([FromBody]ToggleRoleRequest request)
        {
            var mytask = Task.Run(() => repository.ToggleRoleActivation(request));
            var result = await mytask;
            if (result == 0)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }
        }
    }
}