﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AID.BusinessLayer.AuditeeUniverseBL;
using BusinessModel.Auditee;
using IRepository;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebApi.CustomFilter;

namespace WebApi.Controllers.Auditee
{
    [Route("api/[controller]")]
    [ApiController]
    [IADExceptionFilter]
    [IADActionFilter]
    [Authorize]
    public class AuditeeUniverseController : ControllerBase
    {
        private IAuditeeUniverseRepository repository = null;
        public AuditeeUniverseController()
        {
            this.repository = new AuditeeUniverseBL();
        }
        [HttpGet("[action]")]
        public async Task<List<AuditeeUniverseBM>> GetAllAuditee(int userid)
        {
            return await Task.Run(() => repository.GetAllAuditee(userid));

        }
        [HttpGet("[action]")]
        public async Task<List<AuditeeUniverseBM>> GetAllDesignation(int rollId)
        {
            return await Task.Run(() => repository.GetAllDesignation(rollId));

        }
        /// <summary>
        /// GetAllAuditeeRole
        /// </summary>
        /// <returns></returns>
        /// 
        #region
        [HttpGet("[action]")]
        public async Task<List<AuditeeUniverseBM>> GetAuditeeRole()
        {
            return await Task.Run(() => repository.GetAuditeeRole());

        }
        #endregion

        [HttpPost("[action]")]
        public async Task<string> SaveAuditee([FromBody]AuditeeUniverseBM MstDetails, string uID)
        {
            var result = await Task.Run(() => repository.SaveAuditee(uID, MstDetails));
            return result;
        }

        [HttpPost("[action]")]
        public string ActiveDeactiveAuditee(AuditeeUniverseBM MstDetails)
        {
            var Result = repository.ActiveDeactiveUser(MstDetails);
            return Result;
        }

    }
}