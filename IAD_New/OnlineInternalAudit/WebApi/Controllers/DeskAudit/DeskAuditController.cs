﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using AID.BusinessLayer;
using BusinessModel.AuditeeObservation;
using BusinessModel.AuditObservation;
using BusinessModel.DeskAudit;
using IRepository;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace WebApi.Controllers.DeskAudit
{
    [Route("api/[controller]")]
    [ApiController]
    public class DeskAuditController : ControllerBase
    {
        private IDeskAuditRepository repository = null;
        private IHostingEnvironment _hostingEnvironment;
        public DeskAuditController(IHostingEnvironment hostingEnvironment)
        {
            this.repository = new DeskAuditBL();
            _hostingEnvironment = hostingEnvironment;
        }

   /// <summary>
   /// /
   /// </summary>
   /// <param name="formData"></param>
   /// <param name="userId"></param>
   /// <returns></returns>

        [HttpPost("[action]")]
        public async Task<ActionResult> SaveAndUpdateDeskAudit([FromForm]DeskAuditBM formData,string userId)
        {
            var files = Request.Form.Files;
            var id = Request.Form;
            DeskAuditBM request = new DeskAuditBM();
           request.CreatedbyId = Convert.ToInt32(Request.Form["userId"]);
           request.AuditId = Convert.ToInt32(Request.Form["auditId"]);
    
            if (files.Count > 0)
            {
                string fullPath = null;
                string folderName = "Documents";
                string webRootPath = _hostingEnvironment.ContentRootPath;
                string newPath = Path.Combine(webRootPath, folderName);
                request.UploadFiles = new List<DeskAuditUploadFilesRequest>();
                int i = 0;
                foreach (var file in files)
                {
                    i++;
                    var obj = new BusinessModel.DeskAudit.DeskAuditUploadFilesRequest();
                    var imageName = new String(Path.GetFileNameWithoutExtension(file.FileName).Take(10).ToArray()).Replace(" ", "-");
                    imageName = imageName + DateTime.Now.ToString("yymmssfff") + Path.GetExtension(file.FileName);
                    string fileName = ContentDispositionHeaderValue.Parse(file.ContentDisposition).FileName.Trim('"');
                    fullPath = Path.Combine(newPath, imageName);
                    string relativePath = Path.Combine(folderName, imageName);
                    if (!Directory.Exists(newPath))
                    {
                        Directory.CreateDirectory(newPath);
                    }
                    using (var stream = new FileStream(fullPath, FileMode.Create))
                    {
                        obj.Id = i;
                        obj.FileName = fileName;
                        obj.DocumentName = file.Name;
                        obj.FileType = Path.GetExtension(file.FileName);
                        obj.FileSize = file.Length.ToString();
                        obj.RelativePath = relativePath;
                        await file.CopyToAsync(stream);
                        request.UploadFiles.Add(obj);
                    }
                }
            }
            var mytask = Task.Run(() => repository.SaveDeskAudit(request));
            var result = await mytask;
            if (result == string.Empty)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }
        }



        /// <summary>
        /// Download Uploaded Files
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> DownloadFile(string url)
        {
            if (url != "null")
            {
                // get the file and convert it into a bytearray

                var fileData = System.IO.File.ReadAllBytes(url);
                // MediaTypeHeaderValue mediaTypeHeaderValue = new MediaTypeHeaderValue("application/octet");
                return new FileContentResult(fileData, "application/octet")
                {

                };
            }

            return null;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="deactiveDeskAudit"></param>
        /// <returns></returns>
        [HttpPost("[action]")]
        public async Task<ActionResult> active_deactiveDeskAudit([FromBody]DeactiveDeskAuditBM deactiveDeskAudit)
        {
            var mytask = Task.Run(() => repository.active_deactiveDeskAudit(deactiveDeskAudit));
            var result = await mytask;
            if (result == string.Empty)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }
        }

        [HttpGet("[action]")]
        public async Task<List<GetDeskAuditUploadFiles>> getDeskAuditFiles(int auditId)
        {
            return await Task.Run(() => repository.getDeskAuditFiles(auditId));
        }
    }
}