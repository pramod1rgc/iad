﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using AID.BusinessLayer.LoginBL;
using BusinessModel;
using IRepository;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using WebApi.CustomFilter;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [IADExceptionFilter]
    [IADActionFilter]
    [Authorize]
    [EnableCors("AllowAnyOrigin")]
    public class LoginController : ControllerBase
    {
        private IConfiguration _config;

        public LoginController(IConfiguration config)
        {
            this.repository = new LoginBL();
            _config = config;
        }
        private ILoginRepository repository = null;
        //public LoginController()  
        //{
        //    this.repository = new LoginRepositories();
        //}
        [HttpGet("[action]")]
        public ActionResult Index()
        {
            return Ok(1);
        }


        //[HttpPost("[action]")]
        //public async Task<ActionResult> LoginCheck1([FromBody]LoginRequestBM loginModel)
        //{
        //    var mytask = Task.Run(() => repository.Login(loginModel.Username, loginModel.Password));
        //    var result = await mytask;
        //    if (result == null)
        //    {
        //        return NotFound();
        //    }
        //    else
        //    {
        //        return Ok(result);
        //    }
        //}




        [AllowAnonymous]
        [HttpPost("[action]")]
        public async Task<IActionResult> LoginCheck([FromBody]LoginRequestBM loginModel)
        {
            IActionResult response = Unauthorized();
            var mytask = Task.Run(() => repository.Login(loginModel.Username, loginModel.Password));
            var result = await mytask;
            //var user = AuthenticateUser1(loginModel);
            if (result.Count > 0)
            {
                var tokenString = GenerateJSONWebToken(result[0]);
                response = Ok(new { token = tokenString, Status = true, Result = result });
            }
            else
            {
                response = Ok(new { Status = false });
            }
            return response;
        }

        private string GenerateJSONWebToken(LoginResponseBM userInfo)
        {

            try
            {
                var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config["Jwt:Key"]));
                var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);

                var claims = new[] {
                new Claim(JwtRegisteredClaimNames.Sub, userInfo.Username),
                new Claim(JwtRegisteredClaimNames.Email, userInfo.Password),
                //new Claim("DateOfJoing", userInfo.DateOfJoing.ToString("yyyy-MM-dd")),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString())
            };
                var token = new JwtSecurityToken(_config["Jwt:Issuer"],
                _config["Jwt:Issuer"],
                claims,
                expires: DateTime.Now.AddMinutes(120),
                signingCredentials: credentials);
                return new JwtSecurityTokenHandler().WriteToken(token);
            }
            catch (Exception e)
            {
            }
            return null;
        }

        //private LoginRequestBM AuthenticateUser1(LoginRequestBM login)
        //{
        //    LoginRequestBM user = null;

        //    //Validate the User Credentials  
        //    //Demo Purpose, I have Passed HardCoded User Information  
        //    if (login.Username == "admin")
        //    {
        //        user = new LoginRequestBM { Username = "admin", Password = "admin" };
        //    }
        //    return user;
        //}

    }
}