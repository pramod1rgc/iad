﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AID.BusinessLayer.CreateAuditPlanBL;
using BusinessModel.AuditStructure;
using IRepository;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebApi.CustomFilter;

namespace WebApi.Controllers.Auditstructure
{
    [Route("api/[controller]")]
    [ApiController]
    [IADExceptionFilter]
    [IADActionFilter]
    [Authorize]
    public class CreateAuditplanController : ControllerBase
    {
        private ICreateAuditPlan repository = null;
        public CreateAuditplanController()
        {
            this.repository = new CreateAuditPlanBL();
        }
        /// <summary>
        /// BindFYearDropDown
        /// </summary>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<List<CreateAuditPlanModel>> BindFYearDropDown()
        {
            
            return await Task.Run(() => repository.BindFYearDropDown());

        }

        [HttpGet("[action]")]
        public async Task<List<CreateAuditPlanModel>> pendingFinancialForVerifyQuarter(int roleUserMapId)
        {

            return await Task.Run(() => repository.pendingFinancialForVerifyQuarter(roleUserMapId));

        }
        /// <summary>
        /// getController
        /// </summary>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<List<CreateAuditPlanModel>> getController()
        {
            return await Task.Run(() => repository.GetController());

        }
        /// <summary>
        /// GetAllPAO
        /// </summary>
        /// <param name="ControllerID"></param>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<List<CreateAuditPlanModel>> GetAllPAO(int ControllerID)
        {
            return await Task.Run(() => repository.GetAllPAO(ControllerID));
            //var mytask = Task.Run(() => repository.GetAllPAO());
            //var result = await mytask;
            //if (result == null)
            //{
            //    return NotFound();
            //}
            //else
            //{
            //    return Ok(result);
            //}

        }
        /// <summary>
        /// Getauditlist
        /// </summary>
        /// <param name="FYID"></param>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<ActionResult> Getauditlist(int FYID)
        {
            //return await Task.Run(() => repository.Getauditlist(FYID));
            var mytask = Task.Run(() => repository.Getauditlist(FYID));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }

        }
        /// <summary>
        /// GetassignPaoAudit
        /// </summary>
        /// <param name="ObjPaolist"></param>
        /// <returns></returns>
        [HttpPost("[action]")]
        public async Task<string> GetassignPaoAudit([FromBody]List<CreateAuditPlanModel> ObjPaolist)
        {
            return await Task.Run(() => repository.GetassignPaoAudit(ObjPaolist));

        }
        /// <summary>
        /// AssignAuditByController
        /// </summary>
        /// <param name="PAOID"></param>
        /// <param name="Status"></param>
        /// <param name="currFYear"></param>
        /// <param name="paoreason"></param>
        /// <returns></returns>

        [HttpGet("[action]")]
        public string AssignAuditByController(int PAOID, string Status, string currFYear, string paoreason)
        {
            return repository.AssignAuditByController(PAOID, Status, currFYear, paoreason);


        }
        /// <summary>
        /// AssignToSupervisor
        /// </summary>
        /// <param name="currFYear"></param>
        /// <param name="roleUserMapId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        [HttpGet("[action]")]
        public string AssignToSupervisor(string currFYear,int roleUserMapId,int userId)
        {
            return repository.AssignToSupervisor(currFYear, roleUserMapId,  userId);


        }

        #region Quartaly Audit Deatils
        /// <summary>
        /// BindAuditInGrid
        /// </summary>
        /// <param name="FYID"></param>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<ActionResult> BindAuditInGrid(int FYID)
        {
            //return await Task.Run(() => repository.Getauditlist(FYID));
            var mytask = Task.Run(() => repository.BindAuditInGrid(FYID));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }

        }
        //old audit 
        /// <summary>
        /// SaveQuartalyAudit
        /// </summary>
        /// <param name="SaveQuartalyAudit"></param>
        /// <returns></returns>
        [HttpPost("[action]")]
        public async Task<ActionResult> SaveQuartalyAudit([FromBody]List<CreateAuditPlanModel> SaveQuartalyAudit)
        {
            
         var mytask = Task.Run(() => repository.SaveQuartalyAudit(SaveQuartalyAudit));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }

        }
        /// <summary>
        /// SaveQuartalyPlanAudit
        /// </summary>
        /// <param name="objquarterpaoplan"></param>
        /// <param name="financialYearId"></param>
        /// <returns></returns>
        [HttpPost("[action]")]
        public async Task<ActionResult>  SaveQuartalyPlanAudit([FromBody]List<object> objquarterpaoplan,string financialYearId)
        {
           
            var mytask = Task.Run(() => repository.SaveQuartalyPlanAudit(objquarterpaoplan, financialYearId));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }

        }
        #endregion
        /// <summary>
        /// AssignQuarterlyPlanToSupervisor
        /// </summary>
        /// <param name="currFYear"></param>
        /// <param name="roleUserMapId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        //Assign To Supervisor Qurterly
        [HttpGet("[action]")]
        public string AssignQuarterlyPlanToSupervisor(string currFYear, int roleUserMapId, int userId)
        {
            return repository.AssignQuarterlyPlanToSupervisor(currFYear, roleUserMapId, userId);


        }
        /// <summary>
        /// BindSupervisorDropDown
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<List<CreateAuditPlanModel>> BindSupervisorDropDown(int userId)
        {

            return await Task.Run(() => repository.BindSupervisorDropDown(userId));

        }
        //End Of





    }
}