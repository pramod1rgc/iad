﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AID.BusinessLayer.ScheduleAuditBL;
using BusinessModel.AuditStructure;
using IRepository;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebApi.CustomFilter;

namespace WebApi.Controllers.Auditstructure
{
    [Route("api/[controller]")]
    [ApiController]
    [IADExceptionFilter]
    [IADActionFilter]
    [Authorize]
    public class ScheduleAuditController : ControllerBase
    {
        private IScheduleAuditRepository repository = null;
        public ScheduleAuditController()
        {
            this.repository = new ScheduleAuditBL();
        }



        [HttpGet("[action]")]
        public async Task<ActionResult> GetScheduledAudits(int finYearId, string quarter)
        {
            var mytask = Task.Run(() => repository.GetScheduledAudits(finYearId, quarter));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }

        }

        [HttpPost("[action]")]
        public async Task<ActionResult> UpdateScheduledAudit([FromBody]UpdateScheduleAuditRequest request)
        {
            var mytask = Task.Run(() => repository.UpdateScheduledAudit(request));
            var result = await mytask;
            if (result == 0)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }

        }

        [HttpPost("[action]")]
        public async Task<ActionResult> DeleteScheduledAudit([FromBody]DeleteScheduleAuditRequest request)
        {
            var mytask = Task.Run(() => repository.DeleteScheduledAudit(request));
            var result = await mytask;
            if (result == 0)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }

        }
    }
}