﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AID.BusinessLayer.MenuBL;
using BusinessModel.UserManagement;
using IRepository;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebApi.CustomFilter;

namespace WebApi.Controllers.UserManagement
{
    [Route("api/[controller]")]
    [ApiController]
    [IADExceptionFilter]
    [IADActionFilter]
    [Authorize]
    public class MenuController : ControllerBase
    {
        private IMenuRepository repository = null;
        public MenuController()
        {
            this.repository = new MenuBL();
        }
        
        [HttpGet("[action]")]
        public async Task<List<MenuModel>> BindMenuInGride()
        { 
            return await Task.Run(() => repository.BindMenuInGride());

        }
        [HttpGet("[action]")]
        public async Task<List<MenuModel>> bindMenuInDropDown()
        {
            return await Task.Run(() => repository.BindMenuInDropDown());

        }
        [HttpGet("[action]")]
        public async Task<List<MenuModel>> GetAllPredefineRole()
        {
            return await Task.Run(() => repository.GetAllPredefineRole());

        }
    }
}