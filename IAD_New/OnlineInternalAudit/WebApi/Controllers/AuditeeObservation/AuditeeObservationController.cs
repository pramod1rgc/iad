﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using AID.BusinessLayer.AuditeeObservationBL;
using BusinessModel.AuditeeObservation;
using BusinessModel.DeskAudit;
using IRepository;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebApi.CustomFilter;

namespace WebApi.Controllers.AuditeeObservation
{
    [Route("api/[controller]")]
    [ApiController]
    [IADExceptionFilter]

    [Authorize(AuthenticationSchemes = "Bearer")]
    public class AuditeeObservationController : ControllerBase
    {
        private IAuditeeObservationRepository repository = null;
        private IHostingEnvironment _hostingEnvironment;
        public AuditeeObservationController(IHostingEnvironment hostingEnvironment)
        {
            this.repository = new AuditeeObservationBL();
            _hostingEnvironment = hostingEnvironment;
        }


        [HttpGet("[action]")]
        public async Task<ActionResult> GetAuditDHAuditsList(int auditUserId)
        {
            var mytask = Task.Run(() => repository.GetAuditDHAuditsList(auditUserId));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }
        }

        [HttpGet("[action]")]
        public async Task<ActionResult> GetAuditEntryConference(int auditId)
        {
            var mytask = Task.Run(() => repository.GetAuditEntryConference(auditId));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }
        }


        [HttpPost("[action]")]
        public async Task<ActionResult> UpdateAuditEntryConference([FromBody] UpdateAuditeeEntryConferenceRequest request)
        {
            var mytask = Task.Run(() => repository.UpdateAuditEntryConference(request));
            var result = await mytask;
            if (result == 0)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }
        }


        [HttpGet("[action]")]
        public async Task<ActionResult> GetAuditExitConference(int auditId)
        {
            var mytask = Task.Run(() => repository.GetAuditExitConference(auditId));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }
        }


        [HttpPost("[action]")]
        public async Task<ActionResult> UpdateAuditExitConference([FromBody] UpdateAuditeeExitConferenceRequest request)
        {
            var mytask = Task.Run(() => repository.UpdateAuditExitConference(request));
            var result = await mytask;
            if (result == 0)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }
        }


        [HttpGet("[action]")]
        public async Task<ActionResult> GetAuditPara(int roleId,int auditId, int pageSize, int pageNumber, string searchTerm)
      {
            var mytask = Task.Run(() => repository.GetAuditPara(roleId,auditId, pageSize, pageNumber, searchTerm));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }
        }


        [HttpPost("[action]")]
        public async Task<ActionResult> UpdateAuditPara()
        {
            var files = Request.Form.Files;
            UpdateAuditeeParaRequest request = new UpdateAuditeeParaRequest();
            request.AuditId = Convert.ToInt32(Request.Form["AuditId"]);
            request.ParaId = Convert.ToInt32(Request.Form["ParaId"]);
            request.Title = Convert.ToString(Request.Form["Title"]);
            request.Criteria = Convert.ToString(Request.Form["Criteria"]);
            request.Conditions = Convert.ToString(Request.Form["Conditions"]);
            request.Consequences = Convert.ToString(Request.Form["Consequences"]);
            request.Causes = Convert.ToString(Request.Form["Causes"]);
            request.CorrectiveActions = Convert.ToString(Request.Form["CorrectiveActions"]);
            request.Severity = Convert.ToString(Request.Form["Severity"]);
            request.RiskCategoryId = Convert.ToInt32(Request.Form["RiskCategoryId"]);
            request.UserId = Convert.ToInt32(Request.Form["UserId"]);
            if (files.Count > 0)
            {
                string fullPath = null;
                string folderName = "Documents";
                string webRootPath = _hostingEnvironment.ContentRootPath;
                string newPath = Path.Combine(webRootPath, folderName);
                request.UploadFiles = new List<UploadFilesRequest>();
                int i = 0;
                foreach (var file in files)
                {
                    i++;
                    var obj = new UploadFilesRequest();
                    var imageName = new String(Path.GetFileNameWithoutExtension(file.FileName).Take(10).ToArray()).Replace(" ", "-");
                    imageName = imageName + DateTime.Now.ToString("yymmssfff") + Path.GetExtension(file.FileName);
                    string fileName = ContentDispositionHeaderValue.Parse(file.ContentDisposition).FileName.Trim('"');
                    fullPath = Path.Combine(newPath, fileName);
                    string relativePath = Path.Combine(folderName, fileName);
                    if (!Directory.Exists(newPath))
                    {
                        Directory.CreateDirectory(newPath);
                    }
                    using (var stream = new FileStream(fullPath, FileMode.Create))
                    {
                        obj.Id = i;
                        obj.FileName = fileName;
                        obj.FileType = Path.GetExtension(file.FileName);
                        obj.FileSize = file.Length.ToString();
                        obj.RelativePath = relativePath;
                        await file.CopyToAsync(stream);
                        request.UploadFiles.Add(obj);
                    }
                }
            }
            var mytask = Task.Run(() => repository.UpdateAuditPara(request));
            var result = await mytask;
            if (result == 0)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }
        }


        [HttpGet("[action]")]
        public async Task<ActionResult> GetAuditParaDetails(int paraId)
        {
            var mytask = Task.Run(() => repository.GetAuditParaDetails(paraId));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }
        }

        [HttpPost("[action]")]
        public async Task<ActionResult> DeleteAuditPara([FromBody]DeleteAuditeeParaRequest request)
        {
            var mytask = Task.Run(() => repository.DeleteAuditPara(request));
            var result = await mytask;
            if (result == 0)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }
        }

        /// <summary>
        /// Download Uploaded Files
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> DownloadFile(string url)
        {
            if (url != "null")
            {
                // get the file and convert it into a bytearray
     
                var fileData = System.IO.File.ReadAllBytes(url);
                // MediaTypeHeaderValue mediaTypeHeaderValue = new MediaTypeHeaderValue("application/octet");
                return new FileContentResult(fileData, "application/octet")
                {

                };
            }

            return null;
        }

       

        [HttpPost("[action]")]
        public async Task<ActionResult> PostParaCommentByAuditor()
        {
            var files = Request.Form.Files;
            PostParaCommentByAuditorRequest request = new PostParaCommentByAuditorRequest();
            request.AuditId = Convert.ToInt32(Request.Form["AuditId"]);
            request.ParaId = Convert.ToInt32(Request.Form["ParaId"]);
            request.Comment = Convert.ToString(Request.Form["Comment"]);
            request.UserId = Convert.ToInt32(Request.Form["UserId"]);
            if (files.Count > 0)
            {
                string fullPath = null;
                string folderName = "Documents";
                string webRootPath = _hostingEnvironment.ContentRootPath;
                string newPath = Path.Combine(webRootPath, folderName);
                request.Files = new List<PostParaCommentFiles>();
                int i = 0;
                foreach (var file in files)
                {
                    i++;
                    var obj = new PostParaCommentFiles();
                    var imageName = new String(Path.GetFileNameWithoutExtension(file.FileName).Take(10).ToArray()).Replace(" ", "-");
                    imageName = imageName + DateTime.Now.ToString("yymmssfff") + Path.GetExtension(file.FileName);
                    string fileName = ContentDispositionHeaderValue.Parse(file.ContentDisposition).FileName.Trim('"');
                    fullPath = Path.Combine(newPath, fileName);
                    string relativePath = Path.Combine(folderName, fileName);
                    if (!Directory.Exists(newPath))
                    {
                        Directory.CreateDirectory(newPath);
                    }
                    using (var stream = new FileStream(fullPath, FileMode.Create))
                    {
                        obj.Id = i;
                        obj.FileName = fileName;
                        obj.FileType = Path.GetExtension(file.FileName);
                        obj.FileSize = file.Length.ToString();
                        obj.RelativePath = relativePath;
                        await file.CopyToAsync(stream);
                        request.Files.Add(obj);
                    }
                }
            }
            var mytask = Task.Run(() => repository.PostParaCommentByAuditor(request));
            var result = await mytask;
            if (result == 0)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }
        }

        [HttpGet("[action]")]
        public async Task<ActionResult> GetParaComments(int paraId)
        {
            var mytask = Task.Run(() => repository.GetParaComments(paraId));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }
        }

        [HttpGet("[action]")]
        public async Task<ActionResult> GetAuditParaSettlementLetter(int auditId)
        {
            var mytask = Task.Run(() => repository.GetAuditParaSettlementLetter(auditId));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }
        }

        [HttpPost("[action]")]
        public async Task<ActionResult> UpdateAuditParaSettlementLetter([FromBody]UpdateAuditeeParaSettlementLetterRequest request)
        {
            var mytask = Task.Run(() => repository.UpdateAuditParaSettlementLetter(request));
            var result = await mytask;
            if (result == 0)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }
        }

        [HttpGet("[action]")]
        public async Task<ActionResult> UpdateParaSettlementLetterStatus(int auditId, int statusId, string rejectionRemark)
        {
            var mytask = Task.Run(() => repository.UpdateParaSettlementLetterStatus(auditId, statusId, rejectionRemark));
            var result = await mytask;
            return Ok(result);
        }

        [HttpPost("[action]")]
        public async Task<ActionResult> UpdateAuditStatus([FromBody]AuditeeStatusUpdateRequest request)
        {
            var mytask = Task.Run(() => repository.UpdateAuditstatus(request));
            var result = await mytask;
            if (result == 0)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }
        }

        [HttpPost("[action]")]
        public async Task<ActionResult> UpdateAuditParaForwardedStatus([FromBody]AuditeeStatusUpdateRequest request)
        {
            var mytask = Task.Run(() => repository.UpdateAuditstatus(request));
            var result = await mytask;
            if (result == 0)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }
        }


        [HttpPost("[action]")]
        public async Task<ActionResult> UpdateAuditParaStatus([FromBody] AuditeeParaStatusRequest request)
        {
       
            var mytask = Task.Run(() => repository.UpdateAuditParaStatus(request));
            var result = await mytask;
            if (result == 0)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }
        }
    }
}