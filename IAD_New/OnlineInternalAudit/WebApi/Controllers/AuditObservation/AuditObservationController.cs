﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using AID.BusinessLayer.AuditObservationBL;
using BusinessModel.AuditObservation;
using IRepository;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using WebApi.CustomFilter;

namespace WebApi.Controllers.AuditObservation
{
    [Route("api/[controller]")]
    [ApiController]
    [IADExceptionFilter]
    [IADActionFilter]
    //[Authorize]
    public class AuditObservationController : ControllerBase
    {
        private IAuditObservationRepository repository = null;
        private IHostingEnvironment _hostingEnvironment;
        public AuditObservationController(IHostingEnvironment hostingEnvironment)
        {
            this.repository = new AuditObservationBL();
            _hostingEnvironment = hostingEnvironment;
        }


        [HttpGet("[action]")]
        public async Task<ActionResult> GetAuditDHAuditsList(int auditUserId)
        {
            var mytask = Task.Run(() => repository.GetAuditDHAuditsList(auditUserId));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }
        }

        [HttpGet("[action]")]
        public async Task<ActionResult> GetAuditEntryConference(int auditId)
        {
            var mytask = Task.Run(() => repository.GetAuditEntryConference(auditId));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }
        }


        [HttpPost("[action]")]
        public async Task<ActionResult> UpdateAuditEntryConference([FromBody] UpdateAuditEntryConferenceRequest request)
        {
            var mytask = Task.Run(() => repository.UpdateAuditEntryConference(request));
            var result = await mytask;
            if (result == 0)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }
        }


        [HttpGet("[action]")]
        public async Task<ActionResult> GetAuditExitConference(int auditId)
        {
            var mytask = Task.Run(() => repository.GetAuditExitConference(auditId));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }
        }


        [HttpPost("[action]")]
        public async Task<ActionResult> UpdateAuditExitConference([FromBody] UpdateAuditExitConferenceRequest request)
        {
            var mytask = Task.Run(() => repository.UpdateAuditExitConference(request));
            var result = await mytask;
            if (result == 0)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }
        }


        [HttpGet("[action]")]
        public async Task<ActionResult> GetAuditPara(int auditId, int pageSize, int pageNumber, string searchTerm)
        {
            var mytask = Task.Run(() => repository.GetAuditPara(auditId, pageSize, pageNumber, searchTerm));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }
        }


        [HttpPost("[action]")]
        public async Task<ActionResult> UpdateAuditPara()
        {
            var files = Request.Form.Files;
            UpdateAuditParaRequest request = new UpdateAuditParaRequest();
            request.AuditId = Convert.ToInt32(Request.Form["AuditId"]);
            request.ParaId = Convert.ToInt32(Request.Form["ParaId"]);
            request.Title = Convert.ToString(Request.Form["Title"]);
            request.Criteria = Convert.ToString(Request.Form["Criteria"]);
            request.Conditions = Convert.ToString(Request.Form["Conditions"]);
            request.Consequences = Convert.ToString(Request.Form["Consequences"]);
            request.Causes = Convert.ToString(Request.Form["Causes"]);
            request.CorrectiveActions = Convert.ToString(Request.Form["CorrectiveActions"]);
            request.Severity = Convert.ToString(Request.Form["Severity"]);
            request.RiskCategoryId = Convert.ToInt32(Request.Form["RiskCategoryId"]);
            request.UserId = Convert.ToInt32(Request.Form["UserId"]);
            var financialImplications = Convert.ToString(Request.Form["FinancialImplications"]);
            var financialImplicationArray = JsonConvert.DeserializeObject<List<FinancialImplicationCasesRequest>>(financialImplications);
            if (financialImplicationArray.Count() > 0)
            {
                request.ParaFinancialImplications = new List<FinancialImplicationCasesRequest>();
                request.ParaFinancialImplications = financialImplicationArray;
            }
            if (files.Count > 0)
            {
                string fullPath = null;
                string folderName = "Documents";
                string webRootPath = _hostingEnvironment.ContentRootPath;
                string newPath = Path.Combine(webRootPath, folderName);
                request.UploadFiles = new List<UploadFilesRequest>();
                int i = 0;
                foreach (var file in files)
                {
                    i++;
                    var obj = new UploadFilesRequest();
                    var imageName = new String(Path.GetFileNameWithoutExtension(file.FileName).Take(10).ToArray()).Replace(" ", "-");
                    imageName = imageName + DateTime.Now.ToString("yymmssfff") + Path.GetExtension(file.FileName);
                    string fileName = ContentDispositionHeaderValue.Parse(file.ContentDisposition).FileName.Trim('"');
                    fullPath = Path.Combine(newPath, fileName);
                    string relativePath = Path.Combine(folderName, fileName);
                    if (!Directory.Exists(newPath))
                    {
                        Directory.CreateDirectory(newPath);
                    }
                    using (var stream = new FileStream(fullPath, FileMode.Create))
                    {
                        obj.Id = i;
                        obj.FileName = fileName;
                        obj.FileType = Path.GetExtension(file.FileName);
                        obj.FileSize = file.Length.ToString();
                        obj.RelativePath = relativePath;
                        await file.CopyToAsync(stream);
                        request.UploadFiles.Add(obj);
                    }
                }
            }
            var mytask = Task.Run(() => repository.UpdateAuditPara(request));
            var result = await mytask;
            if (result == 0)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }
        }

        [HttpGet("[action]")]
        public async Task<ActionResult> GetAuditParaDetails(int paraId)
        {
            var mytask = Task.Run(() => repository.GetAuditParaDetails(paraId));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }
        }

        [HttpPost("[action]")]
        public async Task<ActionResult> DeleteAuditPara([FromBody]DeleteAuditParaRequest request)
        {
            var mytask = Task.Run(() => repository.DeleteAuditPara(request));
            var result = await mytask;
            if (result == 0)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }
        }

        [HttpPost("[action]")]
        public async Task<ActionResult> PostParaCommentByAuditor()
        {
            var files = Request.Form.Files;
            PostParaCommentByAuditorRequest request = new PostParaCommentByAuditorRequest();
            request.AuditId = Convert.ToInt32(Request.Form["AuditId"]);
            request.ParaId = Convert.ToInt32(Request.Form["ParaId"]);
            request.Comment = Convert.ToString(Request.Form["Comment"]);
            request.UserId = Convert.ToInt32(Request.Form["UserId"]);
            if (files.Count > 0)
            {
                string fullPath = null;
                string folderName = "Documents";
                string webRootPath = _hostingEnvironment.ContentRootPath;
                string newPath = Path.Combine(webRootPath, folderName);
                request.Files = new List<PostParaCommentFiles>();
                int i = 0;
                foreach (var file in files)
                {
                    i++;
                    var obj = new PostParaCommentFiles();
                    var imageName = new String(Path.GetFileNameWithoutExtension(file.FileName).Take(10).ToArray()).Replace(" ", "-");
                    imageName = imageName + DateTime.Now.ToString("yymmssfff") + Path.GetExtension(file.FileName);
                    string fileName = ContentDispositionHeaderValue.Parse(file.ContentDisposition).FileName.Trim('"');
                    fullPath = Path.Combine(newPath, fileName);
                    string relativePath = Path.Combine(folderName, fileName);
                    if (!Directory.Exists(newPath))
                    {
                        Directory.CreateDirectory(newPath);
                    }
                    using (var stream = new FileStream(fullPath, FileMode.Create))
                    {
                        obj.Id = i;
                        obj.FileName = fileName;
                        obj.FileType = Path.GetExtension(file.FileName);
                        obj.FileSize = file.Length.ToString();
                        obj.RelativePath = relativePath;
                        await file.CopyToAsync(stream);
                        request.Files.Add(obj);
                    }
                }
            }
            var mytask = Task.Run(() => repository.PostParaCommentByAuditor(request));
            var result = await mytask;
            if (result == 0)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }
        }

        [HttpGet("[action]")]
        public async Task<ActionResult> GetParaComments(int paraId)
        {
            var mytask = Task.Run(() => repository.GetParaComments(paraId));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }
        }

        [HttpGet("[action]")]
        public async Task<ActionResult> GetAuditParaSettlementLetter(int auditId)
        {
            var mytask = Task.Run(() => repository.GetAuditParaSettlementLetter(auditId));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }
        }

        [HttpPost("[action]")]
        public async Task<ActionResult> UpdateAuditParaSettlementLetter([FromBody]UpdateAuditParaSettlementLetterRequest request)
        {
            var mytask = Task.Run(() => repository.UpdateAuditParaSettlementLetter(request));
            var result = await mytask;
            if (result == 0)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }
        }

        [HttpGet("[action]")]
        public async Task<ActionResult> UpdateParaSettlementLetterStatus(int auditId, int statusId, string rejectionRemark)
        {
            var mytask = Task.Run(() => repository.UpdateParaSettlementLetterStatus(auditId, statusId, rejectionRemark));
            var result = await mytask;
            return Ok(result);
        }

        [HttpPost("[action]")]
        public async Task<ActionResult> UpdateAuditStatus([FromBody] AuditStatusUpdateRequest request)
        {
            var mytask = Task.Run(() => repository.UpdateAuditstatus(request));
            var result = await mytask;
            if (result == 0)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }
        }

        [HttpPost("[action]")]
        public async Task<ActionResult> UpdateEntryConferenceStatus([FromBody] UpdateEntryConferenceStatusRequest request)
        {
            var mytask = Task.Run(() => repository.UpdateEntryConferenceStatus(request));
            var result = await mytask;
            if (result == 0)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }
        }

        [HttpPost("[action]")]
        public async Task<ActionResult> UpdateExitConferenceStatus([FromBody] UpdateExitConferenceStatusRequest request)
        {
            var mytask = Task.Run(() => repository.UpdateExitConferenceStatus(request));
            var result = await mytask;
            if (result == 0)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }
        }

        [HttpPost("[action]")]
        public async Task<ActionResult> UpdateAuditParaStatus([FromBody] AuditParaStatusRequest request)
        {
            var mytask = Task.Run(() => repository.UpdateAuditParaStatus(request));
            var result = await mytask;
            if (result == 0)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }
        }


        [HttpPost("[action]")]
        public async Task<ActionResult> UpdateAuditComplianceStatus([FromBody] AuditParaStatusRequest request)
        {
            var mytask = Task.Run(() => repository.UpdateAuditComplianceStatus(request));
            var result = await mytask;
            if (result == 0)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }
        }

        [HttpGet("[action]")]
        public async Task<ActionResult> GetAllFinancialImplicationCases()
        {
            var mytask = Task.Run(() => repository.GetAllFinancialImplicationCases());
            var result = await mytask;
            if (result.Count == 0)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }
        }

        [HttpGet("[action]")]
        public async Task<ActionResult> GetAuditParaFinancialImplicationCases(int paraId)
        {
            var mytask = Task.Run(() => repository.GetAuditParaFinancialImplicationCases(paraId));
            var result = await mytask;
            return Ok(result);
        }
    }
}