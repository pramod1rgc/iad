﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AID.BusinessLayer.AssignUserBL;
using BusinessModel;
using BusinessModel.AssignUser;
using IRepository;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using WebApi.CustomFilter;

namespace WebApi.Controllers.AssignUser
{
    [Route("api/[controller]")]
    [ApiController]
    [IADExceptionFilter]
    [IADActionFilter]
    [Authorize]
    public class AssignUserController : ControllerBase
    {
        private IAssignUserRepository repository = null;
        private readonly IOptions<CommonKeys> _CommonKeys;

        public AssignUserController(IOptions<CommonKeys> commonKeys)
        {
            this.repository = new AssignUserBL();
            _CommonKeys = commonKeys;
        }


        [HttpGet("[action]")]
        public async Task<ActionResult> GetUserRoleEmployees(int roleId)
        {
            var mytask = Task.Run(() => repository.GetUserRoleEmployees(roleId));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }
        }


        [HttpPost("[action]")]
        public async Task<ActionResult> AssignUserRole([FromBody] AssignUserRoleRequest request)
        {
            var url = _CommonKeys;
            var mytask = Task.Run(() => repository.AssignUserRole(request));
            var result = await mytask;
            if (result == 0)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }
        }


        [HttpGet("[action]")]
        public async Task<ActionResult> GetAssignAuditTeam(int auditId)
        {
            var mytask = Task.Run(() => repository.GetAssignAuditTeam(auditId));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }
        }


        [HttpPost("[action]")]
        public async Task<ActionResult> CheckIfUserAlreadyAssignedToAnotherAudit([FromBody]CheckIfUserAlreadyAssignedToAnotherAuditRequest request)
        {
            var mytask = Task.Run(() => repository.CheckIfUserAlreadyAssignedToAnotherAudit(request));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }
        }


        [HttpPost("[action]")]
        public async Task<ActionResult> AssignAuditTeam([FromBody] AssignAuditTeamRequest request)
        {
            var url = _CommonKeys;
            var mytask = Task.Run(() => repository.AssignAuditTeam(request));
            var result = await mytask;
            if (result == 0)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }
        }

        [HttpGet("[action]")]
        public async Task<ActionResult> GetAuditsForDropdown()
        {
            var mytask = Task.Run(() => repository.GetAuditsForDropdown());
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }
        }
    }
}