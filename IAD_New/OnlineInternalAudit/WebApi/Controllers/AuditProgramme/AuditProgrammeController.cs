﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AID.BusinessLayer.AuditProgrammeBL;
using IRepository;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebApi.CustomFilter;

namespace WebApi.Controllers.AuditProgramme
{
    [Route("api/[controller]")]
    [ApiController]
    [IADExceptionFilter]
    [IADActionFilter]
    //[Authorize]
    public class AuditProgrammeController : ControllerBase
    {
        private IAuditProgrammeRepository repository = null;

        public AuditProgrammeController()
        {
            this.repository = new AuditProgrammeBL();
        }


        [HttpGet("[action]")]
        public async Task<ActionResult> GetAuditProgrammes(int finYear)
        {
            var mytask = Task.Run(() => repository.GetAuditProgrammes(finYear));
            var result = await mytask;
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }
        }
    }
}