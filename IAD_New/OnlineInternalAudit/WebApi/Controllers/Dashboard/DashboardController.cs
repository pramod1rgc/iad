﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AID.BusinessLayer.DashboardBL;
using AID.BusinessModels.Dashboard;
using IRepository;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebApi.CustomFilter;

namespace WebApi.Controllers.Dashboard
{
    [Route("api/[controller]")]
    [ApiController]
    [IADExceptionFilter]
    [IADActionFilter]
    [Authorize]
    public class DashboardController : ControllerBase
    {
        private IDashboardRepository repository = null;
        public DashboardController()
        {
            this.repository = new DashboardBL();
        }
       
        [HttpGet("[action]")]
        public async Task<List<DashboardModel>> getAllMenus(int roleId)
        {
           // DashboardModel lst=new DashboardModel()
          return await  Task.Run(() => repository.getAllMenus(roleId));
          
        }
     
}
}