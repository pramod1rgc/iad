﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AID.BusinessLayer.DashboardhomeBL;
using IRepository;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebApi.CustomFilter;

namespace WebApi.Controllers.Dashboard
{
    [Route("api/[controller]")]
    [ApiController]
    [IADExceptionFilter]
    [IADActionFilter]
    [Authorize]
    public class DashboardhomeController : ControllerBase
    {
        private IDashboardhome repository = null;
        public DashboardhomeController()
        {
            this.repository = new DashboardhomeBL();
        }

        [HttpGet("[action]")]
        public async Task<IActionResult> GetAllDashboardRecord(string username)
        {
            var myTask = Task.Run(() => repository.GetAllDashboardRecord(username));
            var result = await myTask;
            if (result == null)
            {
                return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status304NotModified);

            }
            else
                return Ok(result);
        }
        


        //[HttpGet("[action]")]
        //public async Task<IActionResult> GetAuditDhDashboardAllRecord(string userId)
        //{
        //    var myTask = Task.Run(() => repository.GetAllDashboardRecord(userId));
        //    var result = await myTask;
        //    if (result == null)
        //    {
        //        return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status304NotModified);

        //    }
        //    else
        //        return Ok(result);
        //}
        [HttpGet("[action]")]
        public async Task<IActionResult> getAuditDhDashboardAllRecord(string userId)
        {
            var myTask = Task.Run(() => repository.GetAuditDhDashboardAllRecord(userId));
            var result = await myTask;
            if (result == null)
            {
                return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status304NotModified);

            }
            else
                return Ok(result);
        }


        [HttpGet("[action]")]
        public async Task<IActionResult> GetAll_IADHead_DashboardRecord(string username)
        {
            var myTask = Task.Run(() => repository.GetAll_IADHead_DashboardRecord(username));
            var result = await myTask;
            if (result == null)
            {
                return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status304NotModified);

            }
            else
                return Ok(result);
        }

        [HttpGet("[action]")]
        public async Task<IActionResult> getallschedule()
        {
            var myTask = Task.Run(() => repository.getallschedule());
            var result = await myTask;
            if (result == null)
            {
                return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status304NotModified);

            }
            else
                return Ok(result);
        }


        [HttpGet("[action]")]
        public async Task<IActionResult> GetallAuditDHInchart(string userid)
        {
            var myTask = Task.Run(() => repository.GetallAuditDHInchart(userid));
            var result = await myTask;
            if (result == null)
            {
                return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status304NotModified);

            }
            else
                return Ok(result);
        }
        [HttpGet("[action]")]
        public async Task<IActionResult> Getall_IADHeadInchart(string userid)
        {
            var myTask = Task.Run(() => repository.Getall_IADHeadInchart(userid));
            var result = await myTask;
            if (result == null)
            {
                return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status304NotModified);

            }
            else
                return Ok(result);
        }
        [HttpGet("[action]")]
        public async Task<IActionResult> GetallAuditInchart()
        {
            var myTask = Task.Run(() => repository.GetallAuditInchart());
            var result = await myTask;
            if (result == null)
            {
                return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status304NotModified);

            }
            else
                return Ok(result);
        }
    }
}