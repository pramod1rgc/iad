﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AID.BusinessLayer.EmployeeBL;
using BusinessModel.Employee;
using IRepository;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebApi.CustomFilter;

namespace WebApi.Controllers.Employee
{
    
    [Route("api/[controller]")]
    [ApiController]
    [IADExceptionFilter]
    [IADActionFilter]
    [Authorize]
    public class EmployeeController : ControllerBase
    {
        private IEmployeeRepository repository = null;
        public EmployeeController()
        {
            this.repository = new EmployeeBL();
        }
        [HttpGet("[action]")]
        public async Task<List<EmployeeModel>> GetAllEmployee()
        {
           return await Task.Run(() => repository.GetAllEmployee());

        }
        [HttpGet("[action]")]
        public async Task<List<EmployeeModel>> GetAllDesignation()
        {
            return await Task.Run(() => repository.GetAllDesignation());

        }

        [HttpPost("[action]")]
        public async Task<string> SaveEmployee ([FromBody]EmployeeModel MstempDetails, string hdnempID)
        {
            var result = await Task.Run(() => repository.SaveEmployee(hdnempID, MstempDetails));
            return result;

        }

        [HttpPost("[action]")]
        public string ActiveDeactiveUser(EmployeeModel MstempDetails)
        {
            var  result = repository.ActiveDeactiveUser(MstempDetails);
            return result;

        }
    }
}