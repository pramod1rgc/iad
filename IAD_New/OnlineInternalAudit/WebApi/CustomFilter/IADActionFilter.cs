﻿using BusinessModel;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace WebApi.CustomFilter
{
    public class IADActionFilter : ActionFilterAttribute, IActionFilter
    {
        public override void OnActionExecuted(ActionExecutedContext context)
        {
            IASBaseResponse response = new IASBaseResponse();
            var result = context.Result;
            var json = result as dynamic;

            var value = json.Value;
            response.Result = value;
            response.IsSuccess = true;

            context.Result = new JsonResult(response);
        }
    }
}
