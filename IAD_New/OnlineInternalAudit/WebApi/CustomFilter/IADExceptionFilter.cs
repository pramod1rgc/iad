﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace WebApi.CustomFilter
{
    public class IADExceptionFilter : ExceptionFilterAttribute, IExceptionFilter
    {

        /// <summary>
        /// On exception method
        /// </summary>
        /// <param name="context"></param>
        public override void OnException(ExceptionContext context)
        {
            HttpResponseMessage msg = new HttpResponseMessage(HttpStatusCode.InternalServerError);

            var exceptionType = context.Exception.GetType();
            ExecptionDataLog logData = new ExecptionDataLog();
            logData.ExceptionDataLogs(context);

            if (exceptionType == typeof(UnauthorizedAccessException))
            {
                msg.ReasonPhrase = "Unauthorized Access.Please Contact your Administrator.";
                msg.StatusCode = HttpStatusCode.Unauthorized;
            }
            else if (exceptionType == typeof(NotSupportedException))
            {
                msg.ReasonPhrase = "Not Supported.Please Contact your Administrator.";
                msg.StatusCode = HttpStatusCode.BadRequest;
            }
            else if (exceptionType == typeof(KeyNotFoundException))
            {
                msg.ReasonPhrase = "Key Not Found.Please Contact your Administrator.";
                msg.StatusCode = HttpStatusCode.NotModified;
            }
            else if (exceptionType == typeof(NotImplementedException))
            {
                msg.ReasonPhrase = "Not Implemented.Please Contact your Administrator.";
                msg.StatusCode = HttpStatusCode.NotImplemented;
            }
            else if (exceptionType == typeof(IndexOutOfRangeException))
            {
                msg.ReasonPhrase = "Not Implemented. Please Contact your Administrator.";
                msg.StatusCode = HttpStatusCode.NotFound;
            }
            else if (exceptionType == typeof(AppException))
            {
                msg.ReasonPhrase = context.Exception.ToString();
                msg.StatusCode = HttpStatusCode.InternalServerError;
            }
            else
            {
                msg.Content = new StringContent("An unhandled exception was thrown by service.");
                msg.ReasonPhrase = "Internal Server Error.Please Contact your Administrator.";
                msg.StatusCode = HttpStatusCode.InternalServerError;
            }
            context.HttpContext.Response.Headers.Add("Response", msg.ReasonPhrase.ToString());
            context.HttpContext.Response.Headers.Add("StatusCode", ((int)msg.StatusCode).ToString());
            context.Result = new JsonResult(msg);


        }
    }
}
