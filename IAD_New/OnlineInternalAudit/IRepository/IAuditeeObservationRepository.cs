﻿using BusinessModel.AuditeeObservation;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace IRepository
{
    public interface IAuditeeObservationRepository
    {
        Task<List<GetAuditeeDHAuditsListResponse>> GetAuditDHAuditsList(int auditUserId);
        Task<List<AuditeeEntryConferenceResponse>> GetAuditEntryConference(int auditId);
        Task<int> UpdateAuditEntryConference(UpdateAuditeeEntryConferenceRequest request);
        Task<List<AuditeeExitConferenceResponse>> GetAuditExitConference(int auditId);
        Task<int> UpdateAuditExitConference(UpdateAuditeeExitConferenceRequest request);
        Task<List<GetAuditeeParaResponse>> GetAuditPara(int roleId,int auditId, int pageSize, int pageNumber, string searchTerm);
        Task<int> UpdateAuditPara(UpdateAuditeeParaRequest request);
        Task<GetAuditeeParaDetailsResponse> GetAuditParaDetails(int paraId);
        Task<int> DeleteAuditPara(DeleteAuditeeParaRequest request);
        Task<int> PostParaCommentByAuditor(PostParaCommentByAuditorRequest request);
        Task<List<GetParaCommentResponse>> GetParaComments(int paraId);
        Task<GetAuditeeParaSettlementLetterResponse> GetAuditParaSettlementLetter(int auditId);
        Task<int> UpdateAuditParaSettlementLetter(UpdateAuditeeParaSettlementLetterRequest request);
        Task<int> UpdateParaSettlementLetterStatus(int auditId, int statusId, string rejectionRemark);
        Task<int> UpdateAuditstatus(AuditeeStatusUpdateRequest request);
        Task<int> UpdateAuditParaStatus(AuditeeParaStatusRequest request);
    }
}
