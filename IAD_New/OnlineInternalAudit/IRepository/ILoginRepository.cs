﻿using BusinessModel;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace IRepository
{
   public interface ILoginRepository
    {
         Task<List<LoginResponseBM>> Login(string userid, string password);
    }
}
