﻿using BusinessModel.Verification;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace IRepository
{
    public interface IVerifyAuditComplianceRepository
    {
        Task<List<GetAuditsForwardedToSupervisorResponse>> GetAuditsForwardedToSupervisor();
        Task<List<GetAuditsForwardedToSupervisorResponse>> GetAuditsWhoseParasOrComplainceForwardedToSupervisor(string type);
        Task<List<GetAuditsForwardedToSupervisorResponse>> GetAuditsWhoseSettlementLetterForwardedToSupervisor();
    }
}
