﻿using AID.BusinessModels.Dashboard;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace IRepository
{
    public interface IDashboardRepository
    {
        Task<List<DashboardModel>> getAllMenus(int roleId);
    }
}
