﻿using BusinessModel.AuditStructure;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace IRepository
{
    public interface IVerifyannualplan
    {
       Task<IEnumerable<CreateAuditPlanModel>> Getauditlistforverify(int FYID,int RoleUserMapId);
        string SupervisorTosupervisor(string currFYear, int roleUserMapId, int userId);
        
        string SupervisorTosupervisorQuarterlyPlan(string currFYear, int roleUserMapId, int userId, string Quarter);
        Task<IEnumerable<CreateAuditPlanModel>> getAllPendingQuarter(int FYID, int userId);
        Task< string> AnualplanApprove(int FYID,int RoleUserMapId);
        Task<string> RejectanualsAudit(int FYID,string RejectionReson,int RoleUserMapId);
        Task<IEnumerable<CreateAuditPlanModel>> GetauditlistforverifyQuarterly(int FYID,string Quarter,int userId);
        Task<string> QuarterlylplanApprove(int FYID,string Quarter,int RoleUserMapId);
        Task<string> RejectQuarterlyAudit(int FYID, string RejectionReson,string Quarter, int RoleUserMapId);
    }
}
