﻿using BusinessModel.UserManagement;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace IRepository
{
    public interface IMenuRepository
    {
        
         Task<List<MenuModel>> BindMenuInGride();
        Task<List<MenuModel>> BindMenuInDropDown();
        Task<List<MenuModel>> GetAllPredefineRole();
    }
}
