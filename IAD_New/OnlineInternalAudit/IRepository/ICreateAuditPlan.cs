﻿using BusinessModel.AuditStructure;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace IRepository
{
    public interface ICreateAuditPlan
    {
        Task<List<CreateAuditPlanModel>> BindFYearDropDown();

        Task<List<CreateAuditPlanModel>> pendingFinancialForVerifyQuarter(int roleUserMapId);

        Task<List<CreateAuditPlanModel>> BindSupervisorDropDown(int userId);
        Task<List<CreateAuditPlanModel>> GetController();
        Task<List<CreateAuditPlanModel>> GetAllPAO(int ControllerID);
        Task<IEnumerable<CreateAuditPlanModel>> Getauditlist(int FYID);     
        string AssignAuditByController(int PAOID, string Status, string currFYear, string paoreason);
        string AssignToSupervisor(string currFYear,int roleUserMapId,int userId);
        string AssignQuarterlyPlanToSupervisor(string currFYear, int roleUserMapId, int userId);
        Task<string> GetassignPaoAudit(List<CreateAuditPlanModel> ObjcreateAuditPlan);

        #region QuartarlyAudit
        Task<IEnumerable<CreateAuditPlanModel>> BindAuditInGrid(int FYID);
        Task<string> SaveQuartalyAudit(List<CreateAuditPlanModel> objAuditPlanModel);
        Task<string> SaveQuartalyPlanAudit(List<object> objquarterpaoplan,string financialYearId);
        #endregion
    }
}
