﻿using BusinessModel.AssignUser;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace IRepository
{
    public interface IAssignUserRepository
    {
        Task<List<GetUserRoleEmployeesResponse>> GetUserRoleEmployees(int roleId);
        Task<int> AssignUserRole(AssignUserRoleRequest request);

        Task<List<GetAssignAuditTeamResponse>> GetAssignAuditTeam(int auditId);
        Task<CheckIfUserAlreadyAssignedToAnotherAuditResponse> CheckIfUserAlreadyAssignedToAnotherAudit(CheckIfUserAlreadyAssignedToAnotherAuditRequest request);
        Task<int> AssignAuditTeam(AssignAuditTeamRequest request);

        Task<List<GetAuditsForDropdownResponse>> GetAuditsForDropdown();
    }
}
