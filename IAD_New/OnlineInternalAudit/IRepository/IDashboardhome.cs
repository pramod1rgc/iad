﻿using BusinessModel.AuditStructure;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace IRepository
{
    public interface IDashboardhome
    {
        Task<object[]> GetAllDashboardRecord(string UserName);
        Task<object[]> GetAll_IADHead_DashboardRecord(string UserName);
        Task<object[]> GetAuditDhDashboardAllRecord(string UserName);
        Task<List<GetScheduledAuditsResponse>> getallschedule();
        Task<object[]> GetallAuditInchart();
        Task<object[]> GetallAuditDHInchart(string userid);
        Task<object[]> Getall_IADHeadInchart(string userid);
        

    }
}
