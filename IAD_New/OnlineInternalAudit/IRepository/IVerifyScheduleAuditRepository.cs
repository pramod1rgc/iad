﻿using BusinessModel.Verification;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace IRepository
{
    public interface IVerifyScheduleAuditRepository
    {
        Task<List<GetScheduledAuditsForVerificationResponse>> GetScheduledAuditsForVerification(int finYearId, string quarter, int roleUserMapId);

        Task<int> UpdateScheduleAuditForVerification(UpdateScheduleAuditForVerificationRequest request);
        Task<GetPendingTaskCountResponse> GetPendingTaskCount(int RoleUserMapId);
        Task<List<GetDistinctQuartersBasedOnStatusAndFYResponse>> GetDistinctQuartersBasedOnStatusAndFY(int fyId, string statusId, int statusId2);
    }
}
