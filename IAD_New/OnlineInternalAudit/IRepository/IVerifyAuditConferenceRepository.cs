﻿using BusinessModel.Verification;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace IRepository
{
    public interface IVerifyAuditConferenceRepository
    {
        Task<List<GetAuditsOfConferenceForVerificationResponse>> GetAuditsOfConferenceForVerification(string type);
    }
}
