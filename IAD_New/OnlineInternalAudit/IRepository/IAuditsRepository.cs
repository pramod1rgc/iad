﻿using BusinessModel.Audits;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace IRepository
{
    public interface IAuditsRepository
    {
        Task<List<GetApprovedScheduledAuditsResponse>> GetApprovedScheduledAudits(int finYearId, string quarter, int roleUserMapId);

        Task<List<GetApprovedAuditTeamResponse>> GetApprovedAuditTeam(int auditId);

        Task<int> RevertAuditTeam(RevertAuditTeamRequest request);

        Task<List<GetApprovedAuditTeamAuditsForDropdownResponse>> GetApprovedAuditTeamAuditsForDropdown(int roleUserMapId);

    }
}
