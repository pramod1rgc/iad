﻿using BusinessModel.Verification;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace IRepository
{
    public interface IVerifyAuditTeamRepository
    {
        Task<List<GetAuditTeamForVerificationResponse>> GetTeamOfAuditForVerification(int auditId);
        Task<int> UpdateTeamOfAuditForVerification(UpdateAuditTeamForVerificationRequest request);

        Task<List<GetAuditDetailsForVerificationDropdownResponse>> GetAuditDetailsForVerificationDropdown(int roleUserMapId);
    }
}
