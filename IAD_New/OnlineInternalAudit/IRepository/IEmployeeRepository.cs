﻿using BusinessModel.Employee;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace IRepository
{
    public interface IEmployeeRepository
    {
        Task<List<EmployeeModel>> GetAllEmployee();
        Task<List<EmployeeModel>> GetAllDesignation();
        Task<string> SaveEmployee(string hdnEmpId, EmployeeModel objemployee);
        string  ActiveDeactiveUser(EmployeeModel objemployee);
    }
}
