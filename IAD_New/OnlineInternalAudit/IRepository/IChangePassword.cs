﻿using BusinessModel.ChangePassword;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace IRepository
{
  public interface IChangePassword
    {
        Task<string> ChangePassword(ChangePasswordBM request);
    }
}
