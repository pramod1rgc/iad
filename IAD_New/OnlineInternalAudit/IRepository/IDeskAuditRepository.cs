﻿using BusinessModel.DeskAudit;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace IRepository
{
 public interface IDeskAuditRepository
    {
        Task<List<GetDeskAuditUploadFiles>> getDeskAuditFiles(int auditId);
        
        Task<string> active_deactiveDeskAudit(DeactiveDeskAuditBM objauditee);
        Task<string> SaveDeskAudit(DeskAuditBM objauditee);
        //string ActiveDeactiveUser(DeskAuditBM objauditee);
    }
}
