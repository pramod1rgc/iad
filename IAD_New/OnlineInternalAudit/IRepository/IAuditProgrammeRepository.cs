﻿using BusinessModel.AuditProgramme;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace IRepository
{
   public interface IAuditProgrammeRepository
    {
        Task<List<AuditProgrammeResponse>> GetAuditProgrammes(int finYear);
    }
}
