﻿using BusinessModel.Audits;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace IRepository
{
  public interface ISendIntimationMailsRepository
    {
        Task<List<SendIntimationMailsBM>> GetSendIntimationMailsPao(int fYearid);
        Task<string> SendMail(SendIntimationMailsBM request);
   
    }
}
