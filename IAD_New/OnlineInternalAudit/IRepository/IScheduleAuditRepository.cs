﻿using BusinessModel.AuditStructure;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace IRepository
{
    public interface IScheduleAuditRepository
    {
        Task<List<GetScheduledAuditsResponse>> GetScheduledAudits(int finYearId, string quarter);

        Task<int> UpdateScheduledAudit(UpdateScheduleAuditRequest request);

        Task<int> DeleteScheduledAudit(DeleteScheduleAuditRequest request);
    }
}
