﻿using BusinessModel.AuditObservation;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace IRepository
{
    public interface IAuditObservationRepository
    {
        Task<List<GetAuditDHAuditsListResponse>> GetAuditDHAuditsList(int auditUserId);
        Task<List<AuditEntryConferenceResponse>> GetAuditEntryConference(int auditId);
        Task<int> UpdateAuditEntryConference(UpdateAuditEntryConferenceRequest request);
        Task<List<AuditExitConferenceResponse>> GetAuditExitConference(int auditId);
        Task<int> UpdateAuditExitConference(UpdateAuditExitConferenceRequest request);
        Task<List<GetAuditParaResponse>> GetAuditPara(int auditId, int pageSize, int pageNumber, string searchTerm);
        Task<int> UpdateAuditPara(UpdateAuditParaRequest request);
        Task<GetAuditParaDetailsResponse> GetAuditParaDetails(int paraId);
        Task<int> DeleteAuditPara(DeleteAuditParaRequest request);
        Task<int> PostParaCommentByAuditor(PostParaCommentByAuditorRequest request);
        Task<List<GetParaCommentResponse>> GetParaComments(int paraId);
        Task<GetAuditParaSettlementLetterResponse> GetAuditParaSettlementLetter(int auditId);
        Task<int> UpdateAuditParaSettlementLetter(UpdateAuditParaSettlementLetterRequest request);
        Task<int> UpdateParaSettlementLetterStatus(int auditId, int statusId, string rejectionRemark);
        Task<int> UpdateAuditstatus(AuditStatusUpdateRequest request);
        Task<int> UpdateEntryConferenceStatus(UpdateEntryConferenceStatusRequest request);
        Task<int> UpdateExitConferenceStatus(UpdateExitConferenceStatusRequest request);
        Task<int> UpdateAuditParaStatus(AuditParaStatusRequest request);
        Task<int> UpdateAuditComplianceStatus(AuditParaStatusRequest request);
        Task<List<GetAllFinancialImplicationCasesResponse>> GetAllFinancialImplicationCases();
        Task<List<GetAuditParaFinancialImplicationCasesResponse>> GetAuditParaFinancialImplicationCases(int paraId);
    }
}
