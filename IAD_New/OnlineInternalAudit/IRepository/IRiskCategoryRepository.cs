﻿using BusinessModel.Master;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace IRepository
{
    public interface IRiskCategoryRepository
    {
        Task<string> SaveRiskCategory(string hdnCategoryId, CategoryDetailsModel MstCategoryDetails);
        Task<List<CategoryDetailsModel>> GetAllCategoryDetails();
        Task<string> ActiveDeactive(CategoryDetailsModel MstCategoryDetails);
        Task<List<CategoryDetailsModel>> BindDropDawnCategory();
    }
}
