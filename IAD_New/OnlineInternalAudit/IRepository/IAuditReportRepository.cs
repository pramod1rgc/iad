﻿using BusinessModel.AuditReport;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace IRepository
{
    public interface IAuditReportRepository
    {
        Task<List<GetAuditsForAuditReportBySupervisorResponse>> GetAuditsForAuditReportBySupervisor(int pageSize, int pageNumber, string searchTerm);
        Task<GetAuditReportResponse> GetAuditReport(int auditId);
    }
}
