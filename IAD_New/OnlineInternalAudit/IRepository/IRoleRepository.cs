﻿using BusinessModel.Role;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace IRepository
{
    public interface IRoleRepository
    {
        Task<List<GetRoleResponse>> GetRoles();
        Task<int> UpsertRoles(UpsertRoleRequest request);
        Task<int> ToggleRoleActivation(ToggleRoleRequest request);
    }
}
