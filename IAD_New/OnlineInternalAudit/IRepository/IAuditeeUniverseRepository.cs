﻿using BusinessModel.Auditee;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace IRepository
{
   public interface IAuditeeUniverseRepository
    {
        Task<List<AuditeeUniverseBM>> GetAllAuditee(int userid);
        Task<List<AuditeeUniverseBM>> GetAllDesignation(int rollId);
        Task<List<AuditeeUniverseBM>> GetAuditeeRole();
        
        Task<string> SaveAuditee(string hdnEmpId, AuditeeUniverseBM objauditee);
        string ActiveDeactiveUser(AuditeeUniverseBM objauditee);
    }
}
