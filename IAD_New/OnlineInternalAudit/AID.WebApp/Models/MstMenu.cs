﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IAD.Models
{
    public class MstMenu
    {


        //Menu Class  Property
        public int msMenuID { get; set; }
        public string mainMenuName { get; set; }
        //public string subMenuName { get; set; }
        public string menuURL { get; set; }
        public int parentMenu { get; set; }
        public int childMenu { get; set; }
        public string s_no { get; set; }
        public string Status { get; set; }
        public string IPAddress { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public string ModifiedDate { get; set; }

        public string IsActive { get; set; }
        //public List<string> iDsStatus { get; set; }
        //End Property
    }
}
