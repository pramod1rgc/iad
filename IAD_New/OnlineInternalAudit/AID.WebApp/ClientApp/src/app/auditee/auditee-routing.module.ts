import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RegisterAuditeeComponent } from '../auditee/register-auditee/register-auditee.component';
import { AuditeeModule } from '../auditee/auditee.module';
const routes: Routes = [{
  path: '', component: AuditeeModule, data: {
    breadcrumb: 'Auditee'
  }, children: [
    {
      path: 'auditee-universe', component: RegisterAuditeeComponent, data: {
        breadcrumb: 'Auditee Universe'
      }
    }
  ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AuditeeRoutingModule { }
