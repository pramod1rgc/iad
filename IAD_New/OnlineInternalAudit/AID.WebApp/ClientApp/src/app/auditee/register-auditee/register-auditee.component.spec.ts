import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegisterAuditeeComponent } from './register-auditee.component';

describe('RegisterAuditeeComponent', () => {
  let component: RegisterAuditeeComponent;
  let fixture: ComponentFixture<RegisterAuditeeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegisterAuditeeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegisterAuditeeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
