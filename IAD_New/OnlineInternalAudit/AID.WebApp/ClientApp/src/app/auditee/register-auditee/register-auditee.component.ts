import { Component, OnInit, ViewChild } from '@angular/core';
import {AuditeeuniverseService  } from '../../services/auditee/auditeeuniverse.service';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import swal from 'sweetalert2';
import { ReplaySubject, Subject } from 'rxjs';
import { FormControl,FormsModule, ReactiveFormsModule } from '@angular/forms';
import { takeUntil } from 'rxjs/operators';
import * as $ from 'jquery';
export interface commondll {
  values: string;
  text: string;
}

export interface EmployeeData {
  empName: string;
  roleName: string;
  designation: string;
  email: string;
  mobileNo: string;
  phoneNo: string;
}
@Component({
  selector: 'app-register-auditee',
  templateUrl: './register-auditee.component.html',
  styleUrls: ['./register-auditee.component.css']
})
export class RegisterAuditeeComponent implements OnInit {


  constructor(private _auditeeService: AuditeeuniverseService) { }
  //Variable
  displayedColumns: string[] = ['empName', 'roleName', 'designation', 'email', 'mobileNo', 'phoneNo', 'Action'];//, 'Active'];//, 'leaveTypeDesc', 'action',];
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild('frmempDetails') frmempDetails: any;
  public filteredDesig: ReplaySubject<commondll[]> = new ReplaySubject<commondll[]>(1);
  public filteredRole: ReplaySubject<commondll[]> = new ReplaySubject<commondll[]>(1);

  public DesigFilterCtrl: FormControl = new FormControl();
  public RoleFilterCtrl: FormControl = new FormControl();
  public DesigCtrl: FormControl = new FormControl();
  private _onDestroy = new Subject<void>();
  ArrAllEmployee: any;
  ArrEmpdataSource: any;
  ArrDesig: any;
  ArrRole:any
  MstempDetails: any = {};
  hdnEmpId = 0;
  Message: any;
  empID: string;
  ActiveDactiveHeaderText: string;
  checkUserIsactive: boolean;
  notFound = true;
  ActivateUser: boolean;
  btntext: string;
  userId: number;
  stdCode: any;
  rollId: number;
  ngOnInit() {

    this.GetAllEmployee();

    this.BindDropDownRole();
    this.DesigFilterCtrl.valueChanges.pipe(takeUntil(this._onDestroy)).subscribe(() => { this.filterDesig(); })
    this.RoleFilterCtrl.valueChanges.pipe(takeUntil(this._onDestroy)).subscribe(() => { this.filterRole(); });
    this.btntext = 'Save';
  }
  //Bind Role DropDawnList
  GetAllEmployee() {
    //alert('Service');
    this._auditeeService.GetAllAuditee(JSON.parse(localStorage.getItem("LoginData")).userId).subscribe(result => {
      this.ArrAllEmployee = result;
      this.ArrEmpdataSource = new MatTableDataSource(result);
      this.ArrEmpdataSource.paginator = this.paginator;
      this.ArrEmpdataSource.sort = this.sort;
      //Filetr inData source
      this.ArrEmpdataSource.filterPredicate =
        (data: EmployeeData, filtersJson: string) => {
          const matchFilter = [];
          const filters = JSON.parse(filtersJson);

          filters.forEach(filter => {
            const val = data[filter.id] === null ? '' : data[filter.id];
            matchFilter.push(val.toLowerCase().includes(filter.value.toLowerCase()));
          });
          return matchFilter.every(Boolean);
        };
      //END Of Filetr inData source


    })
  }
  //End Of Bind Role DropDawnList

  BindDropDownRole() {
    this._auditeeService.BindDropDownRole().subscribe(result => {
      this.ArrRole = result;
      // this.MenuFilterCtrl.setValue(this.ArrMenus.parentMenu);
      this.filteredRole.next(this.ArrRole);
    })
  }

  BindDesignationByRole(rollId) {
    this.BindDropDownDesig(rollId);
  }


    //Bind Role DropDawnList
  BindDropDownDesig(rollId) {
    this._auditeeService.BindDropDownDesig(rollId).subscribe(result => {
      debugger;
      this.ArrDesig = result;
      //this.MenuFilterCtrl.setValue(this.ArrMenus.parentMenu);
      this.filteredDesig.next(this.ArrDesig);
    })
  }
      //End Role DropDawnList

  //not use for filter search
  private filterRole() {
    debugger;
    if (!this.filteredRole) {
      return;
    }
    // get the search keyword
    let search = this.DesigFilterCtrl.value;
    if (!search) {

      this.filteredRole.next(this.ArrRole.slice());
      return;
    }
    else {
      search = search.toLowerCase();
    }
    // filter the banks
    //this.filteredMenu.next(

    //  this.ArrMenus.filter(ArrMenus => ArrMenus.text.toLowerCase().indexOf(search) > -1)
    //);
    var a = this.ArrRole;
    this.filteredRole.next(
      this.ArrRole.filter(ArrMenus => ArrMenus.RoleName.toLowerCase().indexOf(search) > -1)
    );
  }

  //not use for filter search
  private filterDesig() {
    debugger;
    if (!this.filteredDesig) {
      return;
    }
    // get the search keyword
    let search = this.DesigFilterCtrl.value;
    if (!search) {

      this.filteredDesig.next(this.ArrDesig.slice());
      return;
    }
    else {
      search = search.toLowerCase();
    }
    // filter the banks
    //this.filteredMenu.next(

    //  this.ArrMenus.filter(ArrMenus => ArrMenus.text.toLowerCase().indexOf(search) > -1)
    //);
    var a = this.ArrDesig;
    this.filteredDesig.next(
      this.ArrDesig.filter(ArrMenus => ArrMenus.desigName.toLowerCase().indexOf(search) > -1)
    );
  }

  SaveEmpDetails() {
    debugger;
    this.MstempDetails.phoneNo = this.MstempDetails.stdCode + "-" + this.MstempDetails.phoneNo;

    this.MstempDetails.userId = JSON.parse(localStorage.getItem("LoginData")).userId;
    this._auditeeService.SaveAuditee(this.hdnEmpId, this.MstempDetails).subscribe(result => {
      this.Message = result;
      if (this.Message != undefined) {
        swal(this.Message)
       this.frmempDetails.resetForm();
      }
      this.btntext = 'Save';
      this.GetAllEmployee();
     this.Cancel();
    })


  }


  UpdateFillForm(empId, empName, email, mobileNo, desigID, phoneNo, roleID) {

    this.hdnEmpId = empId;
    this.MstempDetails.empName = empName;
    //this.MstempDetails.desigName = desigID;
    this.MstempDetails.email = email;
    this.MstempDetails.phoneNo = phoneNo;
    this.MstempDetails.mobileNo = mobileNo;
    this.BindDropDownDesig(roleID);
    this.MstempDetails.roleID = roleID;
    this.MstempDetails.desigID = desigID;
    this.btntext = 'Update';
    //this.hdnEmpId = empId;
    //this.MstempDetails.empName = empName;
    //this.MstempDetails.desigName = desigID;
    //this.MstempDetails.email = email;
    let ph = phoneNo.split("-");
    if (ph.length > 1) {
      this.MstempDetails.stdCode = ph[0];
      this.MstempDetails.phoneNo = ph[1];
    }
    else {
      this.MstempDetails.stdCode = '';
      this.MstempDetails.phoneNo = phoneNo;
    }
    this.MstempDetails.mobileNo = mobileNo;

  }
  //UpdateFillForm(empId, empName, email, mobileNo, desigID, phoneNo) {

  //  this.hdnEmpId = empId;
  //  this.MstempDetails.empName = empName;
  //  this.MstempDetails.desigName = desigID;
  //  this.MstempDetails.email = email;
  //  this.MstempDetails.phoneNo = phoneNo;
  //  this.MstempDetails.mobileNo = mobileNo;
  //  this.btntext = 'Update';
  //}

  Cancel() {
    this.btntext = 'Save';
    this.frmempDetails.resetForm();

  }
  ActiveDeactiveFillPopUpUser(empId, isActive) {

    //alert(mainMenuName);
    this.empID = empId;

    if (isActive == 'Active') {
      this.checkUserIsactive = false//true;
      this.ActiveDactiveHeaderText = 'Do you want to deactivate auditee';
    }
    else {
      this.checkUserIsactive = true//false;
      this.ActiveDactiveHeaderText = 'Do you want to activate auditee';
    }
  }
  ActiveDeactiveUser() {
   // this.MstempDetails.userId = JSON.parse(localStorage.getItem("LoginData")).userId;
    this.MstempDetails = {
      empID: this.empID,
      isActive: this.checkUserIsactive,
      userId: JSON.parse(localStorage.getItem("LoginData")).userId
    }
    JSON.stringify(this.MstempDetails);
    this._auditeeService.ActiveDeactiveUser(this.MstempDetails).subscribe(data => {
      debugger;
      this.Message = data;
      $('.dialog__close-btn').click();
      this.ActivateUser = false;
      swal(this.Message)
      this.GetAllEmployee();

    })
  }


  applyFilter(filterValue: string) {
    const tableFilters = [];
    tableFilters.push({
      id: 'empName',
      value: filterValue
    });


    this.ArrEmpdataSource.filter = JSON.stringify(tableFilters);
    //this.menudataSource.filter = filterValue.trim().toLowerCase();

    if (this.ArrEmpdataSource.paginator) {
      this.ArrEmpdataSource.paginator.firstPage();
    }
    if (this.ArrEmpdataSource.filteredData.length > 0) {
      this.notFound = true;
    } else {
      this.notFound = false;
    }
  }

  CancelPopUp() {
    this.ActivateUser = false;
    this.GetAllEmployee();
  }

}
