import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuditeeRoutingModule } from './auditee-routing.module';
import { RegisterAuditeeComponent } from './register-auditee/register-auditee.component';
import { MaterialModule } from '../material.module';
import { SharedModule } from '../shared-module/shared-module.module';
import { MatSlideToggleModule } from '@angular/material';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { NgxMatSelectSearchModule } from 'ngx-mat-select-search';
import { MatTooltipModule } from '@angular/material/tooltip';
@NgModule({
  declarations: [RegisterAuditeeComponent],
  imports: [
    CommonModule,
    AuditeeRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    MaterialModule,
    SharedModule,
    NgxMatSelectSearchModule,
    MatSlideToggleModule,
    MatTooltipModule
  ]
})
export class AuditeeModule { }
