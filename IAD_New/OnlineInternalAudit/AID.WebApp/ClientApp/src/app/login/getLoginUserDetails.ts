import { Injectable } from '@angular/core';
import { Subject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class getLoginUserDetails {
  private subject = new Subject<any>();

  constructor() { }

  SendLoginUserDetails(Username: string, paoid: string, ddoid: string, controllerID: string, msUserID: string, empPermDDOId: string) {
    debugger;
    this.subject.next({ Username: Username, paoid: paoid, ddoid: ddoid, controllerID: controllerID, msUserID: msUserID, empPermDDOId: empPermDDOId });
  }

  getLoginUserDetails(): Observable<any> {
    debugger;
    return this.subject.asObservable();
  }
}
