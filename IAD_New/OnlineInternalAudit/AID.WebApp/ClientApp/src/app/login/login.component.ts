import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { loginModel } from '../model/loginModel';
import { LoginServices } from './loginservice';
 
import { MatSnackBar } from '@angular/material';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  providers: [LoginServices]
})
export class LoginComponent implements OnInit {
  loginModel: loginModel;
  loginForm: FormGroup;
  captcha: string = '';
  CheckLoginRole: boolean;
  loginResponse: any;
  deptId: string = 'IAD';
  selectedRoleUserMapId: number;

  //selectedRadio: any;

  constructor(private router: Router, private _Service: LoginServices, private _snackBar: MatSnackBar, private route: ActivatedRoute) {
    localStorage.setItem('breadcrumbsData', JSON.stringify([]));
  }

  loginDetails: any = [];
   
  loginUserId: number;

  ngOnInit() {
    //this.loginModel = new loginModel();
    this.createForm();
    this.randomString();
    localStorage.setItem('breadcrumbsData', JSON.stringify([]));
  }

  randomString() {
    //clear the contents of captcha div first
    document.getElementById('captcha').innerHTML = "";
    var charsArray =
      "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
    var lengthOtp = 6;
    var captcha = [];
    for (var i = 0; i < lengthOtp; i++) {
      //below code will not allow Repetition of Characters
      var index = Math.floor(Math.random() * charsArray.length + 1); //get the next character from the array
      if (captcha.indexOf(charsArray[index]) == -1)
        captcha.push(charsArray[index]);
      else i--;
    }
    var canv = document.createElement("canvas");
    canv.id = "captcha";
    canv.width = 200;
    canv.height = 50;
    var ctx = canv.getContext("2d");
    ctx.font = "30px Georgia";
    ctx.strokeText(captcha.join(""), 45, 40);

    //storing captcha so that can validate you can save it somewhere else according to your specific requirements
    this.captcha = captcha.join("");
    document.getElementById("captcha").append(canv); // adds the canvas to the body element
    //return Math.round((Math.pow(36, length + 1) - Math.random() * Math.pow(36, length))).toString(36).slice(1);
  }
 

  createForm() {
    this.loginForm = new FormGroup({
      userName: new FormControl('', Validators.required),
      password: new FormControl('', Validators.required),
      captchaCode: new FormControl('', Validators.required)
    });
  }

 
  RedirecToHome() {
    if (this.loginForm.valid) {
     if (this.loginForm.controls.captchaCode.value == this.captcha) {
       this._Service.Login(this.loginForm.value).subscribe(res => {
          this.loginResponse = res;       
          if (this.loginResponse.status==true) {
            this.CheckLoginRole = true;
          }
          else {
            alert("Username/Password not valid?");
            //this.indexpage(); 
          }
        });
      }
      else {
        alert("Captcha is not correct.");
        this.createForm();
        this.randomString();
      }
    }
  }

  //indexpage() {
  //      this._Service.indexpage(this.loginForm.value).subscribe(res => {
  //        this.loginResponse = res; });     
  //  }
 
  NavigateToDashboard() {
    if (this.selectedRoleUserMapId > 0) {
      this.CheckLoginRole = false;
      let selectedRoleData = this.loginResponse.result.filter(a => a.roleUserMapId == this.selectedRoleUserMapId)[0] as any;
      sessionStorage.setItem('userId', selectedRoleData.userId);
      sessionStorage.setItem('token', JSON.stringify(this.loginResponse.token));
      sessionStorage.setItem('roleId', selectedRoleData.roleId);
      localStorage.setItem("LoginData", JSON.stringify(selectedRoleData));
      localStorage.setItem("LoginResponse", JSON.stringify(this.loginResponse));
      localStorage.setItem("DeptId", JSON.stringify({ "deptId": this.deptId }));
      localStorage.setItem('breadcrumbsData', JSON.stringify([]));
      this.router.navigate(['dashboard/home']);
    }
  }
}


//Hide By Nitesh for jwt implementation
//RedirecToHome() {

//  if (this.loginForm.valid) {
//    if (this.loginForm.controls.captchaCode.value == this.captcha) {
//      this._Service.Login(this.loginForm.value).subscribe(res => {
//        this.loginResponse = res;
//        this.loginUserId = res.userId;
//        if (this.loginUserId > 0) {
//          this.CheckLoginRole = true;
//          sessionStorage.setItem('token', JSON.stringify(this.loginDetails.jwT_Secret));
//        }
//        else {
//          alert("Username/Password not valid?");
//        }
//      });
//    }
//    else {
//      alert("Captcha is not correct.");
//      this.createForm();
//      this.randomString();
//    }
//  }
//}
