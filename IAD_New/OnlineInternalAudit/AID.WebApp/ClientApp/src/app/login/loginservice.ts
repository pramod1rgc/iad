import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AppConfig, APP_CONFIG } from '../global/global.module';
import { Observable } from 'rxjs';
import { Session } from 'protractor';
import { map } from 'rxjs/operators';

@Injectable()

export class LoginServices {
  BaseUrl: any = [];
  headers: any;
  constructor(private httpclient: HttpClient, @Inject(APP_CONFIG) private config: AppConfig) {
    this.headers = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    }
  }
  Login(loginModel: any): Observable<any> {
    return this.httpclient.post(this.config.login, loginModel, this.headers).pipe(map((resp: any) => { return resp.result }));
  }



 
}

