"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var CommonMsg = /** @class */ (function () {
    function CommonMsg() {
        /* common messages use in application */
        this.saveMsg = 'Records has been saved successfully.';
        this.updateMsg = 'Record has been updated successfully.';
        this.deleteMsg = 'Record has been deleted successfully.';
        this.alreadyExistMsg = 'Record Already Exist.';
        this.noRecordMsg = 'No Record Found.';
        this.enterCorrectValueMsg = 'Please Enter correct value.';
        this.checkDeclarationMsg = 'Please check Declaration.';
        this.forwardSupervisorMsg = 'Forwarded to Audit Supervisor';
        this.forwardAuditHeadMsg = 'Forwarded to Audit Head';
        this.forwardAuditDHMsg = 'Forwarded to Audit Entry User';
        this.updateFailedMsg = 'Failed to update Record.';
        this.alreadyForwardedMsg = 'Record Already Forwarded';
        this.deleteFailedMsg = 'Failed to Delete Record.';
        this.forwardCheckerMsg = 'Record Forwarded to Checker Successfully';
        this.saveFailedMsg = 'Failed to Save Record.';
        this.formatfileMsg = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';
        this.pdffileMsg = 'only pdf file upload';
        this.apppdffileMsg = 'application/pdf';
        this.errorMsg = 'An error has occurred. Please contact your system administrator.';
        /* common messages use in application */
        /* New Loan message */
        this.amountLessEqualloanAmountMsg = 'Entered Amount should be less than or equal to Loan Amount';
        this.amountActuallyPaidMsg = 'Entered Actually Paid Amount should be less than or equal to Cost Amount';
        this.loanAmountGreaterthenInstallMsg = 'Loan Amount should be greater than installment Amount';
        this.maximumLimitLoanAmountMsg = 'Maximum Limit for Loan Amount is Rs. 25 Lakhs';
        this.maxLimitLoanAmountMsg = 'Maximum Limit for Loan Amount of Computer Advance is Rs. 50000 or actual price of the PC, whichever is lower';
        this.maxLimitLoanAmount1LK80Msg = 'Maximum Limit for Loan Amount is Rs. 1,80,000.00';
        this.maxLimitLoanAmount2LK50Msg = 'Maximum Limit for Loan Amount is Rs. 2,50,000.00';
        /* New Loan message */
        /* Assign User */
        this.selectAtleastOneAssigner = 'Please assign atleast one employee';
    }
    /* Assign User */
    CommonMsg.prototype.charaterOnlyNoSpace = function (event) {
        if (event.target.value === "") {
            var charCode = (event.which) ? event.which : event.keyCode;
            if (charCode !== 32) {
                return true;
            }
            return false;
        }
    };
    CommonMsg.prototype.getquartersWithDate = function (finYear) {
        var quarters = [];
        if (finYear) {
            var years = finYear.split("-");
            quarters = [{ Id: "Q1", Desc: "Q1 (1 April, " + years[0] + " to 30 June, " + years[0] + ")", StartDate: "04/01/" + years[0], EndDate: "06/30/" + years[0] },
                { Id: "Q2", Desc: "Q2 (1 July, " + years[0] + " to 30 September, " + years[0] + ")", StartDate: "07/01/" + years[0], EndDate: "09/30/" + years[0] },
                { Id: "Q3", Desc: "Q3 (1 October, " + years[0] + " to 31 December, " + years[0] + ")", StartDate: "10/01/" + years[0], EndDate: "12/31/" + years[0] },
                { Id: "Q4", Desc: "Q4 (1 January, 20" + years[1] + " to 31 March, 20" + years[1] + ")", StartDate: "01/01/20" + years[1], EndDate: "03/31/20" + years[1] }];
        }
        return quarters;
    };
    CommonMsg.prototype.getRandomColor = function () {
        var specificColors = ['bf5f82', '9575cd', '8c7b75', '00867d'];
        var letters = '0123';
        var letter = letters[Math.floor(Math.random() * 4)];
        var color = '#' + specificColors[+letter];
        return color;
    };
    return CommonMsg;
}());
exports.CommonMsg = CommonMsg;
//# sourceMappingURL=common-msg.js.map
