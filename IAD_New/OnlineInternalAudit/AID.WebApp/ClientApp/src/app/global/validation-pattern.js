"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var ValidationPattern = /** @class */ (function () {
    function ValidationPattern() {
        this.invalidCharacter = "^[a-zA-Z][a-zA-Z0-9 ]+$";
    }
    return ValidationPattern;
}());
exports.ValidationPattern = ValidationPattern;
//# sourceMappingURL=validation-pattern.js.map