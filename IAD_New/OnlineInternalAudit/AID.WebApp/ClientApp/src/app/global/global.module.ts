import { NgModule, InjectionToken } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule, } from '@angular/router';
import { MaterialModule } from '../material.module';


export let APP_CONFIG = new InjectionToken<AppConfig>('app.config');

export class AppConfig {
  api_base_url: string;
  login: string;
  //Menu
  getAllMenus: string;
  getMenusData: string;
  bindDropDownMenu: string;
  getAllPredefineRole: string;
  getDashboardAllRecord: string;
  getIADHeadDashboardRecord: string;
  getAuditDHDashboardRecord: string;
  getallschedule: string;
  getallAuditInchart: string;
  getallAuditDHInchart: string;
  getallIAdHeadInchart: string;
  //End Of Menu
  getRoles: string;
  upsertRoles: string;
  toggleRoleActivation: string;
  //Employee
  getAllEmployee: string;
  getAllDesig: string;
  SaveEmployee: string;
  ActiveDeactiveUser: string;
  //End Of Employee
  //Master
  SaveRiskCategory: string;
  GetAllCategoryDetails: string;
  ActiveDeactive: string;
  BindDropDawnCategory: string;
  //End Of Master
  //Audit Structure
  BindDropFYear: string;
  getController: string;
  getAllPAO: string;
  getauditlist: string;
  assignAuditByController: string;
  getassignPaoAudit: string;
  getScheduledAudits: string;
  updateScheduledAudit: string;
  deleteScheduledAudit: string;
  pendingFinancialForVerifyQuarter: string;

  //Quartarly Audit
  bindAuditInGrid: string;
  SaveQuartalyAudit: string;
  assignToSupervisor: string;
  SaveQuartalyPlanAudit: string;
  assignQuarterlyPlanToSupervisor: string;
  BindSupervisorDropDown: string;
  //End Of Quartarly Audit
 
  //End Of Audit Structure

  //Verification
  getScheduledAuditsForVerification: string;
  updateScheduledAuditsForVerification: string;
  getPendingTaskCount: string;
  getAuditDetailsForVerificationDropdown: string;
  getAuditsOfConferenceForVerification: string;
  getTeamOfAuditForVerification: string;
  updateTeamOfAuditForVerification: string;
  getAuditsForwardedToSupervisor: string;
  getAuditsWhoseParasOrComplainceForwardedToSupervisor: string;
  getAuditsWhoseSettlementLetterForwardedToSupervisor: string;
  getDistinctQuartersBasedOnStatusAndFY: string;
  //End of Verification

  //Audits
  getApprovedScheduledAudits: string;

  getApprovedAuditTeamAuditsForDropdown: string;
  GetApprovedAuditTeam: string;
  revertAuditTeam: string;

  GetAllPAaoAuditRecord: string;
  SendIntimationMails: string;
  //End of Audits
  //change password
  ChangePassword: string;
  //end changepassword
  

  //Assign User
  getUserRoleEmployees: string;
  assignUserRole: string;
  getAuditsForDropdown: string;
  getAssignAuditTeam: string;
  checkIfUserAlreadyAssignedToAnotherAudit: string;
  assignAuditTeam: string;
  //End of Assign User
  //verify Anual plan
  Getauditlistforverify: string;
  getAllQuarterAudit: string;
  anualplanApprove: string;
  rejectanualsAudit: string;
  GetauditlistforverifyQuarterly: string;
  quarterlylplanApprove: string;
  rejectQuarterlyAudit: string;
  getAllPendingQuarter: string;
  supervisorTosupervisor: string;
  supervisorTosupervisorQuarterlyPlan: string;

  //End Of verify

  //Audit Programme
  getAuditProgramme: string;
  // end of Audit Programme

  //Audit Observation
  getAuditDHAuditList: string;
  getAuditEntryConference: string;
  updateAuditEntryConference: string;
  getAuditExitConference: string;
  updateAuditExitConference: string;
  getAuditPara: string;
  updateAuditPara: string;
  getAuditParaDetail: string;
  deleteAuditPara: string;
  postParaComment: string;
  getParaComments: string;
  getAuditParaSettlementLetter: string;
  updateAuditParaSettlementLetter: string;
  updateAuditParaSettlementLetterStatus: string;
  updateAuditStatus: string;
  updateEntryConferenceStatus: string;
  updateExitConferenceStatus: string;
  updateAuditParaStatus: string;
  updateAuditComplianceStatus: string;
  getAllFinancialImplicationCases: string;
  getAuditParaFinancialImplicationCases: string;
  // end of Audit Observation
  //Auditee Observation
  getAuditeeDHAuditList: string;
  getAuditeeEntryConference: string;
  updateAuditeeEntryConference: string;
  getAuditeeExitConference: string;
  updateAuditeeExitConference: string;
  getAuditeePara: string;
  updateAuditeePara: string;
  getAuditeeParaDetail: string;
  deleteAuditeePara: string;
  postAuditeeParaComment: string;
  getAuditeeParaComments: string;
  getAuditeeParaSettlementLetter: string;
  updateAuditeeParaSettlementLetter: string;
  updateAuditeeParaSettlementLetterStatus: string;
  updateAuditeeStatus: string;
  updateAuditeeParaStatus: string;
  // end of Audit Observation

  //Auditee Universe
  GetAllAuditee: string;
  SaveAuditee: string;
  ActiveDeactiveAuditee: string;
  AuditeeDesignation: string;
  AuditeeRole: string;
  //Auditee Universe
  
  // Audit Report
  getAuditsForAuditReportBySupervisor: string;
  getAuditReport: string;
  // end of audit report
  //desk audit
  getDeskAuditFiles: string;
  DeskAuditSaveAndUpdate: string;
  active_deactiveDeskAudit: string;
  //Common
  bindSupervisorDropDown: string;
  //End of Common
}

const getHostName = window.location.hostname;
let apiHost;
if (getHostName === 'localhost') {
  //apiHost = 'http://localhost:44397/api';
  apiHost = 'https://localhost:44397/api/';
} else {
  //apiHost = 'http://10.199.72.53:83/api/';
  apiHost = 'http://10.72.160.98/api/';
}
export const APP_DI_CONFIG: AppConfig = {
  api_base_url: apiHost,
  //==============DashBoard=======
  login: apiHost + 'Login/LoginCheck',
  getAllMenus: apiHost + 'Dashboard/getAllMenus',
  getDashboardAllRecord: apiHost + 'Dashboardhome/GetAllDashboardRecord',
  getIADHeadDashboardRecord: apiHost + 'Dashboardhome/GetAll_IADHead_DashboardRecord',
  getAuditDHDashboardRecord: apiHost + 'Dashboardhome/GetAuditDhDashboardAllRecord',
  getallschedule: apiHost + 'Dashboardhome/getallschedule',
  getallAuditInchart: apiHost + 'Dashboardhome/GetallAuditInchart',
  getallAuditDHInchart: apiHost + 'Dashboardhome/GetallAuditDHInchart',
  getallIAdHeadInchart: apiHost + 'Dashboardhome/Getall_IADHeadInchart',
  //============End Of DashBoard======
  //==========Menu=========
  getMenusData: apiHost + 'Menu/BindMenuInGride',
  bindDropDownMenu: apiHost + 'Menu/bindMenuInDropDown',
  getAllPredefineRole: apiHost + 'Menu/GetAllPredefineRole',
  //=========End Of Menu===



  //=========Role============
  getRoles: apiHost + 'Role/GetRoles',
  upsertRoles: apiHost + 'Role/UpsertRoles',
  toggleRoleActivation: apiHost + 'Role/ToggleRoleActivation',
  //=========End of Role================


  //=========Employee=========
  getAllEmployee: apiHost + 'Employee/GetAllEmployee',
  getAllDesig: apiHost + 'Employee/GetAllDesignation',
  SaveEmployee: apiHost + 'Employee/SaveEmployee?hdnEmpId=',
  ActiveDeactiveUser: apiHost + 'Employee/ActiveDeactiveUser',

  //=======End Of Employee=========

  //==============Assign User=============
  getUserRoleEmployees: apiHost + 'AssignUser/GetUserRoleEmployees',
  assignUserRole: apiHost + 'AssignUser/AssignUserRole',
  getAuditsForDropdown: apiHost + 'AssignUser/GetAuditsForDropdown',
  getAssignAuditTeam: apiHost + 'AssignUser/GetAssignAuditTeam',
  checkIfUserAlreadyAssignedToAnotherAudit: apiHost + 'AssignUser/CheckIfUserAlreadyAssignedToAnotherAudit',
  assignAuditTeam: apiHost + 'AssignUser/AssignAuditTeam',
  //============End of Assign User=============

  //==============Master==========
  SaveRiskCategory: apiHost + 'RiskCategory/SaveRiskCategory?hdnCategoryId=',
  GetAllCategoryDetails: apiHost + 'RiskCategory/GetAllCategoryDetails',
  ActiveDeactive: apiHost + 'RiskCategory/ActiveDeactive',
  BindDropDawnCategory: apiHost + 'RiskCategory/BindDropDawnCategory',
  //========End of Master=========


   //Audit Structure
  BindDropFYear: apiHost + 'CreateAuditplan/BindFYearDropDown',
  pendingFinancialForVerifyQuarter: apiHost + 'CreateAuditplan/pendingFinancialForVerifyQuarter',
  BindSupervisorDropDown: apiHost + 'CreateAuditplan/BindSupervisorDropDown',
  getAllPAO: apiHost + 'CreateAuditplan/GetAllPAO',
  getassignPaoAudit: apiHost + 'CreateAuditplan/GetassignPaoAudit',
  getController: apiHost + 'CreateAuditplan/getController',

  getauditlist: apiHost + 'CreateAuditplan/Getauditlist',

  assignAuditByController: apiHost + 'CreateAuditplan/AssignAuditByController',
  getScheduledAudits: apiHost + 'ScheduleAudit/GetScheduledAudits',
  updateScheduledAudit: apiHost + 'ScheduleAudit/UpdateScheduledAudit',
  deleteScheduledAudit: apiHost + 'ScheduleAudit/DeleteScheduledAudit',

  
  //Quartaly Audit
  bindAuditInGrid: apiHost + 'CreateAuditplan/BindAuditInGrid',
  SaveQuartalyAudit: apiHost + 'CreateAuditplan/SaveQuartalyAudit',
  assignToSupervisor: apiHost + 'CreateAuditplan/AssignToSupervisor',
  assignQuarterlyPlanToSupervisor: apiHost + 'CreateAuditplan/AssignQuarterlyPlanToSupervisor',
  SaveQuartalyPlanAudit: apiHost + 'CreateAuditplan/SaveQuartalyPlanAudit',
  //End Of 


  //End Of Audit Structure

  //Verification
  getScheduledAuditsForVerification: apiHost + 'VerifyScheduledAudit/GetScheduledAuditsForVerification',
  updateScheduledAuditsForVerification: apiHost + 'VerifyScheduledAudit/UpdateScheduleAuditForVerification',
  getPendingTaskCount: apiHost + 'VerifyScheduledAudit/GetPendingTaskCount',
  getAuditDetailsForVerificationDropdown: apiHost + 'VerifyAuditTeam/GetAuditDetailsForVerificationDropdown',
  getAuditsOfConferenceForVerification: apiHost + 'VerifyAuditConference/GetAuditsOfConferenceForVerification',
  getTeamOfAuditForVerification: apiHost + 'VerifyAuditTeam/GetTeamOfAuditForVerification',
  updateTeamOfAuditForVerification: apiHost + 'VerifyAuditTeam/UpdateTeamOfAuditForVerification',
  getAuditsForwardedToSupervisor: apiHost + 'VerifyAuditCompliance/GetAuditsForwardedToSupervisor',
  getAuditsWhoseParasOrComplainceForwardedToSupervisor: apiHost + 'VerifyAuditCompliance/GetAuditsWhoseParasOrComplainceForwardedToSupervisor',
  getAuditsWhoseSettlementLetterForwardedToSupervisor: apiHost + 'VerifyAuditCompliance/GetAuditsWhoseSettlementLetterForwardedToSupervisor',
  getDistinctQuartersBasedOnStatusAndFY: apiHost + 'VerifyScheduledAudit/GetDistinctQuartersBasedOnStatusAndFY',

  Getauditlistforverify: apiHost + 'Verifyannualplan/Getauditlistforverify',
  getAllQuarterAudit: apiHost + 'Verifyannualplan/GetAllQuarterAudit',
  anualplanApprove: apiHost + 'Verifyannualplan/AnualplanApprove',
  rejectanualsAudit: apiHost + 'Verifyannualplan/RejectanualsAudit',
  GetauditlistforverifyQuarterly: apiHost + 'Verifyannualplan/GetauditlistforverifyQuarterly',
  rejectQuarterlyAudit: apiHost + 'Verifyannualplan/RejectQuarterlyAudit',
  getAllPendingQuarter: apiHost + 'Verifyannualplan/getAllPendingQuarter',
  quarterlylplanApprove: apiHost + 'Verifyannualplan/QuarterlylplanApprove',
  supervisorTosupervisor: apiHost + 'Verifyannualplan/SupervisorTosupervisor',
  supervisorTosupervisorQuarterlyPlan: apiHost + 'Verifyannualplan/SupervisorTosupervisorQuarterlyPlan',
  //End of Verification

  //Audits
  getApprovedScheduledAudits: apiHost + 'Audits/GetApprovedScheduledAudits',

  getApprovedAuditTeamAuditsForDropdown: apiHost + 'Audits/GetApprovedAuditTeamAuditsForDropdown',
  GetApprovedAuditTeam: apiHost + 'Audits/GetApprovedAuditTeam',
  revertAuditTeam: apiHost + 'Audits/RevertAuditTeam',

  GetAllPAaoAuditRecord: apiHost + 'SendIntimationMails/GetAllPAaoAuditRecord',
  SendIntimationMails: apiHost + 'SendIntimationMails/SendMail',
  //End of Audits

  // Audit Programme
  getAuditProgramme: apiHost + 'AuditProgramme/GetAuditProgrammes',
  // End of Audit Programme

  // Audit Observation
  getAuditDHAuditList: apiHost + 'AuditObservation/GetAuditDHAuditsList',
  getAuditEntryConference: apiHost + 'AuditObservation/GetAuditEntryConference',
  updateAuditEntryConference: apiHost + 'AuditObservation/UpdateAuditEntryConference',
  getAuditExitConference: apiHost + 'AuditObservation/GetAuditExitConference',
  updateAuditExitConference: apiHost + 'AuditObservation/UpdateAuditExitConference',
  getAuditPara: apiHost + 'AuditObservation/GetAuditPara',
  updateAuditPara: apiHost + 'AuditObservation/UpdateAuditPara',
  getAuditParaDetail: apiHost + 'AuditObservation/GetAuditParaDetails',
  deleteAuditPara: apiHost + 'AuditObservation/DeleteAuditPara',
  postParaComment: apiHost + 'AuditObservation/PostParaCommentByAuditor',
  getParaComments: apiHost + 'AuditObservation/GetParaComments',
  getAuditParaSettlementLetter: apiHost + 'AuditObservation/GetAuditParaSettlementLetter',
  updateAuditParaSettlementLetter: apiHost + 'AuditObservation/UpdateAuditParaSettlementLetter',
  updateAuditParaSettlementLetterStatus: apiHost + 'AuditObservation/UpdateParaSettlementLetterStatus',
  updateAuditStatus: apiHost + 'AuditObservation/UpdateAuditStatus',
  updateEntryConferenceStatus: apiHost + 'AuditObservation/UpdateEntryConferenceStatus',
  updateExitConferenceStatus: apiHost + 'AuditObservation/UpdateExitConferenceStatus',
  updateAuditParaStatus: apiHost + 'AuditObservation/UpdateAuditParaStatus',
  updateAuditComplianceStatus: apiHost + 'AuditObservation/UpdateAuditComplianceStatus',
  getAllFinancialImplicationCases: apiHost + 'AuditObservation/GetAllFinancialImplicationCases',
  getAuditParaFinancialImplicationCases: apiHost + 'AuditObservation/GetAuditParaFinancialImplicationCases',
  //End of Audit Observation

  // Auditee Observation
  getAuditeeDHAuditList: apiHost + 'AuditeeObservation/GetAuditDHAuditsList',
  getAuditeeEntryConference: apiHost + 'AuditeeObservation/GetAuditEntryConference',
  updateAuditeeEntryConference: apiHost + 'AuditeObservation/UpdateAuditEntryConference',
  getAuditeeExitConference: apiHost + 'AuditeeObservation/GetAuditExitConference',
  updateAuditeeExitConference: apiHost + 'AuditeeObservation/UpdateAuditExitConference',
  getAuditeePara: apiHost + 'AuditeeObservation/GetAuditPara',
  updateAuditeePara: apiHost + 'AuditeeObservation/UpdateAuditPara',
  getAuditeeParaDetail: apiHost + 'AuditeeObservation/GetAuditParaDetails',
  deleteAuditeePara: apiHost + 'AuditeeObservation/DeleteAuditPara',
  postAuditeeParaComment: apiHost + 'AuditeeObservation/PostParaCommentByAuditor',
  getAuditeeParaComments: apiHost + 'AuditeeObservation/GetParaComments',
  getAuditeeParaSettlementLetter: apiHost + 'AuditeeObservation/GetAuditParaSettlementLetter',
  updateAuditeeParaSettlementLetter: apiHost + 'AuditeeObservation/UpdateAuditParaSettlementLetter',
  updateAuditeeParaSettlementLetterStatus: apiHost + 'AuditeeObservation/UpdateParaSettlementLetterStatus',
  updateAuditeeStatus: apiHost + 'AuditeeObservation/UpdateAuditStatus',
  updateAuditeeParaStatus: apiHost + 'AuditeeObservation/UpdateAuditParaStatus',
  //End of Auditee Observation
  //Desk Audit
  getDeskAuditFiles: apiHost + 'DeskAudit/getDeskAuditFiles',
  DeskAuditSaveAndUpdate: apiHost + 'DeskAudit/SaveAndUpdateDeskAudit',
  active_deactiveDeskAudit: apiHost + 'DeskAudit/active_deactiveDeskAudit',
  //change password
  ChangePassword: apiHost + 'ChangePassword/ChangePassword',
  //end
  //Auditee
  GetAllAuditee: apiHost + 'AuditeeUniverse/GetAllAuditee',
  SaveAuditee: apiHost + 'AuditeeUniverse/SaveAuditee',
  ActiveDeactiveAuditee: apiHost + 'AuditeeUniverse/ActiveDeactiveAuditee',
  AuditeeDesignation: apiHost + 'AuditeeUniverse/GetAllDesignation',
  AuditeeRole: apiHost + 'AuditeeUniverse/GetAuditeeRole',
  //Auditee

  //Audit Report
  getAuditsForAuditReportBySupervisor: apiHost + 'AuditReport/GetAuditsForAuditReportBySupervisor',
  getAuditReport: apiHost + 'AuditReport/GetAuditReport',
  //end of Audit Report

  //Common
  bindSupervisorDropDown: apiHost + 'CreateAuditplan/BindSupervisorDropDown',
  //end of Common
};



@NgModule({
  imports: [

    FormsModule,
    HttpModule,
    RouterModule,
    MaterialModule,
    ReactiveFormsModule
  ],
  declarations: [],
  exports: [

    FormsModule,
    HttpModule,
    RouterModule,
    MaterialModule,
    ReactiveFormsModule
  ],
  providers: [{
    provide: APP_CONFIG,
    useValue: APP_DI_CONFIG
  }]
})

export class GlobalModule {
}
