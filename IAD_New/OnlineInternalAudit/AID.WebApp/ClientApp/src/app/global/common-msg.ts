export class CommonMsg {
  /* common messages use in application */
  saveMsg: string = 'Records has been saved successfully.';
  updateMsg: string = 'Record has been updated successfully.';
  updateMsgForAuditeeHeadApprove: string = 'Record has been approved';
  updateMsgForAuditeeHeadReject: string = 'Record has been rejected';
  deleteMsg: string = 'Record has been deleted successfully.';
  alreadyExistMsg: string = 'Record Already Exist.';
  noRecordMsg: string = 'No Record Found.';
  enterCorrectValueMsg: string = 'Please Enter correct value.';
  checkDeclarationMsg: string = 'Please check Declaration.';
  forwardSupervisorMsg: string = 'Forwarded to Audit Supervisor';
  forwardAuditHeadMsg: string = 'Forwarded to Audit Head';
  forwardAuditDHMsg: string = 'Forwarded to Audit Entry User';
  updateFailedMsg: string = 'Failed to update Record.';
  alreadyForwardedMsg: string = 'Record Already Forwarded';
  deleteFailedMsg: string = 'Failed to Delete Record.';
  forwardCheckerMsg: string = 'Record Forwarded to Checker Successfully';
  saveFailedMsg: string = 'Failed to Save Record.';
  formatfileMsg: string = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';
  pdffileMsg: string = 'only pdf file upload';
  apppdffileMsg: string = 'application/pdf';
  errorMsg: string = 'An error has occurred. Please contact your system administrator.';
  /* common messages use in application */

  /* New Loan message */
  amountLessEqualloanAmountMsg: string = 'Entered Amount should be less than or equal to Loan Amount';
  amountActuallyPaidMsg: string = 'Entered Actually Paid Amount should be less than or equal to Cost Amount'
  loanAmountGreaterthenInstallMsg: string = 'Loan Amount should be greater than installment Amount';
  maximumLimitLoanAmountMsg: string = 'Maximum Limit for Loan Amount is Rs. 25 Lakhs';
  maxLimitLoanAmountMsg: string = 'Maximum Limit for Loan Amount of Computer Advance is Rs. 50000 or actual price of the PC, whichever is lower';
  maxLimitLoanAmount1LK80Msg: string = 'Maximum Limit for Loan Amount is Rs. 1,80,000.00';
  maxLimitLoanAmount2LK50Msg: string = 'Maximum Limit for Loan Amount is Rs. 2,50,000.00';
  /* New Loan message */

  /* Assign User */
  selectAtleastOneAssigner: string = 'Please assign atleast one employee';
  selectOnlyOneAssigner: string = 'Please Assign only one employee.';
  /* Assign User */

  charaterOnlyNoSpace(event): boolean {
    if (event.target.value === "") {
      const charCode = (event.which) ? event.which : event.keyCode;
      if (charCode !== 32) {
        return true;
      }
      return false;
    }
  }

  getquartersWithDate(finYear: string): any[] {
    let quarters = [];
    if (finYear) {
      let years = finYear.split("-");

      quarters = [{ Id: "Q1", Desc: "Q1 (1 April, " + years[0] + " to 30 June, " + years[0] + ")", StartDate: "04/01/" + years[0], EndDate: "06/30/" + years[0] },
        { Id: "Q2", Desc: "Q2 (1 July, " + years[0] + " to 30 September, " + years[0] + ")", StartDate: "07/01/" + years[0], EndDate: "09/30/" + years[0] },
        { Id: "Q3", Desc: "Q3 (1 October, " + years[0] + " to 31 December, " + years[0] + ")", StartDate: "10/01/" + years[0], EndDate: "12/31/" + years[0] },
        { Id: "Q4", Desc: "Q4 (1 January, 20" + years[1] + " to 31 March, 20" + years[1] + ")", StartDate: "01/01/20" + years[1], EndDate: "03/31/20" + years[1]}]
    }
    return quarters;
  }

  getRandomColor(): string {
    var specificColors = ['bf5f82', '9575cd', '8c7b75', '00867d'];
    var letters = '0123';
    let letter = letters[Math.floor(Math.random() * 4)];
    var color = '#' + specificColors[+letter];
    return color;
  }
  onEditScrollTop() {
    window.scrollTo(0, 0);
  }
}
