import { NgModule } from '@angular/core';
import { UsermanagementModule } from './usermanagement.module';
import { NewmenucreationComponent } from './newmenucreation/newmenucreation.component';
import { NewRoleCreationComponent } from './new-role-creation/new-role-creation.component';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [

  {
    path: '', component: UsermanagementModule, data: {
      breadcrumb: 'User Management'
    }, children: [
      {
        path: 'menu', component: NewmenucreationComponent, data: {
          breadcrumb: 'New Menu Creation'
        }
      },
      {
        path: 'newrole', component: NewRoleCreationComponent, data: {
          breadcrumb: 'New Role Creation'
        }
      }
    
     
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UsermanagementRoutingModule { }
