import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UsermanagementRoutingModule } from './usermanagement-routing.module';
import { NewmenucreationComponent } from './newmenucreation/newmenucreation.component';
import { NewRoleCreationComponent } from './new-role-creation/new-role-creation.component';
import { MaterialModule } from '../material.module';
import { MatSlideToggleModule } from '@angular/material';
import { RoleServices } from '../services/role/role.services';
import { SharedModule } from '../shared-module/shared-module.module';
import { NgxMatSelectSearchModule } from 'ngx-mat-select-search';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { RoleMenuManagementComponent } from './role-menu-management/role-menu-management.component';

@NgModule({
  declarations: [NewmenucreationComponent, NewRoleCreationComponent, RoleMenuManagementComponent],
  imports: [
    CommonModule,
    UsermanagementRoutingModule,
    MaterialModule,
    FormsModule,
    MatSlideToggleModule,
    SharedModule,
    NgxMatSelectSearchModule,
    ReactiveFormsModule
  ],
  providers: [RoleServices]
})
export class UsermanagementModule { }
