import { Component, OnInit, ViewChild } from '@angular/core';
import { RoleServices } from '../../services/role/role.services';
import { MatTableDataSource } from '@angular/material';
import { FormGroup, FormControl, Validators, FormGroupDirective } from '@angular/forms';

@Component({
  selector: 'app-new-role-creation',
  templateUrl: './new-role-creation.component.html',
  styleUrls: ['./new-role-creation.component.css']
})
export class NewRoleCreationComponent implements OnInit {

  roles: any[] = [];
  dataSource: MatTableDataSource<any>;
  createRolePopUp: boolean = false;
  deactivateRolePopUp: boolean = false;
  roleForm: FormGroup;
  userId: number;
  reason: string = '';
  selectedRole: any;

  @ViewChild(FormGroupDirective) formGroupDirective: FormGroupDirective;

  displayedColumns: string[] = ['RoleName', 'RoleDescription', 'CreatedDate', 'DeactivationReason', 'DeactivationDate', 'Action'];

  constructor(private _service: RoleServices) {
    this.userId = JSON.parse(localStorage.getItem("LoginData")).userId;
  }

  createRoleForm() {
    this.roleForm = new FormGroup({
      roleId: new FormControl(0),
      roleName: new FormControl('', Validators.required),
      roleDescription: new FormControl('', Validators.required),
      roleHierarchy: new FormControl(0),
      userId: new FormControl(this.userId)
    });
  }

  cancelForm() {
    this.createRoleForm();
    this.formGroupDirective.resetForm();
    this.createRolePopUp = false;
  }

  updateRole() {
    if (this.roleForm.valid) {
      this._service.upsertRoles(this.roleForm.value).subscribe(response => {
        if (response > 0) {
          this.cancelForm();
          this.getRoles();
        }
      });
    }
  }

  editRole(role: any) {
    this.roleForm.setValue({
      roleId: role.roleId,
      roleName: role.roleName,
      roleDescription: role.roleDescription,
      roleHierarchy: role.roleHierarchy,
      userId: this.userId
    });
    this.createRolePopUp = true;
  }

  getRoles() {
    this._service.getRoles().subscribe(response => {
      this.roles = response;
      this.dataSource = new MatTableDataSource(this.roles);
    });
  }

  toggleRoleActivation(event, role) {
    this.selectedRole = role;
    if (event.checked) {
      let obj = { RoleId: role.roleId, Reason: this.reason, IsActive: event.checked, UserId: this.userId };
      this._service.toggleRoleActivation(obj).subscribe(response => {
        if (response > 0) {
          this.reason = '';
          this.getRoles();
        }
      });
    }
    else {
      this.deactivateRolePopUp = true;
    }
  }

  saveDeactivateRole() {
    let obj = { RoleId: this.selectedRole.roleId, Reason: this.reason, IsActive: false, UserId: this.userId };
    this._service.toggleRoleActivation(obj).subscribe(response => {
      if (response > 0) {
        this.reason = '';
        this.deactivateRolePopUp = false;
        this.getRoles();
      }
    });
  }

  ngOnInit() {
    this.getRoles();
    this.createRoleForm();
  }

}
