import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RoleMenuManagementComponent } from './role-menu-management.component';

describe('RoleMenuManagementComponent', () => {
  let component: RoleMenuManagementComponent;
  let fixture: ComponentFixture<RoleMenuManagementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RoleMenuManagementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RoleMenuManagementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
