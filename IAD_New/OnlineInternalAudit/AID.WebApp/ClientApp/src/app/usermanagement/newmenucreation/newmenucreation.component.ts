import { Component, OnInit, ViewChild } from '@angular/core';
import { MenuserviceService } from '../../services/UserManagement/menuservice.service';
import { MatTableDataSource, MatPaginator, MatSort, MatTableModule } from '@angular/material';
import { ReplaySubject, Subject } from 'rxjs';
import { FormControl } from '@angular/forms';
import { takeUntil } from 'rxjs/operators';
export interface commondll {
  values: string;
  text: string;
}
@Component({
  selector: 'app-newmenucreation',
  templateUrl: './newmenucreation.component.html',
  styleUrls: ['./newmenucreation.component.css']
})

export class NewmenucreationComponent implements OnInit {

  constructor(private _MenuService: MenuserviceService) { }

  //Variable
  displayedColumns: string[] = ['menuDescriptionName', 'parentMenu', 'menuURL','Action'];//, 'Active'];//, 'leaveTypeDesc', 'action',];
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild('menuassign') roleassign: any;
  public filteredMenu: ReplaySubject<commondll[]> = new ReplaySubject<commondll[]>(1);
  public MenuFilterCtrl: FormControl = new FormControl();
  private _onDestroy = new Subject<void>();
  ArrMenusInGride: any;
  ArrMenudataSource: any;
  isTableHasData: boolean;
  hdnMenuId = 0;
  IsShowHiddl: boolean;
  mstMenu: any = {};
  ArrMenus: any;
  //selrdType: string;
  ArrRoles: any;
  //End Of Variable
  ngOnInit() {
    //this.selrdType = 'Root';
    this.GetAllRoles();
    this.BindMenuInGride();
    this.BindDropDownMenu();
    this.MenuFilterCtrl.valueChanges.pipe(takeUntil(this._onDestroy)).subscribe(() => { this.filterMenu(); });
  }
  //Bind Menu In Gride
  BindMenuInGride() {
    this._MenuService.BindMenuInGride().subscribe(result => {
      this.ArrMenusInGride = result;
      this.ArrMenudataSource = new MatTableDataSource(result);
      this.ArrMenudataSource.paginator = this.paginator;
      this.ArrMenudataSource.sort = this.sort;
     
    })

  }
  //Search Row
  //applyFilter(filterValue: string) {
  //  this.ArrMenudataSource.filter = filterValue.trim().toLowerCase();
  //  if (this.ArrMenudataSource.paginator) {
  //    this.ArrMenudataSource.paginator.firstPage();
  //  }
  //  if (this.ArrMenudataSource.filteredData.length > 0)
  //    this.isTableHasData = true;
  //  else
  //    this.isTableHasData = false;
  //}
  //End Of Search Row
  //End Of Bind Menu In Gride

  //Save Menu
  SaveMenu() {
   
   
  }
  //End Of Save  Menu
  //HideandShowradio(Rdfleg) {
  //  if (Rdfleg == 'Root') {
  //    this.IsShowHiddl = false;

  //  }
  //  if (Rdfleg == 'SubMenu') {
  //    this.IsShowHiddl = true;
  //  }
  //}

  BindDropDownMenu() {
    this._MenuService.BindDropDownMenu().subscribe(result => {
      debugger;
      this.ArrMenus = result;
      this.mstMenu.parentMenu = 0;      
      this.filteredMenu.next(this.ArrMenus);
    })
  }
  // use for filter search in dropdawn
  private filterMenu() {
    debugger;
    if (!this.filteredMenu) {
      return;
    }
    // get the search keyword
    let search = this.MenuFilterCtrl.value;
    if (!search) {

      this.filteredMenu.next(this.ArrMenus.slice());
      return;
    }
    else {
      search = search.toLowerCase();
    }
    // filter the banks
    //this.filteredMenu.next(

    //  this.ArrMenus.filter(ArrMenus => ArrMenus.text.toLowerCase().indexOf(search) > -1)
    //);
    var a = this.ArrMenus;
    this.filteredMenu.next(
      this.ArrMenus.filter(ArrMenus => ArrMenus.menuDescriptionName.toLowerCase().indexOf(search) > -1)
    );
  }

  //Bind Role DropDawnList
  GetAllRoles() {
    this._MenuService.GetAllPredefineRole().subscribe(result => {
      this.ArrRoles = result;
      this.mstMenu.parentMenu = 0;
    })
  }
  //End Of Bind Role DropDawnList


}
