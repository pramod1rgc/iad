import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewmenucreationComponent } from './newmenucreation.component';

describe('NewmenucreationComponent', () => {
  let component: NewmenucreationComponent;
  let fixture: ComponentFixture<NewmenucreationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewmenucreationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewmenucreationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
