import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VerifyAuditComplianceComponent } from './verify-audit-compliance.component';

describe('VerifyAuditComplianceComponent', () => {
  let component: VerifyAuditComplianceComponent;
  let fixture: ComponentFixture<VerifyAuditComplianceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VerifyAuditComplianceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VerifyAuditComplianceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
