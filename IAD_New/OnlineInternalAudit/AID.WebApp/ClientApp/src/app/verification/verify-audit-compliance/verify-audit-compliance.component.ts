import { Component, OnInit, ViewChild } from '@angular/core';
import { VerifyAuditComplianceService } from '../../services/verification/verifyAuditCompliance.service';
import { AuditObservationService } from '../../services/audit-observation/audit-observation.service';
import { MatTableDataSource, MatPaginator } from '@angular/material';

@Component({
  selector: 'app-verify-audit-compliance',
  templateUrl: './verify-audit-compliance.component.html',
  styleUrls: ['./verify-audit-compliance.component.css']
})
export class VerifyAuditComplianceComponent implements OnInit {

  userId: number;
  roleId: number;
  auditList: any[] = [];
  paraList: any[] = [];
  selectedAuditId: any;
  searchTerm: string = '';
  pageSize: number = 10;
  pageNumber: number = 1;
  totalCount: number = 0;

  displayedColumns: string[] = ['ParaTitle', 'ObservationType', 'RiskLevel', 'Status', 'Action'];

  dataSource: MatTableDataSource<any> = new MatTableDataSource<any>();

  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(private _service: VerifyAuditComplianceService, private auditObservationService: AuditObservationService) {
    this.userId = JSON.parse(localStorage.getItem("LoginData")).userId;
    this.roleId = JSON.parse(localStorage.getItem("LoginData")).roleId;
  }

  getAuditList() {
    this._service.getAuditsWhoseParasOrComplainceForwardedToSupervisor('compliance').subscribe(response => {
      this.auditList = response;
    });
  }

  getAuditDetails() {
    if (this.selectedAuditId) {
      localStorage.setItem("AuditDetailsForVerification", JSON.stringify(this.auditList.filter(a => a.auditId == this.selectedAuditId)[0] as any));
      this.auditObservationService.getAuditParas(this.selectedAuditId, this.pageSize, this.pageNumber, this.searchTerm).subscribe(response => {
        this.paraList = response;
        if (this.paraList.length > 0) {
          this.totalCount = this.paraList[0].totalCount;
          this.dataSource = new MatTableDataSource(this.paraList);
        }
      });
    }
  }

  applyFilter() {
    this.totalCount = 0;
    this.pageNumber = 1;
    this.paginator.firstPage();
    this.getAuditDetails();
  }

  getPaginationData(event) {
    this.pageSize = event.pageSize;
    this.pageNumber = event.pageIndex + 1;
    this.getAuditDetails();
  }

  ngOnInit() {
    this.getAuditList();
  }

}
