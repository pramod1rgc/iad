import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VerifyComplianceDetailComponent } from './verify-compliance-detail.component';

describe('VerifyAuditParaComponent', () => {
  let component: VerifyComplianceDetailComponent;
  let fixture: ComponentFixture<VerifyComplianceDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [VerifyComplianceDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VerifyComplianceDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
