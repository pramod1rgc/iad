import { Component, OnInit } from '@angular/core';
import { VerifyAuditConferenceService } from '../../services/verification/verifyAuditConference.service';
import { AuditObservationService } from '../../services/audit-observation/audit-observation.service';
import swal from 'sweetalert2';
import { CommonMsg } from '../../global/common-msg';

@Component({
  selector: 'app-verify-entry-conference',
  templateUrl: './verify-entry-conference.component.html',
  styleUrls: ['./verify-entry-conference.component.css']
})
export class VerifyEntryConferenceComponent implements OnInit {

  auditsList: any[] = [];
  selectedAuditId: any;
  entryConferenceDetails: any;
  userId: number;
  RejectPopUp: boolean = false;
  rejectionRemark: string = '';

  constructor(private _service: VerifyAuditConferenceService, private auditObservationService: AuditObservationService, private _msg: CommonMsg) {
    this.userId = JSON.parse(localStorage.getItem("LoginData")).userId;
  }

  getAuditsForDropdown() {
    this._service.getAuditsOfConferenceForVerification('entry').subscribe(response => {
      this.auditsList = response;
    });
  }

  getEntryConferenceDetails() {
    this.auditObservationService.getAuditEntryConference(this.selectedAuditId).subscribe(response => {
      if (response && response.length > 0) {
        this.entryConferenceDetails = response[0];
      }
    });
  }

  updateStatus(statusId) {
    if (this.entryConferenceDetails) {
      let obj = { StatusId: statusId, EntryConferenceId: this.entryConferenceDetails.entryConferenceId, UserId: this.userId, RejectionRemark: this.rejectionRemark };
      this.auditObservationService.updateEntryConferenceStatus(obj).subscribe(response => {
        if (response > 0) {
          swal(this._msg.updateMsg);
          this.getAuditsForDropdown();
          this.entryConferenceDetails = null;
          this.RejectPopUp = false;
          this.rejectionRemark = '';
        }
      });
    }
  }

  ngOnInit() {
    this.getAuditsForDropdown();
  }

}
