import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VerifyEntryConferenceComponent } from './verify-entry-conference.component';

describe('VerifyEntryConferenceComponent', () => {
  let component: VerifyEntryConferenceComponent;
  let fixture: ComponentFixture<VerifyEntryConferenceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VerifyEntryConferenceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VerifyEntryConferenceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
