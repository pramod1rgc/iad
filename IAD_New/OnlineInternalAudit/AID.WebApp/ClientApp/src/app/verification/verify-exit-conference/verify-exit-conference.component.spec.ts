import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VerifyExitConferenceComponent } from './verify-exit-conference.component';

describe('VerifyExitConferenceComponent', () => {
  let component: VerifyExitConferenceComponent;
  let fixture: ComponentFixture<VerifyExitConferenceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VerifyExitConferenceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VerifyExitConferenceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
