import { Component, OnInit } from '@angular/core';
import { VerifyAuditConferenceService } from '../../services/verification/verifyAuditConference.service';
import { AuditObservationService } from '../../services/audit-observation/audit-observation.service';
import { CommonMsg } from '../../global/common-msg';
import swal from 'sweetalert2';

@Component({
  selector: 'app-verify-exit-conference',
  templateUrl: './verify-exit-conference.component.html',
  styleUrls: ['./verify-exit-conference.component.css']
})
export class VerifyExitConferenceComponent implements OnInit {

  auditsList: any[] = [];
  selectedAuditId: any;
  exitConferenceDetails: any;
  userId: number;
  RejectPopUp: boolean = false;
  rejectionRemark: string = '';

  constructor(private _service: VerifyAuditConferenceService, private auditObservationService: AuditObservationService, private _msg: CommonMsg) {
    this.userId = JSON.parse(localStorage.getItem("LoginData")).userId;
  }

  getAuditsForDropdown() {
    this._service.getAuditsOfConferenceForVerification('exit').subscribe(response => {
      this.auditsList = response;
    });
  }

  getExitConferenceDetails() {
    this.auditObservationService.getAuditExitConference(this.selectedAuditId).subscribe(response => {
      if (response && response.length > 0) {
        this.exitConferenceDetails = response[0];
      }
    });
  }

  updateStatus(statusId) {
    if (this.exitConferenceDetails) {
      let obj = { StatusId: statusId, ExitConferenceId: this.exitConferenceDetails.exitConferenceId, UserId: this.userId, RejectionRemark: this.rejectionRemark };
      this.auditObservationService.updateExitConferenceStatus(obj).subscribe(response => {
        if (response > 0) {
          swal(this._msg.updateMsg);
          this.getAuditsForDropdown();
          this.exitConferenceDetails = null;
          this.RejectPopUp = false;
          this.rejectionRemark = '';
        }
      });
    }
  }

  ngOnInit() {
    this.getAuditsForDropdown();
  }

}
