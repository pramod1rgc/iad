import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { VerificationModule } from './verification.module';
import { PendingTaskComponent } from './pending-task/pending-task.component';
import { VerifyAuditScheduleComponent } from './verify-audit-schedule/verify-audit-schedule.component';
import { VerifyAuditTeamComponent } from './verify-audit-team/verify-audit-team.component';
import { VerifyAnnualAuditPlanComponent } from './verify-annual-audit-plan/verify-annual-audit-plan.component';
import { VerifyQuarterlyPlanComponent } from './verify-quarterly-plan/verify-quarterly-plan.component';
import { VerifyAuditComplianceComponent } from './verify-audit-compliance/verify-audit-compliance.component';
import { VerifySettlementLetterComponent } from './verify-settlement-letter/verify-settlement-letter.component';
import { VerifyComplianceDetailComponent } from './verify-audit-compliance/verify-compliance-detail/verify-compliance-detail.component';
import { VerifyAuditParaComponent } from './verify-audit-para/verify-audit-para.component';
import { VerifyParaDetailComponent } from './verify-audit-para/verify-para-detail/verify-para-detail.component';
import { VerifyEntryConferenceComponent } from './verify-entry-conference/verify-entry-conference.component';
import { VerifyExitConferenceComponent } from './verify-exit-conference/verify-exit-conference.component';


const routes: Routes = [

  {
    path: '', component: VerificationModule, data: {
      breadcrumb: 'Verification'
    }, children: [
      {
        path: 'pending-task', component: PendingTaskComponent, data: {
          breadcrumb: 'Pending Tasks'
        }
      },
      {
        path: 'verify-audit-schedule', component: VerifyAuditScheduleComponent, data: {
          breadcrumb: 'Verify Audit Schedules'
        }
      },
      {
        path: 'verify-audit-team', component: VerifyAuditTeamComponent, data: {
          breadcrumb: 'Verify Audit Team'
        }
      },
      {
        path: 'verify-annual-audit-plan', component: VerifyAnnualAuditPlanComponent, data: {
          breadcrumb: 'Verify annual audit plan'
        }
      },
      {
        path: 'verify-quarterly-plan', component: VerifyQuarterlyPlanComponent, data: {
          breadcrumb: 'Verify quarterly plan'
        }
      },
      {
        path: 'verify-audit-compliance', component: VerifyAuditComplianceComponent, data: {
          breadcrumb: 'Verify Audit Complaince'
        }
      },
      {
        path: 'verify-compliance-detail/:paraId', component: VerifyComplianceDetailComponent, data: {
          breadcrumb: 'Verify Compliance Details'
        }
      },
      {
        path: 'verify-audit-para', component: VerifyAuditParaComponent, data: {
          breadcrumb: 'Verify Audit Para'
        }
      },
      {
        path: 'verify-para-detail/:paraId', component: VerifyParaDetailComponent, data: {
          breadcrumb: 'Verify Para Details'
        }
      },
      {
        path: 'verify-settlement-letter', component: VerifySettlementLetterComponent, data: {
          breadcrumb: 'Verify Settlement Letter'
        }
      },
      {
        path: 'verify-entry-conference', component: VerifyEntryConferenceComponent, data: {
          breadcrumb: 'Verify Entry Conference'
        }
      },
      {
        path: 'verify-exit-conference', component: VerifyExitConferenceComponent, data: {
          breadcrumb: 'Verify Exit Conference'
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VerificationRoutingModule { }
