import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VerifyAuditTeamComponent } from './verify-audit-team.component';

describe('VerifyAuditTeamComponent', () => {
  let component: VerifyAuditTeamComponent;
  let fixture: ComponentFixture<VerifyAuditTeamComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VerifyAuditTeamComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VerifyAuditTeamComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
