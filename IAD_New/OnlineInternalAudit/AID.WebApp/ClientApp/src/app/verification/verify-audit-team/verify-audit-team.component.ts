import { Component, OnInit } from '@angular/core';
import { VerifyAuditTeamService } from '../../services/verification/verifyAuditTeam.service';
import { CommonMsg } from '../../global/common-msg';
import swal from 'sweetalert2';
import { CommonService } from '../../services/common/common.service';

@Component({
  selector: 'app-verify-audit-team',
  templateUrl: './verify-audit-team.component.html',
  styleUrls: ['./verify-audit-team.component.css']
})
export class VerifyAuditTeamComponent implements OnInit {

  auditsList: any[] = [];
  selectedAuditId: number;
  empList: any[] = [];
  selectedAudit: any;
  userId: number;
  RejectEventPopup: boolean = false;
  approveEventPopup: boolean = false;
  rejectionRemark: string;
  roleUserMapId: number;
  supervisorsList: any[] = [];
  selectedSupervisorId: number = 0;
  showForwardToSupervisorPopup: boolean = false;

  constructor(private _service: VerifyAuditTeamService, private _msg: CommonMsg, private commonService: CommonService) {
    this.userId = JSON.parse(localStorage.getItem("LoginData")).userId;
    this.roleUserMapId = JSON.parse(localStorage.getItem("LoginData")).roleUserMapId;
  }

  getAuditsForDropdown() {
    this._service.getAuditDetailsForVerificationDropdown(this.roleUserMapId).subscribe(response => {
      this.auditsList = response;
    });
  }


  getAssignAuditTeam() {
    if (this.selectedAuditId) {
      this.selectedAudit = this.auditsList.filter(a => a.auditId == this.selectedAuditId)[0] as any;
      this._service.getTeamOfAuditForVerification(this.selectedAuditId).subscribe(response => {
        this.empList = response;
      });
    }
  }

  updateAuditTeamVerification(statusId) {
    if (this.selectedAuditId) {
      let obj = { StatusId: statusId, AuditId: this.selectedAuditId, UserId: this.userId, RejectionRemark: this.rejectionRemark, ForwardedToUserId: this.roleUserMapId, AuditDecisionStatusId: statusId == 12 ? 2 : 3 };
      this._service.updateTeamOfAuditForVerification(obj).subscribe(response => {
        if (response && response > 0) {
          swal(this._msg.updateMsg);
          this.getAuditsForDropdown();
          this.selectedAuditId = null;
          this.selectedAudit = null;
          this.empList = [];
          this.rejectionRemark = '';
          this.RejectEventPopup = false;
          this.approveEventPopup = false;
        }
      });
    }
  }

  getSupervisorsList() {
    this.commonService.bindSupervisorDropDown().subscribe(response => {
      this.supervisorsList = response;
    });
  }

  forwardToSupervisor(statusId: number) {
    if (this.selectedAuditId && this.selectedSupervisorId > 0) {
      let obj = { StatusId: statusId, AuditId: this.selectedAuditId, UserId: this.userId, ForwardedToUserId: this.selectedSupervisorId, AuditDecisionStatusId: 4 };
      this._service.updateTeamOfAuditForVerification(obj).subscribe(response => {
        if (response && response > 0) {
          swal(this._msg.forwardSupervisorMsg);
          this.getAuditsForDropdown();
          this.selectedAuditId = null;
          this.selectedAudit = null;
          this.empList = [];
          this.rejectionRemark = '';
          this.showForwardToSupervisorPopup = false;
          this.selectedSupervisorId = null;
        }
      });
    }
  }

  ngOnInit() {
    this.getAuditsForDropdown();
    this.getSupervisorsList();
  }

}
