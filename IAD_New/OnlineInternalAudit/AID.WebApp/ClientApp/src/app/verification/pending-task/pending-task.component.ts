import { Component, OnInit } from '@angular/core';
import { VerifyScheduledAuditService } from '../../services/verification/verifyScheduledAudit.service';

@Component({
  selector: 'app-pending-task',
  templateUrl: './pending-task.component.html',
  styleUrls: ['./pending-task.component.css']
})
export class PendingTaskComponent implements OnInit {

  pendingTaskCount: any;
  roleUserMapId: number;

  constructor(private _service: VerifyScheduledAuditService) {
    this.roleUserMapId = JSON.parse(localStorage.getItem("LoginData")).roleUserMapId;
  }

  getPendingTaskCount() {
    this._service.getPendingTaskCount(this.roleUserMapId).subscribe(response => {
      this.pendingTaskCount = response;
    });
  }

  ngOnInit() {
    this.getPendingTaskCount();
  }

}
