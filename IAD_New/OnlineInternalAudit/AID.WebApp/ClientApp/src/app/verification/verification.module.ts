import { NgModule, Component} from '@angular/core';

import { CommonModule } from '@angular/common';
import { NgxMatSelectSearchModule } from 'ngx-mat-select-search';
import { MaterialModule } from '../material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatDatepickerModule, MatTabsModule, DateAdapter, MatCheckboxModule } from '@angular/material';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { SharedModule } from '../shared-module/shared-module.module';
import { FlatpickrModule } from 'angularx-flatpickr';
import { CalendarModule } from 'angular-calendar';
import { adapterFactory } from 'angular-calendar/date-adapters/date-fns';
import { PendingTaskComponent } from './pending-task/pending-task.component';
import { VerifyAuditScheduleComponent } from './verify-audit-schedule/verify-audit-schedule.component';
import { VerificationRoutingModule } from './verification.routing';
import { CommonMsg } from '../global/common-msg';
import { VerifyAuditTeamComponent } from './verify-audit-team/verify-audit-team.component';
import { VerifyAnnualAuditPlanComponent } from './verify-annual-audit-plan/verify-annual-audit-plan.component';
import { VerifyQuarterlyPlanComponent } from './verify-quarterly-plan/verify-quarterly-plan.component';
import { VerifyAuditComplianceComponent } from './verify-audit-compliance/verify-audit-compliance.component';
import { VerifySettlementLetterComponent } from './verify-settlement-letter/verify-settlement-letter.component';
import { VerifyComplianceDetailComponent } from './verify-audit-compliance/verify-compliance-detail/verify-compliance-detail.component';
import { VerifyAuditParaComponent } from './verify-audit-para/verify-audit-para.component';
import { VerifyParaDetailComponent } from './verify-audit-para/verify-para-detail/verify-para-detail.component';
import { VerifyEntryConferenceComponent } from './verify-entry-conference/verify-entry-conference.component';
import { VerifyExitConferenceComponent } from './verify-exit-conference/verify-exit-conference.component';


@NgModule({
  declarations: [PendingTaskComponent, VerifyAuditScheduleComponent, VerifyAuditTeamComponent, VerifyAnnualAuditPlanComponent, VerifyQuarterlyPlanComponent, VerifyAuditComplianceComponent, VerifyComplianceDetailComponent, VerifySettlementLetterComponent, VerifyAuditParaComponent, VerifyParaDetailComponent, VerifyEntryConferenceComponent, VerifyExitConferenceComponent],
  imports: [
    CommonModule, VerificationRoutingModule, 
    NgxMatSelectSearchModule,
    MaterialModule, FormsModule, ReactiveFormsModule, MatDatepickerModule, DragDropModule, SharedModule, MatTabsModule,MatCheckboxModule,
    FlatpickrModule.forRoot(),
    CalendarModule.forRoot({
      provide: DateAdapter,
      useFactory: adapterFactory
    }),
  ],
  providers: [CommonMsg]
})
export class VerificationModule { }
