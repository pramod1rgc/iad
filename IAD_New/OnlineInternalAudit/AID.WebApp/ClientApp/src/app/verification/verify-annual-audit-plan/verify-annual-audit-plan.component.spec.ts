import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VerifyAnnualAuditPlanComponent } from './verify-annual-audit-plan.component';

describe('VerifyAnnualAuditPlanComponent', () => {
  let component: VerifyAnnualAuditPlanComponent;
  let fixture: ComponentFixture<VerifyAnnualAuditPlanComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VerifyAnnualAuditPlanComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VerifyAnnualAuditPlanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
