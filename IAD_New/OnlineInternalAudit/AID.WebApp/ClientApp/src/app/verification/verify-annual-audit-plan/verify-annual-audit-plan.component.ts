import { Component, OnInit, ViewChild } from '@angular/core';
//import { CreateannualplanService } from '../../services/auditstructure/createannualplan.service';
import { VerifyannualplanService } from '../../services/auditstructure/verifyannualplan.service';
import { Subject, ReplaySubject } from 'rxjs';
import { FormControl } from '@angular/forms';
import { takeUntil } from 'rxjs/operators';
import { CdkDragDrop, transferArrayItem, moveItemInArray } from '@angular/cdk/drag-drop';
import swal from 'sweetalert2';
import { Console } from '@angular/core/src/console';
import { MatTableDataSource, MatPaginator, MatSort } from '@angular/material';

import { CommonService } from '../../services/common/common.service';
export interface commondll {
  values: string;
  text: string;
}
export interface AuditData {
  paoName: string;
  paoCode: string;
  auditReason: string;
}
@Component({
  selector: 'app-verify-annual-audit-plan',
  templateUrl: './verify-annual-audit-plan.component.html',
  styleUrls: ['./verify-annual-audit-plan.component.css'],
  providers: [CommonService]
})


export class VerifyAnnualAuditPlanComponent implements OnInit {
  userId: any;
  constructor(private _VerifyannualplanService: VerifyannualplanService, private _CommonService: CommonService) {
    this.userId = JSON.parse(localStorage.getItem("LoginData")).roleUserMapId;
  }
  displayedColumns: string[] = ['paoName', 'paoCode', 'auditReason', 'status'];//'index','paoId', ['select', 'empName', 'desigination', 'oldBasic', 'newBasic', 'wefDate', 'nextIncDate', 'payLevel', 'remark'];//,
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  arrSupervisor: any;
  notFound = true;
  dataSource: any;
  ArrFyear: any;
  mstFyear: any = {};
  message: any;
  rejectreson: any;
  isTableHasData = true;
  private _onDestroy = new Subject<void>();
  //Financial Year
  public filteredYear: ReplaySubject<commondll[]> = new ReplaySubject<commondll[]>(1);
  public YearFilterCtrl: FormControl = new FormControl();
  //End Of Financial
  ngOnInit() {
   
    this.BindDropFYear();
    this.YearFilterCtrl.valueChanges.pipe(takeUntil(this._onDestroy)).subscribe(() => { this.filterYear(); });
  }
  BindDropFYear() {
    //alert('alert')
    this._VerifyannualplanService.BindDropFYear().subscribe(result => {
      // debugger;
      this.ArrFyear = result;
      this.mstFyear.FYear = 0;
      this.filteredYear.next(this.ArrFyear);
    })
  }
  // use for filter search in dropdawn
  private filterYear() {
    debugger;
    if (!this.filteredYear) {
      return;
    }
    // get the search keyword
    let search = this.YearFilterCtrl.value;
    if (!search) {

      this.filteredYear.next(this.ArrFyear.slice());
      return;
    }
    else {
      search = search.toLowerCase();
    }
    var a = this.ArrFyear;
    this.filteredYear.next(
      this.ArrFyear.filter(ArrFyear => ArrFyear.financialYear.toLowerCase().indexOf(search) > -1)
    );
  }
  //Filter Controller

  //Get All Audit By Finanacial Year
  getAllAudit(value) {
 
    this._VerifyannualplanService.getauditlist(value, this.userId).subscribe(response => {
      debugger;
      
      this.dataSource = new MatTableDataSource(response);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
      if (this.dataSource.filteredData.length > 0) { this.isTableHasData = false; }

      else { this.isTableHasData = true;}
      //Filetr inData source
      this.dataSource.filterPredicate =
        (data: AuditData, filtersJson: string) => {
          const matchFilter = [];
          const filters = JSON.parse(filtersJson);

          filters.forEach(filter => {
            const val = data[filter.id] === null ? '' : data[filter.id];
            matchFilter.push(val.toLowerCase().includes(filter.value.toLowerCase()));
          });
          return matchFilter.every(Boolean);
        };
      //END Of Filetr inData source
    });
  }
  applyFilter(filterValue: string) {
    const tableFilters = [];
    tableFilters.push({
      id: 'paoName',
      value: filterValue
    });


    this.dataSource.filter = JSON.stringify(tableFilters);
    //this.menudataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
    if (this.dataSource.filteredData.length > 0) {
      this.notFound = true;
    } else {
      this.notFound = false;
    }
  }


  anualplanApprove() {
    debugger;
    if (this.mstFyear.financialYearId == undefined) {
      alert('please select financial year')
      return false;
    }
   // alert(this.userId);
    this._VerifyannualplanService.anualplanApprove(this.mstFyear.financialYearId, this.userId).subscribe(result => {
     
      this.message = result;
      this.getAllAudit(this.mstFyear.financialYearId);
      swal(this.message);
    });
  }


  rejectanualsAudit() {
    if (this.mstFyear.financialYearId == undefined) {
      alert('please select financial year')
      return false;
    }
    this._VerifyannualplanService.rejectanualsAudit(this.mstFyear.financialYearId, this.rejectreson, this.userId).subscribe(result => {
      
      this.message = result;
      this.getAllAudit(this.mstFyear.financialYearId);
      swal(this.message);
    });
  }
  resettextarea() {
    this.rejectreson = '';
  }
  bindSupervisorDropDown() {
    //alert('alert')
    this._CommonService.bindSupervisorDropDown().subscribe(result => {
      // debugger;
      this.arrSupervisor = result;
     
    })
  }
  supervisor: any;
  supervisorTosupervisor() {
    //alert('supervisorTosupervisor')
    this._VerifyannualplanService.supervisorTosupervisor(this.mstFyear.financialYearId, this.supervisor, this.userId).subscribe(result => {
      this.message = result
      swal(this.message);
      this.getAllAudit(this.mstFyear.financialYearId);
      this.bindSupervisorDropDown();
    });
  }
  Cancel() {
    this.supervisor = '';
    this.bindSupervisorDropDown();
  }
}
