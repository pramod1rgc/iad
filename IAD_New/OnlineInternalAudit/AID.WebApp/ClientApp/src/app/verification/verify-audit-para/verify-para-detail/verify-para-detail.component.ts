import { Component, OnInit, ViewChild } from '@angular/core';
import { ScrollToBottomDirective } from '../../../directives/scroll-to-bottom.directive';
import { ActivatedRoute } from '@angular/router';
import { AuditObservationService } from '../../../services/audit-observation/audit-observation.service';
import { CommonMsg } from '../../../global/common-msg';
import swal from 'sweetalert2';

@Component({
  selector: 'app-verify-para-detail',
  templateUrl: './verify-para-detail.component.html',
  styleUrls: ['./verify-para-detail.component.css']
})
export class VerifyParaDetailComponent implements OnInit {

  paraId: number;
  details: any;
  userId: number;
  roleId: number;
  selectedAudit: any;
  commentsList: any[] = [];
  comment: string;
  uploadFiles: any[] = [];
  rejectionRemark: string = '';
  RejectPopUp: boolean = false;

  @ViewChild(ScrollToBottomDirective) scroll: ScrollToBottomDirective;

  constructor(private route: ActivatedRoute, private _service: AuditObservationService, private _msg: CommonMsg) {
    this.route.params.subscribe(params => {
      if (params['paraId']) {
        this.paraId = params['paraId'];
        this.getParaDetails();
        this.getParaComments();
      }
    });
    this.userId = JSON.parse(localStorage.getItem("LoginData")).userId;
    this.roleId = JSON.parse(localStorage.getItem("LoginData")).roleId;
    this.selectedAudit = JSON.parse(localStorage.getItem("AuditDetailsForVerification"));
  }

  getParaDetails() {
    this._service.getAuditParaDetails(this.paraId).subscribe(response => {
      this.details = response;
    });
  }

  getParaComments() {
    this._service.getParaComments(this.paraId).subscribe(response => {
      this.commentsList = response;
    });
  }


  updateAuditParaStatus(statusId) {
    if (this.paraId) {
      let obj = { paraId: this.paraId, auditId: this.selectedAudit.auditId, statusId: statusId, rejectionRemark: this.rejectionRemark };
      this._service.updateAuditParaStatus(obj).subscribe(response => {
        if (response > 0) {
          swal(this._msg.updateMsg);
          this.rejectionRemark = '';
          this.RejectPopUp = false;
          this.getParaDetails();
          //this._location.back();
        }
      });
    }
  }

  ngOnInit() {
  }

}
