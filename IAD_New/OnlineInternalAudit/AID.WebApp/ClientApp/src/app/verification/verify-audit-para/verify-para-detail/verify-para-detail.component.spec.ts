import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VerifyParaDetailComponent } from './verify-para-detail.component';

describe('VerifyParaDetailComponent', () => {
  let component: VerifyParaDetailComponent;
  let fixture: ComponentFixture<VerifyParaDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VerifyParaDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VerifyParaDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
