import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource, MatPaginator } from '@angular/material';
import { VerifyAuditComplianceService } from '../../services/verification/verifyAuditCompliance.service';
import { AuditObservationService } from '../../services/audit-observation/audit-observation.service';

@Component({
  selector: 'app-verify-audit-para',
  templateUrl: './verify-audit-para.component.html',
  styleUrls: ['./verify-audit-para.component.css']
})
export class VerifyAuditParaComponent implements OnInit {

  userId: number;
  roleId: number;
  auditList: any[] = [];
  paraList: any[] = [];
  selectedAuditId: any;
  searchTerm: string = '';
  pageSize: number = 10;
  pageNumber: number = 1;
  totalCount: number = 0;

  displayedColumns: string[] = ['ParaTitle', 'ObservationType', 'RiskLevel', 'Status', 'Action'];

  dataSource: MatTableDataSource<any> = new MatTableDataSource<any>();

  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(private _service: VerifyAuditComplianceService, private auditObservationService: AuditObservationService) {
    this.userId = JSON.parse(localStorage.getItem("LoginData")).userId;
    this.roleId = JSON.parse(localStorage.getItem("LoginData")).roleId;
  }

  getAuditList() {
    this._service.getAuditsWhoseParasOrComplainceForwardedToSupervisor('para').subscribe(response => {
      this.auditList = response;
    });
  }

  getAuditDetails() {
    if (this.selectedAuditId) {
      localStorage.setItem("AuditDetailsForVerification", JSON.stringify(this.auditList.filter(a => a.auditId == this.selectedAuditId)[0] as any));
      this.auditObservationService.getAuditParas(this.selectedAuditId, this.pageSize, this.pageNumber, this.searchTerm).subscribe(response => {
        this.paraList = response;
        if (this.paraList.length > 0) {
          this.totalCount = this.paraList[0].totalCount;
          this.dataSource = new MatTableDataSource(this.paraList);
        }
      });
    }
  }

  applyFilter() {
    this.totalCount = 0;
    this.pageNumber = 1;
    this.paginator.firstPage();
    this.getAuditDetails();
  }

  getPaginationData(event) {
    this.pageSize = event.pageSize;
    this.pageNumber = event.pageIndex + 1;
    this.getAuditDetails();
  }

  ngOnInit() {
    this.getAuditList();
  }

}
