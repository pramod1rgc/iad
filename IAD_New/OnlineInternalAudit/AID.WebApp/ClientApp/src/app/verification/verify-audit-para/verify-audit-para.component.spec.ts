import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VerifyAuditParaComponent } from './verify-audit-para.component';

describe('VerifyAuditParaComponent', () => {
  let component: VerifyAuditParaComponent;
  let fixture: ComponentFixture<VerifyAuditParaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VerifyAuditParaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VerifyAuditParaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
