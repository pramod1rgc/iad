import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VerifySettlementLetterComponent } from './verify-settlement-letter.component';

describe('VerifySettlementLetterComponent', () => {
  let component: VerifySettlementLetterComponent;
  let fixture: ComponentFixture<VerifySettlementLetterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VerifySettlementLetterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VerifySettlementLetterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
