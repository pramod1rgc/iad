import { Component, OnInit } from '@angular/core';
import { VerifyAuditComplianceService } from '../../services/verification/verifyAuditCompliance.service';
import { AuditObservationService } from '../../services/audit-observation/audit-observation.service';
import swal from 'sweetalert2';
import { CommonMsg } from '../../global/common-msg';

@Component({
  selector: 'app-verify-settlement-letter',
  templateUrl: './verify-settlement-letter.component.html',
  styleUrls: ['./verify-settlement-letter.component.css']
})
export class VerifySettlementLetterComponent implements OnInit {

  userId: number;
  roleId: number;
  auditList: any[] = [];
  selectedAuditId: any;
  paraSettlementLetterDetails: any;
  RejectPopUp: boolean = false;
  rejectionRemark: string = '';

  constructor(private _service: VerifyAuditComplianceService, private auditObservationService: AuditObservationService, private _msg: CommonMsg) {
    this.userId = JSON.parse(localStorage.getItem("LoginData")).userId;
    this.roleId = JSON.parse(localStorage.getItem("LoginData")).roleId;
  }

  getAuditList() {
    this._service.getAuditsWhoseSettlementLetterForwardedToSupervisor().subscribe(response => {
      this.auditList = response;
    });
  }

  getAuditDetails() {
    if (this.selectedAuditId) {
      this.auditObservationService.getAuditParaSettlementLetter(this.selectedAuditId).subscribe(response => {
        this.paraSettlementLetterDetails = response;
      });
    }
  }

  forward(statusId) {
    this.auditObservationService.updateAuditParaSettlementLetterStatus(this.selectedAuditId, statusId, this.rejectionRemark).subscribe(response => {
      if (response > 0) {
        switch (statusId) {
          case 5:
            swal(this._msg.forwardAuditDHMsg);
            break;
        }
        this.getAuditDetails();
        this.rejectionRemark = '';
      }
      else {
        swal("Cannot forward until all paras are approved.");
      }
      this.RejectPopUp = false;
    })
  }

  ngOnInit() {
    this.getAuditList();
  }

}
