import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VerifyAuditScheduleComponent } from './verify-audit-schedule.component';

describe('VerifyAuditScheduleComponent', () => {
  let component: VerifyAuditScheduleComponent;
  let fixture: ComponentFixture<VerifyAuditScheduleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VerifyAuditScheduleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VerifyAuditScheduleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
