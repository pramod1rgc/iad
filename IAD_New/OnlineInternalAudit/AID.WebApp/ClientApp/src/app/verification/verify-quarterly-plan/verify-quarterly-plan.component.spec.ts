import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VerifyQuarterlyPlanComponent } from './verify-quarterly-plan.component';

describe('VerifyQuarterlyPlanComponent', () => {
  let component: VerifyQuarterlyPlanComponent;
  let fixture: ComponentFixture<VerifyQuarterlyPlanComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VerifyQuarterlyPlanComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VerifyQuarterlyPlanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
