import { Component, OnInit, ViewChild } from '@angular/core';
//import { CreateannualplanService } from '../../services/auditstructure/createannualplan.service';
import { VerifyannualplanService } from '../../services/auditstructure/verifyannualplan.service';
import { Subject, ReplaySubject } from 'rxjs';
import { FormControl } from '@angular/forms';
import { takeUntil } from 'rxjs/operators';
import { CdkDragDrop, transferArrayItem, moveItemInArray } from '@angular/cdk/drag-drop';
import swal from 'sweetalert2';
import { Console } from '@angular/core/src/console';
import { MatTableDataSource, MatPaginator, MatSort } from '@angular/material';
import { CommonService } from '../../services/common/common.service';
import { CommonMsg } from '../../global/common-msg';
export interface commondll {
  values: string;
  text: string;
}
export interface AuditData {
  paoName: string;
  paoCode: string;
  auditReason: string;
}
@Component({
  selector: 'app-verify-quarterly-plan',
  templateUrl: './verify-quarterly-plan.component.html',
  styleUrls: ['./verify-quarterly-plan.component.css'],
  providers: [CommonService]
})
export class VerifyQuarterlyPlanComponent implements OnInit {
  userId: any;
  constructor(private _VerifyannualplanService: VerifyannualplanService, private _CommonService: CommonService, private _msg: CommonMsg) {
    this.userId = JSON.parse(localStorage.getItem("LoginData")).roleUserMapId;
  }
  displayedColumns: string[] = ['paoName', 'paoCode', 'quarter', 'auditReason', 'status'];//'index','paoId', ['select', 'empName', 'desigination', 'oldBasic', 'newBasic', 'wefDate', 'nextIncDate', 'payLevel', 'remark'];//,
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  arrSupervisor: any;
  supervisor: any;
  notFound = true;
  arrQuarter: any;
  dataSource: any;
  ArrFyear: any;
  isTableHasData = true;
  rejectreson: any;
  private _onDestroy = new Subject<void>();
  //Financial Year
  public filteredYear: ReplaySubject<commondll[]> = new ReplaySubject<commondll[]>(1);
  public YearFilterCtrl: FormControl = new FormControl();
  //End Of Financial
  finnacialpendigRecord = true;

  message: any;
  mstFyear: any = {};
  ngOnInit() {
    this.BindDropFYear();
    this.YearFilterCtrl.valueChanges.pipe(takeUntil(this._onDestroy)).subscribe(() => { this.filterYear(); });
  }

  BindDropFYear() {
    //alert('alert')
    this._VerifyannualplanService.PendingFinancialForVerifyQuarter(this.userId).subscribe(result => {
      // debugger;
      this.ArrFyear = result;
      this.mstFyear.FYear = 0;
      this.filteredYear.next(this.ArrFyear);
    })
  }


  // use for filter search in dropdawn
  private filterYear() {
    debugger;
    if (!this.filteredYear) {
      return;
    }
    // get the search keyword
    let search = this.YearFilterCtrl.value;
    if (!search) {

      this.filteredYear.next(this.ArrFyear.slice());
      return;
    }
    else {
      search = search.toLowerCase();
    }
    var a = this.ArrFyear;
    this.filteredYear.next(
      this.ArrFyear.filter(ArrFyear => ArrFyear.financialYear.toLowerCase().indexOf(search) > -1)
    );
  }
  //Filter Controller

  //Get All Audit By Finanacial Year
  getAllQuarterAudit() {

    //alert(this.mstFyear.financialYearId + '__' + this.mstFyear.selectedQuarter)
    this._VerifyannualplanService.Getauditlistforverify(this.mstFyear.financialYearId, this.mstFyear.selectedQuarter, this.userId).subscribe(response => {
      debugger;

      this.dataSource = new MatTableDataSource(response);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
      if (this.dataSource.filteredData.length > 0) {
        this.isTableHasData = false;
        this.notFound = true;

      }
      else {
        this.isTableHasData = true;
        this.notFound = false;
      }
      //Filetr inData source
      this.dataSource.filterPredicate =
        (data: AuditData, filtersJson: string) => {
          const matchFilter = [];
          const filters = JSON.parse(filtersJson);

          filters.forEach(filter => {
            const val = data[filter.id] === null ? '' : data[filter.id];
            matchFilter.push(val.toLowerCase().includes(filter.value.toLowerCase()));
          });
          return matchFilter.every(Boolean);
        };
      //END Of Filetr inData source
    });
  }
  applyFilter(filterValue: string) {
    const tableFilters = [];
    tableFilters.push({
      id: 'paoName',
      value: filterValue
    });


    this.dataSource.filter = JSON.stringify(tableFilters);
    //this.menudataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
    if (this.dataSource.filteredData.length > 0) {
      this.notFound = true;
    } else {
      this.notFound = false;
    }
  }

  quarterlylplanApprove() {
    debugger;
    if (this.mstFyear.financialYearId == undefined) {
      alert('please select financial year')
      return false;
    }
    //alert(this.mstFyear.financialYearId);
    this._VerifyannualplanService.quarterlylplanApprove(this.mstFyear.financialYearId, this.mstFyear.selectedQuarter, this.userId).subscribe(result => {

      this.message = result;
      this.getAllQuarterAudit();
      swal(this.message);
    });
  }

  rejectQuarterlyAudit() {
    if (this.mstFyear.financialYearId == undefined) {
      alert('please select financial year')
      return false;
    }
    this._VerifyannualplanService.rejectQuarterlyAudit(this.mstFyear.financialYearId, this.rejectreson, this.mstFyear.selectedQuarter, this.userId).subscribe(result => {

      this.message = result;
      this.getAllQuarterAudit();
      swal(this.message);
    });
  }
  resettextarea() {
    this.rejectreson = '';
  }


  quarters: any[] = [];
  getAllPendingQuarter(FYID) {

    this._VerifyannualplanService.getAllPendingQuarter(FYID, this.userId).subscribe(result => {
      debugger;
      if (result.length== 0) {
        this.finnacialpendigRecord = false;
       
      }
      if (result.length != 0) {
        this.finnacialpendigRecord = true;
        this.arrQuarter = result;
        let allQuarters = this._msg.getquartersWithDate(this.ArrFyear.filter(a => a.financialYearId == FYID)[0].financialYear);
        for (var i = 0; i < allQuarters.length; i++) {
          console.log(result.filter(a => a.quarter == allQuarters[i].Id));
          if (result.filter(a => a.quarter == allQuarters[i].Id).length > 0) {
            this.quarters.push(allQuarters[i]);
          }
        }
      }
      this.dataSource = null;
    });
  }

  bindSupervisorDropDown() {
    //alert('alert')
    this._CommonService.bindSupervisorDropDown().subscribe(result => {
      // debugger;
      this.arrSupervisor = result;

    })
  }
  Cancel() {
    this.supervisor = '';
    this.bindSupervisorDropDown();
  }
  ForwardToSupervisorPopUp: boolean;
  supervisorTosupervisorQuarterlyPlan() {
    //alert('supervisorTosupervisor')
    this._VerifyannualplanService.supervisorTosupervisorQuarterlyPlan(this.mstFyear.financialYearId, this.supervisor, this.userId,this.mstFyear.selectedQuarter).subscribe(result => {
      this.message = result
      swal(this.message);
      this.ForwardToSupervisorPopUp = false;
      this.getAllQuarterAudit();
      this.bindSupervisorDropDown();
    });
  }
}
