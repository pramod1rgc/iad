import { Component, OnInit } from '@angular/core';
import { CalendarEvent } from 'calendar-utils';
import { CalendarView, CalendarMonthViewDay } from 'angular-calendar';
import swal from 'sweetalert2';
import { isSameDay, isSameMonth } from 'date-fns/esm';
import { CommonMsg } from '../../global/common-msg';
import { Subject } from 'rxjs';
import { AuditsService } from '../../services/audits/audits.services';
import { VerifyScheduledAuditService } from '../../services/verification/verifyScheduledAudit.service';

@Component({
  selector: 'app-audit-schedule',
  templateUrl: './audit-schedule.component.html',
  styleUrls: ['./audit-schedule.component.css']
})
export class AuditScheduleComponent implements OnInit {

  financialYears: any[] = [];

  allPaoList: any[] = [];

  selectedFinYearId: number;
  selectedQuarter: string;
  userId: number;

  RejectEventPopup: boolean = false;

  selectedPAO: CalendarEvent[] = [];

  selectAllCheckBox: boolean = false;

  rejectionRemark: string = '';

  roleUserMapId: number;

  quarters: any[] = [];

  //////---------------------------- Calendar Begin ------------------------------------////////

  view: CalendarView = CalendarView.Month;

  CalendarView = CalendarView;

  viewDate: Date = new Date();

  refresh: Subject<any> = new Subject();

  events: CalendarEvent[] = [];

  activeDayIsOpen: boolean = false;

  dayClicked({ date, events }: { date: Date; events: CalendarEvent[] }): void {
    if (isSameMonth(date, this.viewDate)) {
      if (
        (isSameDay(this.viewDate, date) && this.activeDayIsOpen === true) ||
        events.length === 0
      ) {
        this.activeDayIsOpen = false;
      } else {
        this.activeDayIsOpen = true;
      }
      this.viewDate = date;
    }
    this.selectEvent(events);
  }

  beforeMonthViewRender({ body }: { body: CalendarMonthViewDay[] }): void {
    body.forEach(day => {
      day.badgeTotal = day.events.filter(
        event => event.meta.incrementsBadgeTotal
      ).length;
    });
  }

  closeOpenMonthViewDay() {
    this.activeDayIsOpen = false;
  }

  //////---------------------------- Calendar End ------------------------------------////////

  constructor(private _service: AuditsService, private _msg: CommonMsg, private verifyAuditService: VerifyScheduledAuditService) {
    this.userId = JSON.parse(localStorage.getItem("LoginData")).userId;
    this.roleUserMapId = JSON.parse(localStorage.getItem("LoginData")).roleUserMapId;
  }

  getQuartersList() {
    this.quarters = [];
    this.verifyAuditService.getDistinctQuartersBasedOnStatusAndFY(this.selectedFinYearId, '5', 0).subscribe(response => {
      if (response && response.length > 0) {
        let allQuarters = this._msg.getquartersWithDate(this.financialYears.filter(a => a.financialYearId == this.selectedFinYearId)[0].financialYear);
        for (var i = 0; i < allQuarters.length; i++) {
          if (response.filter(a => a.quarter == allQuarters[i].Id).length > 0) {
            this.quarters.push(allQuarters[i]);
          }
        }
      }
    });
  }


  getApprovedScheduledAudits() {
    this.selectAllCheckBox = false;
    this.rejectionRemark = '';
    if (this.selectedFinYearId && this.selectedQuarter) {
      this._service.getApprovedScheduledAudits(this.selectedFinYearId, this.selectedQuarter, this.roleUserMapId).subscribe(response => {
        this.allPaoList = response;
        this.events = [];
        for (var i = 0; i < this.allPaoList.length; i++) {
          let clr = this._msg.getRandomColor();
          let colr = {
            evntColor: {
              primary: clr,
              secondary: clr
            }
          }
          this.allPaoList[i].selected = false;
          let evnt: CalendarEvent = {
            title: this.allPaoList[i].paoName,
            color: colr.evntColor,
            start: new Date(this.allPaoList[i].auditFromDate),
            end: new Date(this.allPaoList[i].auditToDate),
            meta: this.allPaoList[i],
            draggable: false
          }
          evnt.meta.incrementsBadgeTotal = false;
          this.events.push(evnt);
        }
        let selectedQuarterDetails = this.quarters.filter(a => a.Id == this.selectedQuarter)[0] as any;
        this.viewDate = new Date(selectedQuarterDetails.StartDate);
        this.refresh.next();
      });
    }
  }

  getFinancialYears() {
    this._service.BindDropFYear().subscribe(response => {
      this.financialYears = response;
    });
  }

  saveStatus(statusId: number) {
    let requestEvents: any[] = [];
    for (var i = 0; i < this.events.length; i++) {
      if (this.events[i].meta.selected) {
        let evnt = {
          Id: i + 1,
          AuditId: this.events[i].meta.auditId,
          AuditFromDate: this.events[i].start,
          AuditToDate: this.events[i].end
        }
        requestEvents.push(evnt);
      }
    }
    requestEvents.forEach(a => a.Id = requestEvents.indexOf(a) + 1);
    if (requestEvents.length > 0) {
      let obj = { AuditSchedulesType: requestEvents, UserId: this.userId, StatusId: statusId, RejectionRemark: this.rejectionRemark, ForwardedToUserId: this.roleUserMapId, AuditDecisionStatusId: 3 };
      this._service.updateScheduledAuditForVerification(obj).subscribe(response => {
        if (response > 0) {
          swal(this._msg.saveMsg);
          this.getApprovedScheduledAudits();
        }
        else {
          swal(this._msg.errorMsg);
        }
        this.RejectEventPopup = false;
      });
    }
    else {
      swal("Select atleast one schedule!!!!");
      this.RejectEventPopup = false;
    }
  }

  selectEvent(paos: CalendarEvent[]) {
    this.selectedPAO = paos;
  }

  selectSchedule(pao: CalendarEvent) {
    this.viewDate = pao.start;
    this.selectedPAO = [];
    this.selectedPAO.push(pao);
  }

  checkIfSelectedPAOExists(pao: CalendarEvent): boolean {
    return this.selectedPAO.filter(a => a.meta.paoName == pao.meta.paoName).length > 0;
  }

  selectAll(evnt) {
    this.events.forEach(a => a.meta.selected = evnt.checked);
  }

  ngOnInit() {
    this.getFinancialYears();
  }

}
