import { Component, OnInit, ViewChild, } from '@angular/core';
import { SendintimationmailsService } from '../../services/audits/sendintimationmails.service';
import { RiskCategoryService } from '../../services/Master/risk-category.service';
import swal from 'sweetalert2';
import { MatTableDataSource, MatPaginator, MatSort, MatCheckboxModule} from '@angular/material';
import { ReplaySubject, Subject } from 'rxjs';
import { FormControl } from '@angular/forms';
import { takeUntil } from 'rxjs/operators';
import { AuditScheduleService } from '../../services/auditstructure/audit-schedule.service'
export interface commondll {
  values: string;
  text: string;
}
@Component({
  
  selector: 'app-sendintimationmails',
  templateUrl: './sendintimationmails.component.html',
  styleUrls: ['./sendintimationmails.component.css']
})
export class SendintimationmailsComponent implements OnInit {
  constructor(private inimatemailsService: SendintimationmailsService, private auditScheduleService: AuditScheduleService) { }
  displayedColumns: string[] = [ 'paoName', 'email', 'auditFromDate', 'auditToDate', 'intimationStatus', 'view','action'];//, 'Active'];//, 'leaveTypeDesc', 'action',];
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild('frmriskDetails') frmriskDetails: any;
  Arrlist: any;
  requestmodel: any={};
  ArrCategoryDetailDataSource: any;
  ArrCategoryDetails: any;
  isActive: boolean;
  pAOID: number;
  mstFyear: any = {};
  email: string;
  ArrFyear: any;
  isForwardToSupervisorbtn: boolean;
  remarks: string;
  intimationStatus: boolean;
  ActivateCategory: boolean = false;
  checkCategoryIsactive: boolean;
  ActiveDactiveHeaderText: string;
  fYearid: string;
  reportUrl: string = '';
  selectedFinYearId: string;
  checkbxotheremail: boolean;
  otheremail: string;
  otheremailstatus: boolean = false;

  
  public filteredYear: ReplaySubject<commondll[]> = new ReplaySubject<commondll[]>(1);
  public YearFilterCtrl: FormControl = new FormControl();
  ngOnInit() {
    //this.GetAllPAoAuditRecord();
    this.BindDropFYear();
  }
  otherEmail(value: boolean) {
    if (value===false) {
      this.checkbxotheremail = true;
      this.otheremailstatus = true;
    }
    if (value=== true) {
      this.checkbxotheremail = false;
      this.otheremailstatus = false;
    }
  }

  BindDropFYear() {
    debugger;
    //alert('alert')
    this.auditScheduleService.BindDropFYear().subscribe(result => {
      // debugger;
      this.ArrFyear = result;
      this.mstFyear.FYear = 0;
      this.filteredYear.next(this.ArrFyear);
    })
  }
  private filterYear() {
    debugger;
    if (!this.filteredYear) {
      return;
    }
    // get the search keyword
    let search = this.YearFilterCtrl.value;
    if (!search) {

      this.filteredYear.next(this.ArrFyear.slice());
      return;
    }
    else {
      search = search.toLowerCase();
    }
    var a = this.ArrFyear;
    this.filteredYear.next(
      this.ArrFyear.filter(ArrFyear => ArrFyear.financialYear.toLowerCase().indexOf(search) > -1)
    );
  }
  GetAllPAoAuditRecord() {
    debugger;

    this.inimatemailsService.GetAllPAoAuditRecord(this.fYearid).subscribe(result => {
      this.ArrCategoryDetails = result;
      this.Arrlist = new MatTableDataSource(this.ArrCategoryDetails);
      this.Arrlist.paginator = this.paginator;
      this.Arrlist.sort = this.sort;

      //Filetr inData source
      //this.ArrCategoryDetailDataSource.filterPredicate =
      //  (data: EmployeeData, filtersJson: string) => {
      //    const matchFilter = [];
      //    const filters = JSON.parse(filtersJson);
      //    filters.forEach(filter => {
      //      const val = data[filter.id] === null ? '' : data[filter.id];
      //      matchFilter.push(val.toLowerCase().includes(filter.value.toLowerCase()));
      //    });
      //    return matchFilter.every(Boolean);
      //  };
      //END Of Filetr inData source
    });
  }

  printReport(paoId: number) {
    this.reportUrl = 'IntimationReport&rs:Embed=true&rc:Parameters=false&ID=' + this.fYearid + '&PAOId=' + paoId;
    sessionStorage.setItem('reportUrl', this.reportUrl);
    window.open('/report', "_blank");
  }

  IntimateMails(pAOID, email) {
    this.pAOID = pAOID;
    this.email = email;
    this.ActivateCategory = true;
    this.checkCategoryIsactive = true//true;
    this.requestmodel.otheremailstatus = false;
    this.checkbxotheremail = false;

    this.checkbxotheremail = false;
    this.otheremailstatus = false;
 
   
  }

  applyFilter(filterValue: string) {
    this.Arrlist.filter = filterValue.trim().toLowerCase();
    if (this.Arrlist.paginator) {
      this.Arrlist.paginator.firstPage();
    }
  }


  //applyFilter(filterValue: string) {

  //  const Arrlist = [];
  //  Arrlist.push({
  //    id: 'paoId',
  //    value: filterValue
  //  });
  //}

  //  this.ArrCategoryDetailDataSource.filter = JSON.stringify(tableFilters);
  //  //this.menudataSource.filter = filterValue.trim().toLowerCase();

  //  if (this.ArrCategoryDetailDataSource.paginator) {
  //    this.ArrCategoryDetailDataSource.paginator.firstPage();
  //  }
  //  if (this.ArrCategoryDetailDataSource.filteredData.length > 0) {
  //    this.notFound = true;
  //  } else {
  //    this.notFound = false;
  //  }
  //}
  CancelPopUp() {
    this.ActivateCategory = false;
    this.GetAllPAoAuditRecord();
    this.frmriskDetails.resetForm();
  }

  SendMail() {
    debugger;
    if (this.checkbxotheremail === true) {
      this.requestmodel.email = this.requestmodel.otheremail;
    }
    else {
      this.requestmodel.email = this.email;
    }
    this.requestmodel.pAOID = this.pAOID;
    this.requestmodel.checkbxotheremail = this.checkbxotheremail;    
    this.inimatemailsService.SendMail(this.requestmodel).subscribe(result => {
      if (result=="OnSuccess"){
        this.ActivateCategory = false;
        this.GetAllPAoAuditRecord();
        this.frmriskDetails.resetForm();
        swal('Email Send Successfully');
      
      } 
      if (result == "failed") {
        this.ActivateCategory = false;
        this.frmriskDetails.resetForm();
        swal('Failed to send email');
      }
    })
  }
}
