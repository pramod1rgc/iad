import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SendintimationmailsComponent } from './sendintimationmails.component';

describe('SendintimationmailsComponent', () => {
  let component: SendintimationmailsComponent;
  let fixture: ComponentFixture<SendintimationmailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SendintimationmailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SendintimationmailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
