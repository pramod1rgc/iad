import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuditsModule } from './audits.module';
import { AuditScheduleComponent } from './audit-schedule/audit-schedule.component';
import { SendintimationmailsComponent } from './sendintimationmails/sendintimationmails.component';
import { AuditTeamComponent } from './audit-team/audit-team.component';

const routes: Routes = [

  {
    path: '', component: AuditsModule, data: {
      breadcrumb: 'Audits'
    }, children: [
      {
        path: 'audit-schedule', component: AuditScheduleComponent, data: {
          breadcrumb: 'Audit Schedule'
        }
      },
      {
        path: 'SendMails', component: SendintimationmailsComponent, data: {
          breadcrumb: 'Send Intimation Mails'
        }
      },
      {
        path: 'audit-team', component: AuditTeamComponent, data: {
          breadcrumb: 'Audit Team'
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AuditsRoutingModule { }
