import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AuditTeamComponent } from './audit-team.component';

describe('AuditTeamComponent', () => {
  let component: AuditTeamComponent;
  let fixture: ComponentFixture<AuditTeamComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AuditTeamComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AuditTeamComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
