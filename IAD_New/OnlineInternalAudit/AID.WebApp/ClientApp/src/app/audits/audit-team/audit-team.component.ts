import { Component, OnInit } from '@angular/core';
import { AuditsService } from '../../services/audits/audits.services';
import { CommonMsg } from '../../global/common-msg';
import swal from 'sweetalert2';

@Component({
  selector: 'app-audit-team',
  templateUrl: './audit-team.component.html',
  styleUrls: ['./audit-team.component.css']
})
export class AuditTeamComponent implements OnInit {

  auditsList: any[] = [];
  selectedAuditId: number;
  empList: any[] = [];
  selectedAudit: any;
  userId: number;
  RejectEventPopup: boolean = false;
  approveEventPopup: boolean = false;
  rejectionRemark: string;
  roleUserMapId: number;

  constructor(private _service: AuditsService, private _msg: CommonMsg) {
    this.userId = JSON.parse(localStorage.getItem("LoginData")).userId;
    this.roleUserMapId = JSON.parse(localStorage.getItem("LoginData")).roleUserMapId;
  }

  getAuditsForDropdown() {
    this._service.getApprovedAuditTeamAuditsForDropdown(this.roleUserMapId).subscribe(response => {
      this.auditsList = response;
    });
  }


  getAssignAuditTeam() {
    if (this.selectedAuditId) {
      this.selectedAudit = this.auditsList.filter(a => a.auditId == this.selectedAuditId)[0] as any;
      this._service.getApprovedAuditTeam(this.selectedAuditId).subscribe(response => {
        this.empList = response;
      });
    }
  }

  updateAuditTeamVerification(statusId) {
    if (this.selectedAuditId) {
      let obj = { StatusId: statusId, AuditId: this.selectedAuditId, UserId: this.userId, RejectionRemark: this.rejectionRemark, ForwardedToUserId: this.roleUserMapId, AuditDecisionStatusId: 3 };
      this._service.revertAuditTeam(obj).subscribe(response => {
        if (response && response > 0) {
          swal(this._msg.updateMsg);
          this.getAuditsForDropdown();
          this.selectedAuditId = null;
          this.selectedAudit = null;
          this.empList = [];
          this.rejectionRemark = '';
          this.RejectEventPopup = false;
          this.approveEventPopup = false;
        }
      });
    }
  }

  ngOnInit() {
    this.getAuditsForDropdown();
  }

}
