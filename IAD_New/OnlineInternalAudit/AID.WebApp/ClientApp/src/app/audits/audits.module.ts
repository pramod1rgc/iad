import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuditsRoutingModule } from './audits.routing';
import { NgxMatSelectSearchModule } from 'ngx-mat-select-search';
import { MaterialModule } from '../material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatDatepickerModule, MatTabsModule, MatCheckboxModule, DateAdapter } from '@angular/material';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { SharedModule } from '../shared-module/shared-module.module';
import { FlatpickrModule } from 'angularx-flatpickr';
import { CalendarModule } from 'angular-calendar';
import { adapterFactory } from 'angular-calendar/date-adapters/date-fns';
import { CommonMsg } from '../global/common-msg';
import { AuditScheduleComponent } from './audit-schedule/audit-schedule.component';
import { SendintimationmailsComponent } from './sendintimationmails/sendintimationmails.component';
import { AuditTeamComponent } from './audit-team/audit-team.component';

@NgModule({
  declarations: [AuditScheduleComponent, SendintimationmailsComponent, AuditTeamComponent],
  imports: [
    CommonModule, AuditsRoutingModule,
    NgxMatSelectSearchModule,
    MaterialModule, FormsModule, ReactiveFormsModule, MatDatepickerModule, DragDropModule, SharedModule, MatTabsModule, MatCheckboxModule,
    FlatpickrModule.forRoot(),
    CalendarModule.forRoot({
      provide: DateAdapter,
      useFactory: adapterFactory
    }),
  ],
  providers: [CommonMsg]
})
export class AuditsModule { }
