import { NgModule, CUSTOM_ELEMENTS_SCHEMA, } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DashboardRoutingModule } from './dashboard-routing.module';
import { ChartComponent } from './chart/chart.component';
import { HeaderComponent } from './header/header.component';
import {MaterialModule} from '../material.module';
import { HomeComponent } from './home/home.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { NavMenuComponent } from './nav-menu/nav-menu.component';
import { ChartsModule } from 'ng2-charts/ng2-charts';
import { NavService } from '../services/nav.service';
import { PagenotfoundComponent } from './pagenotfound/pagenotfound.component';
import { CalendarModule, DateAdapter } from 'angular-calendar';
import { adapterFactory } from 'angular-calendar/date-adapters/date-fns';
import { FlatpickrModule } from 'angularx-flatpickr';
import { SharedModule } from '../shared-module/shared-module.module';
import { CommonMsg } from '../global/common-msg';
//import { ReportViewerModule } from 'ngx-ssrs-reportviewer';
import { MatTooltipModule } from '@angular/material/tooltip';
 

@NgModule({
  declarations: [ChartComponent, HeaderComponent, HomeComponent, DashboardComponent, NavMenuComponent, PagenotfoundComponent],
  imports: [
    CommonModule,
    DashboardRoutingModule,
    MaterialModule,
    SharedModule,
    ChartsModule,
    MatTooltipModule,
    //ReportViewerModule,
    FlatpickrModule.forRoot(),
    CalendarModule.forRoot({
      provide: DateAdapter,
      useFactory: adapterFactory
    }),
  ],
  providers: [NavService, CommonMsg],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class DashboardModule { }
