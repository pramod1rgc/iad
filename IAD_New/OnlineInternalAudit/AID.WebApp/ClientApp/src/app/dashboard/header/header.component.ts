import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router"

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  constructor(private router: Router) { }
  username: any;
  userRole: any;
  userRoleId: number;
  deptId: string;
  loginResponse: any;
  totalRoles: any[] = [];

  ngOnInit() {
    this.deptId = JSON.parse(localStorage.getItem('DeptId')).deptId;
    this.userRole = JSON.parse(localStorage.getItem('LoginData')).roleName;
    this.userRoleId = JSON.parse(localStorage.getItem('LoginData')).roleId;
    this.loginResponse = JSON.parse(localStorage.getItem("LoginResponse"));
    if (this.loginResponse.result.length > 1) {
      this.totalRoles = this.loginResponse.result.filter(a => a.roleId != this.userRoleId);
    }
  }

  switchRole(selectedRoleUserMapId) {
    sessionStorage.clear();
    localStorage.clear();
    let selectedRoleData = this.loginResponse.result.filter(a => a.roleUserMapId == selectedRoleUserMapId)[0] as any;
    sessionStorage.setItem('userId', selectedRoleData.userId);
    sessionStorage.setItem('token', JSON.stringify(this.loginResponse.token));
    sessionStorage.setItem('roleId', selectedRoleData.roleId);
    localStorage.setItem("LoginData", JSON.stringify(selectedRoleData));
    localStorage.setItem("LoginResponse", JSON.stringify(this.loginResponse));
    localStorage.setItem("DeptId", JSON.stringify({ "deptId": this.deptId }));
    localStorage.setItem('breadcrumbsData', JSON.stringify([]));
    //this.router.navigate(['dashboard/home']);
    location.href = 'dashboard/home';
    //location.reload();
  }

  LogOut() {
    sessionStorage.clear();
    localStorage.clear();
    //alert('LogOut');
    this.router.navigate(['/login'])
  }
}
