import { Component, OnInit, ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core';
import { DashboardService } from '../../services/dashboard/dashboard.service'
import { Console } from '@angular/core/src/console';



@Component({
  selector: 'app-chart',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './chart.component.html',
  styleUrls: ['./chart.component.css']
})
export class ChartComponent implements OnInit {
  constructor(private cdref: ChangeDetectorRef, private _DashboardService: DashboardService) {
    //ref.detach();

  }
  public barChartOptions: any = {
    scaleShowVerticalLines: false,
    responsive: true
  };
  public barChartLabels: string[] = [];
  public arrChartLabels: string[] = [];

  //public barChartLabels:string[] = ['2006', '2007', '2008', '2009', '2010', '2011', '2012'];
  public barChartType: string = 'bar';
  public barChartLegend: boolean = true;
  arrChartdata: any;
  arrcurrentYear: any;
  arrtotalAlocated: any;

  ngOnInit() {
    if (parseInt(sessionStorage.getItem('roleId')) == 2) {
      //alert('chart');
      this.GetallAuditInchart();
    }
  }

  public barChartData: any[] = [];
  //  [{data: [65, 59, 80, 81, 56, 55, 40], label: 'Series A'},
  //  { data: [28, 48, 40, 19, 86, 27, 90], label: 'Series B' },
  //  { data: [2, 10, 25, 1, 16, 13, 5], label: 'Series C' }
  //];

  // events
  public chartClicked(e: any): void {
    console.log(e);
  }

  public chartHovered(e: any): void {
    console.log(e);
  }

  public randomize(): void {
    // Only Change 3 values
    let data = [
      Math.round(Math.random() * 100),
      59,
      80,
      (Math.random() * 100),
      56,
      (Math.random() * 100),
      40];
    let clone = JSON.parse(JSON.stringify(this.barChartData));
    clone[0].data = data;
    this.barChartData = clone;
    /**
     * (My guess), for Angular to recognize the change in the dataset
     * it has to change the dataset variable directly,
     * so one way around it, is to clone the data, change it and then
     * assign it;
     */
  }

  GetallAuditInchart() {
    this._DashboardService.GetallAuditInchart().subscribe(result => {
      debugger;
      //var arrToConvert = [[0, 0, 1], [2, 3, 3], [4, 4, 5]];
      //var newArr = [];


      //for (var i = 0; i < arrToConvert.length; i++) {
      //  newArr = newArr.concat(arrToConvert[i]);
      //}

      //console.log(newArr);
      //this.arrcurrentYear = result[0];
      this.arrChartdata = result[0];
      //this.arrtotalAlocated = result[2];
      //for (var i = 0; i < this.arrcurrentYear.length; i++) {

      //  this.arrChartLabels.push(this.arrcurrentYear[i].financialYear);

      //}



      for (var j = 0; j < this.arrChartdata.length; j++) {
        //  debugger;
        this.barChartData.push(
          { data: [this.arrChartdata[j].totalAudit, 0], label: this.arrChartdata[j].totalPara }, // {label: this.arrChartdata[j].financialYear}
          //{ data: [this.arrChartdata[j].totalAudit, this.arrChartdata[j].totalAudit, 0], label: this.arrChartdata[j].totalPara }
          //,
          //{ data: [28, 48, 40, 19, 86, 27, 90], label: 'Series B' },
          //{ data: [2, 10, 25, 1, 16, 13, 5], label: 'Series C' }
        )
        //this.barChartLabels.push(0);
        this.barChartLabels = ['2019-20'];
      }
      console.log(this.barChartData)
      //for (var k = 0; k < this.arrtotalAlocated.length; k++) {
      //  debugger;
      //  this.barChartData.push(
      //    { data: [this.arrtotalAlocated[k].totalAlocated], label: this.arrChartdata[k].financialYear }
      //    //,
      //    //{ data: [28, 48, 40, 19, 86, 27, 90], label: 'Series B' },
      //    //{ data: [2, 10, 25, 1, 16, 13, 5], label: 'Series C' }
      //  )

      //}


      //,'Total Alocated'];//this.arrChartLabels;

      this.cdref.detectChanges();
      //console.log(this.barChartData);
    })
  }
    
}


