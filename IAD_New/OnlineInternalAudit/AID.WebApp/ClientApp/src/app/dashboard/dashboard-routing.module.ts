import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ChartComponent } from './chart/chart.component';
import { HomeComponent } from './home/home.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { PagenotfoundComponent } from './pagenotfound/pagenotfound.component';
const routes: Routes = [
  {
    path: '', component: DashboardComponent, children: [
      {
        path: 'home', component: HomeComponent, data: {
          breadcrumb: 'Home'
        } },
      { path: 'chart', component: ChartComponent },
      {
        path: 'usermanagement', loadChildren: '../usermanagement/usermanagement.module#UsermanagementModule'//'../role-managment/role-managment.module#RoleManagmentModule'
      },
      {
        path: 'assignuser', loadChildren: '../assign-user/assign-user.module#AssignUserModule'
      },
      {
        path: 'establishment', loadChildren: '../establishment/establishment.module#EstablishmentModule'//'../role-managment/role-managment.module#RoleManagmentModule'
      },
      {
        path: 'master', loadChildren: '../master/master.module#MasterModule'//'../role-managment/role-managment.module#RoleManagmentModule'
      },
      {
        path: 'auditstructure', loadChildren: '../audit-structure/audit-structure.module#AuditStructureModule'
      },
      {
        path: 'verification', loadChildren: '../verification/verification.module#VerificationModule'
      },
      {
        path: 'audits', loadChildren: '../audits/audits.module#AuditsModule'
      },
      {
        path: 'audit-programme', loadChildren: '../audit-programme/audit-programme.module#AuditProgrammeModule'
      },
      {
        path: 'auditee', loadChildren: '../auditee/auditee.module#AuditeeModule'
      },
      {
        path: 'audit-observation', loadChildren: '../audit-observation/audit-observation.module#AuditObservationModule'
      },
      {
        path: 'auditee-observation', loadChildren: '../auditee-observation/auditee-observation.module#AuditeeObservationModule'
      },
      {
        path: 'audit-report', loadChildren: '../audit-report/audit-report.module#AuditReportModule'
      },
      { path: 'page', component: PagenotfoundComponent },
      { path: '**', redirectTo: 'page', pathMatch: 'full' },

    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class DashboardRoutingModule { }
