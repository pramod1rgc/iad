import { Component, ViewEncapsulation } from '@angular/core';
import { NavService } from '../../services/nav.service';
import { NavItem, IBreadcrumb } from '../../model/nav-item';
import { VERSION } from '@angular/material';
import { DashboardService } from '../../services/dashboard/dashboard.service';
import { Router, ActivatedRoute, NavigationEnd, PRIMARY_OUTLET, ActivatedRouteSnapshot } from "@angular/router";
import "rxjs/add/operator/filter";
import { Location } from '@angular/common';
import { filter, distinctUntilChanged, map, subscribeOn } from 'rxjs/operators';
import { Title } from "@angular/platform-browser";


interface MyTreeNode {
  mainMenuName: string
  children?: MyTreeNode[]
}
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})

export class DashboardComponent {
  navItems: NavItem[];
  version = VERSION;
  public breadcrumbs: IBreadcrumb[];
  public breadcrumbsDtls: any = {};
  name: string;
  menu: Array<any> = [];
  breadcrumbList: Array<any> = [];
  roleId: number;
  screenHeading: string;
  constructor(public navService: NavService, private _DashboardService: DashboardService,
    private activatedRoute: ActivatedRoute,
    private router: Router, private titleService: Title
  ) {
    this.breadcrumbs = [];
    let breadcrumb: IBreadcrumb = {
      label: 'Home',
      url: '/dashboard/home'
    };
    let loginData = JSON.parse(localStorage.getItem("LoginData"));
    this.roleId = loginData.roleId;
    this.getAllMenus();
    //this.listenRouting();
    this.router.events.pipe(filter(event => event instanceof NavigationEnd)).subscribe(event => {
      //debugger
      let root: ActivatedRoute = this.activatedRoute.root;
      //this.breadcrumbs = this.getBreadcrumbs(root);
      this.getActivatedBreadcrumbs(event);
      localStorage.setItem('breadcrumbsData', JSON.stringify(this.breadcrumbs));
      //this.breadcrumbs = [breadcrumb, ...this.breadcrumbs];
      let lastBreadCrumbLabel = this.breadcrumbs[this.breadcrumbs.length - 1].label;
      this.titleService.setTitle(lastBreadCrumbLabel + " : IAD");
      this.screenHeading = lastBreadCrumbLabel;
    });


  }

  username: any;
  uRoleID: any;
  ngOnInit() {
    this.onNumberGenerated();

    //this.getAllMenus();
    // this.listenRouting();
    //this.username = sessionStorage.getItem('username');
    //alert(sessionStorage.getItem('username'));

  }
  public onNumberGenerated() {

    //alert(this.randomNumber.displayName);
    //this.randomNumber = randomNumber;
    //console.log(this.randomNumber);
    this.breadcrumbsDtls.displayName = sessionStorage.getItem('menudisplayName');
    this.breadcrumbsDtls.route = sessionStorage.getItem('menuroute');
    //this.breadcrumbsDtls.displayName = sessionStorage.getItem('menudisplayName');
    //this.breadcrumbsDtls.route = sessionStorage.getItem('menuroute');
    // alert(this.breadcrumbsDtls.route);
    // console.log(sessionStorage.getItem('menudisplayName'));
  }


  resetbreadcrumbsDtls() {
    sessionStorage.setItem('menudisplayName', "");
    sessionStorage.setItem('menuroute', "");
  }

  ArrAllMenus: any;
  getAllMenus() {

    this._DashboardService.getAllMenus(this.roleId).subscribe(result => {
      this.ArrAllMenus = result;
      this.navItems = this.ArrAllMenus;
      this.menu = this.navItems;
    })
  }

  navigateFromBreadcrumb(bread) {
    this.router.navigateByUrl(bread.url);
  }


  getActivatedBreadcrumbs(evnt) {
    this.breadcrumbs = [];
    this.breadcrumbs = JSON.parse(localStorage.getItem("breadcrumbsData"));
    if (!this.breadcrumbs || this.breadcrumbs.length == 0) {
      this.breadcrumbs = [];
      let breadcrumb: IBreadcrumb = {
        label: 'Home',
        url: '/dashboard/home'
      };
      this.breadcrumbs.push(breadcrumb);
    }
    let currentLabel = '';
    let lastChild = this.getLastChild(this.activatedRoute.root);
    if (lastChild) {
      currentLabel = lastChild.snapshot.data['breadcrumb'];
    }
    let currentBreadcrumb: IBreadcrumb = {
      label: currentLabel,
      url: evnt.urlAfterRedirects
    };

    let isBreadcrumbExists = this.breadcrumbs.filter(a => a.label == currentLabel);
    if (isBreadcrumbExists.length == 0) {
      this.breadcrumbs.push(currentBreadcrumb);
    }
    else {
      let indexOfBreadcrumb = this.breadcrumbs.indexOf(isBreadcrumbExists[0]);
      this.breadcrumbs.splice(indexOfBreadcrumb + 1, this.breadcrumbs.length);
    }

  }


  getLastChild(root: any): any {
    let child: any;
    let children = root.children;
    if (children.length > 0) {
      if (children[0].children.length > 0) {
        return this.getLastChild(children[0]);
      }
      else {
        child = children[0];
      }
    }
    return child;
  }

  private getBreadcrumbs(route: ActivatedRoute, url: string = '', breadcrumbs: IBreadcrumb[] = []) {
    //debugger
    const ROUTE_DATA_BREADCRUMB: string = 'breadcrumb';
    let children: ActivatedRoute[] = route.children;

    if (children.length === 0) {
      return breadcrumbs;
    }
    for (let child of children) {
      if (child.outlet !== PRIMARY_OUTLET) {
        continue;
      }

      if (!child.snapshot.data.hasOwnProperty(ROUTE_DATA_BREADCRUMB)) {
        return this.getBreadcrumbs(child, url, breadcrumbs);
      }


      let routeURL: string = child.snapshot.url.map(segment => segment.path).join("/");

      url += `/${routeURL}`;

      let breadcrumb: IBreadcrumb = {
        label: child.snapshot.data[ROUTE_DATA_BREADCRUMB],
        params: child.snapshot.params,
        url: url
      };
      breadcrumbs.push(breadcrumb);

      return this.getBreadcrumbs(child, url, breadcrumbs);
    }

    return breadcrumbs;

  }


}


