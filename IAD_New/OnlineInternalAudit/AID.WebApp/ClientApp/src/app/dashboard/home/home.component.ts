import { Component, OnInit, NgModule } from '@angular/core';
import { DashboardService } from '../../services/dashboard/dashboard.service';
import { CalendarEvent } from 'calendar-utils';
import { Subject } from 'rxjs';
import { CalendarEventTimesChangedEvent, CalendarEventAction, CalendarView } from 'angular-calendar';
import { isSameMonth, isSameDay } from 'date-fns/esm';
import { ReportComponent } from '../../report/report.component';
import { Chart } from 'chart.js'; 
import { CommonMsg } from '../../global/common-msg';


@NgModule({
  imports: [ReportComponent],
  declarations: [ReportComponent],
  providers: [],
})

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
  })



export class HomeComponent implements OnInit {
  //Chart
  data: any[];
  Categeory = [];
  Run = [];
  barchart = [];
  auditdhbarchart = [];  
  //End Of Chart
  IsShowSupervisor: boolean;
  IsShowIadHead: boolean;
  IsShowAuditHead: boolean;
  IsShowAuditDH: boolean;
  IsShowAuditeeHead: boolean;
  IsShowAuditeeDH: boolean;
  dashboard: any = {};
  totalAudit: any;
  auditComplete: any;
  upcomingAudit: any;
  totalAuditors: any;
  totalAlocated: any;
  totalNonAlocated: any;
  SettelmentStatus: any;
  totalAssigned: any;
  totalPara: any;
  reportUrl: string = '';
  //Calender
  selectedEvent: CalendarEvent;
  EditEventPopup: boolean = false;
  DeleteEventPopup: boolean = false;
  viewDate: Date = new Date();
  //End Of Calender
  constructor(private dashboardService: DashboardService, private commonMessage: CommonMsg) { }

  ngOnInit() {


    if (parseInt(sessionStorage.getItem('roleId')) == 2) {
      this.getDashboardAllRecord();
      this.GetallAuditInchart();
      this.IsShowSupervisor = true;
      this.getallschedule();

    }
    if (parseInt(sessionStorage.getItem('roleId')) == 3) {
      this.getIADHeadDashboardRecord();
      this.GetallIAdHeadAuditInchart();
      this.IsShowIadHead = true;
      this.getallschedule();

    }
 if (parseInt(sessionStorage.getItem('roleId')) == 5) {
      this.getDashboardAllRecord();
      this.GetallAuditInchart();
      this.IsShowAuditHead = true;
      this.getallschedule();
 }
    if (parseInt(sessionStorage.getItem('roleId')) == 6) {
      this.getAuditDhDashboardAllRecord();
      this.GetallAuditDHInchart();
      this.IsShowAuditDH = true;
      this.getallschedule();
    }
    if (parseInt(sessionStorage.getItem('roleId')) == 7) {
      this.getDashboardAllRecord();
      this.GetallAuditInchart();
      this.IsShowAuditeeHead = true;
      this.getallschedule();
    }
    if (parseInt(sessionStorage.getItem('roleId')) == 8) {
      this.getDashboardAllRecord();
      this.GetallAuditInchart();
      this.IsShowAuditeeDH = true;
      this.getallschedule();
    }
  
  }
  
  getIADHeadDashboardRecord() {
   
    this.dashboardService.getIADHeadDashboardRecord(sessionStorage.getItem('userId')).subscribe(res => {
      var a = res;
      this.totalAudit = res[0][0].totalAudit;
      //this.totalPara = res[1][0].totalPara;
      this.auditComplete = res[2][0].completeAudit;
      this.totalAssigned = res[3][0].totalAssignedAudit;
      //alert(res[3][0].totalAssignedAudit);

      //this.upcomingAudit = res[2][0].upcomingAudit;
      //this.totalAuditors = res[3][0].totalAuditor;
      //this.totalAlocated = res[4][0].totalAlocated;
      //this.totalNonAlocated = res[5][0].totalNonAlocated;

      //
      // this.startCountDown();
    })


  }
  getAuditDhDashboardAllRecord() {
    //  alert(sessionStorage.getItem('UserID'));

    this.dashboardService.getAuditDHDashboardRecord(sessionStorage.getItem('userId')).subscribe(res => {
      var a = res;
      this.totalAudit = res[0][0].totalAudit;
      this.totalPara = res[1][0].totalPara;
      this.auditComplete = res[2][0].completeAudit;
      this.SettelmentStatus = res[3][0].settelmentStatus;

      //alert(res[1][0].completeAudit);

      //this.upcomingAudit = res[2][0].upcomingAudit;
      //this.totalAuditors = res[3][0].totalAuditor;
      //this.totalAlocated = res[4][0].totalAlocated;
      //this.totalNonAlocated = res[5][0].totalNonAlocated;

      //
      // this.startCountDown();
    })


  }
  getDashboardAllRecord() {
    //  alert(sessionStorage.getItem('UserID'));
    debugger;

    this.dashboardService.getDashboardAllRecord("shashank").subscribe(res => {
      var a = res;
      this.totalAudit = res[0][0].totalAudit;
      this.totalPara = res[1][0].totalPara;
      this.auditComplete = res[2][0].completeAudit;
      this.totalAuditors = res[4][0].totalAuditor;
      //alert(res[1][0].completeAudit);
      
      //this.upcomingAudit = res[2][0].upcomingAudit;
      //this.totalAuditors = res[3][0].totalAuditor;
      //this.totalAlocated = res[4][0].totalAlocated;
      //this.totalNonAlocated = res[5][0].totalNonAlocated;

      //
     // this.startCountDown();
    })


  }
  // counter = 0;
  //startCountDown() {
  //  this.totalAudit = setInterval(() => {
  //    if (this.counter < this.totalNonAlocated) {
  //     this.counter++;
  //    }
  //  }, 1000);
  //}

  //////---------------------------- Calendar Begin ------------------------------------////////

  view: CalendarView = CalendarView.Month;

  CalendarView = CalendarView;


  editEvent(evnt) {
    this.EditEventPopup = true;
    this.selectedEvent = evnt;
  }
  deleteEvent(eventToBeDeleted: CalendarEvent) {
    this.DeleteEventPopup = true;
    this.selectedEvent = eventToBeDeleted;
  }
  actions: CalendarEventAction[] = [
    {
      label: '<a class="material-icons i-edit" matTooltip="Edit">edit</a>',
      a11yLabel: 'Edit',
      onClick: ({ event }: { event: CalendarEvent }): void => {
        this.editEvent(event);
      }
    },
    {
      label: '<a class="material-icons i-delete" matTooltip="Delete">delete</a>',
      a11yLabel: 'Delete',
      onClick: ({ event }: { event: CalendarEvent }): void => {
        this.deleteEvent(event);
      }
    }
  ];

  refresh: Subject<any> = new Subject();

  events: CalendarEvent[] = [];

  activeDayIsOpen: boolean = false;

  dayClicked({ date, events }: { date: Date; events: CalendarEvent[] }): void {
    if (isSameMonth(date, this.viewDate)) {
      if (
        (isSameDay(this.viewDate, date) && this.activeDayIsOpen === true) ||
        events.length === 0
      ) {
        this.activeDayIsOpen = false;
      } else {
        this.activeDayIsOpen = true;
      }
      this.viewDate = date;
    }
 
  }

  eventDropped({
    event,
    newStart,
    newEnd,
    allDay
  }: CalendarEventTimesChangedEvent): void {
    //const externalIndex = this.unassignedEvents.indexOf(event);
    if (typeof allDay !== 'undefined') {
      event.allDay = allDay;
    }
    //if (externalIndex > -1) {
    //  this.unassignedEvents.splice(externalIndex, 1);
    //  this.events.push(event);
    //}
    event.start = newStart;
    if (newEnd) {
      event.end = newEnd;
    }
    if (this.view === 'month') {
      this.viewDate = newStart;
      this.activeDayIsOpen = true;
    }
    this.events = [...this.events];
    //if (externalIndex > -1) {
    //  this.ChooseEndDatePopup = true;
    //  this.currentPAOName = event.meta.name;
    //  this.selectedEvent = event;
    //}
  }

  closeOpenMonthViewDay() {
    this.activeDayIsOpen = false;
  }

  //////---------------------------- Calendar End ------------------------------------////////


  arrtotalschedule: any;
  getallschedule() {
    this.dashboardService.getallschedule().subscribe(res => {
      this.arrtotalschedule = res;
      //console.log(this.arrtotalschedule);
      for (var i = 0; i < this.arrtotalschedule.length; i++) {
        let clr = this.commonMessage.getRandomColor();
        let colr = {
          evntColor: {
            primary: clr,
            secondary: clr
          }
        }
        let evnt: CalendarEvent = {
          title: this.arrtotalschedule[i].paoName,
          color: colr.evntColor,
          start: new Date(this.arrtotalschedule[i].auditFromDate),
          end: new Date(this.arrtotalschedule[i].auditToDate),
          meta: this.arrtotalschedule[i],
          draggable: false
        }
        this.events.push(evnt);
      }
      this.refresh.next();
      //this.selectSchedule(this.arrtotalschedule);
      // this.startCountDown();
    })
  }

  //selectSchedule(pao: CalendarEvent) {
  //  this.viewDate = pao.start;
    
  //}
  reportServer: string;

  showParameters: string;
  language: string;
  toolbar: string;

 
  //================Audit chart for Audit DH=========================
  GetallAuditDHInchart() {
    this.dashboardService.GetallAuditDHInchart(sessionStorage.getItem('userId')).subscribe(result => {
      for (var i = 0; i <= result[0].length - 1; i++) {
        if (result[0][i].totalPara != '' && result[0][i].totalPara != null) {
          this.Categeory.push(result[0][i].totalPara);
        }
        this.Run.push(result[0][i].totalAudit);
      }
      this.auditdhbarchart = new Chart('canvas', {
        type: 'bar',
        data: {
          labels: this.Categeory,
          datasets: [
            {
              data: this.Run,
              borderColor: '#3cba9f',
              backgroundColor: [
                "#bf5f82",
                "#9575cd",
                //"#8c7b75",
                //"#00867d",
                //"#0000FF",
                //"#9966FF",
                //"#4C4CFF",
                //"#00FFFF",
                //"#f990a7",
                //"#aad2ed",
                //"#FF00FF",
                //"Blue",
                //"Red",
                //"Blue"
              ],
              fill: true
            }
          ]
        },
        options: {
          legend: {
            display: false
          },
          scales: {
            xAxes: [{
              display: true
            }],
            yAxes: [{
              display: true
            }],
          }
        }
      });
    })
  }
  //================Audit chart for IAD Head=========================
  GetallIAdHeadAuditInchart() {
    this.dashboardService.GetallIAdHeadAuditInchart(sessionStorage.getItem('userId')).subscribe(result => {
      for (var i = 0; i <= result[0].length - 1; i++) {
        if (result[0][i].totalPara != '' && result[0][i].totalPara != null) {
          this.Categeory.push(result[0][i].totalPara);
        }
        this.Run.push(result[0][i].totalAudit);
      }
      this.auditdhbarchart = new Chart('canvas', {
        type: 'bar',
        data: {
          labels: this.Categeory,
          datasets: [
            {
              data: this.Run,
              borderColor: '#3cba9f',
              backgroundColor: [
                "#bf5f82",
                "#9575cd",
                //"#8c7b75",
                //"#00867d",
                //"#0000FF",
                //"#9966FF",
                //"#4C4CFF",
                //"#00FFFF",
                //"#f990a7",
                //"#aad2ed",
                //"#FF00FF",
                //"Blue",
                //"Red",
                //"Blue"
              ],
              fill: true
            }
          ]
        },
        options: {
          legend: {
            display: false
          },
          scales: {
            xAxes: [{
              display: true
            }],
            yAxes: [{
              display: true
            }],
          }
        }
      });
    })
  }

  //================Audit chart for Supervisor=========================
  GetallAuditInchart() {
    this.dashboardService.GetallAuditInchart().subscribe(result => {
      for (var i = 0; i <= result[0].length - 1; i++) {
        if (result[0][i].totalPara != '' && result[0][i].totalPara != null) {
          this.Categeory.push(result[0][i].totalPara);
        }
        this.Run.push(result[0][i].totalAudit);
      }
      this.barchart = new Chart('canvas', {
        type: 'bar',
        data: {
          labels: this.Categeory,
          datasets: [
            {
              data: this.Run,
              borderColor: '#3cba9f',
              backgroundColor: [
                "#bf5f82",
                "#9575cd",
                "#8c7b75",
                "#00867d",
                //"#0000FF",
                //"#9966FF",
                //"#4C4CFF",
                //"#00FFFF",
                //"#f990a7",
                //"#aad2ed",
                //"#FF00FF",
                //"Blue",
                //"Red",
                //"Blue"
              ],
              fill: true
            }
          ]
        },
        options: {
          legend: {
            display: false
          },
          scales: {
            xAxes: [{
              display: true
            }],
            yAxes: [{
              display: true
            }],
          }
        }
      });  
    })
  }
  IADHowManyAsssignedReport() {
    this.reportUrl = 'DashTotalAudit&rs:Embed=true&rc:Parameters=false';
    sessionStorage.setItem('reportUrl', this.reportUrl);
    window.open('/report', "_blank");
  }

  TotalIAD_OnFieldStatusReport() {
    this.reportUrl = 'DashTotalAudit&rs:Embed=true&rc:Parameters=false';
    sessionStorage.setItem('reportUrl', this.reportUrl);
    window.open('/report', "_blank");
  }
  ApprovalStatus() {
    this.reportUrl = 'DashTotalAudit&rs:Embed=true&rc:Parameters=false';
    sessionStorage.setItem('reportUrl', this.reportUrl);
    window.open('/report', "_blank");
  }
  TotalAuditorsOpenReport() {
    this.reportUrl = 'DashTotalAudit&rs:Embed=true&rc:Parameters=false';
    sessionStorage.setItem('reportUrl', this.reportUrl);
    window.open('/report', "_blank");
  }
  TotalAuditReport() {
    this.reportUrl = 'DashTotalAuditor&rs:Embed=true&rc:Parameters=false';
    sessionStorage.setItem('reportUrl', this.reportUrl);
    window.open('/report', "_blank");
  }
 
}


 
