import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RiskcategoryComponent } from './riskcategory.component';

describe('RiskcategoryComponent', () => {
  let component: RiskcategoryComponent;
  let fixture: ComponentFixture<RiskcategoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RiskcategoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RiskcategoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
