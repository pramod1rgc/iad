import { Component, OnInit, ViewChild, } from '@angular/core';
import { RiskCategoryService } from '../../services/Master/risk-category.service';
import swal from 'sweetalert2';
import { MatTableDataSource, MatPaginator, MatSort } from '@angular/material';
import { ReplaySubject, Subject } from 'rxjs';
import { FormControl } from '@angular/forms';
import { takeUntil } from 'rxjs/operators';
import { parse } from 'url';
import { CommonMsg } from '../../global/common-msg';
export interface commondll {
  values: string;
  text: string;
}
export interface EmployeeData {
  riskCategoryName: string;
  riskCategoryDesc: string;

}
@Component({
  selector: 'app-riskcategory',
  templateUrl: './riskcategory.component.html',
  styleUrls: ['./riskcategory.component.css']
})
export class RiskcategoryComponent implements OnInit {

  constructor(private _RiskCategoryService: RiskCategoryService, private _CommonMsg: CommonMsg) { }

  public filteredCategory: ReplaySubject<commondll[]> = new ReplaySubject<commondll[]>(1);
  public CategoryFilterCtrl: FormControl = new FormControl();
  private _onDestroy = new Subject<void>();



  displayedColumns: string[] = ['riskCategoryName','parentCategoryName', 'riskCategoryDesc', 'Action'];//, 'Active'];//, 'leaveTypeDesc', 'action',];
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild('frmriskDetails') frmriskDetails: any;
  MstCategoryDetails: any = {}
  hdnCategoryId = 0;
  Message: string;
  ArrCategoryDetails: any;
  ArrCategoryDetailDataSource: any;
  ActiveDactiveHeaderText: any;
  checkCategoryIsactive: boolean;
  ActivateCategory: boolean;
  CategoryDetailsModel: any;
  riskCategoryID: any;
  notFound: boolean;
  selrdType: any;
  ArrCategory: any;
  mstCategory: any;
  Ishowhide: boolean;
  btnUpdateText = 'Save';
  isHideShowDDL:boolean;
  HideandShowradio(rdFleg) {
    if (rdFleg == 'C') {
      this.isHideShowDDL = false;
    }
    if (rdFleg == 'S') {
      this.isHideShowDDL = true;
    }
  }
  ngOnInit() {
    this.notFound = true;
    this.isHideShowDDL = false;
    this.selrdType = 'Category';
    this.GetAllCategoryDetails();
    //debugger;
    this.BindDropDawnCategory();
    this.CategoryFilterCtrl.valueChanges.pipe(takeUntil(this._onDestroy)).subscribe(() => { this.filterCategory(); });
  }
  SaveRiskCategory() {
    debugger;
    this._RiskCategoryService.SaveRiskCategory(this.hdnCategoryId, this.MstCategoryDetails).subscribe(result => {
      this.Message = result;
      if (this.Message != undefined) {
        swal(this.Message)
        this.Ishowhide = false;
        this.GetAllCategoryDetails();
      }
    });

    this.GetAllCategoryDetails();
    this.BindDropDawnCategory();
    this.Cancel();
  }
  Cancel() {
    this.frmriskDetails.resetForm();
    //alert(this.MstCategoryDetails.category);
    this.MstCategoryDetails.category = undefined;
    this.MstCategoryDetails.riskCategoryName = '';
    this.MstCategoryDetails.riskCategoryDesc = '';
    this.selrdType = 'Category';
    this.Ishowhide = false;
    this.isHideShowDDL = false;
    this.hdnCategoryId = 0;
    this.btnUpdateText = 'Save';
  }
  GetAllCategoryDetails() {
    this._RiskCategoryService.GetAllCategoryDetails().subscribe(result => {
      this.ArrCategoryDetails = result;
      this.ArrCategoryDetailDataSource = new MatTableDataSource(result);
      this.ArrCategoryDetailDataSource.paginator = this.paginator;
      this.ArrCategoryDetailDataSource.sort = this.sort;

      //Filetr inData source
      this.ArrCategoryDetailDataSource.filterPredicate =
        (data: EmployeeData, filtersJson: string) => {
          const matchFilter = [];
          const filters = JSON.parse(filtersJson);

          filters.forEach(filter => {
            //const val = data[filter.id] === null ? '' : data[filter.id];
            //matchFilter.push(val.toLowerCase().includes(filter.value.toLowerCase()));
            //Additional code
            if (filter.id == undefined || filter.id == null) {
              const val = data[filter.id] === null ? '' : data[filter.id];
              matchFilter.push(val.toLowerCase().includes(filter.value.toLowerCase()));
            }
            else {
              const val = data[filter.parent] === null ? '' : data[filter.parent];
              matchFilter.push(val.toLowerCase().includes(filter.value.toLowerCase()));
            }
            // End of additional code
            
          });
          return matchFilter.every(Boolean);
        };
      //END Of Filetr inData source

    });
    this.BindDropDawnCategory();
  }
  UpdateFillForm(riskCategoryID, riskCategoryName, riskCategoryDesc, ParentRiskCategoryID) {
   // debugger;

    //alert(ParentRiskCategoryID);
    this.hdnCategoryId = riskCategoryID;
    if (ParentRiskCategoryID == 0) {
      this.Ishowhide = true;
      this.isHideShowDDL = false;
      this.selrdType = 'Category';

    } else {
      this.Ishowhide = true;
      this.selrdType = 'SubCategory';
      this.isHideShowDDL = true;
      
    }
    this.MstCategoryDetails.riskCategoryName = riskCategoryName;
    this.MstCategoryDetails.riskCategoryDesc = riskCategoryDesc;

    this.MstCategoryDetails.category =  ParentRiskCategoryID;
   
    this.btnUpdateText = 'Update';
  }

  ActiveDeactiveFillPopUp(riskCategoryID, isActive) {
   // alert(isActive)
    //return;
    this.riskCategoryID = riskCategoryID;
    debugger;
    if (isActive == true) {

      this.checkCategoryIsactive = false//true;
      this.ActiveDactiveHeaderText = 'Do you want to deactivate category?';
    }
    else {
      this.checkCategoryIsactive = true//false;
      this.ActiveDactiveHeaderText = 'Do you want to activate category?';
    }

  }
  ActiveDeactiveUsercategory() {
    
    this.CategoryDetailsModel = {
      RiskCategoryID: this.riskCategoryID,
      isActive: this.checkCategoryIsactive

    }
    JSON.stringify(this.CategoryDetailsModel);
    this._RiskCategoryService.ActiveDeactive(this.CategoryDetailsModel).subscribe(data => {
      //debugger;
      this.Message = data;
      //$('.dialog__close-btn').click();
      this.ActivateCategory = false;
      swal(this.Message)
      this.GetAllCategoryDetails();

    })
  }


  CancelPopUp() {
    this.ActivateCategory = false;
    this.GetAllCategoryDetails();
  }
  applyFilter(filterValue: string) {
    const tableFilters = [];
    tableFilters.push({
      id: 'parentCategoryName',
      parent: 'riskCategoryName',
      value: filterValue
    });

    this.ArrCategoryDetailDataSource.filter = JSON.stringify(tableFilters);
    //this.menudataSource.filter = filterValue.trim().toLowerCase();

    if (this.ArrCategoryDetailDataSource.paginator) {
      this.ArrCategoryDetailDataSource.paginator.firstPage();
    }
    if (this.ArrCategoryDetailDataSource.filteredData.length > 0) {
      this.notFound = true;
    } else {
      this.notFound = false;
    }
  }

  BindDropDawnCategory() {
    //alert('alert')
    debugger
    this._RiskCategoryService.BindDropDawnCategory().subscribe(result => {
       
      this.ArrCategory = result;
      debugger;
     this.MstCategoryDetails.Category = 0;
      this.filteredCategory.next(this.ArrCategory);
    })
  }

  // use for filter search in dropdawn
  private filterCategory() {
    debugger;
    if (!this.filteredCategory) {
      return;
    }
    // get the search keyword
    let search = this.CategoryFilterCtrl.value;
    if (!search) {

      this.filteredCategory.next(this.ArrCategory.slice());
      return;
    }
    else {
      search = search.toLowerCase();
    }
    //var a = this.ArrFyear;
    this.filteredCategory.next(
      this.ArrCategory.filter(ArrCategory => ArrCategory.riskCategoryName.toLowerCase().indexOf(search) > -1)
    );

  }
  charaterOnlyNoSpace(event): boolean {
    // debugger;
    if (event.target.value === "") {
      const charCode = (event.which) ? event.which : event.keyCode;
      if (charCode !== 32) {
        return true;
      }
      return false;
    }
  }

  onEdit() {
    this._CommonMsg.onEditScrollTop();
  }
}
