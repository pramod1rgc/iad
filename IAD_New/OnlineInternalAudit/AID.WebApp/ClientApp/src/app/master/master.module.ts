import { NgModule,CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from '../material.module';
import { SharedModule } from '../shared-module/shared-module.module';
import { MatSlideToggleModule } from '@angular/material';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { NgxMatSelectSearchModule } from 'ngx-mat-select-search'
import { MatTooltipModule } from '@angular/material/tooltip';

import { MasterRoutingModule } from './master-routing.module';
import { RiskcategoryComponent } from './riskcategory/riskcategory.component';
//import { ReportComponent} from './report/report.component';
import { ValidationPattern } from '../global/validation-pattern';
import { ReportComponent } from './report/report.component';
//import { ReportViewerModule } from 'ngx-ssrs-reportviewer';, ReportComponent

@NgModule({
  declarations: [RiskcategoryComponent, ReportComponent],
  imports: [
    CommonModule,
    MasterRoutingModule,
    MaterialModule,
    SharedModule,
    MatSlideToggleModule,
    NgxMatSelectSearchModule,
    ReactiveFormsModule,
    FormsModule,
    MatTooltipModule
  
 
  ],
  providers:[ValidationPattern],
  //schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class MasterModule { }
