import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-report',
  templateUrl: './report.component.html',
  styleUrls: ['./report.component.css']
})
export class ReportComponent {
  reportServer: string = 'http://10.199.72.197/reportserver';
  // reportServer: string = 'http://localhost:9273';
  reportUrl: string = 'IAD/AuditProgrammeReport';
  showParameters: string = "false"; //true, false, collapsed
   parameters: any = {
     "FinYear":'10',
  };
  //parameters: any = {
  //  "SampleStringParameter": "String",
  //  "SampleBooleanParameter": true,
  //  "SampleDateTimeParameter": "2/9/2019",
  //  "SampleIntParameter": 12345,
  //  "SampleFloatParameter": "123.1234",
  //  "SampleMultipleStringParameter": ["Parameter1", "Parameter2"]
  //};
  language: string = "en-us";
  width: number = 50;
  height: number = 50;
  toolbar: string = "true";
}
