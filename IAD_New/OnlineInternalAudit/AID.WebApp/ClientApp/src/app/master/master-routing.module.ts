import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MasterModule } from '../master/master.module';
import { RiskcategoryComponent } from '../master/riskcategory/riskcategory.component';
import { ReportComponent} from '../master/report/report.component';
const routes: Routes = [

  {
    path: '', component: MasterModule, data: {
      breadcrumb: 'Master'
    }, children: [
      {
        path: 'riskcategory', component: RiskcategoryComponent, data: {
          breadcrumb: 'Risk Category'
        }
      //},
      // {
      //   path: 'report', component: ReportComponent, data: {
      //    breadcrumb: 'report'
      //  }
      }


    ]
  }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MasterRoutingModule { }
