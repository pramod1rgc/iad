import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuditReportListComponent } from './audit-report-list/audit-report-list.component';
import { AuditReportDetailComponent } from './audit-report-detail/audit-report-detail.component';
import { AuditReportRoutingModule } from './audit-report.routing';
import { MatSelectModule, MatCardModule, MatDatepickerModule, MatFormFieldModule, MatCheckboxModule } from '@angular/material';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from '../material.module';
import { CKEditorModule } from '@ckeditor/ckeditor5-angular';
import { SharedModule } from '../shared-module/shared-module.module';

@NgModule({
  declarations: [AuditReportListComponent, AuditReportDetailComponent],
  imports: [
    CommonModule, AuditReportRoutingModule, MatSelectModule, FormsModule, MatCardModule, ReactiveFormsModule, MatDatepickerModule, MaterialModule, MatFormFieldModule, MatCheckboxModule, CKEditorModule, SharedModule
  ]
})
export class AuditReportModule { }
