import { Component, OnInit, ViewChild } from '@angular/core';
import { AuditReportService } from '../../services/audit-report/audit-report.service';
import { Router } from '@angular/router';
import { MatTableDataSource, MatPaginator } from '@angular/material';

@Component({
  selector: 'app-audit-report-list',
  templateUrl: './audit-report-list.component.html',
  styleUrls: ['./audit-report-list.component.css']
})
export class AuditReportListComponent implements OnInit {

  auditList: any[] = [];
  searchTerm: string = '';
  pageSize: number = 10;
  pageNumber: number = 1;
  totalCount: number = 0;

  displayedColumns: string[] = ['Name', 'PeriodOfUnit', 'AuditIntimationDate', 'Status', 'Action'];

  dataSource: MatTableDataSource<any> = new MatTableDataSource<any>();

  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(private _service: AuditReportService, private router: Router) { }

  getAuditReportList() {
    this.totalCount = 0;
    this._service.getAuditsForAuditReportBySupervisor(this.pageSize, this.pageNumber, this.searchTerm).subscribe(response => {
      this.auditList = response;
      if (this.auditList.length > 0) {
        this.totalCount = this.auditList[0].totalCount;
      }
      this.dataSource = new MatTableDataSource(this.auditList);
    });
  }

  applyFilter() {
    this.totalCount = 0;
    this.pageNumber = 1;
    this.paginator.firstPage();
    this.getAuditReportList();
  }

  getPaginationData(event) {
    this.pageSize = event.pageSize;
    this.pageNumber = event.pageIndex + 1;
    this.getAuditReportList();
  }

  navigateToReportDetail(audit) {
    localStorage.setItem("AuditReportDetailsData", JSON.stringify(audit));
  }

  ngOnInit() {
    this.getAuditReportList();
  }

}
