import { Component, OnInit } from '@angular/core';
import { AuditReportService } from '../../services/audit-report/audit-report.service';
import { AuditsService } from '../../services/audits/audits.services';

@Component({
  selector: 'app-audit-report-detail',
  templateUrl: './audit-report-detail.component.html',
  styleUrls: ['./audit-report-detail.component.css']
})
export class AuditReportDetailComponent implements OnInit {

  userId: number;
  roleId: number;
  selectedAudit: any;
  auditReport: any;
  auditTeam: any[] = [];

  constructor(private _service: AuditReportService, private auditService: AuditsService) {
    this.userId = JSON.parse(localStorage.getItem("LoginData")).userId;
    this.roleId = JSON.parse(localStorage.getItem("LoginData")).roleId;
    this.selectedAudit = JSON.parse(localStorage.getItem("AuditReportDetailsData"));
  }

  getAuditReport() {
    this._service.getAuditReport(this.selectedAudit.auditId).subscribe(response => {
      this.auditReport = response;
    });
  }

  getAuditTeam() {
    this.auditService.getApprovedAuditTeam(this.selectedAudit.auditId).subscribe(response => {
      this.auditTeam = response;
    });
  }

  assignAuditDetails() {
    localStorage.setItem("selectedAuditDetailsForEntryConference", JSON.stringify(this.auditReport));
    localStorage.setItem("selectedAuditDetailsForExitConference", JSON.stringify(this.auditReport));
  }

  ngOnInit() {
    this.getAuditReport();
    this.getAuditTeam();
  }

}
