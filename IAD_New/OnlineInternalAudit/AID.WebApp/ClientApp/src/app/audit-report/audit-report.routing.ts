import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuditReportModule } from './audit-report.module';
import { AuditReportListComponent } from './audit-report-list/audit-report-list.component';
import { AuditReportDetailComponent } from './audit-report-detail/audit-report-detail.component';

const routes: Routes = [

  {
    path: '', component: AuditReportModule, data: {
      breadcrumb: 'Audit Report'
    }, children: [
      {
        path: 'audit-report-list', component: AuditReportListComponent, data: {
          breadcrumb: 'Audit Report List'
        }
      },
      {
        path: 'audit-report-detail', component: AuditReportDetailComponent, data: {
          breadcrumb: 'Audit Report Detail'
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AuditReportRoutingModule { }
