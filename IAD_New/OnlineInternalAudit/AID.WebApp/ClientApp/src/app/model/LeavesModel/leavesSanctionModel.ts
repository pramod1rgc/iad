export class LeavesSanctionMainModel {
  leaveMainId: number;
  empCd: string;
  empName: string;
  permDdoId: string;
  orderNo: string;
  orderDT: any;
  leaveCategory: string ="Single";
  leaveReasonCD: string;
  leaveReasonDesc: string;
  verifyFlg: string;
  remarks: string;
  leaveSanctionDetails: any[];
}

export class LeavesSanctionDetailsModel {
  leaveCd: string;
  fromDT: any;
  toDT: any;
  noDays: string;
  maxLeave: string;
  isMaxLeave: boolean;
}
export class ReasonForLeaveModel {
  cddirCodeValue: string;
  cddirCodeText: string;
} 
export class LeavesTypeModel {
  leaveTypeLeaveCd: string;
  leaveTypeLeaveDesc: string;
  leaveMaxAtATime: string;
  
}

export class LeavesSanctionHistoryModel {

     histryOrderNo:string ;
     histryOrderDt:string ;
   histryLeavetype:string ;
      histryFromDt:string ;
        histryToDt:string ;
    histryNoofDays:string ;
  histryLeaveStatus:string; 
}


