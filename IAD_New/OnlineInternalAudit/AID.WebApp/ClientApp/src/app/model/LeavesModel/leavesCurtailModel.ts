export class LeavesSanctionMainModel {
  leaveMainId: number;
  empCd: string;
  permDdoId: string;
  orderNo: string;
  orderDT: any;
  fromDT: any;
  toDT: any;
  //leaveCategory: string = "Single";
  //leaveReasonCD: string;
  //leaveReasonDesc: string;
  verifyFlg: string;
  remarks: string;
  //leaveSanctionDetails: any[];
}
