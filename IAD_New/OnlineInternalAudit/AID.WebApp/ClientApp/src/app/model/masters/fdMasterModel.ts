export class fdMasterModel {
  fdMasterID: number;
  relationShip: string;
  commutationPer: number;
  age: number;
  serialNo: number;
  commutationValue: number;
  loginUser: string;
  constructor() { }
}
