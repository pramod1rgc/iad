export class PayscaleModel {
  msPayScaleID: number;
  msCddirID: number;
  cddirCodeText: string;
  paysfm_id: number;
  paysfm_Text: string;
  paysGroup_id: number;
  paysGroup_Text: string;
  payScaleCode: string;
  payScale: string;
  payScaleGradePay: string;
  payGISGroupID: number;
  payGISGroupName: string;
  payScaleWithEffectFrom: string;
  payScaledescription: string;
  payScalePscLoLimit: string;
  payScalePscInc1: string;
  payScalePscStage1: string;
  payScalePscInc2: string;
  payScalePscStage2: string;
  payScalePscInc3: string;
  payScalePscStage3: string;
  payScalePscInc4: string;
  payScalePscUpLimit: string;
  payScaleLevel: string;
  constructor(){ }
}
