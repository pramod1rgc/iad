export class hraModel {
  payScaleCode: string;
  payScaleID: string;
  city: string;
  cityCode: string;
  serialNo: string;
  EmployeeType: string;
  payCommissionCode: string;
  payCommission: string;
  payCommisionName: string;
  stateId: number;
  loginUser: string;
  empTypeID: string;
  hraAmount: string;
  cityClass: string;
  className: string;
  stateCode: string;
  hraMasterID: number;
  tptaMasterID: number;
  tptaAmount: string;
  slabType: number;
  slabtypDesc: string;
  value: string;
  minvalue: number;
  PayCommID: number;
  constructor() { }
}
