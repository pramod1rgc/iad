export class DuesRateMasterModel {
  

  public  MsPayRatesID :number;
  public  PayRatesPayItemCD :number;
  public  PayRatesRuleID :number;
  public  PayRatesRateID :number;
  public  PayRatesRateSlNo :number;
  public  PayRatesRateDesc : string;
  public  PayRatesFromDate :Date;
  public  PayRatesToDate :Date;
  public  PayRatesScaleFrom :number;
  public  PayRatesScaleTo :number;
  public  PayRatesSlabFrom :number;
  public  PayRatesSlabTo :number; 
  
  public  PayRatesCityCD  :string;
  public  PayRatesGroupCD :number;
  public  PayRatesStateCodeCensus :number;
  public  PayRatesValue :number;
  public  PayRatesAddFixAmt :number;
  public  PayRatesMaxValue :number;
  public  PayRatesMinValue :number;
  public  PayRatesVideLetterNo :number;
  public  PayRatesVideLetterDt :Date;
  public  PayRatesUpdDt :Date;
  public  CreatedIP  :string;
  public  ModifiedIP   :string;
  public  CreatedBy   :string;
  public  CreatedDate :Date;
  public  ModifiedBy   :string;
  public  ModifiedDate :Date;
  public  msPayCommID :number;
  public  PayCommDesc :string;
  public  SlabType :string;
  public   MsSlabTypeID :number;
  public  Code :string;
  public  Description :string;
  public  msPayItemsID :number
  public  payItemsName :string;
  public  StateId : number;
  public  stateCode :string;
  public  stateName :string;
  constructor() { }
}
