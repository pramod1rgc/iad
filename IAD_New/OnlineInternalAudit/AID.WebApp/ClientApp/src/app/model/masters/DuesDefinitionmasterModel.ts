export class DuesDefinitionmasterModel {
  msPayItemsID: number;
  payItemsCD: string;
  payItemsName: string;
  payItemsNameShort: string;
  payItemsWef: Date;
  payItemsComputable: number;
  payItemsIsTaxable:number;
    constructor() { }
  }
