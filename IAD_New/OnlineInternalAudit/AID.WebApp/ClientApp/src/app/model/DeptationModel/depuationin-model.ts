import { FormControl, FormGroup, Validators } from '@angular/forms';


export class Deputation_Deduction {

  payItemsDeductionName: string;
  payItemsDeductionAccCdScheme: string;
  deduction_Schedule: string;
  communication_Address: string;
  payItemsCd: string;
  msEmpDuesId: string
  static asFormGroup(deputation_Deduction: Deputation_Deduction): FormGroup {
    const fg = new FormGroup({
      payItemsDeductionName: new FormControl(deputation_Deduction.payItemsDeductionName, Validators.required),
      payItemsDeductionAccCdScheme: new FormControl(deputation_Deduction.payItemsDeductionAccCdScheme, Validators.required),
      deduction_Schedule: new FormControl(deputation_Deduction.deduction_Schedule, Validators.required),
      communication_Address: new FormControl(deputation_Deduction.communication_Address, Validators.required),
      payItemsCd: new FormControl(deputation_Deduction.payItemsCd),
      msEmpDuesId: new FormControl(deputation_Deduction.msEmpDuesId)
    });
    return fg;
  }
}

export class SecemeCode {
  payItemsDeductionAccCdScheme: string;
}
//export class Designation {
//  desigId: string;
//  desigDesc: string;
//}

//export interface CommunicationAddress {
//  id: number;
//  username: string;
//}
//export interface State {
//  stateId: number;
//  stateName: string
//}

