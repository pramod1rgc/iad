export class Endofservicemodel {
  msEmpID: any;
  desigID: any;
  empCd: any;
  empName: any;
  msCddirID: any;     
  cddirCodeText: any;
  msEmpServiceEndID: any;
  empApptType: any;
  superanuationDate: any;
  currentPostingMode: any;
  dateofEntry: any;
  billMonthName: string;
  billYear: number;
  orderNo: any;
  orderDate: any;
  remark: string;
  endServDt: any;
  endReasonId: number;
  endReason: string;
  statusId: any;
  dateofDeath: any;
  endRemark: string;


  createdBy: string; 
  createdDate: any;  
 
}



