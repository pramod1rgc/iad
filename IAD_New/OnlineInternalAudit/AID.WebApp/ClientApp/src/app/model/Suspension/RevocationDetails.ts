export class RevocationDetails {
  EmpRevocID: number;
  EmpTransSusId: number;
  RevocationDate: Date;
  RevocOrderNo: string;
  RevocOrderDt: Date;
  Remarks: string;
  IPAddress: string;
}
