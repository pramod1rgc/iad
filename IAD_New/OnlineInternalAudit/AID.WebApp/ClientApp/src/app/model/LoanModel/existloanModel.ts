export class ExistloanModel {
  mode: number;
  msPayAdvEmpdetID: number;
  loanCD: string;
  sancOrdNo: number;
  sancOrdDT: Date;
  empLoanDetailsID: number ;
  loanAmtSanc: number;
  loanAmtDisbursed: number;
  priInstAmt: number;
  oddInstAmtInt: number;
  oddInstNoInt: number;
  //oddInstNoInt: number;
  schemeId: string;
  oddInstNoPri: number;
  oddInstAmtPri: number;
  EmpCd: string;
  payLoanRefLoanShortDesc: string;
  recoveryStatus: boolean;
  //RecoveryStatus: string;
  priVerifFlag: number;
  payLoanRefLoanDesc: string;
  payLoanRefLoanHeadACP: string;
  schemeCode: string;
  payLoanRefLoanCD: string;
  empPersVerifFlag: string;
  BillMonth: string;
  LoanCD: string;

}
