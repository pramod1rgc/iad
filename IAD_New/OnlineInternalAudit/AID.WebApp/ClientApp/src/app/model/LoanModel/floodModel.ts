export class FloodModel {

  SancOrdNo: string;
  SancOrdDT: Date;
  LoanAmtDisbursed: number;
  IntTotInst: number;
  IntInstAmt: number;
  MSPayAdvEmpdetID: string;
  LoanBillID: string;
  SancOrdID: string;
  EmpCD: string;
  IntAmt: number;
  AccountHead: number;
  SancOrdDt: number;
  SchemeId: number;
  PriInstAmt: number;
  PriLstInstRec: number;
  PriTotInst: number;
  PriBalance: number;
  Mode: number;

  totalEmpcode: any[] = [];
} 
  
  

