export class EmpSanction {
  sancOrdDT: Date;  
  sancOrdNo: string;   
  loanAmtDisbursed: number;
  priTotInst: number;
  priInstAmt: number;
  oddInstNoPri: number;         
  oddInstAmtPri: number; 
  priLstInstRec: number;
  billMonth: number;
  outstandingAmt: number;
  deductedAmt: number;
}

export class EmpSancModel {
  orderNo: string;
  orderDate: Date;
  recoveredInstNo: number;
  loantype: number;
  mode: number;
  EmpCD: string;

}
