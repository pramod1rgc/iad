export interface PayBillGroupModel {
    filter(arg0: (design: any) => boolean): DesignationModel;
    slice(): any;
  billgrId: number;
  billgrDesc: string;
  payBillGroupId: number;
  serviceType: string;
  groups: number;
}

export interface EmpModel {
  empCd: number;
  empName: string;
}

export interface DesignationModel {
    msDesigMastID: any;
  desigId: number;
  desigDesc: string;
}

export interface OrderModel {
  orderId: string;
  orderNo: string;
}

export interface BillCode {
  id: string;
  name: string;
}
export interface DesignCode {
  id: string;
  name: string;
}
export interface EMPCode {
  id: string;
  name: string;
}
//export class LeavesSanctionModel {
//  permDDOId: string;
//  empCode: string;
//  orderNo: string;
//  orderDT: string;
//  singleLeave: boolean;
//  reasonForLeave: string;
//  leaveCd: string;
//  fromDT: string;
//  toDT: string;
//  noDays: string;
//  remarks: string;
//}
