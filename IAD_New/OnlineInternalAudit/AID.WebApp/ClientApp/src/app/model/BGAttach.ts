export class BillGroupAttach {
  public payBillGroupId: Number;

  public schemeId: number;
  public serviceType: string;
  public group: string;
  public groupId: string;
  public billGroupName: string;
  public billForGAR: string;
  public isActive: boolean;
  public EmployeeType: string;
  public EmployeeSubType: string;
  public Designation: string;
  public billgroupId: string;
  public billgrDesc: string;
  public billgrFor: string;
  public empDOB: string;
  //Attach Model
}
