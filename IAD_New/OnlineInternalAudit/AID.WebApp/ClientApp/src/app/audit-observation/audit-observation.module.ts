import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PendingAuditComponent } from './pending-audit/pending-audit.component';
import { AuditEntryConferenceComponent } from './audit-entry-conference/audit-entry-conference.component';
import { AuditObservationRoutingModule } from './audit-observation.routing';
import { MatSelectModule, MatCardModule, MatDatepickerModule, MatFormFieldModule, MatCheckboxModule } from '@angular/material';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from '../material.module';
import { CKEditorModule } from '@ckeditor/ckeditor5-angular';
import { CommonMsg } from '../global/common-msg';
import { AuditExitConferenceComponent } from './audit-exit-conference/audit-exit-conference.component';
import { AuditParaListComponent } from './audit-para/audit-para-list/audit-para-list.component';
import { AuditParaCreateComponent } from './audit-para/audit-para-create/audit-para-create.component';
import { AuditParaDetailComponent } from './audit-para/audit-para-detail/audit-para-detail.component';
import { SharedModule } from '../shared-module/shared-module.module';
import { ParaSettlementLetterComponent } from './para-settlement-letter/para-settlement-letter.component';

@NgModule({
  declarations: [PendingAuditComponent, AuditEntryConferenceComponent, AuditExitConferenceComponent, AuditParaListComponent, AuditParaCreateComponent, AuditParaDetailComponent, ParaSettlementLetterComponent],
  imports: [
    CommonModule, AuditObservationRoutingModule, MatSelectModule, FormsModule, MatCardModule, ReactiveFormsModule, MatDatepickerModule, MaterialModule, MatFormFieldModule, MatCheckboxModule, CKEditorModule, SharedModule
  ],
  providers:[CommonMsg]
})
export class AuditObservationModule { }
