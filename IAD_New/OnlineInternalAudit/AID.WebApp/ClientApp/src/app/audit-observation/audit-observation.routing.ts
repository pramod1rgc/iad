import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuditObservationModule } from './audit-observation.module';
import { PendingAuditComponent } from './pending-audit/pending-audit.component';
import { AuditEntryConferenceComponent } from './audit-entry-conference/audit-entry-conference.component';
import { AuditExitConferenceComponent } from './audit-exit-conference/audit-exit-conference.component';
import { AuditParaListComponent } from './audit-para/audit-para-list/audit-para-list.component';
import { AuditParaCreateComponent } from './audit-para/audit-para-create/audit-para-create.component';
import { AuditParaDetailComponent } from './audit-para/audit-para-detail/audit-para-detail.component';
import { ParaSettlementLetterComponent } from './para-settlement-letter/para-settlement-letter.component';

const routes: Routes = [

  {
    path: '', component: AuditObservationModule, data: {
      breadcrumb: 'Audit Observation'
    }, children: [
      {
        path: 'pending-audit', component: PendingAuditComponent, data: {
          breadcrumb: 'Select Audit'
        }
      },
      {
        path: 'entry-conference', component: AuditEntryConferenceComponent, data: {
          breadcrumb: 'Entry Conference'
        }
      },
      {
        path: 'exit-conference', component: AuditExitConferenceComponent, data: {
          breadcrumb: 'Exit Conference'
        }
      },
      {
        path: 'audit-para-list/:filter', component: AuditParaListComponent, data: {
          breadcrumb: 'Audit Para List'
        }
      },
      {
        path: 'audit-para-create', component: AuditParaCreateComponent, data: {
          breadcrumb: 'Create Audit Para'
        }
      },
      {
        path: 'audit-para-edit/:paraId', component: AuditParaCreateComponent, data: {
          breadcrumb: 'Edit Audit Para'
        }
      },
      {
        path: 'audit-para-detail/:paraId', component: AuditParaDetailComponent, data: {
          breadcrumb: 'Audit Para Detail'
        }
      },
      {
        path: 'para-settlement-letter', component: ParaSettlementLetterComponent, data: {
          breadcrumb: 'Audit Para settlement Letter'
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AuditObservationRoutingModule { }
