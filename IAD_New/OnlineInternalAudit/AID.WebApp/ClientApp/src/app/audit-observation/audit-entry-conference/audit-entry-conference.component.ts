import { Component, OnInit, ViewChild } from '@angular/core';
import { AuditObservationService } from '../../services/audit-observation/audit-observation.service';
import { FormGroup, FormControl, Validators, FormArray, FormGroupDirective } from '@angular/forms';
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { AuditsService } from '../../services/audits/audits.services';
import swal from 'sweetalert2';
import { CommonMsg } from '../../global/common-msg';

@Component({
  selector: 'app-audit-entry-conference',
  templateUrl: './audit-entry-conference.component.html',
  styleUrls: ['./audit-entry-conference.component.css']
})
export class AuditEntryConferenceComponent implements OnInit {

  @ViewChild(FormGroupDirective) formGroupDirective: FormGroupDirective;

  selectedAudit: any;
  entryConferenceDetails: any;
  entryConferenceForm: FormGroup;
  auditTeam: any[] = [];
  selectedAuditTeam: any[] = [];
  public Editor = ClassicEditor;
  userId: number;
  roleId: number;
  saveButtonDisabled: boolean = false;
  saveButtonClicked: boolean = false;
  auditFromDate: Date = new Date();
  auditToDate: Date = new Date();
  rejectionRemark: string = '';
  RejectPopUp: boolean = false;

  constructor(private _service: AuditObservationService, private auditService: AuditsService, private _msg: CommonMsg) {
    this.userId = JSON.parse(localStorage.getItem("LoginData")).userId;
    this.roleId = JSON.parse(localStorage.getItem("LoginData")).roleId;
    this.selectedAudit = JSON.parse(localStorage.getItem("selectedAuditDetailsForEntryConference"));
    this.auditFromDate = new Date(this.selectedAudit.auditFromDate);
    this.auditToDate = new Date(this.selectedAudit.auditToDate);
    this.getEntryConferenceDetails();
    this.getAuditTeam();
  }

  createForm() {
    this.entryConferenceForm = new FormGroup({
      entryConferenceId: new FormControl(0),
      auditId: new FormControl(this.selectedAudit.auditId),
      conferenceDate: new FormControl('', Validators.required),
      agenda: new FormControl('', Validators.required),
      userId: new FormControl(this.userId),
      participants: new FormArray([])
    });
  }

  getEntryConferenceDetails() {
    this._service.getAuditEntryConference(this.selectedAudit.auditId).subscribe(response => {
      if (response && response.length > 0) {
        this.entryConferenceDetails = response[0];
        this.saveButtonDisabled = true;
      }
    });
  }

  getAuditTeam() {
    this.auditService.getApprovedAuditTeam(this.selectedAudit.auditId).subscribe(response => {
      this.auditTeam = response;
      if (!this.selectedAudit.entryConferenceId || this.selectedAudit.entryConferenceId == 0) {
        this.selectedAuditTeam = this.auditTeam;
      }
    });
  }

  onSubmit() {
    this.saveButtonClicked = true;
    if (this.entryConferenceForm.valid) {
      if (this.selectedAuditTeam.length > 0) {
        for (var i = 0; i < this.selectedAuditTeam.length; i++) {
          let ob = { Id: i + 1, AuditUserMapId: this.selectedAuditTeam[i].auditUserRoleId };
          this.entryConferenceForm.value.participants.push(ob);
        }
        this._service.updateAuditEntryConference(this.entryConferenceForm.value).subscribe(response => {
          if (response > 0) {
            this.saveButtonClicked = false;
            swal(this._msg.updateMsg);
            this.createForm();
            this.saveButtonDisabled = true;
            this.getEntryConferenceDetails();
            this.formGroupDirective.resetForm();
            this.selectedAuditTeam = [];
          }
        });
      }
      else {
        swal("Please select atleast one attendee.");
      }
    }
  }

  updateStatus(statusId) {
    if (this.entryConferenceDetails) {
      let obj = { StatusId: statusId, EntryConferenceId: this.entryConferenceDetails.entryConferenceId, UserId: this.userId, RejectionRemark: this.rejectionRemark };
      this._service.updateEntryConferenceStatus(obj).subscribe(response => {
        if (response > 0) {
          swal(this._msg.updateMsg);
          this.getEntryConferenceDetails();
          this.RejectPopUp = false;
          this.rejectionRemark = '';
        }
      });
    }
  }

  editEntryConference() {
    this.entryConferenceForm = new FormGroup({
      entryConferenceId: new FormControl(this.entryConferenceDetails.entryConferenceId),
      auditId: new FormControl(this.selectedAudit.auditId),
      conferenceDate: new FormControl(this.entryConferenceDetails.conferenceDate, Validators.required),
      agenda: new FormControl(this.entryConferenceDetails.agenda, Validators.required),
      userId: new FormControl(this.userId),
      participants: new FormArray([])
    });
    this.selectedAuditTeam = [];
    for (var i = 0; i < this.entryConferenceDetails.participants.length; i++) {
      let emp = this.auditTeam.filter(a => a.auditUserRoleId == this.entryConferenceDetails.participants[i].auditUserMapId);
      if (emp.length > 0) {
        this.selectedAuditTeam.push(emp[0]);
      }
    }
    this.saveButtonDisabled = false;
  }

  ngOnInit() {
    this.createForm();
  }

}
