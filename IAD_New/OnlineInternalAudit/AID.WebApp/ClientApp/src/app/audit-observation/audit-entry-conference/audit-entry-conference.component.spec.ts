import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AuditEntryConferenceComponent } from './audit-entry-conference.component';

describe('AuditEntryConferenceComponent', () => {
  let component: AuditEntryConferenceComponent;
  let fixture: ComponentFixture<AuditEntryConferenceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AuditEntryConferenceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AuditEntryConferenceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
