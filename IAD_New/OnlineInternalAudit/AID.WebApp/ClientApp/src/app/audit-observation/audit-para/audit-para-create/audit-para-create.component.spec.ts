import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AuditParaCreateComponent } from './audit-para-create.component';

describe('AuditParaCreateComponent', () => {
  let component: AuditParaCreateComponent;
  let fixture: ComponentFixture<AuditParaCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AuditParaCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AuditParaCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
