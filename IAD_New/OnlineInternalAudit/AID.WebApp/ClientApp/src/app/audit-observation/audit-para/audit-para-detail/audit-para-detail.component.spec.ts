import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AuditParaDetailComponent } from './audit-para-detail.component';

describe('AuditParaDetailComponent', () => {
  let component: AuditParaDetailComponent;
  let fixture: ComponentFixture<AuditParaDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AuditParaDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AuditParaDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
