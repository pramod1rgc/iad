import { Component, OnInit, ElementRef, ViewChild, AfterViewChecked, AfterViewInit, AfterContentChecked } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AuditObservationService } from '../../../services/audit-observation/audit-observation.service';
import swal from 'sweetalert2';
import { ScrollToBottomDirective } from '../../../directives/scroll-to-bottom.directive';
import { CommonMsg } from '../../../global/common-msg';

@Component({
  selector: 'app-audit-para-detail',
  templateUrl: './audit-para-detail.component.html',
  styleUrls: ['./audit-para-detail.component.css'],
  providers: [ScrollToBottomDirective]
})
export class AuditParaDetailComponent implements OnInit {

  paraId: number;
  details: any;
  userId: number;
  roleId: number;
  selectedAudit: any;
  commentsList: any[] = [];
  comment: string;
  uploadFiles: any[] = [];
  rejectionRemark: string = '';
  RejectPopUp: boolean = false;
  financialImplications: any[] = [];

  //@ViewChild(ScrollToBottomDirective)scroll: ScrollToBottomDirective;

  constructor(private route: ActivatedRoute, private _service: AuditObservationService, private _msg: CommonMsg) {
    this.route.params.subscribe(params => {
      if (params['paraId']) {
        this.paraId = params['paraId'];
        this.getParaDetails();
        this.getParaComments();
      }
    });
    this.userId = JSON.parse(localStorage.getItem("LoginData")).userId;
    this.roleId = JSON.parse(localStorage.getItem("LoginData")).roleId;
    this.selectedAudit = JSON.parse(localStorage.getItem("selectedAuditDetailsForExitConference"));
  }

  getParaDetails() {
    this._service.getAuditParaDetails(this.paraId).subscribe(response => {
      this.details = response;
      this.getFinancialImplicationDetails();
    });
  }

  getFinancialImplicationDetails() {
    this._service.getAuditParaFinancialImplications(this.paraId).subscribe(response => {
      this.financialImplications = response;
    });
  }

  getParaComments() {
    this._service.getParaComments(this.paraId).subscribe(response => {
      this.commentsList = response;
    });
  }

  postParaComment() {
    debugger;
    let obj = { auditId: this.selectedAudit.auditId, paraId: this.paraId, comment: this.comment, userId: this.userId };
    this._service.postParaComment(obj, this.uploadFiles).subscribe(response => {
      if (response > 0) {
        this.getParaComments();
        this.comment = '';
        this.uploadFiles = [];
      }
    });
  }

  filesToBeUploaded(files) {
    this.uploadFiles = [];
    this.uploadFiles = files;
    if (this.getTotalFileSize() > 50) {
      swal("Files Size cannot be exceeded by 50MB");
    }
  }

  getTotalFileSize(): any {
    let sum = 0;
    for (var i = 0; i < this.uploadFiles.length; i++) {
      sum = +sum + +this.convertFileSizeFromBytesToMegaBytes(this.uploadFiles[i].size);
    }
    return sum;
  }

  convertFileSizeFromBytesToMegaBytes(size): any {
    return (size / (1024 * 1024)).toFixed(4);
  }

  updateAuditParaStatus(statusId) {
    if (this.paraId) {
      let obj = { paraId: this.paraId, auditId: this.selectedAudit.auditId, statusId: statusId, rejectionRemark: this.rejectionRemark };
      this._service.updateAuditParaStatus(obj).subscribe(response => {
        if (response > 0) {
          swal(this._msg.updateMsg);
          this.rejectionRemark = '';
          this.RejectPopUp = false;
          this.getParaDetails();
        }
      });
    }
  }

  ngOnInit() {
  }

}
