import { Component, OnInit, ViewChild } from '@angular/core';
import { AuditObservationService } from '../../../services/audit-observation/audit-observation.service';
import swal from 'sweetalert2';
import { CommonMsg } from '../../../global/common-msg';
import { ActivatedRoute } from '@angular/router';
import { MatTableDataSource, MatPaginator } from '@angular/material';

@Component({
  selector: 'app-audit-para-list',
  templateUrl: './audit-para-list.component.html',
  styleUrls: ['./audit-para-list.component.css']
})
export class AuditParaListComponent implements OnInit {

  userId: number;
  roleId: number;
  selectedAudit: any;
  auditParaList: any[] = [];
  deletePopUp: boolean = false;
  selectedPara: any;
  filterParameter: string;
  searchTerm: string = '';
  pageSize: number = 10;
  pageNumber: number = 1;
  totalCount: number = 0;

  displayedColumns: string[] = [ 'ParaTitle', 'ObservationType', 'RiskLevel', 'Status', 'Action'];

  dataSource: MatTableDataSource<any> = new MatTableDataSource<any>();

  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(private _service: AuditObservationService, private _msg: CommonMsg, private route: ActivatedRoute) {
    this.route.params.subscribe(params => {
      if (params['filter']) {
        this.filterParameter = params['filter'];
        //this.filterAuditParas();
      }
    });
    this.userId = JSON.parse(localStorage.getItem("LoginData")).userId;
    this.roleId = JSON.parse(localStorage.getItem("LoginData")).roleId;
    this.selectedAudit = JSON.parse(localStorage.getItem("selectedAuditDetailsForExitConference"));
  }

  getAuditParas() {
    this.totalCount = 0;
    this._service.getAuditParas(this.selectedAudit.auditId, this.pageSize, this.pageNumber, this.searchTerm).subscribe(response => {
      this.auditParaList = response;
      if (this.auditParaList.length > 0) {
        this.totalCount = this.auditParaList[0].totalCount;
      }
      this.selectedPara = null;
      this.filterAuditParas();
      this.dataSource = new MatTableDataSource(this.auditParaList);
    });
  }

  filterAuditParas() {
    if (this.filterParameter) {
      let paras = null;
      switch (this.filterParameter) {
        case "all":
          break;
        case "approved":
          paras = this.auditParaList.filter(a => a.statusId == 2);
          this.auditParaList = paras;
          break;
        case "partially":
          paras = this.auditParaList.filter(a => a.commentId > 0 && (a.statusId == 1 || a.statusId == 3));
          this.auditParaList = paras;
          break;
        case "open":
          paras = this.auditParaList.filter(a => a.commentId == 0 && a.statusId == 1);
          this.auditParaList = paras;
          break;
        case "high":
          paras = this.auditParaList.filter(a => a.severity == "high");
          this.auditParaList = paras;
          break;
        case "medium":
          paras = this.auditParaList.filter(a => a.severity == "medium");
          this.auditParaList = paras;
          break;
        case "low":
          paras = this.auditParaList.filter(a => a.severity == "low");
          this.auditParaList = paras;
          break;
      }
    }
  }

  selectPara(para) {
    this.selectedPara = para;
  }

  deletePara() {
    if (this.selectedPara) {
      let obj = { ParaId: this.selectedPara.paraId, UserId: this.userId };
      this._service.deleteAuditPara(obj).subscribe(response => {
        if (response > 0) {
          this.getAuditParas();
          this.selectedPara = null;
          this.deletePopUp = false;
          swal(this._msg.deleteMsg);
        }
      });
    }
  }

  applyFilter() {
    this.totalCount = 0;
    this.pageNumber = 1;
    this.paginator.firstPage();
    this.getAuditParas();
  }

  getPaginationData(event) {
    this.pageSize = event.pageSize;
    this.pageNumber = event.pageIndex + 1;
    this.getAuditParas();
  }

  ngOnInit() {
    this.getAuditParas();
  }

}
