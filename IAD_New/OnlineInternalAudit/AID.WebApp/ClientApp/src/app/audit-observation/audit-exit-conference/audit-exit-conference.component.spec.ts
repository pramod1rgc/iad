import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AuditExitConferenceComponent } from './audit-exit-conference.component';

describe('AuditExitConferenceComponent', () => {
  let component: AuditExitConferenceComponent;
  let fixture: ComponentFixture<AuditExitConferenceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AuditExitConferenceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AuditExitConferenceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
