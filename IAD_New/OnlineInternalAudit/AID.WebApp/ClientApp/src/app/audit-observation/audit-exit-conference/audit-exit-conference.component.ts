import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroupDirective, FormGroup, FormControl, Validators, FormArray } from '@angular/forms';
import { AuditObservationService } from '../../services/audit-observation/audit-observation.service';
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { AuditsService } from '../../services/audits/audits.services';
import { CommonMsg } from '../../global/common-msg';
import swal from 'sweetalert2';

@Component({
  selector: 'app-audit-exit-conference',
  templateUrl: './audit-exit-conference.component.html',
  styleUrls: ['./audit-exit-conference.component.css']
})
export class AuditExitConferenceComponent implements OnInit {

  @ViewChild(FormGroupDirective) formGroupDirective: FormGroupDirective;

  selectedAudit: any;
  exitConferenceDetails: any;
  exitConferenceForm: FormGroup;
  auditTeam: any[] = [];
  selectedAuditTeam: any[] = [];
  public Editor = ClassicEditor;
  userId: number;
  roleId: number;
  saveButtonDisabled: boolean = false;
  saveButtonClicked: boolean = false;
  auditFromDate: Date = new Date();
  auditToDate: Date = new Date();
  rejectionRemark: string = '';
  RejectPopUp: boolean = false;

  constructor(private _service: AuditObservationService, private auditService: AuditsService, private _msg: CommonMsg) {
    this.userId = JSON.parse(localStorage.getItem("LoginData")).userId;
    this.roleId = JSON.parse(localStorage.getItem("LoginData")).roleId;
    this.selectedAudit = JSON.parse(localStorage.getItem("selectedAuditDetailsForExitConference"));
    this.auditFromDate = new Date(this.selectedAudit.auditFromDate);
    this.auditToDate = new Date(this.selectedAudit.auditToDate);
    this.getExitConferenceDetails();
    this.getAuditTeam();
  }

  createForm() {
    this.exitConferenceForm = new FormGroup({
      exitConferenceId: new FormControl(0),
      auditId: new FormControl(this.selectedAudit.auditId),
      conferenceDate: new FormControl('', Validators.required),
      agenda: new FormControl('', Validators.required),
      decision: new FormControl('', Validators.required),
      userId: new FormControl(this.userId),
      participants: new FormArray([])
    });
  }

  getExitConferenceDetails() {
    this._service.getAuditExitConference(this.selectedAudit.auditId).subscribe(response => {
      if (response && response.length > 0) {
        this.exitConferenceDetails = response[0];
        this.saveButtonDisabled = true;
      }
    });
  }

  getAuditTeam() {
    this.auditService.getApprovedAuditTeam(this.selectedAudit.auditId).subscribe(response => {
      this.auditTeam = response;
      if (!this.selectedAudit.exitConferenceId || this.selectedAudit.exitConferenceId == 0) {
        this.selectedAuditTeam = this.auditTeam;
      }
    });
  }

  onSubmit() {
    this.saveButtonClicked = true;
    if (this.exitConferenceForm.valid) {
      if (this.selectedAuditTeam.length > 0) {
        for (var i = 0; i < this.selectedAuditTeam.length; i++) {
          let ob = { Id: i + 1, AuditUserMapId: this.selectedAuditTeam[i].auditUserRoleId };
          this.exitConferenceForm.value.participants.push(ob);
        }
        this._service.updateAuditExitConference(this.exitConferenceForm.value).subscribe(response => {
          if (response > 0) {
            this.saveButtonClicked = false;
            swal(this._msg.updateMsg);
            this.createForm();
            this.saveButtonDisabled = true;
            this.getExitConferenceDetails();
            this.formGroupDirective.resetForm();
            this.selectedAuditTeam = [];
          }
        });
      }
      else {
        swal("Please select atleast one attendee.");
      }
    }
  }

  editExitConference() {
    this.exitConferenceForm = new FormGroup({
      exitConferenceId: new FormControl(this.exitConferenceDetails.exitConferenceId),
      auditId: new FormControl(this.selectedAudit.auditId),
      conferenceDate: new FormControl(this.exitConferenceDetails.conferenceDate, Validators.required),
      agenda: new FormControl(this.exitConferenceDetails.agenda, Validators.required),
      decision: new FormControl(this.exitConferenceDetails.decision, Validators.required),
      userId: new FormControl(this.userId),
      participants: new FormArray([])
    });
    this.selectedAuditTeam = [];
    for (var i = 0; i < this.exitConferenceDetails.participants.length; i++) {
      let emp = this.auditTeam.filter(a => a.auditUserRoleId == this.exitConferenceDetails.participants[i].auditUserMapId);
      if (emp.length > 0) {
        this.selectedAuditTeam.push(emp[0]);
      }
    }
    this.saveButtonDisabled = false;
  }

  updateStatus(statusId) {
    if (this.exitConferenceDetails) {
      let obj = { StatusId: statusId, ExitConferenceId: this.exitConferenceDetails.exitConferenceId, UserId: this.userId, RejectionRemark: this.rejectionRemark };
      this._service.updateExitConferenceStatus(obj).subscribe(response => {
        if (response > 0) {
          swal(this._msg.updateMsg);
          this.getExitConferenceDetails();
          this.RejectPopUp = false;
          this.rejectionRemark = '';
        }
      });
    }
  }

  ngOnInit() {
    this.createForm();
  }

}
