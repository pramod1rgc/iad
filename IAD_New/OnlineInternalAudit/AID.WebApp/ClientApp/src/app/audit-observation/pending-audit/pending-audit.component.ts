import { Component, OnInit } from '@angular/core';
import { AuditObservationService } from '../../services/audit-observation/audit-observation.service';
import { AuditsService } from '../../services/audits/audits.services';
import swal from 'sweetalert2';
import { CommonMsg } from '../../global/common-msg';
import { isNullOrUndefined } from 'util';

@Component({
  selector: 'app-pending-audit',
  templateUrl: './pending-audit.component.html',
  styleUrls: ['./pending-audit.component.css']
})
export class PendingAuditComponent implements OnInit {

  userId: number;
  roleId: number;
  auditList: any[] = [];
  selectedAuditId: any;
  selectedAudit: any;
  auditTeam: any[] = [];

  constructor(private _service: AuditObservationService, private auditService: AuditsService, private _msg: CommonMsg) {
    this.userId = JSON.parse(localStorage.getItem("LoginData")).userId;
    this.roleId = JSON.parse(localStorage.getItem("LoginData")).roleId;
  }

  getAuditList() {
    this._service.getAuditDHAuditList(this.userId).subscribe(response => {
      this.auditList = response;
      let auditData = JSON.parse(localStorage.getItem("selectedAuditDetailsForEntryConference") ? localStorage.getItem("selectedAuditDetailsForEntryConference") : JSON.stringify({auditId: 0}));
      if (auditData) {
        this.selectedAuditId = auditData.auditId;
        if (this.selectedAuditId > 0) {
          this.getAuditDetails();
        }
      }
    });
  }

  getAuditTeam() {
    this.auditService.getApprovedAuditTeam(this.selectedAuditId).subscribe(response => {
      this.auditTeam = response;
    });
  }

  getAuditDetails() {
    this.selectedAudit = this.auditList.filter(a => a.auditId == this.selectedAuditId)[0] as any;
    this.getAuditTeam();
    localStorage.setItem("selectedAuditDetailsForEntryConference", JSON.stringify(this.selectedAudit));
    localStorage.setItem("selectedAuditDetailsForExitConference", JSON.stringify(this.selectedAudit));
  }

  forwardToAuditHead(statusId) {
    let obj = { auditId: this.selectedAudit.auditId, statusId: statusId, userId: this.userId };
    this._service.updateAuditStatus(obj).subscribe(response => {
      if (response > 0) {
        switch (statusId) {
          case 19:
            swal(this._msg.forwardAuditHeadMsg);
            break;
          case 20:
            swal(this._msg.forwardSupervisorMsg);
            break;
        }

      }
    });
  }

  ngOnInit() {
    this.getAuditList();
  }

}
