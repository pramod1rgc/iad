import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ParaSettlementLetterComponent } from './para-settlement-letter.component';

describe('ParaSettlementLetterComponent', () => {
  let component: ParaSettlementLetterComponent;
  let fixture: ComponentFixture<ParaSettlementLetterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ParaSettlementLetterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ParaSettlementLetterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
