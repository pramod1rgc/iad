import { Component, OnInit, ViewChild } from '@angular/core';
import { EmployeeService } from '../../services/establishment/employee.service';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import swal from 'sweetalert2';
import { ReplaySubject, Subject } from 'rxjs';
import { FormControl } from '@angular/forms';
import { takeUntil } from 'rxjs/operators';
import { CommonMsg } from '../../global/common-msg';
export interface commondll {
  values: string;
  text: string;
}

export interface EmployeeData {
  empName: string;
  designation: string;
  email: string;
  mobileNo: string;
  phoneNo: string;
}
@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.css']
})

export class EmployeeComponent implements OnInit {

  constructor(private _EmployeeService: EmployeeService,private _CommonMsg: CommonMsg) { }
  //Variable
  displayedColumns: string[] = ['empName', 'designation', 'email', 'mobileNo', 'phoneNo', 'Action'];//, 'Active'];//, 'leaveTypeDesc', 'action',];
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild('frmempDetails') frmempDetails: any;
  public filteredDesig: ReplaySubject<commondll[]> = new ReplaySubject<commondll[]>(1);
  public DesigFilterCtrl: FormControl = new FormControl();
  public DesigCtrl: FormControl = new FormControl();
  private _onDestroy = new Subject<void>();
  ArrAllEmployee: any;
  ArrEmpdataSource: any;
  ArrDesig: any;
  MstempDetails: any = {};
  hdnEmpId = 0;
  Message: any;
  empID: string;
  ActiveDactiveHeaderText: string;
  checkUserIsactive: boolean;
  notFound = true;
  ActivateUser: boolean;
  stdCode: string='';

  ngOnInit() {

    this.GetAllEmployee();
    this.BindDropDownDesig();
    this.DesigFilterCtrl.valueChanges.pipe(takeUntil(this._onDestroy)).subscribe(() => { this.filterDesig(); });
  }
  //Bind Role DropDawnList
  GetAllEmployee() {
    //alert('Service');
    this._EmployeeService.GetAllEmployee().subscribe(result => {
      this.ArrAllEmployee = result;
      this.ArrEmpdataSource = new MatTableDataSource(result);
      this.ArrEmpdataSource.paginator = this.paginator;
      this.ArrEmpdataSource.sort = this.sort;
      //Filetr inData source
      this.ArrEmpdataSource.filterPredicate =
        (data: EmployeeData, filtersJson: string) => {
          const matchFilter = [];
          const filters = JSON.parse(filtersJson);

          filters.forEach(filter => {
            const val = data[filter.id] === null ? '' : data[filter.id];
            matchFilter.push(val.toLowerCase().includes(filter.value.toLowerCase()));
          });
          return matchFilter.every(Boolean);
        };
      //END Of Filetr inData source


    })
  }
  //End Of Bind Role DropDawnList

  BindDropDownDesig() {
    this._EmployeeService.BindDropDownDesig().subscribe(result => {
      debugger;
      this.ArrDesig = result;
      // this.MenuFilterCtrl.setValue(this.ArrMenus.parentMenu);
      this.filteredDesig.next(this.ArrDesig);
    })
  }



  //not use for filter search
  private filterDesig() {
    debugger;
    if (!this.filteredDesig) {
      return;
    }
    // get the search keyword
    let search = this.DesigFilterCtrl.value;
    if (!search) {

      this.filteredDesig.next(this.ArrDesig.slice());
      return;
    }
    else {
      search = search.toLowerCase();
    }
    // filter the banks
    //this.filteredMenu.next(

    //  this.ArrMenus.filter(ArrMenus => ArrMenus.text.toLowerCase().indexOf(search) > -1)
    //);
    var a = this.ArrDesig;
    this.filteredDesig.next(
      this.ArrDesig.filter(ArrMenus => ArrMenus.desigName.toLowerCase().indexOf(search) > -1)
    );
  }

  SaveEmpDetails() {
    this.MstempDetails.phoneNo = this.MstempDetails.stdCode + "-" + this.MstempDetails.phoneNo;
    this._EmployeeService.SaveEmployee(this.hdnEmpId, this.MstempDetails).subscribe(result => {
      this.Message = result;
      debugger;
      if (this.Message != undefined) {
        swal(this.Message)
      }

      this.GetAllEmployee();
      this.Cancel();
    })


  }
  UpdateFillForm(empId, empName, email, mobileNo, desigID, phoneNo) {
    // alert(empId)
    debugger;
    this.hdnEmpId = empId;
    this.MstempDetails.empName = empName;
    this.MstempDetails.desigName = desigID;
    this.MstempDetails.email = email;
    let ph = phoneNo.split("-");
    if (ph.length > 1) {
      this.MstempDetails.stdCode = ph[0];
      this.MstempDetails.phoneNo = ph[1];
    }
    else {
      this.MstempDetails.stdCode = '';
      this.MstempDetails.phoneNo = phoneNo;
    }
    this.MstempDetails.mobileNo = mobileNo;
    console.log(this.MstempDetails);
  }

  Cancel() {
    this.frmempDetails.resetForm();
    this.MstempDetails.stdCode = '';
  }
  ActiveDeactiveFillPopUpUser(empId, isActive) {
    debugger;
    //alert(mainMenuName);
    this.empID = empId;

    if (isActive == 'Active') {
      this.checkUserIsactive = false//true;
      this.ActiveDactiveHeaderText = 'Do you want to deactivate employee?';
    }
    else {
      this.checkUserIsactive = true//false;
      this.ActiveDactiveHeaderText = 'Do you want to activate employee?';
    }
  }
  ActiveDeactiveUser() {
    this.MstempDetails = {
      empID: this.empID,
      isActive: this.checkUserIsactive

    }
    JSON.stringify(this.MstempDetails);
    this._EmployeeService.ActiveDeactiveUser(this.MstempDetails).subscribe(data => {
      debugger;
      this.Message = data;
     // $('.dialog__close-btn').click();
      this.ActivateUser = false;
      swal(this.Message)
      this.GetAllEmployee();

    })
  }


  applyFilter(filterValue: string) {
    const tableFilters = [];
    tableFilters.push({
      id: 'empName',
      value: filterValue
    });


    this.ArrEmpdataSource.filter = JSON.stringify(tableFilters);
    //this.menudataSource.filter = filterValue.trim().toLowerCase();

    if (this.ArrEmpdataSource.paginator) {
      this.ArrEmpdataSource.paginator.firstPage();
    }
    if (this.ArrEmpdataSource.filteredData.length > 0) {
      this.notFound = true;
    } else {
      this.notFound = false;
    }
  }
  
  CancelPopUp() {
    this.ActivateUser = false;
    this.GetAllEmployee();
  }
  onEdit() {
    this._CommonMsg.onEditScrollTop();
  }

  


}
