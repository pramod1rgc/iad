import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EstablishmentModule } from './establishment.module';
import { EmployeeComponent } from '../establishment/employee/employee.component';

const routes: Routes = [
  {
    path: '', component: EstablishmentModule, data: {
      breadcrumb: 'Establishment'
    }, children: [
      {
        path: 'employee', component: EmployeeComponent, data: {
          breadcrumb: 'Employee'
        }
      }



    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EstablishmentRoutingModule { }
