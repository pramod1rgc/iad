import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from '../material.module';
import { SharedModule } from '../shared-module/shared-module.module';
import { MatSlideToggleModule } from '@angular/material';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { NgxMatSelectSearchModule } from 'ngx-mat-select-search';
import { EstablishmentRoutingModule } from './establishment-routing.module';
import { EmployeeComponent } from '../establishment/employee/employee.component';
import { MatTooltipModule } from '@angular/material/tooltip';

@NgModule({
  declarations: [EmployeeComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    EstablishmentRoutingModule,
    MaterialModule,
    SharedModule,
    NgxMatSelectSearchModule,
    MatSlideToggleModule,
    MatTooltipModule
  ]
})
export class EstablishmentModule { }
