import { TestBed } from '@angular/core/testing';

import { RiskCategoryService } from './risk-category.service';

describe('RiskCategoryService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: RiskCategoryService = TestBed.get(RiskCategoryService);
    expect(service).toBeTruthy();
  });
});
