import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { APP_CONFIG, AppConfig } from '../../global/global.module';
import { Observable } from 'rxjs'
import { map } from 'rxjs/operators';
@Injectable({
  providedIn: 'root'
})
export class RiskCategoryService {

  constructor(private _http: HttpClient, @Inject(APP_CONFIG) private config: AppConfig) { }

  SaveRiskCategory(hdnCategoryId, MstCategoryDetails) {
    //debugger;
    //alert(this._http.post(this.config.SaveEmployee + hdnempID, MstempDetails, { responseType: 'text' }))
    return this._http.post(this.config.SaveRiskCategory + hdnCategoryId, MstCategoryDetails, { responseType: 'text' }).pipe(map((resp: any) => { return resp.result }));
  }
  GetAllCategoryDetails(): Observable<any> {
    return this._http.get<any>(this.config.GetAllCategoryDetails, {}).pipe(map((resp: any) => { return resp.result }));
  }
  ActiveDeactive(CategoryDetailsModel) {
    return this._http.post(this.config.ActiveDeactive, CategoryDetailsModel, { responseType: 'text' }).pipe(map((resp: any) => { return resp.result }));
  }

  BindDropDawnCategory(): Observable<any> {
    return this._http.get<any>(this.config.BindDropDawnCategory, {}).pipe(map((resp: any) => { return resp.result }));
  }
}
