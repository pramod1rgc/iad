import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { APP_CONFIG, AppConfig } from '../../global/global.module';
import { map } from 'rxjs/operators';

@Injectable()

export class RoleServices {
  BaseUrl: any = [];
  headers: any;
  constructor(private httpclient: HttpClient, @Inject(APP_CONFIG) private config: AppConfig) {
    this.headers = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    }
  }


  getRoles(): Observable<any> {
    return this.httpclient.get(this.config.getRoles, this.headers).pipe(map((resp: any) => { return resp.result }));
  }

  upsertRoles(roleObj): Observable<any> {
    console.log(roleObj);
    return this.httpclient.post(this.config.upsertRoles, roleObj, this.headers).pipe(map((resp: any) => { return resp.result }));
  }

  toggleRoleActivation(obj): Observable<any> {
    return this.httpclient.post(this.config.toggleRoleActivation, obj, this.headers).pipe(map((resp: any) => { return resp.result }));
  }

}
