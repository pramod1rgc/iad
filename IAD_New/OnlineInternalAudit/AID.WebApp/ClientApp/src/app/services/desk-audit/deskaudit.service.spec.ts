import { TestBed } from '@angular/core/testing';

import { DeskauditService } from './deskaudit.service';

describe('DeskauditService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DeskauditService = TestBed.get(DeskauditService);
    expect(service).toBeTruthy();
  });
});
