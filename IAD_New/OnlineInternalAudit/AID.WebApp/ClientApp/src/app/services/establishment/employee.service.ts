import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { APP_CONFIG, AppConfig } from '../../global/global.module';
import { Observable } from 'rxjs'
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {

  constructor(private _http: HttpClient, @Inject(APP_CONFIG) private config: AppConfig) { }
  GetAllEmployee(): Observable<any> {
    return this._http.get<any>(this.config.getAllEmployee, {}).pipe(map((resp: any) => { debugger; return resp.result }));

  }
  BindDropDownDesig(): Observable<any> {
    return this._http.get<any>(this.config.getAllDesig, {}).pipe(map((resp: any) => { return resp.result }));

  }

  SaveEmployee(hdnempID, MstempDetails){
    //alert(hdnempID);
    return this._http.post(this.config.SaveEmployee + hdnempID, MstempDetails, { responseType: 'text' }).pipe(map((resp: any) => { return JSON.parse(resp).result }));
  }

  ActiveDeactiveUser(MstempDetails:any): Observable<any> {
   debugger;
    var res = this._http.post(this.config.ActiveDeactiveUser, MstempDetails, { responseType: 'text' }).pipe(map((resp: any) => {return JSON.parse(resp).result }));
   // alert(res);
    return res;//this._http.post(this.config.ActiveDeactiveUser, MstempDetails, { responseType: 'text' }).pipe(map((resp: any) => { return resp.result }));
   
  }
}
