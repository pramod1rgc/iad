import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { APP_CONFIG, AppConfig } from '../../global/global.module';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

class demo {
  paoid: number;

}


@Injectable({
  providedIn: 'root'
})
export class CreateannualplanService {
  headers: any;
  constructor(private _http: HttpClient, @Inject(APP_CONFIG) private config: AppConfig) {
    this.headers = {
      headers: new HttpHeaders({
        'Accept': 'application/json',
        'Content-type': 'application/json'
      })
    }
  }

  BindDropFYear() {
    //alert('sa')
    return this._http.get(this.config.BindDropFYear, {}).pipe(map((resp: any) => { return resp.result }));

  }

  
  getController() {
    //alert("getController");
    return this._http.get(this.config.getController, {}).pipe(map((resp: any) => { return resp.result }));

  }
  getAllPAO(ControllerID) {
    return this._http.get(this.config.getAllPAO + "?ControllerID=" + ControllerID, {}).pipe(map((resp: any) => { return resp.result }));
  }




  getauditlist(value): Observable<any> {
    return this._http.get<any>(this.config.getauditlist + '?FYID=' + value, this.headers).pipe(map((resp: any) => { return resp.result }));
  }


  assignAuditByController(value, Status, currFYear, paoreason): Observable<any> {
    return this._http.get<any>(this.config.assignAuditByController + '?PAOID=' + value + "&Status=" + Status + "&currFYear=" + currFYear + "&paoreason=" + paoreason, this.headers).pipe(map((resp: any) => { return resp.result }));
  }

  assignPaoAudit(requestObj) {
    debugger;
    // alert(ObjPaolist);
    return this._http.post(this.config.getassignPaoAudit, requestObj, { responseType: 'text' }).pipe(map((resp: any) => { return resp.result }));
  }


  //Qurtly Audit Details
  
  bindAuditInGrid(FYID): Observable<any>{
    return this._http.get(this.config.bindAuditInGrid + '?FYID=' + FYID, {}).pipe(map((resp: any) => { return resp.result }));

  }
  SaveQuartalyAudit(SaveQuartalyAudit): Observable<any> {
    //alert('sa')
    return this._http.post(this.config.SaveQuartalyAudit, SaveQuartalyAudit, { responseType: 'text' }).pipe(map((resp: any) => { return resp.result }));

  }

  assignToSupervisor(currFYear, roleUserMapId, userId): Observable<any> {
    return this._http.get(this.config.assignToSupervisor + '?currFYear=' + currFYear + '&roleUserMapId=' + roleUserMapId + '&userId=' + userId, this.headers).pipe(map((resp: any) => { return resp.result }));
  }
  //End Of
  //Audit Plan
  
  SaveQuartalyPlanAudit(AuditPaodata, financialYearId): Observable<any> {
    //alert('sa')
    return this._http.post(this.config.SaveQuartalyPlanAudit + "?financialYearId=" + financialYearId, AuditPaodata, { responseType: 'text' }).pipe(map((resp: any) => { return resp.result }));

  }
  //End Of Plan

  assignQuarterlyPlanToSupervisor(currFYear, supervisor, userId): Observable<any> {
    return this._http.get<any>(this.config.assignQuarterlyPlanToSupervisor + '?currFYear=' + currFYear + '&roleUserMapId=' + supervisor + '&userId=' + userId, this.headers).pipe(map((resp: any) => { return resp.result }));
  }
  ////pending method
  //BindSupervisorDropDown(userId) {
  //  //alert('sa')
  //  return this._http.get(this.config.BindSupervisorDropDown + '?userId=' + userId, {});

  //}
}
