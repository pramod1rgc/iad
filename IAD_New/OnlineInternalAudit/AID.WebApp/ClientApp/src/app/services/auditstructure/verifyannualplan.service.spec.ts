import { TestBed } from '@angular/core/testing';

import { VerifyannualplanService } from './verifyannualplan.service';

describe('VerifyannualplanService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: VerifyannualplanService = TestBed.get(VerifyannualplanService);
    expect(service).toBeTruthy();
  });
});
