import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { APP_CONFIG, AppConfig } from '../../global/global.module';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuditScheduleService {

  constructor(private _http: HttpClient, @Inject(APP_CONFIG) private config: AppConfig) { }

  BindDropFYear(): Observable<any> {
    // alert('sa')
    return this._http.get(this.config.BindDropFYear, {}).pipe(map((resp: any) => { return resp.result }));

  }

  getScheduledAudits(finYearId: number, quarter: string): Observable<any> {
    return this._http.get(this.config.getScheduledAudits + "?finYearId=" + finYearId + "&quarter=" + quarter, {}).pipe(map((resp: any) => { return resp.result }));
  }

  updateScheduleAudit(obj: any): Observable<any> {
    return this._http.post(this.config.updateScheduledAudit, obj).pipe(map((resp: any) => { return resp.result }));
  }

  deleteScheduledAudit(obj: any): Observable<any> {
    return this._http.post(this.config.deleteScheduledAudit, obj).pipe(map((resp: any) => { return resp.result }));
  }
}
