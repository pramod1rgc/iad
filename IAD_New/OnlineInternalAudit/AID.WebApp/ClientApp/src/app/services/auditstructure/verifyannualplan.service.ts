import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { APP_CONFIG, AppConfig } from '../../global/global.module';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class VerifyannualplanService {
  headers: any;

  constructor(private _http: HttpClient, @Inject(APP_CONFIG) private config: AppConfig) {
    this.headers = {
      headers: new HttpHeaders({
        'Accept': 'application/json',
        'Content-type': 'application/json'
      })
    }
  }
  BindDropFYear() {
    //alert('sa')
    return this._http.get(this.config.BindDropFYear, {}).pipe(map((resp: any) => { return resp.result }));

  }
  PendingFinancialForVerifyQuarter(roleUserMapId) {
    //alert('sa')
    return this._http.get(this.config.pendingFinancialForVerifyQuarter + '?roleUserMapId=' + roleUserMapId, {}).pipe(map((resp: any) => { return resp.result }));

  }
  getauditlist(value, RoleUserMapId): Observable<any> {
   
    return this._http.get<any>(this.config.Getauditlistforverify + '?FYID=' + value + '&RoleUserMapId=' + RoleUserMapId).pipe(map((resp: any) => { return resp.result }));
  }


  anualplanApprove(value, userId): Observable<any>  {
    
    return this._http.get<any>(this.config.anualplanApprove + '?FYID=' + value + '&RoleUserMapId=' + userId, this.headers).pipe(map((resp: any) => { return resp.result }));
   
  }
  rejectanualsAudit(FYID, rejectreson, userId): Observable<any> {
    
    return this._http.get<any>(this.config.rejectanualsAudit + '?FYID=' + FYID + '&rejectreson=' + rejectreson + '&RoleUserMapId=' + userId, this.headers).pipe(map((resp: any) => { return resp.result }));

  }


  //VerifyQuarterly
  Getauditlistforverify(financialYearId, Quarter, userId): Observable<any> {
   
    return this._http.get<any>(this.config.GetauditlistforverifyQuarterly + '?FYID=' + financialYearId + '&Quarter=' + Quarter + '&userId=' + userId).pipe(map((resp: any) => { return resp.result }));
  }

  quarterlylplanApprove(value, Quarter, userId): Observable<any> {
    return this._http.get<any>(this.config.quarterlylplanApprove + '?FYID=' + value + '&Quarter=' + Quarter + '&RoleUserMapId=' + userId, this.headers).pipe(map((resp: any) => { return resp.result }));

  }

  rejectQuarterlyAudit(FYID, rejectreson, Quarter, userId): Observable<any> {
    return this._http.get<any>(this.config.rejectQuarterlyAudit + '?FYID=' + FYID + '&rejectreson=' + rejectreson + '&Quarter=' + Quarter + '&RoleUserMapId=' + userId, this.headers).pipe(map((resp: any) => { return resp.result }));

  }


  getAllPendingQuarter(FYID, userId): Observable<any> {
    return this._http.get<any>(this.config.getAllPendingQuarter + '?FYID=' + FYID + '&userId=' + userId).pipe(map((resp: any) => { return resp.result }));

  }

  supervisorTosupervisor(currFYear, roleUserMapId, userId): Observable<any> {
    return this._http.get(this.config.supervisorTosupervisor + '?currFYear=' + currFYear + '&roleUserMapId=' + roleUserMapId + '&userId=' + userId, this.headers).pipe(map((resp: any) => { return resp.result }));

  }

  supervisorTosupervisorQuarterlyPlan(currFYear, roleUserMapId, userId, selectedQuarter): Observable<any> {
    debugger;
    //alert(this.config.Getauditlistforverify + '?FYID=' + value);
    //return this._http.get<any>('');// this.config.supervisorTosupervisor + '?uID=' + supervisor
    return this._http.get(this.config.supervisorTosupervisorQuarterlyPlan + '?currFYear=' + currFYear + '&roleUserMapId=' + roleUserMapId + '&userId=' + userId + '&Quarter=' + selectedQuarter, this.headers).pipe(map((resp: any) => { return resp.result }));

  }
  //End Of 
}
