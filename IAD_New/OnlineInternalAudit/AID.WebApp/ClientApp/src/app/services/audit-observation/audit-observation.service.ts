import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { APP_CONFIG, AppConfig } from '../../global/global.module';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuditObservationService {

  constructor(private _http: HttpClient, @Inject(APP_CONFIG) private config: AppConfig) { }

  BindDropFYear(): Observable<any> {
    // alert('sa')
    return this._http.get(this.config.BindDropFYear, {}).pipe(map((resp: any) => { return resp.result }));

  }

  getAuditDHAuditList(auditUserId): Observable<any> {
    return this._http.get(this.config.getAuditDHAuditList + "?auditUserId=" + auditUserId, {}).pipe(map((resp: any) => { return resp.result }));
  }

  getAuditEntryConference(auditId): Observable<any> {
    return this._http.get(this.config.getAuditEntryConference + "?auditId=" + auditId, {}).pipe(map((resp: any) => { return resp.result }));
  }

  updateAuditEntryConference(obj): Observable<any> {
    return this._http.post(this.config.updateAuditEntryConference, obj).pipe(map((resp: any) => { return resp.result }));
  }

  getAuditExitConference(auditId): Observable<any> {
    return this._http.get(this.config.getAuditExitConference + "?auditId=" + auditId, {}).pipe(map((resp: any) => { return resp.result }));
  }

  updateAuditExitConference(obj): Observable<any> {
    return this._http.post(this.config.updateAuditExitConference, obj).pipe(map((resp: any) => { return resp.result }));
  }

  getAuditParas(auditId, pageSize, pageNumber, searchTerm): Observable<any> {
    return this._http.get(this.config.getAuditPara + "?auditId=" + auditId + "&pageSize=" + pageSize + "&pageNumber=" + pageNumber + "&searchTerm=" + searchTerm, {}).pipe(map((resp: any) => { return resp.result }));
  }

  getAuditParaDetails(paraId): Observable<any> {
    return this._http.get(this.config.getAuditParaDetail + "?paraId=" + paraId, {}).pipe(map((resp: any) => { return resp.result }));
  }

  updateAuditPara(obj, files): Observable<any> {
    const formData: FormData = new FormData();
    if (files) {
      for (var i = 0; i < files.length; i++) {
        formData.append('UploadFiles', files[i]);
      }
    }
    formData.append('AuditId', obj.auditId);
    formData.append('ParaId', obj.paraId);
    formData.append('Title', obj.title.toString());
    formData.append('Criteria', obj.criteria.toString());
    formData.append('Conditions', obj.conditions.toString());
    formData.append('Consequences', obj.consequences.toString());
    formData.append('Causes', obj.causes.toString());
    formData.append('CorrectiveActions', obj.correctiveActions.toString());
    formData.append('Severity', obj.severity.toString());
    formData.append('RiskCategoryId', obj.riskCategoryId);
    formData.append('UserId', obj.userId);
    formData.append('FinancialImplications', JSON.stringify(obj.paraFinancialImplications));
    return this._http.post(this.config.updateAuditPara, formData).pipe(map((resp: any) => { return resp.result }));
  }

  deleteAuditPara(obj): Observable<any> {
    return this._http.post(this.config.deleteAuditPara, obj).pipe(map((resp: any) => { return resp.result }));
  }

  postParaComment(obj, files): Observable<any> {
    const formData: FormData = new FormData();
    if (files) {
      for (var i = 0; i < files.length; i++) {
        formData.append('UploadFiles', files[i]);
      }
    }
    formData.append('AuditId', obj.auditId);
    formData.append('ParaId', obj.paraId);
    formData.append('Comment', obj.comment.toString());
    formData.append('UserId', obj.userId);
    return this._http.post(this.config.postParaComment, formData).pipe(map((resp: any) => { return resp.result }));
  }

  getParaComments(paraId): Observable<any> {
    return this._http.get(this.config.getParaComments + "?paraId=" + paraId, {}).pipe(map((resp: any) => { return resp.result }));
  }

  getAuditParaSettlementLetter(auditId): Observable<any> {
    return this._http.get(this.config.getAuditParaSettlementLetter + "?auditId=" + auditId, {}).pipe(map((resp: any) => { return resp.result }));
  }

  updateAuditParaSettlementLetter(obj): Observable<any> {
    return this._http.post(this.config.updateAuditParaSettlementLetter, obj).pipe(map((resp: any) => { return resp.result }));
  }

  updateAuditParaSettlementLetterStatus(auditId, statusId, rejectionRemark): Observable<any> {
    return this._http.get(this.config.updateAuditParaSettlementLetterStatus + "?auditId=" + auditId + "&statusId=" + statusId + "&rejectionRemark=" + rejectionRemark, {}).pipe(map((resp: any) => { return resp.result }));
  }

  updateAuditStatus(obj): Observable<any> {
    return this._http.post(this.config.updateAuditStatus, obj).pipe(map((resp: any) => { return resp.result }));
  }

  updateEntryConferenceStatus(obj): Observable<any> {
    return this._http.post(this.config.updateEntryConferenceStatus, obj).pipe(map((resp: any) => { return resp.result }));
  }

  updateExitConferenceStatus(obj): Observable<any> {
    return this._http.post(this.config.updateExitConferenceStatus, obj).pipe(map((resp: any) => { return resp.result }));
  }

  updateAuditParaStatus(obj): Observable<any> {
    return this._http.post(this.config.updateAuditParaStatus, obj).pipe(map((resp: any) => { return resp.result }));
  }

  updateAuditComplianceStatus(obj): Observable<any> {
    return this._http.post(this.config.updateAuditComplianceStatus, obj).pipe(map((resp: any) => { return resp.result }));
  }

  getFinancialImplicationsForDropdown(): Observable<any> {
    return this._http.get(this.config.getAllFinancialImplicationCases, {}).pipe(map((resp: any) => { return resp.result }));
  }

  getAuditParaFinancialImplications(paraId: any): Observable<any> {
    return this._http.get(this.config.getAuditParaFinancialImplicationCases + "?paraId=" + paraId, {}).pipe(map((resp: any) => { return resp.result }));
  }
}
