import {EventEmitter, Injectable, Inject} from '@angular/core';
import {Event, NavigationEnd, Router} from '@angular/router';
import {BehaviorSubject, Observable} from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { APP_CONFIG, AppConfig } from '../../global/global.module';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class CommonService {
  public sideNav: any;
  public currentUrl = new BehaviorSubject<string>(undefined);

  constructor(private _http: HttpClient, @Inject(APP_CONFIG) private config: AppConfig) { }

  bindSupervisorDropDown(): Observable<any> {
    return this._http.get(this.config.bindSupervisorDropDown + "?userId=" + sessionStorage.getItem('userId'), {}).pipe(map((resp: any) => { return resp.result }));

  }
}
