import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { APP_CONFIG, AppConfig } from '../../global/global.module';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
@Injectable({
  providedIn: 'root'
})
export class MenuserviceService {

  constructor(private _http: HttpClient, @Inject(APP_CONFIG) private config: AppConfig) {

  }
  BindMenuInGride(): Observable<any> {
    debugger;
    return this._http.get<any>(this.config.getMenusData, {}).pipe(map((resp: any) => { return resp.result }));

  }
  BindDropDownMenu() {
    return this._http.get(this.config.bindDropDownMenu, {}).pipe(map((resp: any) => { return resp.result }));

  }

  GetAllPredefineRole() {
    return this._http.get(this.config.getAllPredefineRole, {}).pipe(map((resp: any) => { return resp.result }));

  }
}
