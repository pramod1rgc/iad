import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AppConfig, APP_CONFIG } from '../../global/global.module'
import { ResponseContentType } from '@angular/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class DashboardService {

  constructor(private httpclient: HttpClient, @Inject(APP_CONFIG) private config: AppConfig) { }


  getAllMenus(roleId): Observable<any> {
    return this.httpclient.get(this.config.getAllMenus + "?roleId=" + roleId, {}).pipe(map((resp: any) => { return resp.result }));
  }


  getDashboardAllRecord(roleId) {
    return this.httpclient.get(this.config.getDashboardAllRecord + "?roleId=" + roleId, {}).pipe(map((resp: any) => { return resp.result }));
  }
  getIADHeadDashboardRecord(roleId) {
    return this.httpclient.get(this.config.getIADHeadDashboardRecord + "?roleId=" + roleId, {}).pipe(map((resp: any) => { return resp.result }));
  }
  getAuditDHDashboardRecord(userId) {
    return this.httpclient.get(this.config.getAuditDHDashboardRecord + "?userId=" + userId, {}).pipe(map((resp: any) => { return resp.result }));
  }

  getallschedule() {
    return this.httpclient.get(this.config.getallschedule, {}).pipe(map((resp: any) => { return resp.result }));
  }

  GetallAuditInchart() {
    return this.httpclient.get(this.config.getallAuditInchart, {}).pipe(map((resp: any) => { return resp.result }));
  }
  GetallAuditDHInchart(userid) {

    return this.httpclient.get(this.config.getallAuditDHInchart + "?userId=" + userid, {}).pipe(map((resp: any) => { return resp.result }));
  }
  GetallIAdHeadAuditInchart(userid) {

    return this.httpclient.get(this.config.getallIAdHeadInchart + "?userId=" + userid, {}).pipe(map((resp: any) => { return resp.result }));
  }
  
 
   
 
}
