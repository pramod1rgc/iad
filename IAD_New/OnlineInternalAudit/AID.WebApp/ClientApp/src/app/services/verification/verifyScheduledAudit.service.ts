import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { APP_CONFIG, AppConfig } from '../../global/global.module';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class VerifyScheduledAuditService {

  constructor(private _http: HttpClient, @Inject(APP_CONFIG) private config: AppConfig) { }

  BindDropFYear(): Observable<any> {
    // alert('sa')
    return this._http.get(this.config.BindDropFYear, {}).pipe(map((resp: any) => { return resp.result }));

  }

  getScheduledAuditsForVerification(finYearId: number, quarter: string, roleUserMapId: number): Observable<any> {
    return this._http.get(this.config.getScheduledAuditsForVerification + "?finYearId=" + finYearId + "&quarter=" + quarter + "&roleUserMapId=" + roleUserMapId, {}).pipe(map((resp: any) => { return resp.result }));
  }

  updateScheduledAuditForVerification(obj: any): Observable<any> {
    return this._http.post(this.config.updateScheduledAuditsForVerification, obj).pipe(map((resp: any) => { return resp.result }));
  }

  getPendingTaskCount(roleUserMapId): Observable<any> {
    return this._http.get(this.config.getPendingTaskCount + "?RoleUserMapId=" + roleUserMapId, {}).pipe(map((resp: any) => { return resp.result }));
  }

  getDistinctQuartersBasedOnStatusAndFY(fyId, statusId, statusId2): Observable<any> {
    return this._http.get(this.config.getDistinctQuartersBasedOnStatusAndFY + "?fyId=" + fyId + "&statusId=" + statusId + "&statusId2=" + statusId2, {}).pipe(map((resp: any) => { return resp.result }));
  }
}
