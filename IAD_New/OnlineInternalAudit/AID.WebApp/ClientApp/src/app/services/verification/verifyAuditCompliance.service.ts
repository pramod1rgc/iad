import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { APP_CONFIG, AppConfig } from '../../global/global.module';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class VerifyAuditComplianceService {

  constructor(private _http: HttpClient, @Inject(APP_CONFIG) private config: AppConfig) { }

  getAuditsForwardedToSupervisor(): Observable<any> {
    return this._http.get(this.config.getAuditsForwardedToSupervisor, {}).pipe(map((resp: any) => { return resp.result }));
  }

  getAuditsWhoseParasOrComplainceForwardedToSupervisor(type): Observable<any> {
    return this._http.get(this.config.getAuditsWhoseParasOrComplainceForwardedToSupervisor + "?type=" + type, {}).pipe(map((resp: any) => { return resp.result }));
  }

  getAuditsWhoseSettlementLetterForwardedToSupervisor(): Observable<any> {
    return this._http.get(this.config.getAuditsWhoseSettlementLetterForwardedToSupervisor, {}).pipe(map((resp: any) => { return resp.result }));
  }
}
