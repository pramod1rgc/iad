import { TestBed } from '@angular/core/testing';

import { AuditeeObservationService } from './auditee-observation.service';

describe('AuditeeObservationService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AuditeeObservationService = TestBed.get(AuditeeObservationService);
    expect(service).toBeTruthy();
  });
});
