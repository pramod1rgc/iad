import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { APP_CONFIG, AppConfig } from '../../global/global.module';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Jsonp } from '@angular/http';
@Injectable({
  providedIn: 'root'
})
export class AuditeeObservationService {

  constructor(private _http: HttpClient, @Inject(APP_CONFIG) private config: AppConfig) { }

  BindDropFYear(): Observable<any> {

    return this._http.get(this.config.BindDropFYear, {}).pipe(map((resp: any) => { return resp.result }));

  }

  getdownloadDetails(url: string) {
    return this._http.get(this.config.api_base_url + 'AuditeeObservation/DownloadFile?url=' + url, { responseType: 'arraybuffer' }).pipe(map((resp: any) => { return resp }));
  }


  getAuditDHAuditList(auditUserId): Observable<any> {
    var Result = this._http.get(this.config.getAuditeeDHAuditList + "?auditUserId=" + auditUserId, {}).pipe(map((resp: any) => {return resp}));
    return Result;
  }

  getAuditEntryConference(auditId): Observable<any> {
    return this._http.get(this.config.getAuditeeEntryConference + "?auditId=" + auditId, {}).pipe(map((resp: any) => { return resp }));
  }

  updateAuditEntryConference(obj): Observable<any> {
    return this._http.post(this.config.updateAuditeeEntryConference, obj).pipe(map((resp: any) => { return resp  }));
  }

  getAuditExitConference(auditId): Observable<any> {
    return this._http.get(this.config.getAuditeeExitConference + "?auditId=" + auditId, {}).pipe(map((resp: any) => { return resp }));
  }

  updateAuditExitConference(obj): Observable<any> {
    return this._http.post(this.config.updateAuditeeExitConference, obj).pipe(map((resp: any) => { return resp }));
  }

  getAuditParas(roleId,auditId, pageSize, pageNumber, searchTerm): Observable<any> {
    return this._http.get(this.config.getAuditeePara + "?roleId=" + roleId + "&auditId=" + auditId + "&pageSize=" + pageSize + "&pageNumber=" + pageNumber + "&searchTerm=" + searchTerm, {}).pipe(map((resp: any) => { return resp }));
  }


  getAuditParaDetails(paraId): Observable<any> {
    return this._http.get(this.config.getAuditeeParaDetail + "?paraId=" + paraId, {}).pipe(map((resp: any) => { return resp  }));
  }

  updateAuditPara(obj, files): Observable<any> {
    const formData: FormData = new FormData();
    if (files) {
      for (var i = 0; i < files.length; i++) {
        formData.append('UploadFiles', files[i]);
      }
    }
    formData.append('AuditId', obj.auditId);
    formData.append('ParaId', obj.paraId);
    formData.append('Title', obj.title.toString());
    formData.append('Criteria', obj.criteria.toString());
    formData.append('Conditions', obj.conditions.toString());
    formData.append('Consequences', obj.consequences.toString());
    formData.append('Causes', obj.causes.toString());
    formData.append('CorrectiveActions', obj.correctiveActions.toString());
    formData.append('Severity', obj.severity.toString());
    formData.append('RiskCategoryId', obj.riskCategoryId);
    formData.append('UserId', obj.userId);
    return this._http.post(this.config.updateAuditeePara, formData).pipe(map((resp: any) => { return resp  }));
  }

  deleteAuditPara(obj): Observable<any> {
    return this._http.post(this.config.deleteAuditeePara, obj).pipe(map((resp: any) => { return resp  }));
  }

  postParaComment(obj, files): Observable<any> {
    const formData: FormData = new FormData();
    if (files) {
      for (var i = 0; i < files.length; i++) {
        formData.append('UploadFiles', files[i]);
      }
    }
    formData.append('AuditId', obj.auditId);
    formData.append('ParaId', obj.paraId);
    formData.append('Comment', obj.comment.toString());
    formData.append('UserId', obj.userId);
    return this._http.post(this.config.postAuditeeParaComment, formData).pipe(map((resp: any) => { return resp  }));
  }

  getParaComments(paraId): Observable<any> {
    debugger;
    return this._http.get(this.config.getParaComments + "?paraId=" + paraId, {}).pipe(map((resp: any) => { return resp.result  }));
  }

  getAuditParaSettlementLetter(auditId): Observable<any> {
    return this._http.get(this.config.getAuditeeParaSettlementLetter + "?auditId=" + auditId, {}).pipe(map((resp: any) => { return resp  }));
  }

  updateAuditParaSettlementLetter(obj): Observable<any> {
    return this._http.post(this.config.updateAuditeeParaSettlementLetter, obj).pipe(map((resp: any) => { return resp  }));
  }

  updateAuditParaSettlementLetterStatus(auditId, statusId, rejectionRemark): Observable<any> {
    return this._http.get(this.config.updateAuditeeParaSettlementLetterStatus + "?auditId=" + auditId + "&statusId=" + statusId + "&rejectionRemark=" + rejectionRemark, {}).pipe(map((resp: any) => { return resp }));
  }

  updateAuditStatus(obj): Observable<any> {
    return this._http.post(this.config.updateAuditeeStatus, obj).pipe(map((resp: any) => { return resp }));
  }

  updateAuditParaStatus(obj): Observable<any> {
    return this._http.post(this.config.updateAuditeeParaStatus, obj).pipe(map((resp: any) => { return resp }));
  }

}
