import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { APP_CONFIG, AppConfig } from '../../global/global.module';
import { map } from 'rxjs/operators';

@Injectable()

export class AssignUserServices {
  BaseUrl: any = [];
  headers: any;
  constructor(private httpclient: HttpClient, @Inject(APP_CONFIG) private config: AppConfig) {
    this.headers = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    }
  }


  getUserRoleEmployees(roleId): Observable<any> {
    return this.httpclient.get(this.config.getUserRoleEmployees + "?roleId=" + roleId, this.headers).pipe(map((resp: any) => { return resp.result }));
  }

  assignUserRole(obj): Observable<any> {
    return this.httpclient.post(this.config.assignUserRole, obj, this.headers).pipe(map((resp: any) => { return resp.result }));
  }

  getAuditsForDropdown(): Observable<any> {
    return this.httpclient.get(this.config.getAuditsForDropdown, this.headers).pipe(map((resp: any) => { return resp.result }));
  }

  getAssignAuditTeam(auditId: number): Observable<any> {
    return this.httpclient.get(this.config.getAssignAuditTeam + "?auditId=" + auditId, this.headers).pipe(map((resp: any) => { return resp.result }));
  }

  checkIfUserAlreadyAssignedToAnotherAudit(obj: any): Observable<any> {
    return this.httpclient.get(this.config.checkIfUserAlreadyAssignedToAnotherAudit, obj).pipe(map((resp: any) => { return resp.result }));
  }

  assignAuditTeam(obj): Observable<any> {
    return this.httpclient.post(this.config.assignAuditTeam, obj, this.headers).pipe(map((resp: any) => { return resp.result }));
  }
}
