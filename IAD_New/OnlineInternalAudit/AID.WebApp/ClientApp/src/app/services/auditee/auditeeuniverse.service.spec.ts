import { TestBed } from '@angular/core/testing';

import { AuditeeuniverseService } from './auditeeuniverse.service';

describe('AuditeeuniverseService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AuditeeuniverseService = TestBed.get(AuditeeuniverseService);
    expect(service).toBeTruthy();
  });
});
