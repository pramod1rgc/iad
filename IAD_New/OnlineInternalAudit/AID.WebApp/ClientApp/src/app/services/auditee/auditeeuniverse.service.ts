import { Injectable,Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { APP_CONFIG, AppConfig } from '../../global/global.module';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
@Injectable({
  providedIn: 'root'
})
export class AuditeeuniverseService {

  constructor(private _http: HttpClient, @Inject(APP_CONFIG) private config: AppConfig) { }
  GetAllAuditee(userId): Observable<any> {
    return this._http.get<any>(this.config.GetAllAuditee + '?userId=' + userId, {}).pipe(map((resp: any) => { return resp.result }));
  }
  BindDropDownDesig(rollId): Observable<any> {
    return this._http.get<any>(this.config.AuditeeDesignation + '?rollId=' + rollId, {}).pipe(map((resp: any) => { return resp.result }));
  }
  BindDropDownRole(): Observable<any> {
    return this._http.get<any>(this.config.AuditeeRole, {}).pipe(map((resp: any) => { return resp.result }));
  }
  SaveAuditee(uID, MstDetails) {
    return this._http.post(this.config.SaveAuditee + "?uID=" + uID, MstDetails, { responseType: 'text' }).pipe(map((resp: any) => { return JSON.parse(resp).result }));
  }

  ActiveDeactiveUser(MstDetails: any): Observable<any> {
    return this._http.post(this.config.ActiveDeactiveAuditee, MstDetails, { responseType: 'text' }).pipe(map((resp: any) => { return JSON.parse(resp).result }));
  }
}
