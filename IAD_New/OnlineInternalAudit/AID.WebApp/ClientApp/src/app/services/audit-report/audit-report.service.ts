import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { APP_CONFIG, AppConfig } from '../../global/global.module';
import { Observable } from 'rxjs';
import { audit, map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuditReportService {

  constructor(private _http: HttpClient, @Inject(APP_CONFIG) private config: AppConfig) { }

  BindDropFYear(): Observable<any> {
    // alert('sa')
    return this._http.get(this.config.BindDropFYear, {}).pipe(map((resp: any) => { return resp.result }));

  }

  getAuditsForAuditReportBySupervisor(pageSize, pageNumber, searchTerm): Observable<any> {
    return this._http.get(this.config.getAuditsForAuditReportBySupervisor + "?pageSize=" + pageSize + "&pageNumber=" + pageNumber + "&searchTerm=" + searchTerm, {}).pipe(map((resp: any) => {return resp.result}));
  }

  getAuditReport(auditId): Observable<any> {
    return this._http.get(this.config.getAuditReport + "?auditId=" + auditId, {}).pipe(map((resp: any) => { return resp.result }));
  }
}
