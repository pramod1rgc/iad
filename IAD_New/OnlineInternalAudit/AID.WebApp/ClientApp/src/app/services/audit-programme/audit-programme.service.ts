import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { APP_CONFIG, AppConfig } from '../../global/global.module';
import { Observable } from 'rxjs';
import { audit, map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuditProgrammeService {

  constructor(private _http: HttpClient, @Inject(APP_CONFIG) private config: AppConfig) { }

  BindDropFYear(): Observable<any> {
    // alert('sa')
    return this._http.get(this.config.BindDropFYear, {}).pipe(map((resp: any) => { return resp.result }));

  }

  getAuditProgrammes(finYear): Observable<any> {
    return this._http.get(this.config.getAuditProgramme + "?finYear=" + finYear, {}).pipe(map((resp: any) => { return resp.result }));
  }

}
