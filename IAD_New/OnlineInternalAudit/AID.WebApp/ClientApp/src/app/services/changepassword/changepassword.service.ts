import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { APP_CONFIG, AppConfig } from '../../global/global.module';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
@Injectable({
  providedIn: 'root'
})
export class ChangepasswordService {
  constructor(private _http: HttpClient, @Inject(APP_CONFIG) private config: AppConfig) { }
  ChangePassword(requestmodel): Observable<any> {
    return this._http.post(this.config.ChangePassword, requestmodel, { responseType: 'text' }).pipe(map((resp: any) => { return resp.result }));
  }
}
