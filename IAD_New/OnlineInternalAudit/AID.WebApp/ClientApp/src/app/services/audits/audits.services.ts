import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { APP_CONFIG, AppConfig } from '../../global/global.module';
import { Observable } from 'rxjs';
import { audit, map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuditsService {

  constructor(private _http: HttpClient, @Inject(APP_CONFIG) private config: AppConfig) { }

  BindDropFYear(): Observable<any> {
    // alert('sa')
    return this._http.get(this.config.BindDropFYear, {}).pipe(map((resp: any) => { return resp.result }));

  }

  getApprovedScheduledAudits(finYearId: number, quarter: string, roleUserMapId: number): Observable<any> {
    return this._http.get(this.config.getApprovedScheduledAudits + "?finYearId=" + finYearId + "&quarter=" + quarter + "&roleUserMapId=" + roleUserMapId, {}).pipe(map((resp: any) => { return resp.result }));
  }

  updateScheduledAuditForVerification(obj: any): Observable<any> {
    return this._http.post(this.config.updateScheduledAuditsForVerification, obj).pipe(map((resp: any) => { return resp.result }));
  }

  getApprovedAuditTeamAuditsForDropdown(roleUserMapId): Observable<any> {
    return this._http.get(this.config.getApprovedAuditTeamAuditsForDropdown + "?roleUserMapId=" + roleUserMapId, {}).pipe(map((resp: any) => { return resp.result }));
  }

  getApprovedAuditTeam(auditId: any): Observable<any> {
    return this._http.get(this.config.GetApprovedAuditTeam + "?auditId=" + auditId, {}).pipe(map((resp: any) => { return resp.result }));
  }

  revertAuditTeam(obj): Observable<any> {
    return this._http.post(this.config.revertAuditTeam, obj).pipe(map((resp: any) => { return resp.result }));
  }
}
