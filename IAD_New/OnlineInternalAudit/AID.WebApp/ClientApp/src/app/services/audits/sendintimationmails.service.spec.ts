import { TestBed } from '@angular/core/testing';

import { SendintimationmailsService } from './sendintimationmails.service';

describe('SendintimationmailsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SendintimationmailsService = TestBed.get(SendintimationmailsService);
    expect(service).toBeTruthy();
  });
});
