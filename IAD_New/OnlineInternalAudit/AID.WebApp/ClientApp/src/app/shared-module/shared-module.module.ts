import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DialogComponent } from './dialog/dialog.component';
import { MaterialModule } from '../material.module';
 
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxMatSelectSearchModule } from 'ngx-mat-select-search';
import { BlankSpaceDirective } from './blank-space-textbox.directive';

@NgModule({
  declarations: [DialogComponent, BlankSpaceDirective],
  imports: [
    CommonModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    NgxMatSelectSearchModule
  ],
  exports: [DialogComponent, BlankSpaceDirective]
})
export class SharedModule { }

