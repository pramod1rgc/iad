import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AuditeeEntryConfrenceComponent } from './auditee-entry-confrence.component';

describe('AuditeeEntryConfrenceComponent', () => {
  let component: AuditeeEntryConfrenceComponent;
  let fixture: ComponentFixture<AuditeeEntryConfrenceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AuditeeEntryConfrenceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AuditeeEntryConfrenceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
