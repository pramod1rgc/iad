import { Component, OnInit, ViewChild } from '@angular/core';
import { AuditeeObservationService} from '../../services/auditee_observation/auditee-observation.service';
import { FormGroup, FormControl, Validators, FormArray, FormGroupDirective } from '@angular/forms';
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { AuditsService } from '../../services/audits/audits.services';
import swal from 'sweetalert2';
import { CommonMsg } from '../../global/common-msg';

@Component({
  selector: 'app-auditee-entry-confrence',
  templateUrl: './auditee-entry-confrence.component.html',
  styleUrls: ['./auditee-entry-confrence.component.css']
})
export class AuditeeEntryConfrenceComponent implements OnInit {


  @ViewChild(FormGroupDirective) formGroupDirective: FormGroupDirective;

  selectedAudit: any;
  entryConferenceDetails: any;
  entryConferenceForm: FormGroup;
  auditTeam: any[] = [];
  selectedAuditTeam: any[] = [];
  public Editor = ClassicEditor;
  userId: number;
  roleId: number;
  saveButtonDisabled: boolean = false;
  saveButtonClicked: boolean = false;

  constructor(private _service: AuditeeObservationService, private auditService: AuditsService, private _msg: CommonMsg) {
    this.userId = JSON.parse(localStorage.getItem("LoginData")).userId;
    this.roleId = JSON.parse(localStorage.getItem("LoginData")).roleId;
    this.selectedAudit = JSON.parse(localStorage.getItem("selectedAuditDetailsForEntryConference"));
    this.getEntryConferenceDetails();
    this.getAuditTeam();
  }

  createForm() {
    this.entryConferenceForm = new FormGroup({
      entryConferenceId: new FormControl(0),
      auditId: new FormControl(this.selectedAudit.auditId),
      conferenceDate: new FormControl('', Validators.required),
      agenda: new FormControl('', Validators.required),
      userId: new FormControl(this.userId),
      participants: new FormArray([])
    });
  }

  getEntryConferenceDetails() {

    if (this.selectedAudit.entryConferenceId > 0) {
      this._service.getAuditEntryConference(this.selectedAudit.auditId).subscribe(response => {
        this.entryConferenceDetails = response[0];
        this.saveButtonDisabled = true;
      });
    }
  }

  getAuditTeam() {

    this.auditService.getApprovedAuditTeam(this.selectedAudit.auditId).subscribe(response => {
      this.auditTeam = response;
      if (!this.selectedAudit.entryConferenceId || this.selectedAudit.entryConferenceId == 0) {
        this.selectedAuditTeam = this.auditTeam;
      }
    });
  }

  onSubmit() {

    this.saveButtonClicked = true;
    if (this.entryConferenceForm.valid) {
      if (this.selectedAuditTeam.length > 0) {
        for (var i = 0; i < this.selectedAuditTeam.length; i++) {
          let ob = { Id: i + 1, AuditUserMapId: this.selectedAuditTeam[i].auditUserRoleId };
          this.entryConferenceForm.value.participants.push(ob);
        }
        this._service.updateAuditEntryConference(this.entryConferenceForm.value).subscribe(response => {
          if (response > 0) {
            this.saveButtonClicked = false;
            swal(this._msg.updateMsg);
            this.createForm();
            this.saveButtonDisabled = true;
            this.getEntryConferenceDetails();
            this.formGroupDirective.resetForm();
            this.selectedAuditTeam = [];
          }
        });
      }
      else {
        swal("Please select atleast one attendee.");
      }
    }
  }

  editEntryConference() {

    this.entryConferenceForm = new FormGroup({
      entryConferenceId: new FormControl(this.entryConferenceDetails.entryConferenceId),
      auditId: new FormControl(this.selectedAudit.auditId),
      conferenceDate: new FormControl(this.entryConferenceDetails.conferenceDate, Validators.required),
      agenda: new FormControl(this.entryConferenceDetails.agenda, Validators.required),
      userId: new FormControl(this.userId),
      participants: new FormArray([])
    });
    this.selectedAuditTeam = [];
    for (var i = 0; i < this.entryConferenceDetails.participants.length; i++) {
      let emp = this.auditTeam.filter(a => a.auditUserRoleId == this.entryConferenceDetails.participants[i].auditUserMapId);
      if (emp.length > 0) {
        this.selectedAuditTeam.push(emp[0]);
      }
    }
    this.saveButtonDisabled = false;
  }

  ngOnInit() {
    this.createForm();
  }



}
