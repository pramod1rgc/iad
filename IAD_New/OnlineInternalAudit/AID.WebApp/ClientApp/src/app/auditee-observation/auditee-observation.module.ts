import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuditeeObservationRoutingModule } from './auditee-observation-routing.module';
import { PendingAuditeeComponent } from '../auditee-observation/pending-auditee/pending-auditee.component';
import { MatSelectModule, MatCardModule, MatDatepickerModule, MatFormFieldModule, MatCheckboxModule } from '@angular/material';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from '../material.module';
import { CKEditorModule } from '@ckeditor/ckeditor5-angular';
import { CommonMsg } from '../global/common-msg';
import { SharedModule } from '../shared-module/shared-module.module';
import {AuditeeParaSettlementLetterComponent  } from './auditee-para-settlement-letter/auditee-para-settlement-letter.component';
import { AuditeeExitConferenceComponent } from './auditee-exit-conference/auditee-exit-conference.component';
import {AuditeeEntryConfrenceComponent } from './auditee-entry-confrence/auditee-entry-confrence.component';
import { AuditParaCreateComponent } from './auditee-para/auditee-para-create/audit-para-create.component';
import { AuditParaDetailComponent } from './auditee-para/auditee-para-detail/audit-para-detail.component';
import { AuditParaListComponent } from './auditee-para/auditee-para-list/audit-para-list.component';
import {PendingTaskComponent } from './pending-task/pending-task.component';
import { DeskauditComponent } from './deskaudit/deskaudit.component';

@NgModule({
  declarations: [DeskauditComponent,PendingAuditeeComponent, AuditParaCreateComponent, AuditParaListComponent, AuditParaDetailComponent, AuditeeParaSettlementLetterComponent, AuditeeExitConferenceComponent, AuditeeEntryConfrenceComponent, AuditeeEntryConfrenceComponent, AuditeeExitConferenceComponent, PendingTaskComponent, DeskauditComponent],
    imports: [
      CommonModule, AuditeeObservationRoutingModule, MatSelectModule, FormsModule, MatCardModule, ReactiveFormsModule, MatDatepickerModule, MaterialModule, MatFormFieldModule, MatCheckboxModule, CKEditorModule, SharedModule
  ],
  providers: [CommonMsg]
})
export class AuditeeObservationModule { }
