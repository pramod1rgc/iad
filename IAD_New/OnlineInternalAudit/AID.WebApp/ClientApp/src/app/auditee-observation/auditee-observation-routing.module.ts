import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PendingAuditeeComponent } from '../auditee-observation/pending-auditee/pending-auditee.component';
import { AuditeeObservationModule } from '../auditee-observation/auditee-observation.module';
import { AuditeeEntryConfrenceComponent } from './auditee-entry-confrence/auditee-entry-confrence.component';
import {  AuditeeExitConferenceComponent} from './auditee-exit-conference/auditee-exit-conference.component';
import { AuditeeParaSettlementLetterComponent } from './auditee-para-settlement-letter/auditee-para-settlement-letter.component';
import { AuditParaCreateComponent } from './auditee-para/auditee-para-create/audit-para-create.component';
import { AuditParaDetailComponent } from './auditee-para/auditee-para-detail/audit-para-detail.component';
import { AuditParaListComponent } from './auditee-para/auditee-para-list/audit-para-list.component';
import { PendingTaskComponent } from './pending-task/pending-task.component';
import { DeskauditComponent } from './deskaudit/deskaudit.component';
const routes: Routes = [
  {
    path: '', component: AuditeeObservationModule, data: {
      breadcrumb: 'Auditee Observation'
    }, children: [
      {
        path: 'pending-auditee', component: PendingAuditeeComponent, data: {
          breadcrumb: 'Select Auditee'
        }
      },
      {
        path: 'auditee_entry-conference', component: AuditeeEntryConfrenceComponent, data: {
          breadcrumb: 'Entry Conference'
        }
      },
      {
        path: 'exit-conference', component: AuditeeExitConferenceComponent, data: {
          breadcrumb: 'Exit Conference'
        }
      },
      {
        path: 'audit-para-list', component: AuditParaListComponent, data: {
          breadcrumb: 'Audit Para List'
        }
      },
      {
        path: 'audit-para-create', component: AuditParaCreateComponent, data: {
          breadcrumb: 'Create Audit Para'
        }
      },
      {
        path: 'audit-para-edit/:paraId', component: AuditParaCreateComponent, data: {
          breadcrumb: 'Edit Audit Para'
        }
      },
      {
        path: 'audit-para-detail/:paraId', component: AuditParaDetailComponent, data: {
          breadcrumb: 'Audit Para Detail'
        }
      },
      {
        path: 'para-settlement-letter', component: AuditeeParaSettlementLetterComponent, data: {
          breadcrumb: 'Audit Para settlement Letter'
        }
      },
            {
        path: 'pending-task', component: PendingTaskComponent, data: {
          breadcrumb: 'Pending-Task'
        }
            }
      , {
        path: 'desk-audit', component: DeskauditComponent, data: {
          breadcrumb: 'Desk-Audit'
        }
      }
    ]
  }
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AuditeeObservationRoutingModule { }

