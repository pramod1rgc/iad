import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AuditeeExitConferenceComponent } from './auditee-exit-conference.component';

describe('AuditeeExitConferenceComponent', () => {
  let component: AuditeeExitConferenceComponent;
  let fixture: ComponentFixture<AuditeeExitConferenceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AuditeeExitConferenceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AuditeeExitConferenceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
