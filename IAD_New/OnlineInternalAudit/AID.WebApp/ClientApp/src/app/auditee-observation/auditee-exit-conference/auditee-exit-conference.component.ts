import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroupDirective, FormGroup, FormControl, Validators, FormArray } from '@angular/forms';
import { AuditeeObservationService } from '../../services/auditee_observation/auditee-observation.service';
import { } from '../../services/auditee_observation/auditee-observation.service';
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { AuditsService } from '../../services/audits/audits.services';
import { CommonMsg } from '../../global/common-msg';
import swal from 'sweetalert2';

@Component({
  selector: 'app-auditee-exit-conference',
  templateUrl: './auditee-exit-conference.component.html',
  styleUrls: ['./auditee-exit-conference.component.css']
})
export class AuditeeExitConferenceComponent implements OnInit {


  @ViewChild(FormGroupDirective) formGroupDirective: FormGroupDirective;

  selectedAudit: any;
  exitConferenceDetails: any;
  exitConferenceForm: FormGroup;
  auditTeam: any[] = [];
  selectedAuditTeam: any[] = [];
  public Editor = ClassicEditor;
  userId: number;
  roleId: number;
  saveButtonDisabled: boolean = false;
  saveButtonClicked: boolean = false;

  constructor(private _service: AuditeeObservationService, private auditService: AuditsService, private _msg: CommonMsg) {
    this.userId = JSON.parse(localStorage.getItem("LoginData")).userId;
    this.roleId = JSON.parse(localStorage.getItem("LoginData")).roleId;
    this.selectedAudit = JSON.parse(localStorage.getItem("selectedAuditDetailsForExitConference"));
    this.getExitConferenceDetails();
    this.getAuditTeam();
  }

  createForm() {
    this.exitConferenceForm = new FormGroup({
      exitConferenceId: new FormControl(0),
      auditId: new FormControl(this.selectedAudit.auditId),
      conferenceDate: new FormControl('', Validators.required),
      agenda: new FormControl('', Validators.required),
      decision: new FormControl('', Validators.required),
      userId: new FormControl(this.userId),
      participants: new FormArray([])
    });
  }

  getExitConferenceDetails() {
    if (this.selectedAudit.exitConferenceId > 0) {
      this._service.getAuditExitConference(this.selectedAudit.auditId).subscribe(response => {
        this.exitConferenceDetails = response[0];
        this.saveButtonDisabled = true;
      });
    }
  }

  getAuditTeam() {
    this.auditService.getApprovedAuditTeam(this.selectedAudit.auditId).subscribe(response => {
      this.auditTeam = response;
      if (!this.selectedAudit.exitConferenceId || this.selectedAudit.exitConferenceId == 0) {
        this.selectedAuditTeam = this.auditTeam;
      }
    });
  }

  onSubmit() {
    this.saveButtonClicked = true;
    if (this.exitConferenceForm.valid) {
      if (this.selectedAuditTeam.length > 0) {
        for (var i = 0; i < this.selectedAuditTeam.length; i++) {
          let ob = { Id: i + 1, AuditUserMapId: this.selectedAuditTeam[i].auditUserRoleId };
          this.exitConferenceForm.value.participants.push(ob);
        }
        this._service.updateAuditExitConference(this.exitConferenceForm.value).subscribe(response => {
          if (response > 0) {
            this.saveButtonClicked = false;
            swal(this._msg.updateMsg);
            this.createForm();
            this.saveButtonDisabled = true;
            this.getExitConferenceDetails();
            this.formGroupDirective.resetForm();
            this.selectedAuditTeam = [];
          }
        });
      }
      else {
        swal("Please select atleast one attendee.");
      }
    }
  }

  editExitConference() {
    this.exitConferenceForm = new FormGroup({
      exitConferenceId: new FormControl(this.exitConferenceDetails.exitConferenceId),
      auditId: new FormControl(this.selectedAudit.auditId),
      conferenceDate: new FormControl(this.exitConferenceDetails.conferenceDate, Validators.required),
      agenda: new FormControl(this.exitConferenceDetails.agenda, Validators.required),
      decision: new FormControl(this.exitConferenceDetails.decision, Validators.required),
      userId: new FormControl(this.userId),
      participants: new FormArray([])
    });
    this.selectedAuditTeam = [];
    for (var i = 0; i < this.exitConferenceDetails.participants.length; i++) {
      let emp = this.auditTeam.filter(a => a.auditUserRoleId == this.exitConferenceDetails.participants[i].auditUserMapId);
      if (emp.length > 0) {
        this.selectedAuditTeam.push(emp[0]);
      }
    }
    this.saveButtonDisabled = false;
  }

  ngOnInit() {
    this.createForm();
  }


}
