import { Component, OnInit, ViewChild } from '@angular/core';
import { RiskCategoryService } from '../../services/Master/risk-category.service';
import swal from 'sweetalert2';
import { MatTableDataSource, MatPaginator, MatSort } from '@angular/material';
import { ReplaySubject, Subject } from 'rxjs';
import { FormControl } from '@angular/forms';
import { DeskauditService } from '../../services/desk-audit/deskaudit.service';
import { ToastrService } from 'ngx-toastr';
import { saveAs } from 'file-saver';
import { AuditObservationService } from '../../services/audit-observation/audit-observation.service';
export interface IDeskAudit {
  nameofDocument: string;
  uploadedPath: string;
}

export interface uploadfiles {
  id: string;
  allfiles: string;
}
@Component({
  selector: 'app-deskaudit',
  templateUrl: './deskaudit.component.html',
  styleUrls: ['./deskaudit.component.css']
})
export class DeskauditComponent implements OnInit {
  displayedColumns: string[] = ['documentName', 'DocumentList', 'Action'];//, 'Active'];//, 'leaveTypeDesc', 'action',];
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild('frmDetails') frmDetails: any;
  savebuttonstatus: boolean;
  btnUpdatetext: string;
  uploadFileName: any = {};
  nameofDocument: any = {};
  objDetails: any = {}
  uploadfilesobj: any = {}
  uploadFiles: any[];
  userId: any;
  deactiveDeskAudit: any;
  FilesList: any[] = [];
  dynamicArray: Array<IDeskAudit> = [];
  newDynamic: any = {};
  documentName: string;
  fileName: string;
  relativePath: string;
  Message: string;
  ActiveDactiveHeaderText: any;
  checkCategoryIsactive: boolean;
  ActivateCategory: boolean;
  isActive: any;

  auditList: any[] = [];
  selectedAuditId: any;
  constructor(private _service: AuditObservationService, private services: DeskauditService, private toastr: ToastrService) {
    this.userId = JSON.parse(localStorage.getItem("LoginData")).userId;
  }

  ngOnInit() {
    this.btnUpdatetext = "Save";
    //this.obj = new IDeskAudit();
    this.newDynamic = { nameofDocument: "", uploadedPath: "" };
    this.dynamicArray.push(this.newDynamic);
    //this.getDeskAuditFiles();
    this.getAuditList();
  }

  getAuditList() {
    this._service.getAuditDHAuditList(this.userId).subscribe(response => {
      this.auditList = response;
      console.log(response);
    });
  }


  getDeskAuditFiles() {
    //debugger;
    this.services.getDeskAuditFiles(this.selectedAuditId).subscribe(result => {
      this.objDetails = result;
    });
  }

  SaveAndUpdate() {
    const formData: FormData = new FormData();
    //debugger;



    this.files = this.files;
    //===================this is for validation===================================
    if (this.dynamicArray.length > 0) {
      for (var i = 0; i < this.dynamicArray.length; i++) {
        document.getElementById('id_' + i).innerHTML = '';
        document.getElementById('idfiles_' + i).innerHTML = '';
        if (this.dynamicArray[i].nameofDocument == '') {
          document.getElementById('id_' + i).innerHTML = 'Please enter document name.';
          return false;
        }
        if (this.dynamicArray[i].uploadedPath == '') {
          document.getElementById('idfiles_' + i).innerHTML = 'Please upload files';

          return false;
        }

      }
    }



    for (var i = 0; i < this.files.length; i++) {
      var obj: any = {};
      obj = this.files[i].allfiles;
      for (var j = 0; j < obj.length; j++) {
        formData.append(this.dynamicArray[i].nameofDocument, obj[j]);
      }
    }

    this.userId = JSON.parse(localStorage.getItem("LoginData")).userId;
    formData.append('userId', this.userId);
    formData.append('auditId', this.selectedAuditId);

    this.services.SaveAndUpdate(formData, this.userId).subscribe(resp => {

      this.Message = resp;
      swal(this.Message)

      this.uploadFiles = [];
      this.getDeskAuditFiles();
      this.Cancel();

    });
  }

  DownloadFile(url: string) {
    this.services.getdownloadDetails(url).subscribe(res => {
      saveAs(new Blob([res], { type: 'application/pdf;charset=utf-8' }), url);
    })
  }

  array: any = {};
  files: Array<uploadfiles> = [];
  allfiles: any;
  filesToBeUploaded(files, i) {

    //debugger;
    this.uploadFiles = [];
    this.uploadFiles = files;
    this.array = { id: i, allfiles: this.uploadFiles };
    this.files.push(this.array);
    if (this.getTotalFileSize() > 50) {
      swal("Files Size cannot be exceeded by 50MB");
      return false;
    }
    let filename = '';
    for (var j = 0; j < this.uploadFiles.length; j++) {
      filename = filename + "  " + this.uploadFiles[j].name;
    }
    document.getElementById('fileslist_' + i).innerHTML = filename;
  }

  getTotalFileSize(): any {
    let sum = 0;
    let filename = '';
    for (var i = 0; i < this.uploadFiles.length; i++) {
      sum = +sum + +this.convertFileSizeFromBytesToMegaBytes(this.uploadFiles[i].size);
      filename = filename + "  " + this.uploadFiles[i].name;
    }
    return sum;
  }

  addRow() {
   // debugger;

    this.newDynamic = { nameofDocument: "", uploadedPath: "" };
    this.dynamicArray.push(this.newDynamic);
    //this.toastr.success('New row added successfully', 'New Row');
    //console.log(this.dynamicArray);
    return true;
  }


  deleteRow(index) {
    if (this.dynamicArray.length == 1) {
      //this.toastr.error("Can't delete the row when there is only one row", 'Warning');
      return false;
    } else {
      this.dynamicArray.splice(index, 1);
      this.files.splice(index, 1);
      //this.toastr.warning('Row deleted successfully', 'Delete row');
      return true;
    }
  }



  ActiveDeactiveFillPopUp(documentName, isActive) {
    // alert(isActive)
    //return;
    this.documentName = documentName;
    this.isActive = isActive;
    if (isActive == true) {
      this.isActive = false//true;

      this.ActiveDactiveHeaderText = 'Do you want to deactivate all files?';
    }
    else {

      this.isActive = true//false;
      this.ActiveDactiveHeaderText = 'Do you want to activate all files?';
    }


  }
  ActiveDeactiveUsercategory() {
    this.deactiveDeskAudit = {
      documentName: this.documentName,
      isActive: this.isActive,
      createdbyId: JSON.parse(localStorage.getItem("LoginData")).userId,
      auditId: this.selectedAuditId
    };
    this.services.ActiveDeactive(this.deactiveDeskAudit).subscribe(data => {
      //debugger;
      this.Message = data;

      //$('.dialog__close-btn').click();
      this.ActivateCategory = false;
      swal(this.Message)
      this.getDeskAuditFiles();

    })
  }

  CancelPopUp() {
    this.ActivateCategory = false;
    this.getDeskAuditFiles();
  }
  Cancel() {

    this.frmDetails.resetForm();
    if (this.dynamicArray.length == 1) {
      //this.toastr.error("Can't delete the row when there is only one row", 'Warning');
      return false;
    } else {

      for (var i = this.dynamicArray.length; i >= 0; i--) {

        this.dynamicArray.splice(i, 1);
        this.files.splice(i, 1);

      }
      this.newDynamic = { nameofDocument: "", uploadedPath: "" };
      this.dynamicArray.push(this.newDynamic);
      return true;
    }
    this.btnUpdatetext = 'Save';
  }

  convertFileSizeFromBytesToMegaBytes(size): any {
    return (size / (1024 * 1024)).toFixed(4);
  }


}
