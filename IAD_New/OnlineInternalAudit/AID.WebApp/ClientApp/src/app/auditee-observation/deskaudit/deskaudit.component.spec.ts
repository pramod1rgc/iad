import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeskauditComponent } from './deskaudit.component';

describe('DeskauditComponent', () => {
  let component: DeskauditComponent;
  let fixture: ComponentFixture<DeskauditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeskauditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeskauditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
