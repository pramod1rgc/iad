import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AuditeeParaSettlementLetterComponent } from './auditee-para-settlement-letter.component';

describe('AuditeeParaSettlementLetterComponent', () => {
  let component: AuditeeParaSettlementLetterComponent;
  let fixture: ComponentFixture<AuditeeParaSettlementLetterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AuditeeParaSettlementLetterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AuditeeParaSettlementLetterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
