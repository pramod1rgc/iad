import { Component, OnInit, ViewChild } from '@angular/core';
import { AuditeeObservationService } from '../../services/auditee_observation/auditee-observation.service';
import { FormGroup, FormControl, Validators, FormGroupDirective } from '@angular/forms';
import swal from 'sweetalert2';
import { CommonMsg } from '../../global/common-msg';
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';

@Component({
  selector: 'app-auditee-para-settlement-letter',
  templateUrl: './auditee-para-settlement-letter.component.html',
  styleUrls: ['./auditee-para-settlement-letter.component.css']
})
export class AuditeeParaSettlementLetterComponent implements OnInit {
  @ViewChild(FormGroupDirective) formGroupDirective: FormGroupDirective;
  userId: number;
  roleId: number;
  selectedAudit: any;
  paraSettlementLetterDetails: any;
  letterForm: FormGroup;
  public Editor = ClassicEditor;
  saveButtonClicked: boolean = false;
  saveButtonDisabled: boolean = false;
  RejectPopUp: boolean = false;
  rejectionRemark: string = '';

  constructor(private _service: AuditeeObservationService, private _msg: CommonMsg) {
    this.userId = JSON.parse(localStorage.getItem("LoginData")).userId;
    this.roleId = JSON.parse(localStorage.getItem("LoginData")).roleId;
    this.selectedAudit = JSON.parse(localStorage.getItem("selectedAuditDetailsForEntryConference"));
    this.getAuditParaSettlementLetter();
  }

  getAuditParaSettlementLetter() {
    this._service.getAuditParaSettlementLetter(this.selectedAudit.auditId).subscribe(response => {
      this.paraSettlementLetterDetails = response;
      if (this.paraSettlementLetterDetails.auditParaSettlementLetterId > 0) {
        this.saveButtonDisabled = true;
      }
    });
  }

  createForm() {
    this.letterForm = new FormGroup({
      auditParaSettlementLetterId: new FormControl(0),
      letterNumber: new FormControl('', Validators.required),
      letterDate: new FormControl('', Validators.required),
      letterTo: new FormControl('', Validators.required),
      letterSubject: new FormControl('', Validators.required),
      letterFrom: new FormControl('', Validators.required),
      letterCopyTo: new FormControl('', Validators.required),
      letterContent: new FormControl('', Validators.required),
      auditId: new FormControl(this.selectedAudit.auditId),
      userId: new FormControl(this.userId),
    });
  }

  updateAuditParaSettlementLetter() {
    this.saveButtonClicked = true;
    if (this.letterForm.valid) {
      this._service.updateAuditParaSettlementLetter(this.letterForm.value).subscribe(response => {
        if (response > 0) {
          swal(this._msg.updateMsg);
          this.createForm();
          this.formGroupDirective.resetForm();
          this.saveButtonClicked = false;
          this.getAuditParaSettlementLetter();
        }
      });
    }
  }

  editLetter() {
    if (this.paraSettlementLetterDetails) {
      this.letterForm = new FormGroup({
        auditParaSettlementLetterId: new FormControl(this.paraSettlementLetterDetails.auditParaSettlementLetterId),
        letterNumber: new FormControl(this.paraSettlementLetterDetails.letterNumber, Validators.required),
        letterDate: new FormControl(this.paraSettlementLetterDetails.letterDate, Validators.required),
        letterTo: new FormControl(this.paraSettlementLetterDetails.letterTo, Validators.required),
        letterSubject: new FormControl(this.paraSettlementLetterDetails.letterSubject, Validators.required),
        letterFrom: new FormControl(this.paraSettlementLetterDetails.letterFrom, Validators.required),
        letterCopyTo: new FormControl(this.paraSettlementLetterDetails.letterCopyTo, Validators.required),
        letterContent: new FormControl(this.paraSettlementLetterDetails.letterContent, Validators.required),
        auditId: new FormControl(this.selectedAudit.auditId),
        userId: new FormControl(this.userId),
      });
      this.saveButtonDisabled = false;
    }
  }

  forward(statusId) {
    this._service.updateAuditParaSettlementLetterStatus(this.selectedAudit.auditId, statusId, this.rejectionRemark).subscribe(response => {
      if (response > 0) {
        switch (statusId) {
          case 2:
            swal(this._msg.forwardAuditHeadMsg);
            break;
          case 3:
            swal(this._msg.forwardSupervisorMsg);
            break;
          case 5:
            swal(this._msg.forwardAuditDHMsg);
            break;
        }
        this.getAuditParaSettlementLetter();
        this.rejectionRemark = '';
      }
      else {
        swal("Cannot forward until all paras are approved.");
      }
      this.RejectPopUp = false;
    })
  }

  ngOnInit() {
    this.createForm();
  }



}
