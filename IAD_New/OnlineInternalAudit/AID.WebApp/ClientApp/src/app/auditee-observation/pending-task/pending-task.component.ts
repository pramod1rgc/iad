import { Component, OnInit, ViewChild } from '@angular/core';
import { AuditeeObservationService } from '../../services/auditee_observation/auditee-observation.service';
import swal from 'sweetalert2';
import { CommonMsg } from '../../global/common-msg';
import { ActivatedRoute } from '@angular/router';
import { MatTableDataSource, MatPaginator, MatSort } from '@angular/material';
@Component({
  selector: 'app-pending-task',
  templateUrl: './pending-task.component.html',
  styleUrls: ['./pending-task.component.css']
})
export class PendingTaskComponent implements OnInit {

  userId: number;
  roleId: number;
  selectedAudit: any;
  auditParaList: any[] = [];
  deletePopUp: boolean = false;
  selectedPara: any;
  filterParameter: string;
  searchTerm: string = '';
  pageSize: number = 10;
  pageNumber: number = 1;
  totalCount: number = 0;
  ParaId: number;
  auditList: any[] = [];
  selectedAuditId: any;
  displayedColumns: string[] = ['title', 'riskCategoryName', 'severity', 'status', 'Action'];

  dataSource: any; //MatTableDataSource<any> = new MatTableDataSource<any>();
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;


  constructor(private _service: AuditeeObservationService, private _msg: CommonMsg, private route: ActivatedRoute) {
    this.route.params.subscribe(params => {
      if (params['filter']) {
        this.filterParameter = params['filter'];
        //this.filterAuditParas();
      }
    });
    this.userId = JSON.parse(localStorage.getItem("LoginData")).userId;
    this.roleId = JSON.parse(localStorage.getItem("LoginData")).roleId;
    //this.selectedAudit = JSON.parse(localStorage.getItem("selectedAuditDetailsForExitConference"));
  }

  getAuditList() {
    debugger;
    this._service.getAuditDHAuditList(this.userId).subscribe(response => {
      this.auditList = response;
      this.selectedAuditId = this.auditList[0]['auditId'];
      localStorage.setItem("selectedAuditId", JSON.stringify(this.selectedAuditId));
      this.getAuditDetails();

    });
  }

  getAuditParas() {
  
    this.totalCount = 0;
    debugger;
    this._service.getAuditParas(this.roleId, Number(localStorage.getItem("selectedAuditId")), this.pageSize, this.pageNumber, this.searchTerm).subscribe(response => {
      this.auditParaList = response;
      if (this.auditParaList.length > 0) {
        this.totalCount = this.auditParaList[0].totalCount;
      }
      this.selectedPara = null;
      this.filterAuditParas();
      this.dataSource = new MatTableDataSource(this.auditParaList);
      //this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });
  }

  filterAuditParas() {
    if (this.filterParameter) {
      let paras = null;
      switch (this.filterParameter) {
        case "all":
          break;
        case "approved":
          paras = this.auditParaList.filter(a => a.statusId == 2);
          this.auditParaList = paras;
          break;
        case "partially":
          paras = this.auditParaList.filter(a => a.commentId > 0 && (a.statusId == 1 || a.statusId == 3));
          this.auditParaList = paras;
          break;
        case "open":
          paras = this.auditParaList.filter(a => a.commentId == 0 && a.statusId == 1);
          this.auditParaList = paras;
          break;
        case "high":
          paras = this.auditParaList.filter(a => a.severity == "high");
          this.auditParaList = paras;
          break;
        case "medium":
          paras = this.auditParaList.filter(a => a.severity == "medium");
          this.auditParaList = paras;
          break;
        case "low":
          paras = this.auditParaList.filter(a => a.severity == "low");
          this.auditParaList = paras;
          break;
      }
    }
  }

  selectPara(para) {
    this.selectedPara = para;
  }



  applyFilter() {
    this.totalCount = 0;
    this.pageNumber = 1;
    this.paginator.firstPage();
    this.getAuditParas();
  }

  getPaginationData(event) {
    this.pageSize = event.pageSize;
    this.pageNumber = event.pageIndex + 1;
    this.getAuditParas();
  }

  ngOnInit() {
    this.getAuditList();
    this.getAuditParas();
  }
  getAuditDetails() {
    this.selectedAudit = this.auditList.filter(a => a.auditId == this.selectedAuditId)[0] as any;

    localStorage.setItem("selectedAuditDetailsForEntryConference", JSON.stringify(this.selectedAudit));
    localStorage.setItem("selectedAuditDetailsForExitConference", JSON.stringify(this.selectedAudit));
  }





}
