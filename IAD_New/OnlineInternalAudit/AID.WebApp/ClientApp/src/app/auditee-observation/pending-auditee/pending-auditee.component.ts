import { Component, OnInit } from '@angular/core';
import swal from 'sweetalert2';
import { AuditeeObservationService } from '../../services/auditee_observation/auditee-observation.service';
import { AuditsService } from '../../services/audits/audits.services';
import { CommonMsg } from '../../global/common-msg';
@Component({
  selector: 'app-pending-auditee',
  templateUrl: './pending-auditee.component.html',
  styleUrls: ['./pending-auditee.component.css']
})
export class PendingAuditeeComponent implements OnInit {


  userId: number;
  roleId: number;
  auditList: any[] = [];
  selectedAuditId: any;
  selectedAudit: any;
  auditTeam: any[] = [];

  constructor(private _service: AuditeeObservationService, private auditService: AuditsService, private _msg: CommonMsg) {
    this.userId = JSON.parse(localStorage.getItem("LoginData")).userId;
    this.roleId = JSON.parse(localStorage.getItem("LoginData")).roleId;
  }

  getAuditList() {
    this._service.getAuditDHAuditList(this.userId).subscribe(response => {
      this.auditList = response;
      console.log(this.auditList);
      this.selectedAuditId = this.auditList[0]['auditId'];
      this.getAuditDetails();
      
    });
  }

  getAuditTeam() {
    this.auditService.getApprovedAuditTeam(this.selectedAuditId).subscribe(response => {
      this.auditTeam = response;
    });
  }

  getAuditDetails() {
    this.selectedAudit = this.auditList.filter(a => a.auditId == this.selectedAuditId)[0] as any;
    this.getAuditTeam();
    localStorage.setItem("selectedAuditDetailsForEntryConference",JSON.stringify(this.selectedAudit));
    localStorage.setItem("selectedAuditDetailsForExitConference",JSON.stringify(this.selectedAudit));
  }

  //forwardToAuditHead(statusId) {
  //  let obj = { auditId: this.selectedAudit.auditId, statusId: statusId, userId: this.userId };
  //  this._service.updateAuditStatus(obj).subscribe(response => {
  //    if (response > 0) {
  //      switch (statusId) {
  //        case 25:
  //          swal(this._msg.forwardAuditHeadMsg);
  //          break;
  //        //case 20:
  //        //  swal(this._msg.forwardSupervisorMsg);
  //        //  break;
  //      }
        
  //    }
  //  });
  //}

  ngOnInit() {
    this.getAuditList();
 
  }

}
