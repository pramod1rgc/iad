import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PendingAuditeeComponent } from './pending-auditee.component';

describe('PendingAuditeeComponent', () => {
  let component: PendingAuditeeComponent;
  let fixture: ComponentFixture<PendingAuditeeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PendingAuditeeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PendingAuditeeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
