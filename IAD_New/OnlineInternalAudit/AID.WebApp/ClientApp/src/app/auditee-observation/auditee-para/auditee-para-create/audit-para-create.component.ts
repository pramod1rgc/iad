import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormControl, Validators, FormGroupDirective } from '@angular/forms';
import { Location } from '@angular/common';
import { AuditObservationService } from '../../../services/audit-observation/audit-observation.service';
import swal from 'sweetalert2';
import { CommonMsg } from '../../../global/common-msg';
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { RiskCategoryService } from '../../../services/Master/risk-category.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-audit-para-create',
  templateUrl: './audit-para-create.component.html',
  styleUrls: ['./audit-para-create.component.css']
})
export class AuditParaCreateComponent implements OnInit {

  @ViewChild(FormGroupDirective) formGroupDirective: FormGroupDirective;

  auditParaForm: FormGroup;
  paraId: number;
  userId: number;
  selectedAudit: any;
  riskCategories: any[] = [];
  public Editor = ClassicEditor;
  uploadFiles: any[] = [];
  paraDetails: any;

  constructor(private route: ActivatedRoute, private router: Router, private _service: AuditObservationService, private _msg: CommonMsg, private riskCategoryService: RiskCategoryService, private _location: Location) {
    this.route.params.subscribe(params => {
      if (params['paraId']) {
        this.paraId = params['paraId'];
        this.getParaDetails();
      }
    });
    this.userId = JSON.parse(localStorage.getItem("LoginData")).userId;
    this.selectedAudit = JSON.parse(localStorage.getItem("selectedAuditDetailsForExitConference"));
  }

  createForm() {
    this.auditParaForm = new FormGroup({
      auditId: new FormControl(this.selectedAudit.auditId),
      paraId: new FormControl(0),
      title: new FormControl('', Validators.required),
      criteria: new FormControl('', Validators.required),
      conditions: new FormControl('', Validators.required),
      consequences: new FormControl('', Validators.required),
      causes: new FormControl('', Validators.required),
      correctiveActions: new FormControl('', Validators.required),
      severity: new FormControl('', Validators.required),
      riskCategoryId: new FormControl('', Validators.required),
      userId: new FormControl(this.userId, Validators.required),
    });
  }

  getParaDetails() {
    this._service.getAuditParaDetails(this.paraId).subscribe(response => {
      if (response) {
        this.paraDetails = response;
        this.auditParaForm = new FormGroup({
          auditId: new FormControl(this.selectedAudit.auditId),
          paraId: new FormControl(this.paraDetails.paraId),
          title: new FormControl(this.paraDetails.title, Validators.required),
          criteria: new FormControl(this.paraDetails.criteria, Validators.required),
          conditions: new FormControl(this.paraDetails.conditions, Validators.required),
          consequences: new FormControl(this.paraDetails.consequences, Validators.required),
          causes: new FormControl(this.paraDetails.causes, Validators.required),
          correctiveActions: new FormControl(this.paraDetails.correctiveActions, Validators.required),
          severity: new FormControl(this.paraDetails.severity, Validators.required),
          riskCategoryId: new FormControl(this.paraDetails.riskCategoryId, Validators.required),
          userId: new FormControl(this.userId, Validators.required),
        });
        if (this.paraDetails.files && this.paraDetails.files.length > 0) {
          this.uploadFiles = [];
          for (var i = 0; i < this.paraDetails.files.length; i++) {
            var obj = { name: this.paraDetails.files[i].fileName };
            this.uploadFiles.push(obj);
          }
        }
      }
    });
  }

  getRiskCategories() {
    this.riskCategoryService.GetAllCategoryDetails().subscribe(response => {
      this.riskCategories = response;
    });
  }

  submit() {
    if (this.auditParaForm.valid) {
      this._service.updateAuditPara(this.auditParaForm.value, this.uploadFiles).subscribe(response => {
        if (response > 0) {
          swal(this._msg.updateMsg);
          this.createForm();
          this.uploadFiles = [];
          this.formGroupDirective.resetForm();
          if (this.paraId > 0) {
            this._location.back();
          }
        }
      });
    }
  }

  filesToBeUploaded(files) {
    //this.uploadFiles = [];
    for (var i = 0; i < files.length; i++) {
      this.uploadFiles.push(files[i]);
    }
    //this.uploadFiles = files;
    //for (var i = 0; i < files.length; i++) {
    //  this.uploadFiles.push({ fileName: files[i].name, fileType: files[i].type, fileSize: this.convertFileSizeFromBytesToMegaBytes(files[i]) });
    //}
    if (this.getTotalFileSize() > 50) {
      swal("Files Size cannot be exceeded by 50MB");
    }
  }

  getTotalFileSize(): any {
    let sum = 0;
    for (var i = 0; i < this.uploadFiles.length; i++) {
      sum = +sum + +this.convertFileSizeFromBytesToMegaBytes(this.uploadFiles[i].size);
    }
    return sum;
  }

  convertFileSizeFromBytesToMegaBytes(size): any {
    return (size / (1024 * 1024)).toFixed(4);
  }

  ngOnInit() {
    this.createForm();
    this.getRiskCategories();
   
  }

}
