import { Component, OnInit, ElementRef, ViewChild, AfterViewChecked, AfterViewInit, AfterContentChecked } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AuditeeObservationService } from '../../../services/auditee_observation/auditee-observation.service';
import swal from 'sweetalert2';
import { ScrollToBottomDirective } from '../../../directives/scroll-to-bottom.directive';
import { CommonMsg } from '../../../global/common-msg';
import { saveAs } from 'file-saver';
@Component({
  selector: 'app-audit-para-detail',
  templateUrl: './audit-para-detail.component.html',
  styleUrls: ['./audit-para-detail.component.css'],
  providers: [ScrollToBottomDirective]
})
export class AuditParaDetailComponent implements OnInit {

  paraId: number;
  details: any;
  userId: number;
  roleId: number;
  selectedAudit: any;
  commentsList: any[] = [];
  comment: string;
  uploadFiles: any[] = [];
  rejectionRemark: string;
  RejectPopUp: boolean = false;
  auditeeStatusId: number;
  @ViewChild(ScrollToBottomDirective)scroll: ScrollToBottomDirective;

  constructor(private route: ActivatedRoute, private _service: AuditeeObservationService, private _msg: CommonMsg) {
    this.route.params.subscribe(params => {
      if (params['paraId']) {
        this.paraId = params['paraId'];
        this.getParaDetails();
        this.getParaComments();
      }
    });
    this.userId = JSON.parse(localStorage.getItem("LoginData")).userId;
    this.roleId = JSON.parse(localStorage.getItem("LoginData")).roleId;
    this.selectedAudit = JSON.parse(localStorage.getItem("selectedAuditDetailsForExitConference"));
  }
  DownloadFile(url:string) {
    debugger;
    this._service.getdownloadDetails(url).subscribe(res => {
      saveAs(new Blob([res], { type: 'application/pdf;charset=utf-8' }), url);
    })
  }

  forwardToAuditHead(statusId) {
    let obj = { auditId: this.selectedAudit.auditId, statusId: statusId, userId: this.userId, paraId:this.paraId };
    this._service.updateAuditStatus(obj).subscribe(response => {
      if (response > 0) {
        swal(this._msg.forwardAuditHeadMsg);
        //switch (statusId) {
        //  case 25:
        //    swal(this._msg.forwardAuditHeadMsg);
        //    break;
        //  //case 20:
        //  //  swal(this._msg.forwardSupervisorMsg);
        //  //  break;
        //}

        this.rejectionRemark = '';
        this.RejectPopUp = false;
        this.getParaDetails();
      }
    });
  }

  getParaDetails() {
    debugger;
    this._service.getAuditParaDetails(this.paraId).subscribe(response => {
      this.details = response;
    });
  }

  getParaComments() {
    debugger;
    this._service.getParaComments(this.paraId).subscribe(response => {
      this.commentsList = response;
    });
  }

  postParaComment() {

    let obj = { auditId: this.selectedAudit.auditId, paraId: this.paraId, comment: this.comment, userId: this.userId };
    this._service.postParaComment(obj, this.uploadFiles).subscribe(response => {
      if (response > 0) {
        this.getParaComments();
        this.comment = '';
        this.uploadFiles = [];
        swal("Commented Successfully");
      }
    });
  }

  filesToBeUploaded(files) {
    debugger;
    this.uploadFiles = [];
    this.uploadFiles = files;
    if (this.getTotalFileSize() > 50) {
      swal("Files Size cannot be exceeded by 50MB");
    }
  }

  getTotalFileSize(): any {
    let sum = 0;
    for (var i = 0; i < this.uploadFiles.length; i++) {
      sum = +sum + +this.convertFileSizeFromBytesToMegaBytes(this.uploadFiles[i].size);
    }
    return sum;
  }

  convertFileSizeFromBytesToMegaBytes(size): any {
    return (size / (1024 * 1024)).toFixed(4);
  }

  updateAuditParaStatus(statusId) {
    if (this.paraId) {
      let obj = { paraId: this.paraId, auditId: this.selectedAudit.auditId, statusId: statusId, rejectionRemark: this.rejectionRemark };
      this._service.updateAuditParaStatus(obj).subscribe(response => {
        if (response > 0) {
            switch (statusId) {
          case 2:
             swal(this._msg.updateMsgForAuditeeHeadApprove);
            break;
          case 3:
                  swal(this._msg.updateMsgForAuditeeHeadReject);
            break;
        }
      
          this.rejectionRemark = '';
          this.RejectPopUp = false;
          this.getParaDetails();
        }
      });
    }
  }

  ngOnInit() {
  }

}
