import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AuditParaListComponent } from './audit-para-list.component';

describe('AuditParaListComponent', () => {
  let component: AuditParaListComponent;
  let fixture: ComponentFixture<AuditParaListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AuditParaListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AuditParaListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
