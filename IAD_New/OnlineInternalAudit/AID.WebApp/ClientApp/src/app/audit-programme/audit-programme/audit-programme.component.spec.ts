import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AuditProgrammeComponent } from './audit-programme.component';

describe('AuditProgrammeComponent', () => {
  let component: AuditProgrammeComponent;
  let fixture: ComponentFixture<AuditProgrammeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AuditProgrammeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AuditProgrammeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
