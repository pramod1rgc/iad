import { Component, OnInit, ViewChild } from '@angular/core';
import { AuditProgrammeService } from '../../services/audit-programme/audit-programme.service';
import * as $ from 'jquery';
import { Router, ActivatedRoute } from '@angular/router';
import { MatTableDataSource, MatPaginator } from '@angular/material';
@Component({
  selector: 'app-audit-programme',
  templateUrl: './audit-programme.component.html',
  styleUrls: ['./audit-programme.component.css']
})
export class AuditProgrammeComponent implements OnInit {
  reportUrl: string = '';
  auditProgrammes: any[] = [];
  isbtnData=true;
  selectedFinYearId: any;
  financialYears: any[] = [];
  searchTerm: string = '';

  displayedColumns: string[] = ['NameOfUnit', 'PeriodOfUnit', 'AuditTeam'];

  dataSource: MatTableDataSource<any> = new MatTableDataSource<any>();

  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(private _service: AuditProgrammeService,private router: Router) { }

  getAuditProgramme() {
    this._service.getAuditProgrammes(this.selectedFinYearId).subscribe(response => {
      this.auditProgrammes = response;
      this.dataSource = new MatTableDataSource(this.auditProgrammes);
      this.dataSource.paginator = this.paginator;
     
      if (this.auditProgrammes.length > 0) { this.isbtnData = false; }

      else { this.isbtnData = true; }

    });
  }

  getFinancialYears() {
    this._service.BindDropFYear().subscribe(response => {
      this.financialYears = response;
    });
  }

  printReport() {
    this.reportUrl = 'AuditProgrammeReport&rs:Embed=true&rc:Parameters=false&ID=' + this.selectedFinYearId;
    sessionStorage.setItem('reportUrl', this.reportUrl); 
    window.open('/report', "_blank");
  }

  applyFilter() {
    this.dataSource.filter = this.searchTerm;
  }

  ngOnInit() {
    this.getFinancialYears();

  }

}
