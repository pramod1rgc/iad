import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuditProgrammeComponent } from './audit-programme/audit-programme.component';
import { AuditProgrammeRoutingModule } from './audit-programme.routing';
import { MatSelectModule, MatCardModule } from '@angular/material';
import { FormsModule } from '@angular/forms';
import { SharedModule } from '../shared-module/shared-module.module';
import { MaterialModule } from '../material.module';
@NgModule({
  declarations: [AuditProgrammeComponent],
  imports: [
    CommonModule, AuditProgrammeRoutingModule, MatSelectModule, FormsModule, MatCardModule, SharedModule, MaterialModule
  ],
})
export class AuditProgrammeModule { }
