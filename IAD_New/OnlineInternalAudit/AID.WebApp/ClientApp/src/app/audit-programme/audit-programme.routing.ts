import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuditProgrammeModule } from './audit-programme.module';
import { AuditProgrammeComponent } from './audit-programme/audit-programme.component';

const routes: Routes = [

  {
    path: '', component: AuditProgrammeModule, data: {
      breadcrumb: 'Audits Programme'
    }, children: [
      {
        path: 'audit-programme', component: AuditProgrammeComponent, data: {
          breadcrumb: 'Audit Programme'
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AuditProgrammeRoutingModule { }
