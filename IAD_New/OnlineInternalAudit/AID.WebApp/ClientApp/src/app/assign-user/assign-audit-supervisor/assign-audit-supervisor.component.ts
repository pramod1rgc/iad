import { Component, OnInit } from '@angular/core';
import { CdkDragDrop, transferArrayItem, moveItemInArray } from '@angular/cdk/drag-drop';
import { AssignUserServices } from '../../services/assignUser/assignUser.service';
import { CommonMsg } from '../../global/common-msg';
import swal from 'sweetalert2';

@Component({
  selector: 'app-assign-audit-supervisor',
  templateUrl: './assign-audit-supervisor.component.html',
  styleUrls: ['./assign-audit-supervisor.component.css']
})
export class AssignAuditSupervisorComponent implements OnInit {

  allEmployees: any[] = [];
  selectedEmployees: any[] = [];
  filteredEmployees: any[] = [];
  userId: number;
  searchTerm: string = '';

  constructor(private _service: AssignUserServices, private _msg: CommonMsg) {
    this.userId = JSON.parse(localStorage.getItem("LoginData")).userId;
  }

  assignedDrop(event: CdkDragDrop<string[]>) {
    if (event.previousContainer !== event.container) {
      if (this.searchTerm) {
        let employeeIndex = this.allEmployees.indexOf(this.filteredEmployees[event.previousIndex]);
        this.allEmployees.splice(employeeIndex, 1);
      }
      else {
        this.allEmployees.splice(event.previousIndex, 1);
      }
      transferArrayItem(event.previousContainer.data, event.container.data,
        event.previousIndex, event.currentIndex);
    }
    else {
      moveItemInArray(this.selectedEmployees, event.previousIndex, event.currentIndex);
    }
  }

  unAssignedDrop(event: CdkDragDrop<string[]>) {
    if (event.previousContainer !== event.container) {
      this.allEmployees.push(this.selectedEmployees[event.previousIndex]);
      transferArrayItem(event.previousContainer.data, event.container.data,
        event.previousIndex, event.currentIndex);
    }
    else {
      moveItemInArray(this.filteredEmployees, event.previousIndex, event.currentIndex);
    }
  }

  save() {
    if (this.selectedEmployees.length > 0) {
      this.selectedEmployees.forEach(a => a.roleId = 2);
      for (var i = 0; i < this.selectedEmployees.length; i++) {
        this.selectedEmployees[i].id = i + 1;
      }

      let obj = { "IPUserId": this.userId, "Password": "password@123", "IPRoleId": 2, "AssignUserRole": this.selectedEmployees };
      this._service.assignUserRole(obj).subscribe(response => {
        if (response > 0) {
          swal(this._msg.saveMsg);
        }
      });
    }
    else {
      swal(this._msg.selectAtleastOneAssigner);
    }
  }


  getRoleUserEmployee() {
    this._service.getUserRoleEmployees(2).subscribe(response => {
      this.allEmployees = response.filter(a => a.roleId == 0 || a.roleId != 2 || a.isActive == 0);
      this.selectedEmployees = response.filter(a => a.roleId > 0 && a.roleId == 2 && a.isActive == 1);
      this.filterEmployees();
    });
  }

  filterEmployees() {
    this.filteredEmployees = this.allEmployees.filter(a => a.empName.toString().toLowerCase().indexOf(this.searchTerm.toLowerCase()) > -1 || a.designation.toString().toLowerCase().indexOf(this.searchTerm.toLowerCase()) > -1 || a.email.toString().toLowerCase().indexOf(this.searchTerm.toLowerCase()) > -1 || a.phoneNo.toString().toLowerCase().indexOf(this.searchTerm.toLowerCase()) > -1 || a.mobileNo.toString().toLowerCase().indexOf(this.searchTerm.toLowerCase()) > -1);
  }

  ngOnInit() {
    this.getRoleUserEmployee();
  }

}
