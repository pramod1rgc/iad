import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AssignAuditSupervisorComponent } from './assign-audit-supervisor.component';

describe('AssignAuditSupervisorComponent', () => {
  let component: AssignAuditSupervisorComponent;
  let fixture: ComponentFixture<AssignAuditSupervisorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AssignAuditSupervisorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AssignAuditSupervisorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
