import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AssignUserRoutingModule } from './assign-user.routing';
import { AssignAuditSupervisorComponent } from './assign-audit-supervisor/assign-audit-supervisor.component';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { MaterialModule } from '../material.module';
import { AssignUserServices } from '../services/assignUser/assignUser.service';
import { CommonMsg } from '../global/common-msg';
import { AssignIadHeadComponent } from './assign-iad-head/assign-iad-head.component';
import { AssignAuditTeamComponent } from './assign-audit-team/assign-audit-team.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatSelectModule } from '@angular/material';
import { SharedModule } from '../shared-module/shared-module.module';

@NgModule({
  declarations: [AssignAuditSupervisorComponent, AssignIadHeadComponent, AssignAuditTeamComponent],
  imports: [
    CommonModule,
    AssignUserRoutingModule,
    DragDropModule,MaterialModule, FormsModule, ReactiveFormsModule,MatSelectModule, SharedModule
  ],
  providers: [AssignUserServices, CommonMsg]
})
export class AssignUserModule { }
