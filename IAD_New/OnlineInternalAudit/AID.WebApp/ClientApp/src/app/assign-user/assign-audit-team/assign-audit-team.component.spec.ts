import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AssignAuditTeamComponent } from './assign-audit-team.component';

describe('AssignAuditTeamComponent', () => {
  let component: AssignAuditTeamComponent;
  let fixture: ComponentFixture<AssignAuditTeamComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AssignAuditTeamComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AssignAuditTeamComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
