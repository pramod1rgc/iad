import { Component, OnInit } from '@angular/core';
import { AssignUserServices } from '../../services/assignUser/assignUser.service';
import { CommonMsg } from '../../global/common-msg';
import { CdkDragDrop, transferArrayItem, moveItemInArray } from '@angular/cdk/drag-drop';
import swal from 'sweetalert2';
import { VerifyAuditTeamService } from '../../services/verification/verifyAuditTeam.service';
import { CommonService } from '../../services/common/common.service';

@Component({
  selector: 'app-assign-audit-team',
  templateUrl: './assign-audit-team.component.html',
  styleUrls: ['./assign-audit-team.component.css']
})
export class AssignAuditTeamComponent implements OnInit {

  allEmployees: any[] = [];
  auditHeadEmployees: any[] = [];
  auditDHEmployees: any[] = [];
  selectedEmployees: any[] = [];
  userId: number;
  auditsList: any[] = [];
  selectedAuditId: number;
  employeeResponse: any[] = [];
  draggedData: any;
  draggedFrom: string = '';
  selectedAudit: any;
  disableForwardToSupervisor: boolean = true;
  showRemarkPopup: boolean = false;
  supervisorsList: any[] = [];
  selectedSupervisorId: number = 0;
  showForwardToSupervisorPopup: boolean = false;
  searchTerm: string = '';
  filteredEmployees: any[] = [];

  constructor(private _service: AssignUserServices, private _msg: CommonMsg, private verifyService: VerifyAuditTeamService, private commonService: CommonService) {
    this.userId = JSON.parse(localStorage.getItem("LoginData")).userId;
  }

  getAuditsForDropdown() {
    this._service.getAuditsForDropdown().subscribe(response => {
      this.auditsList = response;
    });
  }

  drop(event: CdkDragDrop<string[]>) {
    if (event.previousContainer !== event.container) {
      this.allEmployees.push(this.draggedData);
      transferArrayItem(event.previousContainer.data, event.container.data,
        event.previousIndex, event.currentIndex)
    }
    else {
      moveItemInArray(this.allEmployees, event.previousIndex, event.currentIndex);
    }
  }


  dropAuditHead(event: CdkDragDrop<string[]>) {
    if (event.previousContainer !== event.container) {
      if (this.draggedFrom == 'all') {
        if (this.searchTerm) {
          let employeeIndex = this.allEmployees.indexOf(this.draggedData);
          this.allEmployees.splice(employeeIndex, 1);
        }
        else {
          this.allEmployees.splice(event.previousIndex, 1);
        }
      }
      transferArrayItem(event.previousContainer.data, event.container.data,
        event.previousIndex, event.currentIndex)
      let data = this.draggedData; // event.container.data[0] as any;
      let containerIndex = event.container.data.indexOf(this.draggedData);
      if (data.designationId != 5 && data.designationId != 8) {
        if (this.draggedFrom == 'all') {
          this.allEmployees.push(data);
        }
        transferArrayItem(event.container.data, event.previousContainer.data,
          containerIndex, event.previousIndex)
        swal("Only Sr.AO/AO can be assigned.");
        return 0;
      }
      if (data.roleId > 0) {
        let currentAudit = [];
        for (var i = 0; i < this.auditsList.length; i++) {
          if (this.auditsList[i].auditId == this.selectedAuditId) {
            currentAudit.push(this.auditsList[i]);
            break;
          }
        }
        if (this.employeeResponse.filter(a => a.empId == data.empId && a.auditId == currentAudit[0].auditId && a.isActive == 1).length == 0) {
          if (this.employeeResponse.filter(a => a.empId == data.empId && a.auditId != this.selectedAuditId && ((currentAudit[0].auditFromDate >= a.auditFromDate && currentAudit[0].auditFromDate <= a.auditToDate) || (currentAudit[0].auditToDate >= a.auditFromDate && currentAudit[0].auditToDate <= a.auditToDate) || (currentAudit[0].auditFromDate < a.auditFromDate && currentAudit[0].auditToDate > a.auditToDate)) && a.isActive == 1).length > 0) {
            if (this.draggedFrom == 'all') {
              this.allEmployees.push(data);
            }
            transferArrayItem(event.container.data, event.previousContainer.data,
              containerIndex, event.previousIndex)
            swal("Employee is already assigned to another Audit on this Date range.");
            return 0;
          }
        }
      }
    }
    else {
      moveItemInArray(this.auditHeadEmployees, event.previousIndex, event.currentIndex);
    }
  }


  dropAuditDH(event: CdkDragDrop<string[]>) {
    if (event.previousContainer !== event.container) {
      if (this.draggedFrom == 'all') {
        if (this.searchTerm) {
          let employeeIndex = this.allEmployees.indexOf(this.draggedData);
          this.allEmployees.splice(employeeIndex, 1);
        }
        else {
          this.allEmployees.splice(event.previousIndex, 1);
        }
      }
      transferArrayItem(event.previousContainer.data, event.container.data,
        event.previousIndex, event.currentIndex)
      let data = this.draggedData; // event.container.data[0] as any;
      let containerIndex = event.container.data.indexOf(this.draggedData);
      if (data.designationId != 7 && data.designationId != 9 && data.designationId != 6) {
        if (this.draggedFrom == 'all') {
          this.allEmployees.push(data);
        }
        transferArrayItem(event.container.data, event.previousContainer.data,
          containerIndex, event.previousIndex)
        swal("Only Sr.Accountant/Accountant/AAO can be assigned.");
        return 0;
      }
      if (data.roleId > 0) {
        let currentAudit = [];
        for (var i = 0; i < this.auditsList.length; i++) {
          if (this.auditsList[i].auditId == this.selectedAuditId) {
            currentAudit.push(this.auditsList[i]);
            break;
          }
        }
        if (this.employeeResponse.filter(a => a.empId == data.empId && a.auditId == currentAudit[0].auditId && a.isActive == 1).length == 0) {
          if (this.employeeResponse.filter(a => a.empId == data.empId && a.auditId != this.selectedAuditId && ((currentAudit[0].auditFromDate >= a.auditFromDate && currentAudit[0].auditFromDate <= a.auditToDate) || (currentAudit[0].auditToDate >= a.auditFromDate && currentAudit[0].auditToDate <= a.auditToDate) || (currentAudit[0].auditFromDate < a.auditFromDate && currentAudit[0].auditToDate > a.auditToDate)) && a.isActive == 1).length > 0) {
            if (this.draggedFrom == 'all') {
              this.allEmployees.push(data);
            }
            transferArrayItem(event.container.data, event.previousContainer.data,
              containerIndex, event.previousIndex)
            swal("Employee is already assigned to another Audit on this Date range.");
            return 0;
          }
        }
      }
    }
    else {
      moveItemInArray(this.auditDHEmployees, event.previousIndex, event.currentIndex);
    }
  }


  checkIfUserAlreadyAssigned(roleUserMapId, auditId, auditFromDate, auditToDate): any {
    let obj = { RoleUserMapId: roleUserMapId, AuditId: auditId, AuditFromDate: auditFromDate, AuditToDate: auditToDate };
    this._service.checkIfUserAlreadyAssignedToAnotherAudit(obj).subscribe(response => {
      return response;
    });
  }


  save() {
    if (this.auditHeadEmployees.length > 0 || this.auditDHEmployees.length > 0) {
      this.selectedEmployees = [];
      for (var i = 0; i < this.auditHeadEmployees.length; i++) {
        let obj = {
          id: i,
          empId: this.auditHeadEmployees[i].empId,
          roleId: 5,
          email: this.auditHeadEmployees[i].email
        }
        this.selectedEmployees.push(obj);
      }
      for (var i = 0; i < this.auditDHEmployees.length; i++) {
        let obj = {
          id: i,
          empId: this.auditDHEmployees[i].empId,
          roleId: 6,
          email: this.auditDHEmployees[i].email
        }
        this.selectedEmployees.push(obj);
      }
      for (var i = 0; i < this.selectedEmployees.length; i++) {
        this.selectedEmployees[i].id = i + 1;
      }

      let obj = { IPUserId: this.userId, Password: "password@123", AssignUserRole: this.selectedEmployees, AuditId: this.selectedAuditId };
      this.disableForwardToSupervisor = false;
      this._service.assignAuditTeam(obj).subscribe(response => {
        if (response > 0) {
          swal(this._msg.saveMsg);
        }
      });
    }
    else {
      swal(this._msg.selectAtleastOneAssigner);
    }
  }

  getAssignAuditTeam() {
    if (this.selectedAuditId) {
      this.allEmployees = [];
      this.auditDHEmployees = [];
      this.auditHeadEmployees = [];
      this.disableForwardToSupervisor = true;
      this.selectedAudit = this.auditsList.filter(a => a.auditId == this.selectedAuditId)[0] as any;
      this._service.getAssignAuditTeam(this.selectedAuditId).subscribe(response => {
        this.employeeResponse = response;
        let data = response.filter(a => a.auditId == this.selectedAuditId);
        // Get audit head and audit DH of selected Audit
        if (data.length > 0) {
          this.auditHeadEmployees = data.filter(a => a.roleId > 0 && a.roleId == 5 && a.isActive == 1);
          this.auditDHEmployees = data.filter(a => a.roleId > 0 && a.roleId == 6 && a.isActive == 1);
        }
        // Get unassigned Employees 
        this.allEmployees = response.filter(a => a.roleId == 0 || a.isActive == 0);

        // Get Another audit Employees
        let anotherAuditEmp = response.filter(a => a.roleId != 0 && a.auditId != this.selectedAuditId);
        if (anotherAuditEmp.length > 0) {
          for (var i = 0; i < anotherAuditEmp.length; i++) {
            if (this.allEmployees.filter(a => a.empId == anotherAuditEmp[i].empId).length == 0) {
              this.allEmployees.push(anotherAuditEmp[i]);
            }
          }
        }

        // Get those Employees who are not assigned to any audit
        if (data.length == 0) {
          for (var i = 0; i < response.length; i++) {
            if (this.allEmployees.filter(a => a.empId == response[i].empId).length == 0) {
              this.allEmployees.push(response[i]);
            }
          }
        }

        // Check for duplicate employees in audit head
        for (var i = 0; i < this.auditHeadEmployees.length; i++) {
          if (this.allEmployees.filter(a => this.auditHeadEmployees[i].empId == a.empId).length > 0) {
            let emp = this.allEmployees.filter(a => this.auditHeadEmployees[i].empId == a.empId);
            let index = this.allEmployees.indexOf(emp[0]);
            this.allEmployees.splice(index, 1);
          }
        }

        // Check for duplicate employees in audit DH
        for (var i = 0; i < this.auditDHEmployees.length; i++) {
          if (this.allEmployees.filter(a => this.auditDHEmployees[i].empId == a.empId).length > 0) {
            let emp = this.allEmployees.filter(a => this.auditDHEmployees[i].empId == a.empId);
            let index = this.allEmployees.indexOf(emp[0]);
            this.allEmployees.splice(index, 1);
          }
        }

        // Check for duplicate entries
        let empl = this.allEmployees;
        let j = 0;
        while (j + 1 < empl.length) {
          if (this.allEmployees.filter(a => a.empId == empl[j].empId).length > 1) {
            let em = this.allEmployees.filter(a => a.empId == empl[j].empId)[0] as any;
            let index = this.allEmployees.indexOf(em);
            this.allEmployees.splice(index, 1);
          }
          j++;
        }


        // Check if Employee is already assigned to any audit which lie in between selected Audit
        empl = this.allEmployees;
        j = 0;
        while (j + 1 < empl.length) {
          let employee = empl[j];
          if (employee.auditFromDate && employee.auditToDate) {
            if (this.employeeResponse.filter(a => a.empId == employee.empId && a.auditId != this.selectedAuditId && ((this.selectedAudit.auditFromDate >= a.auditFromDate && this.selectedAudit.auditFromDate <= a.auditToDate) || (this.selectedAudit.auditToDate >= a.auditFromDate && this.selectedAudit.auditToDate <= a.auditToDate) || (this.selectedAudit.auditFromDate < a.auditFromDate && this.selectedAudit.auditToDate > a.auditToDate)) && a.isActive == 1).length > 0) {
              let em = this.allEmployees.filter(a => a.empId == empl[j].empId)[0] as any;
              let index = this.allEmployees.indexOf(em);
              this.allEmployees.splice(index, 1);
              j--;
            }
          }
          j++;
        }
        this.filterEmployees();
      });
    }
  }

  forwardToSupervisor(statusId: number) {
    if (this.selectedAuditId && this.selectedSupervisorId > 0) {
      let obj = { StatusId: statusId, AuditId: this.selectedAuditId, UserId: this.userId, ForwardedToUserId: this.selectedSupervisorId, AuditDecisionStatusId: 1 };
      this.verifyService.updateTeamOfAuditForVerification(obj).subscribe(response => {
        if (response && response > 0) {
          swal(this._msg.forwardSupervisorMsg);
          this.getAuditsForDropdown();
          this.selectedAuditId = null;
          this.selectedAudit = null;
          this.auditHeadEmployees = [];
          this.auditDHEmployees = [];
          this.allEmployees = [];
          this.disableForwardToSupervisor = true;
          this.showForwardToSupervisorPopup = false;
          this.selectedSupervisorId = null;
        }
      });
    }
  }

  getSupervisorsList() {
    this.commonService.bindSupervisorDropDown().subscribe(response => {
      this.supervisorsList = response;
    });
  }

  filterEmployees() {
    this.filteredEmployees = this.allEmployees.filter(a => a.empName.toString().toLowerCase().indexOf(this.searchTerm.toLowerCase()) > -1 || a.designation.toString().toLowerCase().indexOf(this.searchTerm.toLowerCase()) > -1);
  }

  onDrag(from, emp) {
    this.draggedData = emp;
    this.draggedFrom = from;
  }

  ngOnInit() {
    this.getAuditsForDropdown();
    this.getSupervisorsList();
  }

}
