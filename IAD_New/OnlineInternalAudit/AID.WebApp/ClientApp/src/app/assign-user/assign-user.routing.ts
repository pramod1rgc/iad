import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AssignUserModule } from './assign-user.module';
import { AssignAuditSupervisorComponent } from './assign-audit-supervisor/assign-audit-supervisor.component';
import { AssignIadHeadComponent } from './assign-iad-head/assign-iad-head.component';
import { AssignAuditTeamComponent } from './assign-audit-team/assign-audit-team.component';

const routes: Routes = [

  {
    path: '', component: AssignUserModule, data: {
      breadcrumb: 'Assign User'
    }, children: [
      {
        path: 'audit-supervisor', component: AssignAuditSupervisorComponent, data: {
          breadcrumb: 'Assign Audit Supervisor'
        }
      },
      {
        path: 'iad-head', component: AssignIadHeadComponent, data: {
          breadcrumb: 'Assign IAD Head'
        }
      },
      {
        path: 'audit-team', component: AssignAuditTeamComponent, data: {
          breadcrumb: 'Assign Audit Team'
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AssignUserRoutingModule { }
