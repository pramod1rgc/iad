import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AssignIadHeadComponent } from './assign-iad-head.component';

describe('AssignIadHeadComponent', () => {
  let component: AssignIadHeadComponent;
  let fixture: ComponentFixture<AssignIadHeadComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AssignIadHeadComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AssignIadHeadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
