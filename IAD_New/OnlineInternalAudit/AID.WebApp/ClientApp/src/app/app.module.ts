import { BrowserModule } from '@angular/platform-browser';
import { AuthguardGuard } from './authguard/authguard.guard';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { HashLocationStrategy, LocationStrategy } from '@angular/common';
import { RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { MaterialModule } from './material.module';
import { CdkTreeModule } from '@angular/cdk/tree';
import { CKEditorModule } from '@ckeditor/ckeditor5-angular';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ChartsModule } from 'ng2-charts/ng2-charts';
import { LoginComponent } from './login/login.component';

import { GlobalModule } from './global/global.module';
import { ErrorInterceptor } from './global/error.interceptor';
import { SharedModule } from './shared-module/shared-module.module';

import { LoginServices } from './login/loginservice';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { NgHttpLoaderModule } from 'ng-http-loader';
import { CalendarModule, DateAdapter } from 'angular-calendar';
import { adapterFactory } from 'angular-calendar/date-adapters/date-fns';
import { FlatpickrModule } from 'angularx-flatpickr';
import { ToastrModule } from 'ngx-toastr';
import { httpInterceptor } from './global/jwt.interceptor';
import { ChangepasswordComponent } from './changepassword/changepassword.component';
import { ReportViewerModule } from 'ngx-ssrs-reportviewer';
import { ReportComponent } from './report/report.component';
import { ScrollToBottomDirective } from './directives/scroll-to-bottom.directive';
@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    ChangepasswordComponent,
    ReportComponent,
    ScrollToBottomDirective
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
    HttpClientModule,
    FormsModule,
    SharedModule,
    MaterialModule,
    CdkTreeModule,
    DragDropModule,
    ChartsModule,
    GlobalModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    NgHttpLoaderModule,
    CKEditorModule,
    ReportViewerModule,
    FlatpickrModule.forRoot(),
    ToastrModule.forRoot(),
    CalendarModule.forRoot({
      provide: DateAdapter,
      useFactory: adapterFactory
    }),
    RouterModule.forRoot([
      {
        path: '', redirectTo: 'login', pathMatch: 'full', data: {
          breadcrumb: 'Login'
        } },
      {
        path: 'login', component: LoginComponent, data: {
          breadcrumb: 'Login'
        } },
      //{ path: 'login', component: LogintokenComponent },
      { path: 'changepassword', component: ChangepasswordComponent },
      { path: 'report', component: ReportComponent },
      //{ path: 'activate', component: LoginActivateComponent },
      { path: 'dashboard', loadChildren: './dashboard/dashboard.module#DashboardModule', canActivate: [AuthguardGuard] }, /*, canActivate: [AuthguardGuard]*/
      { path: '**', redirectTo: 'dashboard', pathMatch: 'full' },


    ])

  ],
  providers: [{ provide: HashLocationStrategy, useClass: LocationStrategy },
  { provide: HTTP_INTERCEPTORS, useClass: httpInterceptor, multi: true },
  { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
  [LoginServices], [AuthguardGuard],

  ],

  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule { }
