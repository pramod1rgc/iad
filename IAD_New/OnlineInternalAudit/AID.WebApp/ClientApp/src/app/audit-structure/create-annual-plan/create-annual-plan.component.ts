import { Component, OnInit, ViewChild } from '@angular/core';
import { CreateannualplanService } from '../../services/auditstructure/createannualplan.service';
import { Subject, ReplaySubject } from 'rxjs';
import { FormControl } from '@angular/forms';
import { takeUntil } from 'rxjs/operators';
import { CdkDragDrop, transferArrayItem, moveItemInArray } from '@angular/cdk/drag-drop';
import swal from 'sweetalert2';
import { Console } from '@angular/core/src/console';
import { MatTableDataSource, MatPaginator, MatSort } from '@angular/material';

import { CommonService } from '../../services/common/common.service';
export interface commondll {
  values: string;
  text: string;
}
export interface AuditData {
  paoName: string;
  paoCode: string;
  auditReason: string;
}
@Component({
  selector: 'app-create-annual-plan',
  templateUrl: './create-annual-plan.component.html',
  styleUrls: ['./create-annual-plan.component.css'],
  providers: [CommonService]
})
export class CreateAnnualPlanComponent implements OnInit {

  userId: number;
 // roleUserMapId: number;
  constructor(private _CreateannualplanService: CreateannualplanService, private _CommonService: CommonService) {
    this.userId = JSON.parse(localStorage.getItem("LoginData")).userId;
    //this.roleUserMapId = JSON.parse(localStorage.getItem("LoginData")).roleUserMapId;
  }
  supervisor: any;
  displayedColumns: string[] = ['paoName', 'paoCode', 'auditReason', 'status','rejectionRemark','Actions'];//'index','paoId', ['select', 'empName', 'desigination', 'oldBasic', 'newBasic', 'wefDate', 'nextIncDate', 'payLevel', 'remark'];//,
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild('frauditDetails') frauditDetails: any;
  dataSource: any;
  //Financial Year
  public filteredYear: ReplaySubject<commondll[]> = new ReplaySubject<commondll[]>(1);
  public YearFilterCtrl: FormControl = new FormControl();
  //End Of Financial
  //Controller
  public filteredController: ReplaySubject<commondll[]> = new ReplaySubject<commondll[]>(1);
  public ControllerFilterCtrl: FormControl = new FormControl();
  //End Of Controller
  //Controller
  public filteredPAO: ReplaySubject<commondll[]> = new ReplaySubject<commondll[]>(1);
  public PAOFilterCtrl: FormControl = new FormControl();
  //End Of Controller
  private _onDestroy = new Subject<void>();
  deactivatePAOAuditPopUp: boolean;
  isTableHasData=true;
  ArrFyear: any;
  mstFyear: any = {};
  Controller: any;
  CreateAuditPlanModel: any;
  //allPAOsList: any[];
  allPAOsList: any;
  allAuditList: any = {};
  PAOData: any;
  currFYear: any;
  paoreason: any;
  currPAOData: any;
  allControllerList: any;
  notFound = true;
  ForwardToSupervisorPopUp: boolean;
  arrSupervisor: any;
  

  ngOnInit() {
 
 
    this.BindDropFYear();
    this.getController();
    //this.getAllPAO();
    this.YearFilterCtrl.valueChanges.pipe(takeUntil(this._onDestroy)).subscribe(() => { this.filterYear(); });
    this.ControllerFilterCtrl.valueChanges.pipe(takeUntil(this._onDestroy)).subscribe(() => { this.filterController(); });
    this.PAOFilterCtrl.valueChanges.pipe(takeUntil(this._onDestroy)).subscribe(() => { this.filterPAO(); });
  }
  BindDropFYear() {
    //alert('alert')
    this._CreateannualplanService.BindDropFYear().subscribe(result => {
     // debugger;
      this.ArrFyear = result;
      this.mstFyear.FYear = 0;
      this.filteredYear.next(this.ArrFyear);
    })
  }

  getController() {
    this._CreateannualplanService.getController().subscribe(response => {
      debugger;
      this.allControllerList = response;
      this.Controller = null;
      this.filteredController.next(this.allControllerList);
    });
  }

  //Get All Audit By Finanacial Year
  getAllAudit(value) {
    this.currFYear = value;
    this.PAOData = null;
    this.Controller = null;
    this._CreateannualplanService.getauditlist(value).subscribe(response => {
      debugger;
      this.allAuditList = response;
      this.dataSource = new MatTableDataSource(response);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
      if (this.dataSource.filteredData.length > 0) { this.isTableHasData = false; }

      else { this.isTableHasData = true; }
      //Filetr inData source
      this.dataSource.filterPredicate =
        (data: AuditData, filtersJson: string) => {
          const matchFilter = [];
          const filters = JSON.parse(filtersJson);

          filters.forEach(filter => {
            const val = data[filter.id] === null ? '' : data[filter.id];
            matchFilter.push(val.toLowerCase().includes(filter.value.toLowerCase()));
          });
          return matchFilter.every(Boolean);
        };
       //END Of Filetr inData source
    });
  }
  applyFilter(filterValue: string) {
    const tableFilters = [];
    tableFilters.push({
      id: 'paoName',
      value: filterValue
    });


    this.dataSource.filter = JSON.stringify(tableFilters);
    //this.menudataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
    if (this.dataSource.filteredData.length > 0) {
      this.notFound = true;
    } else {
      this.notFound = false;
    }
  }


  // use for filter search in dropdawn
  private filterYear() {
    debugger;
    if (!this.filteredYear) {
      return;
    }
    // get the search keyword
    let search = this.YearFilterCtrl.value;
    if (!search) {

      this.filteredYear.next(this.ArrFyear.slice());
      return;
    }
    else {
      search = search.toLowerCase();
    }
    var a = this.ArrFyear;
    this.filteredYear.next(
      this.ArrFyear.filter(ArrFyear => ArrFyear.financialYear.toLowerCase().indexOf(search) > -1)
    );
  }
  //Filter Controller
  private filterController() {
    debugger;
    if (!this.filteredController) {
      return;
    }
    // get the search keyword
    let search = this.ControllerFilterCtrl.value;
    if (!search) {

      this.filteredController.next(this.allControllerList.slice());
      return;
    }
    else {
      search = search.toLowerCase();
    }
   
    this.filteredController.next(
      this.allControllerList.filter(allControllerList => allControllerList.controllerName.toLowerCase().indexOf(search) > -1)
    );
  }
  //End Of Controller

  //Filter Controller
  private filterPAO() {
    debugger;
    if (!this.filteredPAO) {
      return;
    }
    // get the search keyword
    let search = this.PAOFilterCtrl.value;
    if (!search) {

      this.filteredPAO.next(this.allPAOsList.slice());
      return;
    }
    else {
      search = search.toLowerCase();
    }

    this.filteredPAO.next(
      this.allPAOsList.filter(allPAOsList => allPAOsList.paoName.toLowerCase().indexOf(search) > -1)
    );
  }
  //End Of Controller

  //Get Pao List By ControllerId
  getAllPAO(value) {
    debugger;
   // alert(value);
    this._CreateannualplanService.getAllPAO(value).subscribe(result => {
      debugger;
      this.allPAOsList = result;
      this.PAOData = null;
      //this.Controller = 0;
      this.filteredPAO.next(this.allPAOsList);
      //this.dataSource = new MatTableDataSource(this.allPAOsList);
    });
  }
  //End Of Get Pao List By ControllerId

 

  //Drop Row Table
  //drop(event: CdkDragDrop<string[]>) {
  //  if (event.previousContainer !== event.container) {
  //    transferArrayItem(event.previousContainer.data, event.container.data,
  //      event.previousIndex, event.currentIndex)
  //  } else {
  //    moveItemInArray(this.allPAOsList, event.previousIndex, event.currentIndex);
  //  }
  //}

  //End Of Row Table


  save() {
    debugger;
    console.log(this.allAuditList);
    var a = JSON.stringify(this.allAuditList);
    console.log(this.allAuditList);
    debugger;
    //alert(a);
    if (this.allAuditList.length > 0) {
      let requestObj = [];
      for (var i = 0; i < this.allAuditList.length; i++) {
        debugger;
        let obj = { IsActive: 1, PAOId: this.allAuditList[i].paoId };
        //this.allAuditList[i].id = i + 1;
        requestObj.push(obj);
        //alert(a[i]['paoid']);
      }
      console.log(requestObj);



      this._CreateannualplanService.assignPaoAudit(requestObj).subscribe(response => {
        //if (response > 0) {
        //  swal("Data saved successfully!!!!");
        //}
      });
    }
    else {
      alert("Select atleast one audit supervisor.");
    }
  }
  setvaluePaoID(PAOData) {
    this.currPAOData = PAOData;
  }
  assignAuditByController() {

    //alert(this.paoreason);
    //return false;
    if (this.currFYear == undefined) {
      alert("Please select Financial..");
      return false;
    }
    
    this._CreateannualplanService.assignAuditByController(this.currPAOData, 'N', this.currFYear, this.paoreason).subscribe(response => {
      debugger;
      var message = '';
      message = response;
      swal(message);
      this.PAOData = null;
      this.Controller = null;
      this.currPAOData = null;
      this.paoreason = '';
      this.getAllAudit(this.currFYear);
      this.frauditDetails.resetForm();

    });



  }
  deactivateAudit() {
    //alert(PAOid);
    //alert(this.currPAOData);
    this._CreateannualplanService.assignAuditByController(this.currPAOData, 'O','','').subscribe(response => {
      debugger;
      var message = '';
      message = response;
      swal(message);
      this.PAOData = 0;
      this.Controller = 0;
      this.deactivatePAOAuditPopUp = false;
      this.getAllAudit(this.currFYear);

    });
  }

  assignToSupervisor() {
    
    if (this.currFYear == undefined) {
      alert("Please select Financial..");
      return false;
    }

    this._CreateannualplanService.assignToSupervisor(this.currFYear, this.supervisor, this.userId).subscribe(response => {
      debugger;     
      var message = '';
      message = response;
      swal(message);
      this.PAOData = null;
      this.Controller = null;
      this.currPAOData = null;
      this.paoreason = '';
      this.ForwardToSupervisorPopUp = false;
      this.getAllAudit(this.currFYear);
      this.bindSupervisorDropDown();


    });
  }

  bindSupervisorDropDown() {
    //alert('alert')
    this._CommonService.bindSupervisorDropDown().subscribe(result => {
      // debugger;
      this.arrSupervisor = result;

    })
  }
  Cancel() {
    this.supervisor = '';
    this.bindSupervisorDropDown();
  }
  
}
