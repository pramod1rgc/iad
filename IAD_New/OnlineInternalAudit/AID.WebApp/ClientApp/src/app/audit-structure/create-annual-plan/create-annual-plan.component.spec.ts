import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateAnnualPlanComponent } from './create-annual-plan.component';

describe('CreateAnnualPlanComponent', () => {
  let component: CreateAnnualPlanComponent;
  let fixture: ComponentFixture<CreateAnnualPlanComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateAnnualPlanComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateAnnualPlanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
