import { Component, OnInit, ViewChild } from '@angular/core';
import { CreateannualplanService } from '../../services/auditstructure/createannualplan.service';
import { ReplaySubject, Subject } from 'rxjs';
import { FormControl } from '@angular/forms';
import { takeUntil, retry } from 'rxjs/operators';
import { MatSort, MatTableDataSource, MatPaginator } from '@angular/material';

import { SelectionModel } from '@angular/cdk/collections';
import swal from 'sweetalert2';


export interface PaoAuditElement {
  paoId: string;
  paoName: number;
  paoCode: number;
}

export interface commondll {
  values: string;
  text: string;
}
@Component({
  selector: 'app-create-quarterly-plan',
  templateUrl: './create-quarterly-plan.component.html',
  styleUrls: ['./create-quarterly-plan.component.css']
})
export class CreateQuarterlyPlanComponent implements OnInit {

  constructor(private _CreateannualplanService: CreateannualplanService) { }
  //Financial Year
  displayedColumns: string[] = ['paoName', 'paoCode', 'select'];//'index','paoId', ['select', 'empName', 'desigination', 'oldBasic', 'newBasic', 'wefDate', 'nextIncDate', 'payLevel', 'remark'];//,
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  //ForGride Checkbox
  dataSource = new MatTableDataSource<PaoAuditElement>(this.dataSource);
  selectedArray: any[];
  selection = new SelectionModel<PaoAuditElement>(true, []);
  unselection = new SelectionModel<PaoAuditElement>(true, []);
  dataDeAttach: any[] = [];
  
  undataDeAttach: any[] = [];
  mstauditFYear: any = {};
  //End Of CheckBox


  selecteddataSource = new MatTableDataSource<PaoAuditElement>(this.selecteddataSource);


  public filteredYear: ReplaySubject<commondll[]> = new ReplaySubject<commondll[]>(1);
  public YearFilterCtrl: FormControl = new FormControl();
  private _onDestroy = new Subject<void>();
  //dataSource: any;
  notFound = true;
  chkdataSource: any;
  ArranualAuditList: any;
  ArranualAuditList1: any;
  ArrFyear: any;
  mstFyear: any = {};
  //End Of Financial
  ngOnInit() {
    this.BindDropFYear();
    this.YearFilterCtrl.valueChanges.pipe(takeUntil(this._onDestroy)).subscribe(() => { this.filterYear(); });
  }
  // use for filter search in dropdawn
  BindDropFYear() {
    //alert('alert')
    this._CreateannualplanService.BindDropFYear().subscribe(result => {
      // debugger;
      this.ArrFyear = result;
      this.mstFyear.FYear = 0;
      this.filteredYear.next(this.ArrFyear);
    })
  }
  private filterYear() {
    debugger;
    if (!this.filteredYear) {
      return;
    }
    // get the search keyword
    let search = this.YearFilterCtrl.value;
    if (!search) {

      this.filteredYear.next(this.ArrFyear.slice());
      return;
    }
    else {
      search = search.toLowerCase();
    }
    var a = this.ArrFyear;
    this.filteredYear.next(
      this.ArrFyear.filter(ArrFyear => ArrFyear.financialYear.toLowerCase().indexOf(search) > -1)
    );
  }
  //Gride
  bindAuditInGrid(value) {
    //alert('alert')
    
    this._CreateannualplanService.bindAuditInGrid(value).subscribe(result => {
      debugger;
      //alert("hiii");
      this.ArranualAuditList = result;
      
      //this.dataSource = new MatTableDataSource(this.ArranualAuditList);
      this.dataSource = new MatTableDataSource(result);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;

      //Filetr inData source
      this.dataSource.filterPredicate =
        (data: PaoAuditElement, filtersJson: string) => {
          const matchFilter = [];
          const filters = JSON.parse(filtersJson);

          filters.forEach(filter => {
            const val = data[filter.id] === null ? '' : data[filter.id];
            matchFilter.push(val.toLowerCase().includes(filter.value.toLowerCase()));
          });
          return matchFilter.every(Boolean);
        };
      //END Of Filetr inData source

    })
  }

  applyFilter(filterValue: string) {
    const tableFilters = [];
    tableFilters.push({
      id: 'paoName',
      value: filterValue
    });


    this.dataSource.filter = JSON.stringify(tableFilters);
    //this.menudataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
    if (this.dataSource.filteredData.length > 0) {
      this.notFound = true;
    } else {
      this.notFound = false;
    }
  }
  //End Of Gride



  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach(row => this.selection.select(row));
  }

  /** The label for the checkbox on the passed row */
  checkboxLabel(row?: PaoAuditElement): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.paoId + 1}`;
  }

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }


  getSelectedValue(): any {
    debugger;
    if (this.selection.hasValue()) {
       this.selection.selected.forEach(item => {
        let index: number = this.ArranualAuditList.findIndex(d => d === item);
        this.dataSource.data.splice(index, 1);
        this.dataSource = new MatTableDataSource(this.dataSource.data);
        this.dataSource.paginator = this.paginator;
       this.dataDeAttach.push(item);
        this.selecteddataSource = new MatTableDataSource<PaoAuditElement>(this.dataDeAttach);

      });
    }

    this.selection = new SelectionModel<PaoAuditElement>(true, []);
  }


  //Selected Audit In Last Gride

  //End Of  /** Selects all rows if they are not all selected; otherwise clear selection. */
  UnmasterToggle() {
    this.UnisAllSelected() ?
      this.unselection.clear() :
      this.selecteddataSource.data.forEach(row => this.unselection.select(row));
  }

  /** The label for the checkbox on the passed row */
  UncheckboxLabel(row?: PaoAuditElement): string {
    if (!row) {
      return `${this.UnisAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${this.unselection.isSelected(row) ? 'deselect' : 'select'} row ${row.paoId + 1}`;
  }

  /** Whether the number of selected elements matches the total number of rows. */
  UnisAllSelected() {
    const numSelected = this.unselection.selected.length;
    const numRows = this.selecteddataSource.data.length;
    return numSelected === numRows;
  }


  RemoveSelectedValue(): any {
    if (this.unselection.hasValue()) {
      this.unselection.selected.forEach(item => {
          debugger;
        //  let index: number = this.ArranualAuditList1.findIndex(d => d.paoName === item.paoName);
        let index: number = this.selecteddataSource.data.findIndex(d => d.paoId === item.paoId)
          this.selecteddataSource.data.splice(index, 1);
          this.selecteddataSource = new MatTableDataSource(this.selecteddataSource.data);
          this.selecteddataSource.paginator = this.paginator;
          this.undataDeAttach = this.dataSource.data;
          this.undataDeAttach.push(item);
          this.dataSource = new MatTableDataSource<PaoAuditElement>(this.undataDeAttach);
          this.unselection = new SelectionModel<PaoAuditElement>(true, []);

        });
      

        
    }

   // this.unselection = new SelectionModel<PaoAuditElement>(true, []);
  }

  //Save Quartaly Audit
  SaveQuartalyAudit() {
    if (this.mstauditFYear.financialYearId == undefined) {
      alert("Please select financial year..")
      return false;
    }
    if (this.mstauditFYear.Quarter == undefined) {
      alert("Please select Quarterly..")
      return false;
    }
    
    if (this.unselection.hasValue()) {
      this.unselection.selected[0]["financialYearId"] = this.mstauditFYear.financialYearId;
      this.unselection.selected[0]["Quarter"] = this.mstauditFYear.Quarter;  
     
    }
     
    this._CreateannualplanService.SaveQuartalyAudit(this.unselection.selected).subscribe(result => {
      var message = result;
     
      //if (message == 'Quraterly assigned successfully') {
      //  this.bindAuditInGrid(this.mstauditFYear.financialYearId);
       
      //}
      this.selecteddataSource.data = new MatTableDataSource(null);
      this.dataDeAttach = [];
      this.unselection = new SelectionModel<PaoAuditElement>(true, []);
      swal(message);
      
      //debugger;
      //this.bindAuditInGrid(this.mstauditFYear.financialYearId);
      //this.selecteddataSource = null;
   
      //this.unselection = new SelectionModel<PaoAuditElement>(true, []);     

    })
    //this.unselection = new SelectionModel<PaoAuditElement>(true, []);
    //this.selecteddataSource = null;
     
    
  }
 

}
