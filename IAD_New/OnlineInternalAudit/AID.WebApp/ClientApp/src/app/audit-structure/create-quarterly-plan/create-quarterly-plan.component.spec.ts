import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateQuarterlyPlanComponent } from './create-quarterly-plan.component';

describe('CreateQuarterlyPlanComponent', () => {
  let component: CreateQuarterlyPlanComponent;
  let fixture: ComponentFixture<CreateQuarterlyPlanComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateQuarterlyPlanComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateQuarterlyPlanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
