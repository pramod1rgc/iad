import { Component, OnInit, ChangeDetectionStrategy, TemplateRef, ViewChild } from '@angular/core';
import { isSameDay, isSameMonth } from 'date-fns';
import { Subject } from 'rxjs';
import { CalendarEvent, CalendarEventAction, CalendarEventTimesChangedEvent, CalendarView, CalendarMonthViewDay } from 'angular-calendar';
import { AuditScheduleService } from '../../services/auditstructure/audit-schedule.service';
import { CommonMsg } from '../../global/common-msg'
import swal from 'sweetalert2';
import { VerifyScheduledAuditService } from '../../services/verification/verifyScheduledAudit.service';
import { CommonService } from '../../services/common/common.service';
import { MatDialog } from '@angular/material';

@Component({
  selector: 'app-create-annual-schedule',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './create-annual-schedule.component.html',
  styleUrls: ['./create-annual-schedule.component.css']
})
export class CreateAnnualScheduleComponent implements OnInit {

  @ViewChild('ChooseEndDatePopup') ChooseEndDatePopup: TemplateRef<any>;
  @ViewChild('EditEventPopup') EditEventPopup: TemplateRef<any>;
  @ViewChild('DeleteEventPopup') DeleteEventPopup: TemplateRef<any>;
  @ViewChild('showRemarkPopup') showRemarkPopup: TemplateRef<any>;
  @ViewChild('showForwardToSupervisorPopup') showForwardToSupervisorPopup: TemplateRef<any>;

  financialYears: any[] = [];

  currentPAOName: string;
  selectedEvent: CalendarEvent;
  choosenEndDate: string = '';

  paoList: any[] = [];

  allPaoList: any[] = [];

  unassignedEvents: CalendarEvent[] = [];

  selectedFinYearId: number;
  selectedQuarter: string;
  userId: number;

  selectedPAO: CalendarEvent[] = [];

  disableForwardToSupervisorButton: boolean = true;

  selectedRejectionRemark: string;

  quarters: any[] = [];

  supervisorsList: any[] = [];

  selectedSupervisorId: number = 0;

  previousBtnDisabled: boolean = false;

  nextBtnDisabled: boolean = false;


  //////---------------------------- Calendar Begin ------------------------------------////////

  view: CalendarView = CalendarView.Month;

  CalendarView = CalendarView;

  viewDate: Date = new Date();

  actions: CalendarEventAction[] = [
    {
      label: '<a class="material-icons i-edit" matTooltip="Edit">edit</a>',
      a11yLabel: 'Edit',
      onClick: ({ event }: { event: CalendarEvent }): void => {
        this.editEvent(event);
      }
    },
    {
      label: '<a class="material-icons i-delete" matTooltip="Delete">delete</a>',
      a11yLabel: 'Delete',
      onClick: ({ event }: { event: CalendarEvent }): void => {
        this.deleteEvent(event);
      }
    }
  ];

  refresh: Subject<any> = new Subject();

  events: CalendarEvent[] = [];

  activeDayIsOpen: boolean = false;

  dayClicked({ date, events }: { date: Date; events: CalendarEvent[] }): void {
    if (isSameMonth(date, this.viewDate)) {
      if (
        (isSameDay(this.viewDate, date) && this.activeDayIsOpen === true) ||
        events.length === 0
      ) {
        this.activeDayIsOpen = false;
      } else {
        this.activeDayIsOpen = true;
      }
      this.viewDate = date;
    }
    this.selectEvent(events);
  }

  eventDropped({
    event,
    newStart,
    newEnd,
    allDay
  }: CalendarEventTimesChangedEvent): void {
    const externalIndex = this.unassignedEvents.indexOf(event);
    if (typeof allDay !== 'undefined') {
      event.allDay = allDay;
    }
    if (externalIndex > -1) {
      this.unassignedEvents.splice(externalIndex, 1);
      this.events.push(event);
    }
    event.start = newStart;
    if (newEnd) {
      event.end = newEnd;
    }
    if (this.view === 'month') {
      this.viewDate = newStart;
      this.activeDayIsOpen = true;
    }
    this.events = [...this.events];
    if (externalIndex > -1) {
      this.choosenEndDate = newStart.toDateString();
      this.currentPAOName = event.meta.paoName;
      this.selectedEvent = event;
      this.dialog.open(this.ChooseEndDatePopup);
    }
  }

  beforeMonthViewRender({ body }: { body: CalendarMonthViewDay[] }): void {
    body.forEach(day => {
      day.badgeTotal = day.events.filter(
        event => event.meta.incrementsBadgeTotal
      ).length;
    });
  }

  closeOpenMonthViewDay() {
    this.activeDayIsOpen = false;
  }

  //////---------------------------- Calendar End ------------------------------------////////

  constructor(private _service: AuditScheduleService, private _msg: CommonMsg, private verifyService: VerifyScheduledAuditService, private commonService: CommonService, private dialog: MatDialog) {
    this.userId = JSON.parse(localStorage.getItem("LoginData")).userId;
  }

  getQuartersList() {
    this.quarters = [];
    this.verifyService.getDistinctQuartersBasedOnStatusAndFY(this.selectedFinYearId, '2,6,10', 0).subscribe(response => {
      if (response && response.length > 0) {
        let allQuarters = this._msg.getquartersWithDate(this.financialYears.filter(a => a.financialYearId == this.selectedFinYearId)[0].financialYear);
        for (var i = 0; i < allQuarters.length; i++) {
          if (response.filter(a => a.quarter == allQuarters[i].Id).length > 0) {
            this.quarters.push(allQuarters[i]);
          }
        }
      }
    });
  }

  selectEvent(paos: CalendarEvent[]) {
    this.selectedPAO = paos;
  }

  selectSchedule(pao: CalendarEvent) {
    this.viewDate = pao.start;
    this.selectedPAO = [];
    this.selectedPAO.push(pao);
  }

  checkIfSelectedPAOExists(pao: CalendarEvent): boolean {
    return this.selectedPAO.filter(a => a.meta.paoName == pao.meta.paoName).length > 0;
  }

  getScheduledAudits() {
    if (this.selectedFinYearId && this.selectedQuarter) {
      this.disableForwardToSupervisorButton = true;
      this._service.getScheduledAudits(this.selectedFinYearId, this.selectedQuarter).subscribe(response => {
        this.allPaoList = response;
        this.events = [];
        this.unassignedEvents = [];
        for (var i = 0; i < this.allPaoList.length; i++) {
          let colr = {
            evntColor: {
              primary: this._msg.getRandomColor(),
              secondary: this._msg.getRandomColor()
            }
          }
          if (this.allPaoList[i].auditFromDate && this.allPaoList[i].auditToDate) {
            let evnt: CalendarEvent = {
              title: this.allPaoList[i].paoName,
              color: colr.evntColor,
              start: new Date(this.allPaoList[i].auditFromDate),
              end: new Date(this.allPaoList[i].auditToDate),
              meta: this.allPaoList[i],
              actions: this.actions,
              draggable: true,
            }
            evnt.meta.incrementsBadgeTotal = false;
            this.events.push(evnt);
          }
          else {
            let evnt: CalendarEvent = {
              title: this.allPaoList[i].paoName,
              color: colr.evntColor,
              start: new Date(),
              meta: this.allPaoList[i],
              actions: this.actions,
              draggable: true,
            }
            evnt.meta.incrementsBadgeTotal = false;
            this.unassignedEvents.push(evnt);
          }
        }
        let selectedQuarterDetails = this.quarters.filter(a => a.Id == this.selectedQuarter)[0] as any;
        this.viewDate = new Date(selectedQuarterDetails.StartDate);
        this.previousBtnDisabled = true;
        this.nextBtnDisabled = false;
        this.refresh.next();
      });
    }
  }

  disabledBtnClickEvent() {
    let selectedQuarterDetails = this.quarters.filter(a => a.Id == this.selectedQuarter)[0] as any;
    if (selectedQuarterDetails) {
      let startDate = new Date(selectedQuarterDetails.StartDate);
      let endDate = new Date(selectedQuarterDetails.EndDate);
      let startMonth = startDate.getMonth();
      let endMonth = endDate.getMonth();
      let currentMonth = this.viewDate.getMonth();

      if (currentMonth == startMonth) {
        this.previousBtnDisabled = true;
        this.nextBtnDisabled = false;
      }
      if (currentMonth > startMonth && currentMonth < endMonth) {
        this.previousBtnDisabled = false;
        this.nextBtnDisabled = false;
      }
      if (currentMonth == endMonth) {
        this.previousBtnDisabled = false;
        this.nextBtnDisabled = true;
      }
    }
  }

  getFinancialYears() {
    this._service.BindDropFYear().subscribe(response => {
      this.financialYears = response;
    });
  }

  setEndDate() {
    let selectedIndex = this.events.indexOf(this.selectedEvent);
    this.events[selectedIndex].end = new Date(this.choosenEndDate);
    this.selectedEvent = null;
    this.currentPAOName = '';
    this.dialog.closeAll();
    this.choosenEndDate = '';
    this.refresh.next();
  }

  editEvent(evnt) {
    this.dialog.open(this.EditEventPopup);
    this.selectedEvent = evnt;
  }

  setEditedDates() {
    let selectedIndex = this.events.indexOf(this.selectedEvent);
    this.events[selectedIndex].start = this.selectedEvent.start;
    this.events[selectedIndex].end = this.selectedEvent.end;
    this.selectedEvent = null;
    this.dialog.closeAll();
    this.refresh.next();
  }

  externalDrop(event: CalendarEvent) {
    if (this.allPaoList.indexOf(event) === -1) {
      this.events = this.events.filter(iEvent => iEvent !== event);
      this.allPaoList.push(event);
    }
  }

  deleteEvent(eventToBeDeleted: CalendarEvent) {
    this.dialog.open(this.DeleteEventPopup);
    this.selectedEvent = eventToBeDeleted;
  }

  confirmDeleteEvent() {
    let index = this.events.indexOf(this.selectedEvent);
    this.selectedEvent.end = undefined;
    this.events.splice(index, 1);
    this.unassignedEvents.push(this.selectedEvent);
    this.activeDayIsOpen = false;
    this.selectedEvent = null;
    this.dialog.closeAll();
    this.refresh.next();
  }



  //filterPAO(value) {
  //  if (value)
  //    this.unassignedEvents = this.allPaoList.filter(a => a.paoName.toLowerCase().indexOf(value) > -1);
  //  else
  //    this.unassignedEvents = this.allPaoList;
  //}

  save() {
    let requestEvents: any[] = [];
    for (var i = 0; i < this.unassignedEvents.length; i++) {
      let evnt = {
        Id: i + 1,
        AuditId: this.unassignedEvents[i].meta.auditId,
        AuditFromDate: this.unassignedEvents[i].start,
        AuditToDate: this.unassignedEvents[i].end
      }
      requestEvents.push(evnt);
    }
    for (var i = 0; i < this.events.length; i++) {
      let evnt = {
        Id: i + 1,
        AuditId: this.events[i].meta.auditId,
        AuditFromDate: this.events[i].start,
        AuditToDate: this.events[i].end
      }
      requestEvents.push(evnt);
    }

    requestEvents.forEach(a => a.Id = requestEvents.indexOf(a) + 1);
    let obj = { AuditSchedulesType: requestEvents, UserId: this.userId };
    this.disableForwardToSupervisorButton = false;
    this._service.updateScheduleAudit(obj).subscribe(response => {
      if (response > 0) {
        swal(this._msg.saveMsg);
      }
      else {
        swal(this._msg.errorMsg);
      }
    });
  }

  forwardToSupervisor(statusId: number) {
    if (this.selectedSupervisorId > 0) {
      let requestEvents: any[] = [];
      for (var i = 0; i < this.events.length; i++) {
        let evnt = {
          Id: i + 1,
          AuditId: this.events[i].meta.auditId,
          AuditFromDate: this.events[i].start,
          AuditToDate: this.events[i].end
        }
        requestEvents.push(evnt);
      }
      requestEvents.forEach(a => a.Id = requestEvents.indexOf(a) + 1);
      if (requestEvents.length > 0) {
        let obj = { AuditSchedulesType: requestEvents, UserId: this.userId, StatusId: statusId, ForwardedToUserId: this.selectedSupervisorId, AuditDecisionStatusId: 1 };
        this.verifyService.updateScheduledAuditForVerification(obj).subscribe(response => {
          if (response > 0) {
            swal(this._msg.forwardSupervisorMsg);
            this.getScheduledAudits();
            this.dialog.closeAll();
            this.selectedSupervisorId = 0;
          }
          else {
            swal(this._msg.errorMsg);
          }
        });
      }
    }
  }

  reset() {
    this.getScheduledAudits();
  }

  showRejectionRemark(meta) {
    this.selectedRejectionRemark = meta.rejectionRemark;
    this.dialog.open(this.showRemarkPopup);
  }

  getSupervisorsList() {
    this.commonService.bindSupervisorDropDown().subscribe(response => {
      this.supervisorsList = response;
    });
  }

  ngOnInit() {
    this.paoList = this.allPaoList;
    this.getFinancialYears();
    this.getSupervisorsList();
  }

}
