import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateAnnualScheduleComponent } from './create-annual-schedule.component';

describe('CreateAnnualScheduleComponent', () => {
  let component: CreateAnnualScheduleComponent;
  let fixture: ComponentFixture<CreateAnnualScheduleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateAnnualScheduleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateAnnualScheduleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
