import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateauditplanquarterlyComponent } from './createauditplanquarterly.component';

describe('CreateauditplanquarterlyComponent', () => {
  let component: CreateauditplanquarterlyComponent;
  let fixture: ComponentFixture<CreateauditplanquarterlyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateauditplanquarterlyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateauditplanquarterlyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
