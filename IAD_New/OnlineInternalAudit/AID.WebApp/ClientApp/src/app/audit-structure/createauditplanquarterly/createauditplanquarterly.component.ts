import { Component, OnInit, ViewChild } from '@angular/core';
import { CreateannualplanService } from '../../services/auditstructure/createannualplan.service';
import { ReplaySubject, Subject } from 'rxjs';
import { FormControl } from '@angular/forms';
import { takeUntil, retry } from 'rxjs/operators';
import { MatSort, MatTableDataSource, MatPaginator } from '@angular/material';
import * as $ from 'jquery';

import { SelectionModel } from '@angular/cdk/collections';

import { CommonService } from '../../services/common/common.service';
import swal from 'sweetalert2';
export interface commondll {
  values: string;
  text: string;
}
export interface PaoAuditElement {
  paoId: string;
  paoName: number;
  paoCode: number;
}
@Component({
  selector: 'app-createauditplanquarterly',
  templateUrl: './createauditplanquarterly.component.html',
  styleUrls: ['./createauditplanquarterly.component.css'],
  providers: [CommonService]
})
export class CreateauditplanquarterlyComponent implements OnInit {

  userId: number;
  constructor(private _CreateannualplanService: CreateannualplanService, private _CommonService: CommonService) {
    this.userId = JSON.parse(localStorage.getItem("LoginData")).userId;
  }
  //Financial Year
  displayedColumns: string[] = ['paoName', 'Q1', 'Q2', 'Q3', 'Q4','notabailable','status'];//, 'paoCode'];
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  public filteredYear: ReplaySubject<commondll[]> = new ReplaySubject<commondll[]>(1);
  public YearFilterCtrl: FormControl = new FormControl();
  private _onDestroy = new Subject<void>();
  SaveQuarterConfirmationPopUp: boolean;
  ArrFyear: any;
  isTableHasData = true;
  notFound = true;
  mstFyear: any = {};
  dataSource: any;
  mstauditFYear: any = {};
  currYear: any;
  isShowreject: boolean;
  isForwardToSupervisorbtn: boolean;
  ForwardToSupervisorPopUp: boolean;
  rejectedReason: any;
  arrSupervisor: any;


  supervisor: any;
  ngOnInit() {
    this.BindDropFYear();
    this.isForwardToSupervisorbtn = true;
    this.YearFilterCtrl.valueChanges.pipe(takeUntil(this._onDestroy)).subscribe(() => { this.filterYear(); });
  }
  // use for filter search in dropdawn
  BindDropFYear() {
    //alert('alert')
    this._CreateannualplanService.BindDropFYear().subscribe(result => {
      // debugger;
      this.ArrFyear = result;
      this.mstFyear.FYear = 0;
      this.filteredYear.next(this.ArrFyear);
    })
  }
  private filterYear() {
    debugger;
    if (!this.filteredYear) {
      return;
    }
    // get the search keyword
    let search = this.YearFilterCtrl.value;
    if (!search) {

      this.filteredYear.next(this.ArrFyear.slice());
      return;
    }
    else {
      search = search.toLowerCase();
    }
    var a = this.ArrFyear;
    this.filteredYear.next(
      this.ArrFyear.filter(ArrFyear => ArrFyear.financialYear.toLowerCase().indexOf(search) > -1)
    );
  }
  //Gride
  bindAuditInGrid(value) {
    this.currYear = value;
    this._CreateannualplanService.bindAuditInGrid(value).subscribe(result => {
     
      if (result.length > 0) {
      if (result[0].auditStatusId == 11) {
        this.isShowreject = true;
        this.rejectedReason = result[0].rejectionRemark;
        }
      }
      //alert (result[0].auditStatusId)
      //this.dataSource = new MatTableDataSource(this.ArranualAuditList);
      this.dataSource = new MatTableDataSource(result);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
      if (this.dataSource.filteredData.length > 0) { this.isTableHasData = false; }

      else { this.isTableHasData = true; }
      //Filetr inData source
      this.dataSource.filterPredicate =
        (data: PaoAuditElement, filtersJson: string) => {
          const matchFilter = [];
          const filters = JSON.parse(filtersJson);

          filters.forEach(filter => {
            const val = data[filter.id] === null ? '' : data[filter.id];
            matchFilter.push(val.toLowerCase().includes(filter.value.toLowerCase()));
          });
          return matchFilter.every(Boolean);
        };
      //END Of Filetr inData source

    })
  }

  applyFilter(filterValue: string) {
    const tableFilters = [];
    tableFilters.push({
      id: 'paoName',
      value: filterValue
    });


    this.dataSource.filter = JSON.stringify(tableFilters);
    //this.menudataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
    if (this.dataSource.filteredData.length > 0) {
      this.notFound = true;
    } else {
      this.notFound = false;
    }
  }

 



  //End Of Gride
  SaveQuartalyAudit() {
    //alert(this.mstauditFYear.financialYearId)
    //debugger;
    var iDsStatus = "";
    var data = [];
    var data = $.map($("input:radio:checked"), function (elem, idx) {
      // return "&" + $(elem).attr("name") + "=" + $(elem).val();
     
      return  $(elem).val();
    });
    //console.log(data.length);

    this._CreateannualplanService.SaveQuartalyPlanAudit(data, this.mstauditFYear.financialYearId).subscribe(result => {
      var message = result;
      swal(message);
      this.SaveQuarterConfirmationPopUp = false;
      this.isShowreject = false;
      this.isForwardToSupervisorbtn = false;
      
    })

  }

  ActiveSuperToForward() {
    this.isForwardToSupervisorbtn = true;
  }

  assignQuarterlyPlanToSupervisor() {


    if (this.currYear == undefined) {
      alert("Please select Financial..");
      return false;
    }

    this._CreateannualplanService.assignQuarterlyPlanToSupervisor(this.currYear, this.supervisor, this.userId).subscribe(response => {
      debugger;
      var message = '';
      message = response;
      swal(message);
      this.ForwardToSupervisorPopUp = false;
      this.isForwardToSupervisorbtn = true;
      this.bindAuditInGrid(this.currYear);
    });
  }

  bindSupervisorDropDown() {
    //alert('alert')
    this._CommonService.bindSupervisorDropDown().subscribe(result => {
      // debugger;
      this.arrSupervisor = result;

    })
  }
  Cancel() {
    this.supervisor = '';
    this.bindSupervisorDropDown();
  }
}
