import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuditStructureModule } from './audit-structure.module';
import { CreateAnnualScheduleComponent } from './create-annual-schedule/create-annual-schedule.component';
import { CreateAnnualPlanComponent } from './create-annual-plan/create-annual-plan.component';
//import { CreateQuarterlyPlanComponent } from './create-quarterly-plan/create-quarterly-plan.component';

import { CreateauditplanquarterlyComponent } from './createauditplanquarterly/createauditplanquarterly.component';

const routes: Routes = [

  {
    path: '', component: AuditStructureModule, data: {
      breadcrumb: 'Audit Structure'
    }, children: [
      {
        path: 'annual-schedule', component: CreateAnnualScheduleComponent, data: {
          breadcrumb: 'Create Audit Annual Schedule'
        }
      },
      {
        path: 'CreateAnnualPlan', component: CreateAnnualPlanComponent, data: {
          breadcrumb: 'Create Annual Plan '
        }
      },
      {
        path: 'CreateQuarterlyPlan', component: CreateauditplanquarterlyComponent, data: {
          breadcrumb: 'Create Quarterly Plan '
        }

      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AuditStructureRoutingModule { }
