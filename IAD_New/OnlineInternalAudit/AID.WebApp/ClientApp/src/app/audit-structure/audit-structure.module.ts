import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CreateAnnualScheduleComponent } from './create-annual-schedule/create-annual-schedule.component';
import { CreateAnnualPlanComponent } from './create-annual-plan/create-annual-plan.component';
import { AuditStructureRoutingModule } from './audit-structure.routing';
import { MaterialModule } from '../material.module';
import { CalendarModule, DateAdapter } from 'angular-calendar';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { adapterFactory } from 'angular-calendar/date-adapters/date-fns';
import { NgxMatSelectSearchModule } from 'ngx-mat-select-search';
import { FlatpickrModule } from 'angularx-flatpickr';
import { MatDatepickerModule, MatTabsModule } from '@angular/material';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { SharedModule } from '../shared-module/shared-module.module';
import { CommonMsg } from '../global/common-msg';
import { CreateQuarterlyPlanComponent } from './create-quarterly-plan/create-quarterly-plan.component';
import { CreateauditplanquarterlyComponent } from './createauditplanquarterly/createauditplanquarterly.component';

@NgModule({
  declarations: [CreateAnnualScheduleComponent, CreateAnnualPlanComponent, CreateQuarterlyPlanComponent, CreateauditplanquarterlyComponent],
  imports: [
    CommonModule,
    AuditStructureRoutingModule,
    NgxMatSelectSearchModule,
    MaterialModule, FormsModule, ReactiveFormsModule, MatDatepickerModule, DragDropModule, SharedModule, MatTabsModule, 
    FlatpickrModule.forRoot(),
    CalendarModule.forRoot({
      provide: DateAdapter,
      useFactory: adapterFactory
    }),
  ],
  providers: [CommonMsg]
})
export class AuditStructureModule { }
