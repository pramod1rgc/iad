﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public class IASConstant
    {
        #region DBConnectionsring
        public static string IASDataBaseConnection = "IASConnection";
        #endregion

        #region LoginSP's
        public static string SP_UserLogin = "[OIAS].[UserLoginWithRoleDetails]";
        #endregion

        #region Master

        public static string InsUpdateRiskCategory_SP = "[OIAS].[InsUpdateRiskCategory_SP]";
        public static string SP_GetCategoryInGride = "[OIAS].[SP_GetCategoryInGride]";
        public static string SP_GetCategoryDDL = "[OIAS].[SP_GetCategoryDDL]";
        #endregion

        #region Create Audit Plan
        public static string FinancialYear_SP = "OIAS.FinancialYear_SP";
        public static string GetFinancialForVerifyQuarter_SP = "[OIAS].[GetFinancialForVerifyQuarter_SP]";
        public static string GetAllControler_sp = "[OIAS].[GetAllControler]";
        public static string GetAllControlerByPAO_sp = "[OIAS].[GetAllControlerByPAO]";
        public static string UpdateAuditByPAO_sp = "[OIAS].[UpdateAuditByPAO]";
        public static string GetAllAuditList_SP = "[OIAS].[GetAllAuditList_SP]";
        public static string GetAnualAudit_SP = "[OIAS].[GetAnualAudit_SP]";
        public static string AssignAuditQuartaly_SP = "OIAS.AssignAuditQuartaly_SP";
        public static string AssignToSupervisorAnnualPlan_SP = "[OIAS].[AssignToSupervisorAnnualPlan_SP]";
        public static string QuarterlyForwardedtoSupervisor_SP = "[OIAS].[QuarterlyForwardedtoSupervisor_SP]";
        public static string GetQuarterAudit_SP = "[OIAS].[GetQuarterAudit_SP]";
        public static string GetAuditSupervisors = "OIAS.GetAuditSupervisors";
        public static string AssignToSupervisorAnnualPlanOther_SP = "[OIAS].[AssignToSupervisorAnnualPlanOther_SP]";


        #endregion

        #region RoleSP's
        public static string SP_GetRoles = "[OIAS].[usp_GetRoles]";
        public static string SP_UpsertRoles = "[OIAS].[usp_UpsertRoles]";
        public static string SP_ToggleActivationOfRole = "[OIAS].[usp_ToggleActivationOfRole]";
        #endregion

        #region AssignUserSP's
        public static string SP_GetUserRoleEmployees = "[OIAS].[GetUserRoleEmployees]";
        public static string SP_AssignUserRole = "[dbo].[AssignUserRole]";
        public static string SP_GetAuditTeam = "[OIAS].[GetAuditTeam]";
        public static string SP_CheckIfUserAlreadyAssignedToAnotherAudit = "[OIAS].[CheckIfUserAlreadyAssignedToAnotherAudit]";
        public static string SP_AssignAuditTeam = "[OIAS].[AssignAuditTeam]";
        public static string SP_GetAuditDetailsForDropdown = "[OIAS].[GetAuditDetailsForDropdown]";
        #endregion

        #region AuditStructureSP's
        public static string SP_GetScheduledAudits = "[OIAS].[GetScheduledAudits]";
        public static string SP_UpdateScheduledAudit = "[OIAS].[UpdateAuditSchedule]";
        public static string SP_DeleteScheduledAudit = "[OIAS].[DeleteAuditSchedule]";
        #endregion

        #region Verification

        #region Verification of Scheduled Auditss
        public static string SP_GetScheduledAuditForVerification = "[OIAS].[GetScheduledAuditForVerification]";
        public static string SP_UpdateScheduledAuditForVerification = "[OIAS].[UpdateScheduledAuditForVerification]";
        public static string SP_GetPendingTaskCounts = "[OIAS].[GetPendingTaskCounts]";
        public static string SP_GetDistinctQuartersBasedOnStatusAndFY = "[OIAS].[GetDistinctQuartersBasedOnStatusAndFY]";
        #endregion

        #region Verification of Audit Conference
        public static string SP_GetAuditsOfConferenceForVerification = "[OIAS].[GetAuditsOfConferenceForVerification]";
        #endregion

        #region Verification of Audit Team
        public static string SP_GetAuditDetailsForVerificationDropdown = "[OIAS].[GetAuditDetailsForVerificationDropdown]";
        public static string SP_GetTeamOfAuditForVerification = "[OIAS].[GetTeamOfAuditForVerification]";
        public static string SP_UpdateTeamOfAuditForVerification = "[OIAS].[UpdateTeamOfAuditForVerification]";
        public static string SP_GetAuditsForwardedToAuditSupervisor = "[OIAS].[GetAuditsForwardedToAuditSupervisor]";
        public static string SP_GetAuditsWhoseParasOrComplainceForwardedToSupervisor = "[OIAS].[GetAuditsWhoseParasOrComplainceForwardedToSupervisor]";
        public static string SP_GetAuditsWhoseSettlementLetterForwardedToSupervisor = "[OIAS].[GetAuditsWhoseSettlementLetterForwardedToSupervisor]";
        #endregion

        #endregion

        #region GetApprovedScheduledAudits
        public static string SP_GetApprovedScheduledAudits = "[OIAS].[GetApprovedScheduledAudits]";
        #endregion

        #region Send Intimation Mail
        public static string SP_GetSendIntimationMailsAudit = "[OIAS].[GetSendIntimationMailsAudit]";
        public static string Sp_CreateEmp_AssignUser = "[OIAS].[Sp_CreateEmp_AssignUser]";
        public static string usp_EmailIntimationStatus = "[OIAS].[usp_EmailIntimationStatus]";

        #endregion

        #region Approved Audit Teams
        public static string SP_GetApprovedAuditTeam = "[OIAS].[GetApprovedAuditTeam]";
        public static string SP_GetAuditDetailsForApprovedAuditTeamDropdown = "[OIAS].[GetAuditDetailsForApprovedAuditTeamDropdown]";
        #endregion
        #region Change Password
        public static string usp_ChangePassword = "[OIAS].[usp_ChangePassword]";
        #endregion

        #region Audit Programme
        public static string SP_GetAuditsForAuditProgramme = "[OIAS].[GetAuditsForAuditProgramme]";
        #endregion

        #region Audit Observation
        public static string SP_GetAuditDHAuditsList = "[OIAS].[GetAuditDHAuditsList]";
        public static string SP_GetAuditEntryConference = "[OIAS].[GetAuditEntryConference]";
        public static string SP_UpdateAuditEntryConference = "[OIAS].[UpdateAuditEntryConference]";
        public static string SP_GetAuditExitConference = "[OIAS].[GetAuditExitConference]";
        public static string SP_UpdateAuditExitConference = "[OIAS].[UpdateAuditExitConference]";
        public static string SP_GetAuditParas = "[OIAS].[GetAuditParas]";
        public static string SP_UpdateAuditPara = "[OIAS].[UpdateAuditPara]";
        public static string SP_GetAuditParaDetails = "[OIAS].[GetAuditParaDetails]";
        public static string SP_GetAllFinancialImplicationCases = "[OIAS].[SP_GetAllFinancialImplicationCases]";
        public static string SP_GetAuditParaFinancialImplicationCases = "[OIAS].[SP_GetAuditParaFinancialImplicationCases]";
        public static string SP_DeleteAuditPara = "[OIAS].[DeleteAuditPara]";
        public static string SP_PostParaCommentByAuditor = "[OIAS].[PostParaCommentByAuditor]";
        public static string SP_GetParaComments = "[OIAS].[GetParaComments]";
        public static string SP_GetAuditParaSettlementLetter = "[OIAS].[GetAuditParaSettlementLetter]";
        public static string SP_UpdateAuditParaSettlementLetter = "[OIAS].[UpdateAuditParaSettlementLetter]";
        public static string SP_UpdateParaSettlementLetterStatus = "[OIAS].[UpdateParaSettlementLetterStatus]";
        public static string UpdateStatusAuditee_SP = "[OIAS].[UpdateStatusAuditeedh_SP]";
        public static string SP_UpdateAuditParaStatus = "[OIAS].[UpdateAuditParaStatus]";
        public static string SP_UpdateAuditComplianceStatus = "[OIAS].[UpdateAuditComplianceStatus]";
        public static string SP_UpdateAuditStatus = "[OIAS].[UpdateAuditStatus]";
        public static string SP_UpdateEntryConferenceStatus = "[OIAS].[UpdateEntryConferenceStatus]";
        public static string SP_UpdateExitConferenceStatus = "[OIAS].[UpdateExitConferenceStatus]";
        #endregion

        #region Audit Report
        public static string SP_GetAuditsForAuditReportBySupervisor = "[OIAS].[GetAuditsForAuditReportBySupervisor]";
        public static string SP_GetAuditReport = "[OIAS].[GetAuditReport]";
        #endregion

        #region Auditee
        public static string GetAllAuditeeUniverse_SP = "[OIAS].[GetAllAuditeeUniverse_SP]";
        public static string AuditeeDesignation_SP = "[OIAS].AuditeeDesignation_SP";
        public static string InsAuditeeUniverseDetails_SP = "[OIAS].[InsAuditeeUniverseDetails_SP]";
        public static string Mst_AuditeeUniverseActiveAndDeActive_SP = "[OIAS].[Mst_AuditeeUniverseActiveAndDeActive_SP]";
        public static string AuditeeRole_SP = "[OIAS].[AuditeeRole_SP]";
        public static string GetAuditeeDesignationByRoleId_SP = "[OIAS].[GetAuditeeDesignationByRoleId_SP]";
        public static string usp_AuditeeUniverseEmailStatus = "[OIAS].[usp_AuditeeUniverseEmailStatus]";

        #endregion
        #region Auditee Observation
        public static string SP_GetAuditeeParas = "[OIAS].[GetAuditeeParas]";
        public static string PostParaCommentByAuditee = "[OIAS].[PostParaCommentByAuditee]";
        #endregion

    }
}
