﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public enum IASRolesEnums
    {
        RootAdmin=1,
        AuditSupervisor = 2,
        IADHead = 3,
        IAWHead = 4,
        AuditHead = 5,
        AuditDH = 6,
        AuditeeHead = 7,
        AuditeeDH = 8,
    }
}
