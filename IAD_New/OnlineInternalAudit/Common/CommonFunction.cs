﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
  public static class CommonFunction
    {    
        public static string SendMail(string MailTo, string Subject, string strBody,string Password)
        {
            string scheme = "https";
            string host = "localhost";
            string port = "44397/api/ActivateUser/Activate?uid="+MailTo;
            var varifyUrl = scheme + "://" + host + ":" + port; //activationcode;
            MailMessage mail = new MailMessage();
            mail.To.Add(MailTo);//Enter Receiver mail Id
            mail.From = new MailAddress(MailTo);//"nitesh@gmail.com");
            mail.Subject = Subject;// "Refrence id Generate";
            mail.IsBodyHtml = true;
            string Body = "<br/>You have been assigned as a Auditee Head for office IAD.</b><br/><br/>Your login credentials are given below. <br/>" + MailTo + "<br/>" + Password + "<br/><br/>" + "Use the below given link for first time login and change the password." +
          " <br/><br/><a href='" + varifyUrl + "'>" + varifyUrl + "</a> " + "<br/><br/>" + "From <b> " + "IAD Head </b>";
            mail.Body = Body;
            mail.IsBodyHtml = true;
            SmtpClient smtp = new SmtpClient();
            smtp.Host = "smtp.gmail.com";
            smtp.Port = 587;
            smtp.UseDefaultCredentials = false;
            smtp.Credentials = new System.Net.NetworkCredential("epssystem4@gmail.com", "eps@1234"); // Enter senders User name and password  
            smtp.EnableSsl = true;
            smtp.Send(mail);
            mail.DeliveryNotificationOptions = DeliveryNotificationOptions.OnSuccess;
            return mail.DeliveryNotificationOptions.ToString();
        }


        public static string SendMailAuditeeUniverse(string MailTo, string Subject,string RoleName, string strBody, string Password)
        {
            string scheme = "https";
            string host = "localhost";
            string port = "44397/api/ActivateUser/Activate?uid=" + MailTo;
            var varifyUrl = scheme + "://" + host + ":" + port; //activationcode;
            MailMessage mail = new MailMessage();
            mail.To.Add(MailTo);//Enter Receiver mail Id
            mail.From = new MailAddress(MailTo);//"nitesh@gmail.com");
            mail.Subject = Subject;// "Refrence id Generate";
            mail.IsBodyHtml = true;
            string Body = "<br/>You have been assigned as a "+ RoleName + " for office IAD.</b><br/><br/>Your login credentials are given below. <br/>" + MailTo + "<br/>" + Password + "<br/><br/>" + "Use the below given link for first time login and change the password." +
          " <br/><br/><a href='" + varifyUrl + "'>" + varifyUrl + "</a> " + "<br/><br/>" + "From <b> " + "IAD Head </b>";
            mail.Body = Body;
            mail.IsBodyHtml = true;
            SmtpClient smtp = new SmtpClient();
            smtp.Host = "smtp.gmail.com";
            smtp.Port = 587;
            smtp.UseDefaultCredentials = false;
            smtp.Credentials = new System.Net.NetworkCredential("epssystem4@gmail.com", "eps@1234"); // Enter senders User name and password  
            smtp.EnableSsl = true;
            smtp.Send(mail);
            mail.DeliveryNotificationOptions = DeliveryNotificationOptions.OnSuccess;
            return mail.DeliveryNotificationOptions.ToString();
        }

        //public string ActivationGeneratedCode() {
        //    var ActivateionCode = Guid.NewGuid();
        //    string GeneratedCode = Convert.ToString(ActivateionCode);
        //    return GeneratedCode;
        //}

        public static string RandomPassword()
        {
            int _min = 1000;
            int _max = 9999;
            Random _rdm = new Random();
            // _rdm.Next(_min, _max);
            return "EPS@" + _rdm.Next(_min, _max);
        }


        public static string GetIPAddress()
        {
            string strHostName = "";
            strHostName = System.Net.Dns.GetHostName();
            IPHostEntry ipEntry = System.Net.Dns.GetHostEntry(strHostName);
            IPAddress[] addr = ipEntry.AddressList;
            return addr[addr.Length - 1].ToString();

        }


        public static DataTable CreateDataTable<T>(IEnumerable<T> list)
        {

            DataTable table = new DataTable();
            var properties = new PropertyInfo[20]; // typeof(T).GetProperties();
            foreach (T l in list)
            {
                Type t = l.GetType();
                properties = t.GetProperties();
                break;
            }
            foreach (var property in properties)
            {
                Type type = Type.GetType(property.PropertyType.FullName);
                if (type.IsGenericType && type.GetGenericTypeDefinition() == typeof(Nullable<>))
                {
                    type = Nullable.GetUnderlyingType(type);
                }
                table.Columns.Add(property.Name, type);
            }
            foreach (T li in list)
            {
                var values = new object[properties.Length];
                for (int i = 0; i < properties.Length; i++)
                {
                    values[i] = properties[i].GetValue(li, null);
                }
                table.Rows.Add(values);
            }
            return table;
        }


        public static void SendGenericMail(string MailTo, string Subject, string strBody)
        {
            MailMessage mail = new MailMessage();
            mail.To.Add(MailTo);//Enter Receiver mail Id
            mail.From = new MailAddress(MailTo);
            mail.Subject = Subject;// "Refrence id Generate";
            mail.IsBodyHtml = true;
            mail.Body = strBody;
            //if (bytes != null)
            //{
            //    mail.Attachments.Add(new Attachment(new MemoryStream(bytes), Attachments));
            //}
            mail.IsBodyHtml = true;
            SmtpClient smtp = new SmtpClient();
            smtp.Host = "smtp.gmail.com";
            smtp.Port = 587;
            smtp.UseDefaultCredentials = false;
            smtp.Credentials = new System.Net.NetworkCredential("epssystem4@gmail.com", "eps@1234"); // Enter senders User name and password  
            smtp.EnableSsl = true;
            smtp.Send(mail);
        }
    }
}
